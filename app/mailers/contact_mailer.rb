class ContactMailer < ApplicationMailer

  def send_question(params)
    @email = params["email"]
    @message = params["message"]
    @name = params["name"]
    @phone_number = params["phone_number"]


    mail to: "#{ENV['CONTACT_EMAIL']}", subject:"Reefer | Question"
  end
end
