class DispensaryMailer < ApplicationMailer

  def new_join_waiting_list_request(email, dispensary_name, mobile_number)
    @email = email
    @dispensary_name = dispensary_name
    @mobile_number = mobile_number

    mail to: "#{ENV['CONTACT_EMAIL']}", subject:"Reefer | New Dispensary sign up request"
  end

  def invite_dispensary(dispensary)
    @dispensary_name = dispensary.name
    @signup_id = dispensary.magic_link

    mail to: dispensary.email, subject:"Reefer | Invitation request was approved"
  end
  def smart_sms_mailer(to_email,dispensary_name)
    @dispensary_name = dispensary_name
    mail to: 'abhilash@spericorn.com', subject:"Reefer | New Dispensary smart sms request"
  end

end
