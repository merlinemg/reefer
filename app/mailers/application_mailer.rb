class ApplicationMailer < ActionMailer::Base
  default from: 'Reefer <info@reefer.com>'
  layout 'mailer'
end
