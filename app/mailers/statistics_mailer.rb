class StatisticsMailer < ApplicationMailer

  def send_statistics(email, csv, attachement_name)
    attachments["#{attachement_name}-#{Date.current}.csv"] = csv
    mail to: email, subject: "Reefer | Admin statistics", body: "Statistics are in the attachement"
  end

end
