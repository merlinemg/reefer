// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap-sprockets
//= require jquery_ujs
//= require d3
//= require switchery
//= require jquery.inviewport.min
//= require mobile-detect.min
//= require_tree .

$(document).ready(function(){
  setTimeout(function(){
    $(".alert-row").fadeOut(function(){
      $(this).remove();
    })
  }, 3000);
})

function getRandom() {
  return Math.random();
}

$(document).ready(function(){
  $('.dropdown-toggle').dropdown();
})

var selectedDispensaryTabId = null;
$(document).ready(function(){
  $('.user-dispensary-tab-row').scroll(function(){ // bind window scroll event
    var currentDispenaryTabId = $(".user-dispensary-tab:in-viewport").first().attr('id');
    if(selectedDispensaryTabId != currentDispenaryTabId){
      selectedDispensaryTabId = currentDispenaryTabId;
      $.Map.findAndMoveToMarkerCarosell(selectedDispensaryTabId.split('-').pop());
    }
  });
});


$(document).on('click', '.navbar-right .dropdown-menu', function (e) {
  e.stopPropagation();
});


// function toggleDropdown(who){
//   if(who=="notifications"){
//     $("#dropdown-desktop-settings").removeClass("open");
//     if($("#dropdown-desktop-notifcations").hasClass("open")){
//       setTimeout(function(){
//         $("#dropdown-desktop-notifcations").removeClass("open");
//       }, 0);
//     } else{
//       markAsSeen();
//     }
//   }
//   else{
//    $("#dropdown-desktop-notifcations").removeClass("open");
//     if($("#dropdown-desktop-settings").hasClass("open")){
//       setTimeout(function(){
//         $("#dropdown-desktop-settings").removeClass("open");
//       }, 0);
//     } else{
//       markAsSeen();
//     }
//   }

// }


//notification logic
function markAsSeen(){
  seen_at = new Date().toISOString();

  $.ajax({
    url: "/users/notifications/mark_seen",
    data: JSON.stringify({ notification: { seen_at: seen_at }  }),
    method: "POST",
    dataType:"script"
  }).done(function(data){

  })
}

//maps
function openInMaps(address){
  // If it's an iPhone..
  if( (navigator.platform.indexOf("iPhone") != -1)
      || (navigator.platform.indexOf("iPod") != -1)
      || (navigator.platform.indexOf("iPad") != -1))
  {
    window.open("http://maps.apple.com/?q="+address);
  } else{
    window.open("http://www.google.com/maps/place/"+address+"");
  }
}

