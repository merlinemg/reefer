var App = {
    defaultPoint: {latitude: 34.089178, longitude: -118.408321},
    infoWindowTitleAttribute: 'name',   // The attribute on the collection being passed to this partial to use for the title
    infoWindowBodyAttribute: 'address', // The attribute on the collection being passed to this partial to use for the body
    generateIconCache: {},
    generateIconActiveCache: {},
    zoomControl: true,
    scaleControl: true,
    scrollwheel: true,
    disableDoubleClickZoom: true,
    mobile_detail: false,
    zoom: 10,
    markers: [],
    places: [],
    mapElementId: "map-canvas",
    initialize: function() {
      //start_point = (App.places[0]!=null) ? App.places[0] : App.defaultPoint
      var start_point = App.defaultPoint;
      var mapOptions = {
        zoom: App.zoom,
        center: new google.maps.LatLng(start_point.latitude - 0.001, start_point.longitude),
        zoomControl: App.zoomControl,
        scaleControl: App.scaleControl,
        scrollwheel: App.scrollwheel,
        disableDoubleClickZoom: App.disableDoubleClickZoom,
        draggable: App.draggable,
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: false
      };

      App.map = new google.maps.Map(document.getElementById(App.mapElementId), mapOptions)
      App.addZoomBoundEvent();
      App.addMarkers();

    },
    addZoomBoundEvent: function addZoomBoundEvent(){
      google.maps.event.addListenerOnce(App.map, 'bounds_changed', function(event) {
        //if (this.getZoom() > 11) {
          this.setZoom(11);
        //}
      });
    },
    addMarkers: function() {
      for(var jj=0; jj<App.places.length; jj++){

        var place = App.places[jj];

        App.generateIcon(jj, place.id, place.points, place.color, function(src) {
          var p = App.places[src.x];
          var pos = new google.maps.LatLng(p.latitude, p.longitude);

          var icon = {
            url: src.url,
            // scaled size for retina display
            scaledSize: new google.maps.Size(54,54)
          };

          var marker = new google.maps.Marker({
            position: pos,
            map: App.map,
            optimized: false,
            icon: icon,
            title: src.x.toString()
          });
          App.addMarkerClickEvent(marker);

          App.markers[src.x] = {marker: marker, id: src.id, place: App.places[src.x]};

          if(!App.mobile_detail){
            App.makeBounds();
          }
          if(App.mobile_detail){
            console.log("mobile_detail set zoomm");
            App.map.setZoom(14);
            App.map.panBy(0, -40);
          }

        });
        App.generateIcon(jj, place.id, place.points, place.color, function(src) {
        });
      }
    },
    findMarker: function(id){
      for(var i=0;i<App.markers.length;i++){
        if(App.markers[i].id == id){
          return App.markers[i].marker;
        }
      }
    },
    findPlace: function(id){
      for(var i=0;i<App.markers.length;i++){
        if(App.markers[i].id == id){
          return App.markers[i].place;
        }
      }
    },
    findMarkerAndPlace: function(id){
      for(var i=0;i<App.markers.length;i++){
        if(App.markers[i].id == id){
          return App.markers[i];
        }
      }
    },
    findAndMoveToMaker: function(id, zoom){
      App.moveToMarker(App.findMarker(id), zoom);
    },
    findAndMoveToMarkerCarosell: function(id){
      App.moveToMarkerNoZoom(App.findMarker(id));

      // will not work with panto & pan by
      //App.makeBounds();
      //center the map to a specific spot (city)
      //App.map.setCenter(App.findMarker(id).getPosition());
    },
    makeBounds: function(){
      console.log("makeBounds");

      var bounds = new google.maps.LatLngBounds();
      for (var i = 0; i < App.markers.length; i++) {
        bounds.extend(App.markers[i].marker.getPosition());
      }
      bounds.extend(new google.maps.LatLng(App.defaultPoint.latitude - 0.001, App.defaultPoint.longitude));

      //App.map.fitBounds(bounds);
      //remove one zoom level to ensure no marker is on the edge.
      App.map.setZoom(App.map.getZoom()-1);
    },
    moveToMarkerNoZoom: function(marker){
      App.map.panTo(marker.position);

      if ($(window).width() < 992) {
        //console.log("window size", $(window).width());
        App.map.panBy(0, 80);
      }


      var x = marker.get('title');
      for(var i=0;i<App.markers.length;i++){
        var place = App.markers[i].place;
        //console.log(place);
        //App.generateIcon(jj, place.id, place.points, place.color, function(src)
        App.generateIcon(i, place.id, place.points, place.color, function(resp){
          var icon = {
            url: resp.url,
            scaledSize: new google.maps.Size(54,54)
          };
          App.markers[resp.x].marker.setIcon(icon);
          App.markers[resp.x].marker.setZIndex(1);
        })
      }

      var place = App.markers[parseInt(x)].place;
      App.generateIconActive(x, place.id, place.points, place.color, function(resp){
        var icon = {
          url: resp.url,
          scaledSize: new google.maps.Size(54,54)
        };
        marker.setIcon(icon);
        marker.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
      });


    },
    moveToMarker: function(marker, zoom) {
      console.log("moveToMarker", zoom);

      if(zoom == undefined){
        zoom = 20;
      }

      App.map.panTo(marker.position);
      App.map.setZoom(zoom);

      if ($(window).width() < 992) {
        App.map.panBy(0, 300);
      }

    },
    closeAllInfoWindows: function() {
      for (i = 0; i < App.places.length; i++) {
        App.places[i].marker.infoWindow.close();
      }
    },
    addMarkerClickEvent: function(theMarker){
      google.maps.event.addListener(theMarker, 'click', function() {
        //console.log("click marker");

        var x = theMarker.get('title');

        var place = App.places[parseInt(x)];
        //console.log("place", place);
        var y = place.id;

        $("#dispensaryCarousel .user-dispensary-tab").removeClass("active");
        $('#dispensaryCarousel').carousel(parseInt(x));
        $('#dispensaryCarousel #user-dispensary-'+y).addClass("active");

        console.log(x);
        var ele = $("#panel-user-dispensary-"+y)[0].offsetTop;
        var para = $("#para-user-dispensary-container")[0];
        //var para = document.getElementById("para-user-dispensary-container");
        //var parax = $("#para-user-dispensary-container");
        console.log("ele","panel-user-dispensary-"+y, ele);
        console.log("ele","para-user-dispensary-container", ele);
        //console.log("parent", para, parax.offset() );

        $("#para-user-dispensary-container")[0].scrollTop = ele;

        //ele.addClass("d-selected");

        //console.log("x",x, "ud", '#user-dispensary-'+x);

        for(var i=0;i<App.markers.length;i++){
          var place = App.markers[i].place;
          //console.log(place);
          //App.generateIcon(jj, place.id, place.points, place.color, function(src)
          App.generateIcon(i, place.id, place.points, place.color, function(resp){
            var icon = {
              url: resp.url,
              scaledSize: new google.maps.Size(54,54)
            };
            App.markers[resp.x].marker.setIcon(icon);
            App.markers[resp.x].marker.setZIndex(1);
          })
        }

        var place = App.markers[parseInt(x)].place;
        App.generateIconActive(x, place.id, place.points, place.color, function(resp){
          var icon = {
            url: resp.url,
            scaledSize: new google.maps.Size(54,54)
          };
          theMarker.setIcon(icon);
          theMarker.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
          console.log("zindex", google.maps.Marker.MAX_ZINDEX);
        });

      });
    },
    // addMarkerClickEvent: function(place) {
      // google.maps.event.addListener(place.marker, 'click', (function(place) {
      //   return function() {
      //     console.log("stmg");
      //     App.closeAllInfoWindows();
      //     place.marker.infoWindow.setContent(
      //       '<h4>' + place[App.infoWindowTitleAttribute] + '</h4>' + "\n" +
      //       '<p>' + place[App.infoWindowBodyAttribute] + '</p>'
      //     );
      //     place.marker.infoWindow.open(App.map, place.marker);
      //     App.moveToMarker(place.marker);
      //   };
      // })(place));
    //},
    resizeMap: function(parent){
      $('#map-canvas').css("height",parent.height());
      google.maps.event.trigger($.Map, 'resize');
    },
    generateIcon: function generateIcon(x, id, number, color, callback) {
      if (App.generateIconCache[x] !== undefined) {
        callback({x: x, url: App.generateIconCache[x]});
      }

      var fontSize = 18,
        imageWidth = imageHeight = 160;

      if (number >= 1000) {
        fontSize = 10;
        imageWidth = imageHeight = 130;
      } else if (number < 1000 && number > 100) {
        fontSize = 14;
        imageWidth = imageHeight = 100;
      }

      var svg = d3.select(document.createElement('div')).append('svg')
        .attr('viewBox', '0 0 72 72')
        .append('g');

        svg.append('defs')
          .append('style')
          .attr('type', 'text/css')
          .text("@import url('https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800');");

      var circles = svg.append('circle')
        .attr('cx', '36')
        .attr('cy', '36')
        .attr('r', '28')
        .style('fill', "#fff");

      circles = svg.append('circle')
        .attr('cx', '36')
        .attr('cy', '36')
        .attr('r', '22')
        .style('fill', color);

      // var path = svg.append('path')
      //   .attr('d', 'M27.2,0C12.2,0,0,12.2,0,27.2s12.2,27.2,27.2,27.2s27.2-12.2,27.2-27.2S42.2,0,27.2,0z M6,27.2 C6,15.5,15.5,6,27.2,6s21.2,9.5,21.2,21.2c0,11.7-9.5,21.2-21.2,21.2S6,38.9,6,27.2z')
      //   .attr('fill', '#FFFFFF');

      var text = svg.append('text')
        .attr('dx', 36)
        .attr('dy', 42)
        .attr('text-anchor', 'middle')
        .attr('style', 'font-size:' + fontSize + 'px; fill: #FFFFFF; font-family: "Open Sans", sans-serif; font-weight: normal')
        .text(number);

      var svgNode = svg.node().parentNode.cloneNode(true),
        image = new Image();

      d3.select(svgNode).select('clippath').remove();

      var xmlSource = (new XMLSerializer()).serializeToString(svgNode);

      image.onload = (function(imageWidth, imageHeight) {
        var canvas = document.createElement('canvas'),
          context = canvas.getContext('2d'),
          dataURL;

        d3.select(canvas)
          .attr('width', imageWidth)
          .attr('height', imageHeight);

        context.drawImage(image, 0, 0, imageWidth, imageHeight);

        dataURL = canvas.toDataURL();
        App.generateIconCache[x] = dataURL;

        callback({x: x, id: id, url: dataURL});
      }).bind(this, imageWidth, imageHeight);

      image.src = 'data:image/svg+xml;base64,' + btoa(encodeURIComponent(xmlSource).replace(/%([0-9A-F]{2})/g, function(match, p1) {
        return String.fromCharCode('0x' + p1);
      }));
    },
    generateIconActive: function generateIcon(x, id, number, color, callback) {
      if (App.generateIconActiveCache[x] !== undefined) {
        callback({x:x, url:App.generateIconActiveCache[x]});
      }

      var fontSize = 18,
        imageWidth = imageHeight = 160;

      if (number >= 1000) {
        fontSize = 10;
        imageWidth = imageHeight = 130;
      } else if (number < 1000 && number > 100) {
        fontSize = 14;
        imageWidth = imageHeight = 100;
      }

      var svg = d3.select(document.createElement('div')).append('svg')
        .attr('viewBox', '0 0 72 72')
        .append('g');

      var circles = svg.append('circle')
        .attr('cx', '36')
        .attr('cy', '36')
        .attr('r', '34')
        .attr('fill-opacity', 0.5)
        .style('fill', color);

      circles = svg.append('circle')
        .attr('cx', '36')
        .attr('cy', '36')
        .attr('r', '28')
        .style('fill', "#fff");

      circles = svg.append('circle')
        .attr('cx', '36')
        .attr('cy', '36')
        .attr('r', '22')
        .style('fill', color);


      var text = svg.append('text')
        .attr('dx', 36)
        .attr('dy', 42)
        .attr('text-anchor', 'middle')
        .attr('style', 'font-size:' + fontSize + 'px; fill: #FFFFFF; font-family: Verdana; font-weight: normal')
        .text(number);

      var svgNode = svg.node().parentNode.cloneNode(true),
        image = new Image();

      d3.select(svgNode).select('clippath').remove();

      var xmlSource = (new XMLSerializer()).serializeToString(svgNode);

      image.onload = (function(imageWidth, imageHeight) {
        var canvas = document.createElement('canvas'),
          context = canvas.getContext('2d'),
          dataURL;

        d3.select(canvas)
          .attr('width', imageWidth)
          .attr('height', imageHeight);

        context.drawImage(image, 0, 0, imageWidth, imageHeight);

        dataURL = canvas.toDataURL();
        App.generateIconActiveCache[x] = dataURL;

        callback({x: x, id: id, url: dataURL});
      }).bind(this, imageWidth, imageHeight);

      image.src = 'data:image/svg+xml;base64,' + btoa(encodeURIComponent(xmlSource).replace(/%([0-9A-F]{2})/g, function(match, p1) {
        return String.fromCharCode('0x' + p1);
      }));
    }
  };
