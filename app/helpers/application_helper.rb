module ApplicationHelper

  def top_navbar_link_active(ctrl_name, link_name)
    controller_name == ctrl_name && action_name == link_name ? "active" : ""
  end

  def show_navbar?
    !users_onboarding_page? && !users_sessions_edit_page?
  end

  def users_signup_proccess?
    users_onboarding_page? && 
      [
        "enter_confirmation_code", 
        "enter_password", 
        "enter_name", 
        "share_link"
      ].include?(params[:action])
  end

  def step_2_signup_proccess_active?
    users_onboarding_page? && 
      ['enter_password', 'enter_name', 'share_link'].include?(params[:action])
  end

  def users_onboarding_page?
    params[:controller] == 'users/onboardings' || params[:controller] == 'users/invitations' || params[:controller] == 'users/in_store_onboardings'
  end

  def users_share_link_page?
    params[:controller] == 'users/onboardings' && params[:action] == 'share_link'
  end

  def enter_phone_nr_page?
    (params[:controller] == 'users/onboardings' || params[:controller] == 'users/invitations' || params[:controller] == 'users/in_store_onboardings') && params[:action] == 'enter_phone_nr'
  end

  def users_sessions_edit_page?
    params[:controller] == 'users/sessions' && params[:action] == 'edit'
  end
end
