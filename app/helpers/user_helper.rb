module UserHelper

  def dispensary_points_color(points, hex: false)
    if points >= 30
      hex ? '#71cb3d' : '-green'
    elsif points > 0
      hex ? '#63b0e0' : '-blue'
    else
      hex ? '#666666' : ''
    end
  end

  def dispensary_points_class(points, hex: false)
    if points >= 0 && points < 90
      return {'class':'widget-box-1','type':'Small'}
    elsif points >= 90 && points < 150
      return {'class':'widget-box-2','type':'Medium'}
    elsif points >= 150
      return {'class':'widget-box-3','type':'Large'}
    end
  end

  def dispensary_locations(dispensaries)
    dispensaries.reject{|x| x.nil? }.map do |dispensary|
      {
        id: dispensary.fetch(:dispensary, {}).fetch(:id),
        longitude: dispensary.fetch(:dispensary, {}).fetch(:longitude).to_f,
        latitude: dispensary.fetch(:dispensary, {}).fetch(:latitude).to_f,
        points: dispensary.fetch(:points, 0),
        color: dispensary_points_color(dispensary.fetch(:points, 0), hex: true)
      }
    end
  end

  def dispensary_friends(friends)
    if friends.present?
        if friends.size > 2
          return "#{friends[0]}, #{friends[1]} & #{"".concat((friends.size).to_s)} #{friends.size > 3 ? "others": "other friend"} #{friends.size > 4 ? "go": "goes"} here"
        elsif friends.size == 2
          return "#{friends[0]} & #{friends[1]} go here"
        else
          return "#{friends[0]} goes here"
        end
    end
    ""
  end

  def current_dispensaries_list(path)
    case path
    when "/users/home/all_clubs"
      return '<i class="icon-location-pin icons"></i> <span>All Clubs</span>'.html_safe
    when "/users/home/rewards"
      return '<div class="reward-icon"></div> <span style="color:#71cb3d">Rewards</span>'.html_safe
    when "/users/home/my_clubs"
      return '<div class="house-icon"></div> <span style="color:#63b0e0">My Clubs</span>'.html_safe
    else
      ""
    end
  end

  def current_dispensaries_list_tag(path)
    case path
    when "/users/home/all_clubs"
      "allclubs"
    when "/users/home/rewards"
      "rewards"
    when "/users/home/my_clubs"
      "myclubs"
    else
      0
    end
  end

  def current_page_with_list_view(path, list_view)
    list_view.zero? ? list_view = 1 : list_view = 0
    case path
    when "/users/home/all_clubs"
      return all_clubs_users_home_index_path(list_view: list_view)
    when "/users/home/rewards"
      return rewards_users_home_index_path(list_view: list_view)
    when "/users/home/my_clubs"
      return my_clubs_users_home_index_path(list_view: list_view)
    else
      ""
    end
  end

  def open_until_today(hours)
    return "Closed today." if hours.nil?

    weekday = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    open_until = nil
    hours.each do |h|
      if h[:day] == weekday[Time.now.wday - 1]
        open_until = h[:to_time]
      end
    end
    if open_until.blank?
      "Closed today."
    else
      "Open 'til #{open_until} today"
    end
  end

  def points_cap_emoji(points)
    return "🏃" if points.nil?

    if points < 20
      "😋"
    elsif points > 19 && points < 28
      "😬"
    else
      "🏃"
    end
  end

  def points_cap_subtext(points)
    if points == 28
      "Visit to reset your points!"
    else
      "Cap resets every time you visit"
    end
  end

  def html_magic_link_sms_text(magic_link)
    URI.encode("#{magic_link}")
  end

  def magic_link_sms_full_text(dispensary_name, magic_link,referral_deal)
    "Add me on Reefer! We will both get a #{referral_deal} at #{dispensary_name}. #{html_magic_link_sms_text(magic_link)}"
  end

  def get_sorted_patient_by_dispensary(type)
    array = []
    if type == 'daily'
        current_dispensary.dispensary_patients.where(:dispensary_id=>current_dispensary.id).each do |patient|
            dict = {y: 'January', item1: 2666}
            array.push(dict.to_json)
        end
        temp = {array:array}
        return temp.to_json
    elsif type == 'today'
        current_dispensary.dispensary_patients.where(:dispensary_id=>current_dispensary.id).each do |patient|
            dict = {y:patient.id,item1:patient.points}
            array.push(dict.to_json)
        end
        return array
    elsif type == 'weekly'
        current_dispensary.dispensary_patients.where(:dispensary_id=>current_dispensary.id).each do |patient|
            dict = {y:patient.id,item1:patient.points}
            array.push(dict.to_json)
        end
        return array
    else
        current_dispensary.dispensary_patients.where(:dispensary_id=>current_dispensary.id).each do |patient|
            dict = {y:patient.id,item1:patient.points}
            array.push(dict.to_json)
        end
        return array
    end
  end

end
