module HomeHelper

  def create_full_address(dispensary)
    [dispensary[:address], dispensary[:zip_code], dispensary[:state]].compact.join(", ")
  end

  def create_full_address_html(dispensary)
    [dispensary[:address], dispensary[:zip_code], dispensary[:state]].compact.join("+").gsub(" ","+")
  end
end
