class Users::BaseController < ApplicationController
  layout "app_users"
  before_action :set_navbar_for
  before_action :authenticate_user!
  
  protected
    def set_navbar_for
      @navbar_for = "user"
    end
end
