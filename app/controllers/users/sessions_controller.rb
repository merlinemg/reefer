class Users::SessionsController < Devise::SessionsController
  before_action :set_user, except: [:destroy, :new]
  layout "welcome"

  def new
    redirect_to enter_phone_nr_users_onboardings_path
  end

  def create
  end

  def edit
  @user.ref = params[:ref]
  end

  def update
    if @user.valid_password?(user_params[:password])
      sign_in @user
      
      if user_params[:ref].present?
        @inviter = User.find_by(id: user_params[:ref])

        # can't be friends with itself
        if @user.id == @inviter.id
          flash[:alert] = "You can't be friend's with yourself."
          return redirect_to users_root_path
        end

        # get last visited or random dispensary from inviter
        dispensary = @inviter.last_visited_dispensary_or_random

        # do we have an invitation
        invitation = Invitation.where(inviter_id: user_params[:ref], invitee_id: @user.id).first
        if invitation
          invitation.update(invitation_state: Invitation::ACCEPTED_STATUS)
        else
          # create invitation accepted
          Invitation.create(inviter_id: user_params[:ref], invitee_id: @user.id, invitation_state: Invitation::ACCEPTED_STATUS)
          # add friendship points
          # DispensaryPatient.set_friendship_points(@inviter, @user, dispensary)    
        end

        # send sms to inviter
        if @inviter.present?
          message = I18n.t('sms.invitation_existing_user_inviter', friend: @user.name, dispensary: dispensary.name, url: users_invitation_url(@inviter.magic_link))
          SMS.send_to(@inviter.phone_number, message)
        end

      end

      flash[:notice] = "Signed in successfully"
      return redirect_to users_root_path
    else
      @user.ref = user_params[:ref]
      @user.errors.add(:password, "is invalid.")
      render :edit
    end
  end

  def destroy
    sign_out current_user
    redirect_to root_path
  end

  def reset
    token = User.reset_token_generator(@user)
    SMS.send_to(
      @user.phone_number,
      I18n.t("activerecord.models.users.renew_password",
        url: edit_user_password_url(reset_password_token: token)
      )
    )
  end

private
  def user_params
    params.require(:user).permit(:password, :ref)
  end

  def set_user
    @user = User.friendly.find(params[:id])
  end
end
