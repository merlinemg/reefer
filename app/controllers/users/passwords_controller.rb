class Users::PasswordsController < Devise::PasswordsController
  layout "welcome"
  before_action :set_navbar_for

  def new
    @user = User.friendly.find(params[:id])
  end

  def edit
    self.resource = resource_class.new
    resource.reset_password_token = params[:reset_password_token]
  end

  def update
    self.resource = User.reset_password_by_token(resource_params)

    if self.resource.id.present?
      sign_in(:user, resource)
      redirect_to users_root_path
    else
      @user = User.friendly.find(params[:user][:id])
      token = User.reset_token_generator(@user)
      SMS.send_to(
        @user.phone_number,
        I18n.t("activerecord.models.users.renew_password",
          url: edit_user_password_url(reset_password_token: token)
        )
      )
      redirect_to enter_phone_nr_users_onboardings_path
    end
  end

  protected
  def set_navbar_for
    @navbar_for = "user"
  end

  def after_sending_reset_password_instructions_path_for(resource_name)
    dispensaries_passwords_resend_link_path
  end
end
