class Users::RegistrationsController < Devise::RegistrationsController
  before_action :set_navbar_for

  protected
  def set_navbar_for
    @navbar_for = "dispensary"
  end

  def after_update_path_for(resource)
    flash[:notice] = "Password Changed!"
    users_root_path
  end

end
