class Users::InStoreOnboardingsController < Users::BaseController
  layout "welcome"
  before_action :set_user, only: [:enter_confirmation_code, :enter_password, :enter_name, :resend_code]
  before_action :authenticate_user!, only: []

  def show
    if @user = User.find_by(
      magic_link: params[:id],
      sign_up_kind: User.sign_up_kinds['in_store'],
      onboarding_step: User.onboarding_steps['enter_phone_nr'])
      if @user.send_confirmation_code
        redirect_to enter_confirmation_code_users_in_store_onboarding_path(@user)
      else
        raise CanCan::AccessDenied
      end
    else
      raise CanCan::AccessDenied
    end
  end

  def enter_confirmation_code
    if params[:user].present? and user_params[:provided_confirmation_code].present?
      @user = User.sign_up_enter_confirmation_code(@user, user_params[:provided_confirmation_code])
      unless @user.errors.any?
        redirect_to enter_name_users_in_store_onboarding_path(@user)
      end
    end
  end

  def resend_code
    @user.resend_confirmation_code
    redirect_to enter_confirmation_code_users_in_store_onboarding_path(@user)
  end

  def enter_name
    if request.put?
      @user.name = user_params[:name]
      if @user.confirm_enter_name_step!.errors.any?
        render :enter_name
      else
        redirect_to enter_password_users_in_store_onboarding_path(@user)
      end
    end
  end

  def enter_password
    if params[:user].present?
      @user.password= user_params[:password]
      @user.password_confirmation= user_params[:password_confirmation]
      if @user.confirm_enter_password_step!.errors.any?
        render :enter_password
      else
        sign_in @user
        redirect_to share_link_users_onboarding_path(@user)
      end
    end
  end

private
  def user_params
    params.require(:user).permit(:name, :phone_number, :provided_confirmation_code, :password, :password_confirmation)
  end

  def set_user
    unless @user = User.friendly.find(params[:id])
      raise CanCan::AccessDenied
    end
  end
end
