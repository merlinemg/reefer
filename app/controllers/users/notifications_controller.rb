class Users::NotificationsController < Users::BaseController

  def index
    @notifications = current_user.notifications.paginate page: params[:page], per_page: 15

    if params[:seen].present?
      current_user.mark_notifications_seen
    end

    respond_to do |format|
      format.json {
        render json: {
          :current_page => @notifications.current_page,
          :next_page => @notifications.next_page,
          :per_page => @notifications.per_page,
          :total_notifications => @notifications.total_entries,
          :notifications =>  ActiveModel::Serializer::CollectionSerializer.new(@notifications, each_serializer: NotificationSerializer)
        }
      }

      format.html {}
    end
  end

  def update
    @notification = current_user.notifications.find(params[:id])
    @notification.update(notification_params)
    render json: {}
  end

  def mark_seen
    current_user.mark_notifications_seen

    respond_to do |format|
      format.js {}
    end
  end

  protected
  def notification_params
    params.require(:notification).permit(:id, :seen_at)
  end
end
