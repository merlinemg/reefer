class Users::FriendsController < Users::BaseController
  respond_to :html, :js, :json

  def index
  end

  def find_possible_friends
    begin
      # connect to FB and get FB friends
      # then exclude those that are allready our friends
      graph = current_user.koala

      reefer_friends_facebook_ids = graph.get_connections("me", "friends?fields=id").map{|g| g["id"] }
      possible_friends_fb_ids = (reefer_friends_facebook_ids - current_user.all_friendship_relations_facebook_ids).uniq

      render json: {
        possible_friends: User.where(facebook_id: possible_friends_fb_ids).map {|u| u.facebook_attributes(graph) },
        pending_friends_ids: User.where(facebook_id: current_user.pending_friends_ids).map {|u| u.facebook_attributes(graph) }
      }

    rescue Koala::Facebook::ClientError => e
      # if any error occurs, report to rollbar
      # and return an empty list to the client
      Rollbar.error(e)
      render json: {
        possible_friends: [],
        pending_friends_ids: []
      }
    end
  end

  def list_friends
    # return all friends from FB
    graph = Koala::Facebook::API.new(fb_params["accessToken"], ENV['FACEBOOK_APP_SECRET'])
    render json: {
      friends: current_user.friends.map {|u| u.facebook_attributes(graph) }
    }
  end

  def invite_friends
    # handles inviting friends
    if invite_params[:invitee_ids].nil?
      render json: {message: 'Something went wrong while creating invitations', errors: "Invitation params is empty" }, status: :unprocessable_entity
      return
    end

    invitations_map = invite_params[:invitee_ids].map do |invitee_id|
      invitee = User.find_by(id: invitee_id)
      if invitee.present?
        invitation_status = invitee.auto_accept_fb_friends ? Invitation::ACCEPTED_STATUS : Invitation::PENDING_STATUS
        {
          inviter: current_user,
          invitee: invitee,
          invitation_state: invitation_status
        }
      end
    end

    if invitations_map.compact.empty?
      return render json: {message: "Something when wrong while creating invitations", errors: nil}, status: :unprocessable_entity
    end

    invitations_map.each do |invite|
      # check if invitation exists, both ways
      invitation = Invitation.with_deleted.where(inviter_id: current_user.id, invitee_id: invite[:invitee].id).first
      if invitation.nil?
        invitation = Invitation.with_deleted.where(invitee_id: current_user.id, inviter_id: invite[:invitee].id).first
      end

      # if yes restore it
      if invitation.present?
        invitation = invitation.restore
        invitation.update(invitation_state: invite[:invitation_state])

        Notification.restore_friendships(invitation.inviter, invitation.invitee)
      else # else create link and points

        invitation = Invitation.create(invite)
        if invitation.errors.any?
          render json: {message: 'Something went wrong while creating invitation', errors: invitation.errors.full_messages.join(".") }, status: :unprocessable_entity
          return
        else
          # allocate points and create notifications
          dispensary = current_user.last_visited_dispensary_or_random
          if invitation.invitation_state == Invitation::ACCEPTED_STATUS
            DispensaryPatient.set_friendship_points(invitation.inviter, invitation.invitee, dispensary)

            #message = I18n.t('sms.invitation_existing_user_invitee', friend: invitation.inviter)
            #SMS.send_to(invitation.invitee.phone_number, message)

            #message = I18n.t('sms.invitation_existing_user_inviter', friend: invitation.invitee.name, dispensary: dispensary.name)
            #SMS.send_to(invitation.inviter.phone_number, message)
          end

        end

      end
    end

    render json: {message: 'Invitation created.', errors: nil}, status: :created
  end

  def remove_friend
    @friend_id = params[:friend_id]
    # removes all invitations, unfriending the user and the friend
    current_user.accepted_inviter_invitations.where(inviter_id: @friend_id).destroy_all
    current_user.accepted_invitee_invitations.where(invitee_id: @friend_id).destroy_all
    respond_with do |format|
      format.js {}
    end
  end

  def current_friends
    @user = current_user
  end

  private
  def fb_params
    params.require("authResponse").permit("accessToken", "userID")
  end

  def invite_params
    params.permit(:invitee_ids => [])
  end
end
