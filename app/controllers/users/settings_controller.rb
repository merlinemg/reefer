class Users::SettingsController < Users::BaseController

  # user stuff
  def index
    @user = current_user
  end

  def update
    # for updating user attributes (name, phone, ....)
    @user = User.friendly.find(params[:id])

    if user_params[:phone_number]
      user_params[:phone_number] = user_params[:phone_number].gsub("(","").gsub(")", "").gsub("-","")
    end

    @user.update(user_params)

    if user_params[:is_mobile].present?
      redirect_to users_settings_path
    else
      respond_to do |format|
        if @user.errors.any?
          format.html { redirect_to users_root_path, alert: @user.errors.full_messages.join(". ") }
        else
          format.html { redirect_to users_root_path, notice: "Successfully updated." }
        end
        format.js {}
      end

    end
  end

  def fb_connected
    render json: { fb_connect: current_user.fb_valid? }
  end

  def fb_callback
    oauth = Koala::Facebook::OAuth.new(ENV["FACEBOOK_APP_ID"], ENV["FACEBOOK_APP_SECRET"])
    new_access_info = oauth.exchange_access_token_info params["accessToken"]

    new_access_token = new_access_info["access_token"]
    new_access_expires_at = DateTime.now + new_access_info["expires"].to_i.seconds

    # check if any other user has this facebook id already
    if User.where(facebook_id: params["userID"]).any?
      # dont reward 10 points
      @reward_ten_points = false
      # fail the callback
      @fb_status = false
      flash[:alert] = "Facebook account already been connected."

    else
      cu = current_user
      cu.facebook_id = params["userID"]
      cu.fb_access_token = new_access_token
      cu.fb_expires_at = new_access_expires_at
      cu.fb_picture = cu.koala.get_picture('me', {type: 'large'})
      cu.save

      # check if user has already +10pts facebook notification
      if cu.notifications.where(kind: 'fb_reward').any?
        @reward_ten_points = false
      else
        last_visited_or_random = cu.last_visited_dispensary_or_random

        dp = DispensaryPatient.find_or_initialize_by(user: cu, dispensary: last_visited_or_random)
        dp.points = dp.points + 10
        dp.save

        Notification.create_fb_connect(cu, last_visited_or_random)
        @reward_ten_points = true
      end

      @fb_status = true

    end

    @mobile = browser.device.mobile? || browser.device.tablet?
  end

  def fb_disconnect
    if !current_user.fb_disconnect
      flash[:alert] = current_user.errors.full_messages.join(". ")
    end
  end

  # avatar stuff
  def upload_avatar
    # for uploading avatar
    @user = User.friendly.find(params[:id])
    @user.update(user_params)
    redirect_to crop_avatar_users_setting_path
  end

  def crop_avatar
    # page for cropping
    @user = current_user
  end

  def update_cropped_avatar
    # trigger cropping avatar with x,y,w,h params
    @user = User.friendly.find(params[:id])
    @user.update(cropped_params)
    if @user.cropping?
      @user.reprocess_avatar
    end

    if params[:is_mobile].present?
      redirect_to users_settings_path
    else
      flash[:notice] = "Successfully changed profile photo."
      redirect_to users_root_path
    end
  end

  protected
  def user_params
    @user_params ||= params.require(:user).permit(:id, :avatar, :name, :phone_number, :auto_accept_fb_friends, :facebook_id, :fb_access_token)
  end

  def cropped_params
    params.require(:user).permit(:id, :crop_x, :crop_y, :crop_w, :crop_h)
  end

end
