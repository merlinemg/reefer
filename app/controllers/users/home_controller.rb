class Users::HomeController < Users::BaseController
  before_action :set_list_view

  def index
    # main page
    redirect_to my_clubs_users_home_index_path
  end

  def my_clubs
    # returns users dispensaries
    @dispensaries = current_user.my_dispensaries
  end

  def rewards
    # returns users dispensaries, sorted by points
    @dispensaries = current_user.my_dispensaries_sorted
  end

  def all_clubs
    # all dispensaries, sorted by points
    @dispensaries = current_user.all_dispensaries_sorted
  end

  def dispensary_details
    @dispensary = Dispensary.find_by(id: params[:dispensary_id])

    if @dispensary.nil?
      redirect_to all_clubs_users_home_index_path
      return
    else
      @dispensary = current_user.dispensary(@dispensary.id)
      if @dispensary.nil?
        redirect_to all_clubs_users_home_index_path
        return
      end
    end
  end

  private
  def set_list_view
    @list_view = params[:list_view].to_i || 0
  end
end
