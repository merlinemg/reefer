class Users::OnboardingsController < Users::BaseController
  layout "welcome"
  before_action :set_user, only: [:enter_confirmation_code, :enter_password, :enter_name, :resend_code, :share_link]
  before_action :authenticate_user!, only: [:share_link]

  def enter_phone_nr
    if params[:user].present?

      @user = User.find_signed_up_or_create_new(user_params[:phone_number])
      if @user.errors.any?
        return render :enter_phone_nr
      end

      if current_user && @user.is_step_signed_up? && current_user.id == @user.id
        sign_in @user
        redirect_to my_clubs_users_home_index_path
      elsif @user.is_step_signed_up?
        redirect_to user_edit_session_path(@user)
      else
        if @user.send_confirmation_code
          redirect_to enter_confirmation_code_users_onboarding_path(@user)
        else
          redirect_to enter_phone_nr_users_onboardings_path
        end
      end

    elsif current_user.present?
      redirect_to my_clubs_users_home_index_path
    else
      @user = User.new
    end
  end


  def resend_code
    @user.resend_confirmation_code
    redirect_to enter_confirmation_code_users_onboarding_path(@user)
  end

  def enter_confirmation_code
    if @user.present?
      if params[:user].present? and user_params[:provided_confirmation_code].present?
        @user = User.sign_up_enter_confirmation_code(@user, user_params[:provided_confirmation_code])
        unless @user.errors.any?
          redirect_to enter_name_users_onboarding_path(@user)
        end
      end
    else
      redirect_to enter_phone_nr_users_onboarding_path
    end
  end

  def enter_password
    if @user.present?
      if params[:user].present?
        @user.password= user_params[:password]
        @user.password_confirmation= user_params[:password_confirmation]
        unless @user.confirm_enter_password_step!.errors.any?
          send_complete_sign_up_sms(@user)
          sign_in @user
          if params[:user][:mobile].present?
            redirect_to share_link_users_onboarding_path(@user)
          else
            redirect_to users_root_path
          end
        end
      end
    else
      redirect_to enter_phone_nr_users_onboarding_path
    end
  end

  def share_link
  end

  def enter_name
    return redirect_to(enter_phone_nr_users_onboarding_path) if @user.nil?

    if request.put?
      @user.name = user_params[:name]
      unless @user.confirm_enter_name_step!.errors.any?
        redirect_to enter_password_users_onboarding_path(@user)
      end
    end
  end

private
  def send_complete_sign_up_sms(user)
    referral_point = 'free reward'
    if user.last_visited_dispensary_or_random
        referral_point = DispensaryReferralDeal.find_by(dispensaries_id:user.last_visited_dispensary_or_random.id).name
    end
    SMS.send_to(user.phone_number, I18n.t('sms.sign_up_complete_a',referral_point: referral_point,magic_link: users_invitation_url(user.magic_link)))
    SMS.delay_for(30.seconds).send_to(user.phone_number, I18n.t('sms.sign_up_complete_b',referral_point: referral_point))
  end

  def user_params
    params.require(:user).permit(:name, :phone_number, :provided_confirmation_code, :password, :password_confirmation)
  end

  def set_user
    @user = User.friendly.find(params[:id])
  end
end
