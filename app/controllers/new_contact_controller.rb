class NewContactController < ApplicationController

  def index
    @user = User.new
    @body_class = "new-landing-contact__back"
    render layout:"new_landing_page"
  end

end
