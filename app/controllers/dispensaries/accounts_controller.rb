class Dispensaries::AccountsController < Dispensaries::BaseController

  def index
    @dispensary = current_dispensary
  end

  def update
    @dispensary = Dispensary.find_by(id: params[:id])

    if @dispensary.update(dispensary_params)
      flash[:notice] = "Successfully updated email."
      redirect_to dispensaries_accounts_path
    else
      render :index
    end
  end

  protected
  def dispensary_params
    params.require(:dispensary).permit(:email)
  end
end