class Dispensaries::BaseController < ApplicationController
  before_action :set_navbar_for
  layout "app_dispensaries"
  before_action :authenticate_dispensary!

  protected
  def set_navbar_for
    @navbar_for = "dispensary"
  end

end
