class Dispensaries::SignUpController < Dispensaries::BaseController
  layout "welcome"
  before_action :authenticate_dispensary!, except: [:register]

  def register
    @dispensary = Dispensary.find_by_magic_link(params[:id])
    if @dispensary.update(registration_params)
      sign_in @dispensary
      redirect_to dispensaries_root_path, notice: "You've signed up successfuly"
    else
      render 'dispensaries/registrations/new', locals: { resource: @dispensary, resource_name: :dispensary }
    end
  end

  def registration_params
    params.require(:dispensary).permit(:email, :password, :password_confirmation, :name, :address, :city, :state, :zip_code, :phone_number)
  end
end
