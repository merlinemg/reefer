class Dispensaries::DealsController < Dispensaries::BaseController
  layout "green"

  def edit
    @deal = current_dispensary.deals.find_by(id: params[:id])
    if @deal.nil?
      @deal = DispensaryReferralDeal.find_by(id: params[:id])
      if  @deal.nil?
         redirect_to dispensaries_root_path, notice: "Deal does not exist"
       end
    end
  end

  def update

    @deal = current_dispensary.deals.find_by(id: params[:id])
    if @deal.nil?
        @deal = DispensaryReferralDeal.find_by(id: params[:id])
    end
    if @deal.update(deal_params)
      redirect_to dispensaries_root_path
    else
      render :edit
    end
  end

  protected
  def deal_params
    @deal = current_dispensary.deals.find_by(id: params[:id])
    if @deal.nil?
      params.require(:dispensary_referral_deal).permit(:id, :name, :points)
    else
      params.require(:dispensary_deal).permit(:id, :name, :points)
    end
  end
end
