class Dispensaries::PasswordsController < Devise::PasswordsController
  layout "welcome"
  before_action :set_navbar_for

  def resend_link
  end

  protected
  def set_navbar_for
    @navbar_for = "dispensary"
  end

  def after_sending_reset_password_instructions_path_for(resource_name)
    dispensaries_passwords_resend_link_path
  end

end