class Dispensaries::AnalyticsController < Dispensaries::BaseController

    def update
        if params[:sort_type] == "point_update"
            dp =  current_dispensary.dispensary_patients.find_by(:user_id => params[:user_id])
            dp.points = dp.points.to_i + params[:point].to_i
            dp.save
            render json:{status:"success",point:dp.points}
        end
    end

    def fetch
       data = []
       result = []
       date = []
       @total = 0
       start_hour = Time.zone.parse("8:00AM")
       end_hour = Time.zone.parse("1:00AM").strftime("%I:%M%p")
       hours_array = []
       temp_hour = start_hour
       delta = temp_hour.strftime("%I:%M%p")
       while end_hour != delta do
            delta = temp_hour.strftime("%I:%M%p")
            temp_hour += 1.hour
            hours_array.push(delta)
       end
       all_weeks = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
       st =  1.week.ago
       end1=  Time.zone.now
       o_array = []
       o_array.push(Time.zone.now.strftime("%A"))
       while st.to_date <= end1.to_date do
            if !o_array.include? end1.strftime("%A")
                o_array.push(end1.strftime("%A"))
            end
            end1 -= 1.day
       end
       all_weeks =  o_array.reverse
       all_months = ['January','February','March','April','May','June','July','August','September','October','November','December']
       st =  1.year.ago
       end1=  Time.zone.now
       o_array = []
       while st.to_date != end1.to_date do
            y_axis =end1.strftime("%b")+ " "+end1.year.to_s
            if !o_array.include? y_axis
                o_array.push(y_axis)
            end
            end1 -= 1.month
       end
       all_months =  o_array.reverse
       patients = 0
       for_week_before = []
       for_week_before.push(4.week.ago.to_date,3.week.ago.to_date,2.week.ago.to_date,1.week.ago.to_date)
       patients_users = []
       referral_user_checkIn = 0
       referal_users = 0
       @user_visitation = UserVisitation.where(:dispensary_id  => current_dispensary.id).all
       if params[:sort_type] == "daily"
           count = 0
           table_content = self.get_table_content(1.week.ago)
           @user_visitation.each do |user|
                if user.visited_at >= 1.week.ago
                    @total += 1
                    if !patients_users.include? user.user_id
                        patients_users.push(user.user_id)
                        patients += 1
                    end
                    referral_user_checkIn += self.get_reeferedCheckIn_user_count(user.user_id)
                    if self.get_reefered_user_count(1.week.ago,user.user_id) != 0
                        referal_users = self.get_reefered_user_count(1.week.ago,user.user_id)
                    end
                    if !date.include? user.visited_at.strftime("%A")
                        date.push(user.visited_at.strftime("%A"))
                        data.push({y:user.visited_at.strftime("%A"),item1:self.get_count(user.visited_at,"%A",1.week.ago)})
                    end
                end
           end

           all_weeks.each do |week|
                flag = 0
                data.each do |item|
                    if week == item[:y]
                        result.push(item)
                        flag = 1
                    end
                end
                if flag != 1
                    result.push({y:week,item1:0})
                end
           end
           data = result
       elsif params[:sort_type] == "today"
            table_content = self.get_table_content("today")

            @user_visitation.each do |user|
                if user.visited_at.in_time_zone.to_date == Time.zone.now.to_date
                    @total += 1
                    if !patients_users.include? user.user_id
                        patients_users.push(user.user_id)
                        patients += 1
                    end
                    referral_user_checkIn += self.get_reeferedCheckIn_user_count(user.user_id)
                    if self.get_reefered_user_count("today",user.user_id) != 0
                        referal_users = self.get_reefered_user_count("today",user.user_id)
                    end
                    if !date.include? user.visited_at.strftime("%I:%M%p")
                        date.push(user.visited_at.strftime("%I:%M%p"))
                        data.push({y:user.visited_at.strftime("%I:%M%p"),item1:self.get_count(user.visited_at,"%I:%M%p",Time.zone.now.to_date)})
                    end
                end
           end
           result = []
           hour = ''
           limit = hours_array.size-1
           while limit >= 1 do
                count = 0
                flag = 0
                data.each do |item|
                    if Time.zone.parse(item[:y]) == Time.zone.parse(hours_array[limit])
                        count += item[:item1]
                        hour = hours_array[limit]
                        flag = 1
                    end
                    if Time.zone.parse(item[:y]) < Time.zone.parse(hours_array[limit]) && Time.zone.parse(item[:y]) >= Time.zone.parse(hours_array[limit-1])
                        count += item[:item1]
                        hour = hours_array[limit-1]
                        flag = 1
                    end
                end
                if flag == 1
                   result.push({y:hour,item1:count})
                else
                   result.push({y:hours_array[limit-1],item1:0})
                end
                limit -= 1
           end
           data = result.reverse
       elsif params[:sort_type] == "weekly"
            table_content = self.get_table_content(1.month.ago)
            @user_visitation.each do |user|
                if user.visited_at >= 1.month.ago
                    @total += 1
                    if !patients_users.include? user.user_id
                        patients_users.push(user.user_id)
                        patients += 1
                    end
                    referral_user_checkIn += self.get_reeferedCheckIn_user_count(user.user_id)
                    if self.get_reefered_user_count(1.month.ago,user.user_id) != 0
                        referal_users = self.get_reefered_user_count(1.month.ago,user.user_id)
                    end
                    if !date.include? user.visited_at.to_date
                        date.push(user.visited_at.to_date)
                        data.push({y:user.visited_at.to_date,item1:self.get_count(user.visited_at,"to_date",1.month.ago)})
                    end
                end
           end
           flag = 0
           w_0_count = 0
           w_1_count = 0
           w_2_count = 0
           w_3_count = 0
           w_4_count = 0
           data.each do |item|
                if item[:y] == Time.zone.now.to_date
                    w_0_count += item[:item1]
                elsif item[:y] >= 1.week.ago.to_date && item[:y] < Time.zone.now.to_date
                    w_1_count += item[:item1]
                elsif item[:y] < 1.week.ago.to_date && item[:y] >= 2.week.ago.to_date
                    w_2_count += item[:item1]
                elsif item[:y] < 2.week.ago.to_date && item[:y] >= 3.week.ago.to_date
                    w_3_count += item[:item1]
                elsif item[:y] < 3.week.ago.to_date && item[:y] >= 4.week.ago.to_date
                    w_4_count += item[:item1]
                end
            end
           result.push({y:4.week.ago.to_date,item1:w_4_count},{y:3.week.ago.to_date,item1:w_3_count},{y:2.week.ago.to_date,item1:w_2_count},{y:1.week.ago.to_date,item1:w_1_count},{y:Time.zone.now.to_date,item1:w_0_count})

           data = result
       else
            table_content = self.get_table_content(1.year.ago)
            @user_visitation.each do |user|
                if user.visited_at >= 1.year.ago
                    @total += 1
                    if !patients_users.include? user.user_id
                        patients_users.push(user.user_id)
                        patients += 1
                    end
                    referral_user_checkIn += self.get_reeferedCheckIn_user_count(user.user_id)
                    if self.get_reefered_user_count(1.year.ago,user.user_id) != 0
                        referal_users = self.get_reefered_user_count(1.year.ago,user.user_id)
                    end
                    if !date.include? user.visited_at.strftime("%b")
                        date.push(user.visited_at.strftime("%b"))
                        y_axis = user.visited_at.strftime("%b")+ " "+user.visited_at.year.to_s
                        data.push({y:y_axis,item1:self.get_count(user.visited_at,"%B",1.year.ago)})
                    end
                end
           end
           all_months.each do |month|
                flag = 0
                data.each do |item|
                    if month == item[:y]
                        result.push(item)
                        flag = 1
                    end
                end
                if flag != 1
                    result.push({y:month,item1:0})
                end
           end
           data = result
       end
       render json: {chart:data,total_patients_check_in:@total,patients:patients,referral_checkIn:referral_user_checkIn,referal_users:referal_users,table_content:table_content}
    end


    def get_count(date,date_type,duration)
        count = 0
        patients_users = []
        referral_user = []
        if params[:box_type] == "patients"
            @user_visitation.each do |user|
                if !patients_users.include? user.user_id
                    if user.visited_at >= duration
                        patients_users.push(user.user_id)
                    end
                    if date_type == "to_date"
                        if user.visited_at >= duration && user.visited_at.to_date == date.to_date
                             count += 1
                        end
                    else
                        if user.visited_at >= duration && user.visited_at.strftime(date_type) == date.strftime(date_type)
                             count += 1
                        end
                    end
                end
            end
        elsif params[:box_type] == "referals"
            @user_visitation.each do |user|
                current_dispensary.dispensary_patients.where(:dispensary_id=>current_dispensary.id).each do |patient|
                    if patient.reeferal_user_id.present? && patient.user_id == user.user_id
                        if !referral_user.include? patient.user_id
                            referral_user.push(patient.user_id)
                            if date_type == "to_date"
                                if user.visited_at >= duration && user.visited_at.to_date == date.to_date
                                     count += 1
                                end
                            else
                                if user.visited_at >= duration && user.visited_at.strftime(date_type) == date.strftime(date_type)
                                     count += 1
                                end
                            end
                        end
                    end
                end
            end
        elsif params[:box_type] == "referal_check_in"
            @user_visitation.each do |user|
                current_dispensary.dispensary_patients.where(:dispensary_id=>current_dispensary.id).each do |patient|
                if patient.reeferal_user_id.present? && patient.user_id == user.user_id
                        if date_type == "to_date"
                            if user.visited_at >= duration && user.visited_at.to_date == date.to_date
                                 count += 1
                            end
                        else
                            if user.visited_at >= duration && user.visited_at.strftime(date_type) == date.strftime(date_type)
                                 count += 1
                            end
                        end
                    end
                end
            end
        else
            @user_visitation.each do |user|
                if date_type == "to_date"
                    if user.visited_at >= duration && user.visited_at.to_date == date.to_date
                         count += 1
                    end
                else
                    if user.visited_at >= duration && user.visited_at.strftime(date_type) == date.strftime(date_type)
                         count += 1
                    end
                end
            end
        end
        return count
    end

    def get_reeferedCheckIn_user_count(user_id)
        referral_user = 0
        current_dispensary.dispensary_patients.where(:dispensary_id=>current_dispensary.id).each do |patient|
            if patient.reeferal_user_id.present? && patient.user_id == user_id
                referral_user = 1
            end
        end
        return referral_user
    end

    def get_reefered_user_count(date_type,user_id)
        referral_user = 0
        array = []
        if date_type == "today"
            current_dispensary.dispensary_patients.where(:dispensary_id=>current_dispensary.id).each do |patient|
                if patient.reeferal_user_id.present? && patient.user_id == user_id
                    if !array.include? patient.user_id
                        referral_user += 1
                    end
                end
            end
        else
            current_dispensary.dispensary_patients.where(:dispensary_id=>current_dispensary.id).each do |patient|
                if patient.reeferal_user_id.present? && patient.user_id == user_id
                    if !array.include? patient.user_id
                        referral_user += 1
                    end
                end
            end
        end
        return referral_user
    end

    def get_table_content(date_type)
       @sorted_users = []
       if date_type == "today"
            @date_type = Time.zone.now
            @type = "today"
       else
            @date_type = date_type
            @type = ""
       end
       @user_visitation.each do |user|
            if params[:box_type] == "patients"
                if !@sorted_users.include? user.user_id
                    @sorted_users.push(user.user_id)
                end
            elsif params[:box_type] == "referals"
                current_dispensary.dispensary_patients.where(:dispensary_id=>current_dispensary.id).each do |patient|
                    if patient.reeferal_user_id.present? && patient.user_id == user.user_id
                        if !@sorted_users.include? patient.user_id
                            @sorted_users.push(patient.user_id)
                        end
                    end
                end
            elsif params[:box_type] == "referal_check_in"
                current_dispensary.dispensary_patients.where(:dispensary_id=>current_dispensary.id).each do |patient|
                    if patient.reeferal_user_id.present? && patient.user_id == user.user_id
                        @sorted_users.push(user.user_id)
                    end
                end
            else
                @sorted_users.push(user.user_id)
            end

       end
       @box_type = params[:box_type]
       render_to_string(partial:'dispensaries/analytics/sorted_data')
    end


end
