class Dispensaries::SessionsController < Devise::SessionsController
  layout "welcome"
  before_action :set_navbar_for

  protected
  def set_navbar_for
    @navbar_for = "dispensary"
  end
end
