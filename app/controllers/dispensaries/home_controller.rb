class Dispensaries::HomeController < Dispensaries::BaseController

  def index
    current_dispensary.create_default_deals
  end

  def edit
    render layout: "green"
  end

  def update
    if current_dispensary.update(dispensary_params)
      if params[:commit] == "+ Add new row"
        # create a new store hour object
        store_hour = current_dispensary.store_hours.build
        # set its default times
        store_hour.set_default_from_to_times
        store_hour.save
        redirect_to edit_dispensaries_home_index_path
      else
        redirect_to dispensaries_root_path
      end
    else
      render :edit
    end
  end

  def how_it_works
  end

  protected
  def dispensary_params
    params.require(:dispensary).permit(
      :name, :address, :city, :state, :zip_code, :phone_number,
      store_hours_attributes: [:id, :from_time, :to_time, :sunday, :monday, :tuesday, :wednesday, :thursday, :friday, :saturday ]
    )
  end

end
