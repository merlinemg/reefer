class Dispensaries::RegistrationsController < Devise::RegistrationsController
  before_action :set_navbar_for
  before_action :authenticate_dispensary!, except: [:new, :sign_up]
  layout :resolve_layout

  def new
    @dispensary = Dispensary.find_by_magic_link(params[:signup_id])
  end

  protected
  def set_navbar_for
    @navbar_for = "dispensary"
  end

  def after_update_path_for(resource)
    flash[:notice] = "Password Changed!"
    dispensaries_root_path
  end

  def resolve_layout
    case action_name
    when "new"
      "welcome"
    when "edit", "update"
      "app_dispensaries"
    else
      "welcome"
    end
  end

end