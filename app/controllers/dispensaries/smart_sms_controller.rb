class Dispensaries::SmartSmsController < Dispensaries::BaseController
    Thread.abort_on_exception = true

    def index

        if current_dispensary.dispensary_text_access_status.nil?
            redirect_to '/dispensaries/activate-sms'
        else
            render :smart_sms
        end
    end


    def send_sms
         if current_dispensary.is_message_left == ''
             twilio_sid = []
             twilio_auth_token = []
             ph_number = []
             dt = current_dispensary.dispensary_text
             dt.remaining_text -= current_dispensary.get_patients_count
             dt.send_sms_count += current_dispensary.get_patients_count
             dt.save
             @threads = Thread.new do
                 client = Twilio::REST::Client.new
                 client.accounts.list({:phone_number => current_dispensary.dispensary_text.twilio_phone_number}).each do |sub_account|
                    begin
                        test_account  = Twilio::REST::Client.new sub_account.sid,sub_account.auth_token
                        numbers = test_account.account.incoming_phone_numbers.list
                        numbers.each do |number|
                            if number.phone_number == current_dispensary.dispensary_text.twilio_phone_number
                                twilio_sid = sub_account.sid
                                twilio_auth_token = sub_account.auth_token
                                ph_number = number.phone_number
                                break
                            end
                        end
                    rescue
                       print "error"
                    end
                 end
                 print "ph_number>>",ph_number,"<<<",twilio_sid,"SSSSS",twilio_auth_token
                 if twilio_sid && twilio_auth_token
                    service_sid = (0...34).map { ('a'..'z').to_a[rand(26)] }.join
                    sub_account = Twilio::REST::Client.new twilio_sid,twilio_auth_token
                    fails = 0
                    current_dispensary.get_patients.each do |patient|
                        begin
                            response = sub_account.messages.create({
                                 from: ph_number,
                                 to: User.find_by(:id =>patient).phone_number,
                                 body: params['message']
                            })

                            {
                              message: 'OK',
                              status: response.status,
                              response: response
                            }
                            print User.find_by(:id =>patient).phone_number.delete(' ').to_s,"<<<<<\n\n"
                        rescue Twilio::REST::RequestError => error
                            fails += 1
                            puts "----------"
                            puts "Error: #{error.code}"
                            puts "Message: #{error.message}"
                            puts "----------"

                             {
                              message: 'FAILED',
                              error: {
                                message: error.message,
                                code: error.code
                              }
                            }
                        rescue StandardError => error
                            fails += 1
                            puts "----------"
                            puts "Message: #{error.message}"
                            puts "----------"

                             {
                              message: 'FAILED',
                              error: {
                                message: error.message
                              }
                            }
                        end
                    end

                    dt = current_dispensary.dispensary_text
                    dt.remaining_text += fails
                    dt.send_sms_count -= fails
                    dt.save
                    DispensaryTextLog.create(dispensary_id:current_dispensary.id,capaign_size:current_dispensary.get_patients_count,successfull_send:count,from_phone_number:ph_number,remaining_sms:current_dispensary.dispensary_text.remaining_text,send_date:Time.zone.now)
                 end
             end
             status = true
         else
             status = false
         end
         total_count = current_dispensary.dispensary_text.remaining_text+current_dispensary.dispensary_text.send_sms_count
         send_sms = current_dispensary.dispensary_text.send_sms_count
         difference = send_sms.to_f / total_count.to_f
         difference *= 100
         render json:{status:status,send_sms:send_sms,difference:difference.round(2)}
    end

    def activate_sms
        if current_dispensary.dispensary_text_access_status.nil?
            render :inactive_sms
        else
            redirect_to '/dispensaries/smart-sms'
        end
    end

    def smart_sms_request
      @threads = Thread.new do
          DispensaryMailer.smart_sms_mailer("brian@reeferloyalty.com​",current_dispensary.name).deliver_now
          DispensaryMailer.smart_sms_mailer("zach.wise@reeferloyalty.com",current_dispensary.name).deliver_now
      end
      render json:{status:"success"}
    end
end
