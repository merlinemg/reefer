class Api::V1::DispensariesController < Api::V1::BaseController
  before_action :set_user, only: [:show, :update]
  before_action :set_dispensary_patient, only: [:update]

  resource_description do
    header "X-Dispensary-Email", 'Dispensary Email', required: true
    header "X-Dispensary-Token", 'Authentication token', required: true
  end

  api :GET, '/v1/dispensaries', 'Display Enter Phone number page - returns dispensaries small reward'
  error code: 401, description: "Unauthorized"
  def index
    render json: {
      dispensary: {
        name: current_dispensary.name,
        small_reward: current_dispensary.small_reward.name,
        medium_reward: current_dispensary.medium_reward.name,
        large_reward: current_dispensary.large_reward.name,
        small_reward_points: current_dispensary.small_reward.points,
        medium_reward_points: current_dispensary.medium_reward.points,
        large_reward_points: current_dispensary.large_reward.points
      }
    }
  end

  api :GET, '/v1/dispensaries/:id', %Q{Displays customer information and dispensary stats.
    This action will:
      1) partialy create an account if customer isn't on reefer and return stats
      2) add visitation points to customers dispensary relation and return stats if the customer is on reefer and has a relation to dispensary
      3) create customer dispensary relation, add visitation points and return stats if the customer is on reefer and doesn't have a relation to dispensary
  }
  error code: 401, description: "Unauthorized"
  def show
    if @user.present?
      set_dispensary_patient
      if @dispensary_patient.present? # add visitation points
        visit_and_render_json!
      else #create dispensary patient relation and add visitation points
        @dispensary_patient = current_dispensary.dispensary_patients.create(user: @user)
        visit_and_render_json!
      end
    else #create a user, add dispensary relations and sign up points, start in store onboarding
      initiate_in_store_signup_and_render_json!
    end
  end


  api :PUT, '/v1/dispensaries/:id', %Q{Redeem a specified reward for patient
    Only one (free_joint: true, small_reward: true, medium_reward: true, large_reward: true) parameter is expected.
    This action will:
    1) set gets_free_joint to false and create a redeemal record if gets_free_joint is set to true.
    2) reduce points for the reward if user has enough points for claiming a reward.

    This action won't return an error. It will return the same patient dispensary stats as show, but with reduced numbers
  }
  param :dispensary, Hash, :required => true, :action_aware => true do
    param :free_joint, [true]
    param :small_reward, [true]
    param :medium_reward, [true]
    param :large_reward, [true]
  end
  def update
    raise CanCan::AccessDenied unless @dispensary_patient.present?

    previous_points = @dispensary_patient.points
    if redeem_params[:free_joint]
      if @dispensary_patient.gets_free_joint
        @dispensary_patient.update(gets_free_joint: false)
        Notification.claim_free_joint(@dispensary_patient.user, @dispensary_patient.dispensary)
      end
    elsif redeem_params[:small_reward]
      @dispensary_patient.redeem_deal!(@dispensary_patient.dispensary.small_reward)
      Notification.claim_small_reward(@dispensary_patient.user, @dispensary_patient.dispensary)
    elsif redeem_params[:medium_reward]
      @dispensary_patient.redeem_deal!(@dispensary_patient.dispensary.medium_reward)
      Notification.claim_medium_reward(@dispensary_patient.user, @dispensary_patient.dispensary)
    elsif redeem_params[:large_reward]
      @dispensary_patient.redeem_deal!(@dispensary_patient.dispensary.large_reward)
      Notification.claim_large_reward(@dispensary_patient.user, @dispensary_patient.dispensary)
    end
    render json: {
      user: {
        phone_number: @user.phone_number,
        name: @user.name
      },
      dispensary: {
        gets_free_joint: @dispensary_patient.gets_free_joint,
        previous_points: previous_points,
        current_points: @dispensary_patient.points,
        small_reward: current_dispensary.small_reward.to_s,
        medium_reward: current_dispensary.medium_reward.to_s,
        large_reward: current_dispensary.large_reward.to_s,
        user_visited_today: true
      }
    }
  end

private
  def users_phone_number
    params[:id]
  end

  def set_user
    @user = User.find_by(phone_number: users_phone_number)
  end

  def set_dispensary_patient
    @dispensary_patient = current_dispensary.dispensary_patients.joins(:user).find_by(users: {phone_number: users_phone_number})
  end

  def dispensary_params
    params.require(:dispensary).permit(:email, :password)
  end

  def redeem_params
    params.require(:dispensary).permit(:free_joint, :small_reward, :medium_reward, :large_reward)
  end

  def visit_and_render_json!
    # store current points
    previous_points = @dispensary_patient.points

    # lets set that the user did not visit today
    user_visited_today = false
    # variable if the user should be notified that he has incomplete profile
    trigger_incomplete_profile = false

    # last visit present but its not today
    if @dispensary_patient.last_visit.present? and not @dispensary_patient.last_visit.in_time_zone.to_date == Time.zone.now.to_date
      trigger_incomplete_profile = @dispensary_patient.visit!
    # we dont have a last visit
    elsif @dispensary_patient.last_visit.nil?
      trigger_incomplete_profile = @dispensary_patient.visit!
    # default user visited
    else
      # if none of the above match, then user already visited today
      user_visited_today = true
    end

    if trigger_incomplete_profile
      # send sms to user that he needs to add friends
      SMS.send_to(@dispensary_patient.user.phone_number, "Your Reefer account is incomplete. Add a friend to complete your account. Click the link below: #{ users_invitation_url(@dispensary_patient.user.magic_link) }")
    end

    # does user get a free joint?
    if @dispensary_patient.gets_free_joint
      free_joint_params = {free_joint_message: @dispensary_patient.free_joint_message}

      # find inviter
      reeferal_user = @dispensary_patient.reeferal_user

      # reeferal user gets only once the free joint per invitee
      if reeferal_user
        # add free join flag for him as well
        reeferal_user_dp = DispensaryPatient.where(user_id: reeferal_user.id, dispensary_id: @dispensary_patient.dispensary_id).first
        reeferal_user_dp.gets_free_joint = true
        reeferal_user_dp.save

        @dispensary_patient.update(reeferal_user: nil)

        # send sms to reeferal user
        begin
            referal_point = DispensaryReferralDeal.find_by(dispensary_id: @dispensary_patient.dispensary.id).name
        rescue
            referal_point = 'Free Reward'
        end
        message = I18n.t('sms.reeferal_accepted_a', dispensary: @dispensary_patient.dispensary.name,referal_point:referal_point, friend: @dispensary_patient.user.name, url: users_invitation_url(reeferal_user.magic_link))
        SMS.send_to(reeferal_user.phone_number, message)
        
        # create notification about it
        Notification.create_can_collect_free_join(reeferal_user, @dispensary_patient.user, @dispensary_patient.dispensary)
      end
    else
      free_joint_params = {}
    end

    render json: {
      user: {
        phone_number: @user.phone_number,
        name: @user.name
      },
      dispensary: {
        gets_free_joint: @dispensary_patient.gets_free_joint,
        previous_points: previous_points,
        current_points: @dispensary_patient.points,
        small_reward: current_dispensary.small_reward.to_s,
        medium_reward: current_dispensary.medium_reward.to_s,
        large_reward: current_dispensary.large_reward.to_s,
        user_visited_today: user_visited_today,
        small_reward_points: current_dispensary.small_reward.points,
        medium_reward_points: current_dispensary.medium_reward.points,
        large_reward_points: current_dispensary.large_reward.points
      }.merge(free_joint_params)
    }
  end

  def initiate_in_store_signup_and_render_json!
    # create user for dispensary with given phone number
    user = User.in_store_sign_up(current_dispensary.id, users_phone_number)
    if user.errors.any?
      render json: user.errors
    else
      if !Rails.env.test?
        SMS.send_to(user.phone_number, I18n.t("sms.in_store_signup_first", url: users_in_store_onboarding_url(user.magic_link), dispensary_name: current_dispensary.name))
      end

      dispensary_patitent = current_dispensary.dispensary_patients.find_by(user_id: user)
      render json: {
        user: {
          phone_number: user.phone_number
        },
        dispensary: {
          gets_free_joint: dispensary_patitent.gets_free_joint,
          previous_points: 0,
          current_points: dispensary_patitent.points,
          small_reward: current_dispensary.small_reward.to_s,
          medium_reward: current_dispensary.medium_reward.to_s,
          large_reward: current_dispensary.large_reward.to_s,
          user_visited_today: false
        }
      }
    end
  end
end