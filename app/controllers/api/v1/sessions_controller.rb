class Api::V1::SessionsController < Api::V1::BaseController
  acts_as_token_authentication_handler_for Dispensary, except: [:create, :forgot_password]

  api :POST, '/v1/sessions', %Q{Create a session
    - returns an authentication token if the email/password is correct
    - returns status 401 if email/password is not correct
  }
  error code: 401, description: "Unauthorized"
  def create
    dispensary = Dispensary.find_for_database_authentication email: dispensary_params[:email]
    if dispensary.present? and dispensary.valid_password?(dispensary_params[:password])
      render json: {
          authentication_token: dispensary.authentication_token,
        }, status: :ok
    else
      raise CanCan::AccessDenied
    end
  end

  api :POST, '/v1/sessions/forgot_password', "Trigger password reset procedure"
  error code: 401
  def forgot_password
    dispensary = Dispensary.find_by email: dispensary_params[:email]
    if dispensary
      dispensary.send_reset_password_instructions unless Rails.env.test?
      head :ok
    else
      raise CanCan::AccessDenied
    end
  end
  
private
  def dispensary_params
    params.require(:dispensary).permit(:email, :password)
  end
end