class Api::BaseController < ActionController::Base
  acts_as_token_authentication_handler_for Dispensary

  rescue_from ActiveRecord::RecordNotFound do |exception|
    head :not_found
  end

  rescue_from CanCan::AccessDenied do |exception|
    head :unauthorized
  end
end