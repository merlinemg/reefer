class ApplicationController < ActionController::Base
  # protect_from_forgery with: :exception
  protect_from_forgery with: :null_session

  def handle_unverified_request
    super # call the default behaviour, including Devise override
    flash[:notice] = "Please sign in order to continue"
    authenticate_user!
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    render file: "#{Rails.root}/public/404.html" , status: 404
  end

  rescue_from CanCan::AccessDenied do |exception|
    render file: "#{Rails.root}/public/401.html" , status: 401
  end

  def after_sign_in_path_for(resource)
    if resource_name == :dispensary
      dispensaries_root_path
    end
  end
end
