class NewHomeController < ApplicationController

  def index
    @user = User.new
    @video_url = "https://www.youtube.com/embed/4uQ5P40tq-M"
    render layout:"new_landing_page"
  end

end
