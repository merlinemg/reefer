class NewDispensaryController < ApplicationController

  def index
    @new_dispensary = Dispensary.new
    render layout:"new_landing_page"
  end

end
