class WelcomeController < ApplicationController
  layout "welcome"
  before_action :set_navbar_for

  def index
    render layout:"welcome_white"
  end

  def blog
  end

  def faq
    @faqs = Faq.all
  end

  def contact_us
  end

  def privacy_policy
  end

  def lets_talk
    unless params[:email].present? && params[:message].present?
      render :contact_us
    end

    ContactMailer.send_question(params).deliver_now
  end

  protected
  def set_navbar_for
    @navbar_for = "user"
  end

end
