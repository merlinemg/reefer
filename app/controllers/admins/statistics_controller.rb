class Admins::StatisticsController < Admins::BaseController
  layout "admin"

  def index
  end

  def export_reefer_network
    month = params[:month] || Date.current.month
    year = params[:year] || Date.current.year

    if DateTime.now < DateTime.new(year.to_i, month.to_i, 1)
      flash[:notice] = "Cannot get data from the future"
      return redirect_to admins_statistics_path
    end

    GenerateReeferStatisticsWorker.perform_at((Time.now), current_admin.email, month, year, "reefernetwork#{year}#{month}")
    redirect_to admins_statistics_path, notice: "Data is preparing, and will be sent to this email #{current_admin.email}."
  end

  def export_per_dispensary
    month = params[:month] || Date.current.month
    GenerateDispensaryStatisticsWorker.perform_at((Time.now), current_admin.email, params[:dispensary_id], month, "dispensary")
    redirect_to admins_statistics_path, notice: "Data is preparing, and will be sent to this email #{current_admin.email}."
  end

  def export_all_dispensaries
    GenerateAllDispensariesStatisticsWorker.perform_at((Time.now), current_admin.email, "all-dispensaries")
    redirect_to admins_statistics_path, notice: "Data is preparing, and will be sent to this email #{current_admin.email}."
  end

end
