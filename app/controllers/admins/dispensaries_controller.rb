class Admins::DispensariesController < Admins::BaseController

  def index
    # dispensarties that requested onboarding
    @onboarding_request_dispensaries = Dispensary.onboarding_request
    # dispensaries that request approval
    @onboarding_request_approved_dispensaries = Dispensary.onboarding_request_approved
    # dispensaries that are signed up
    @signed_up_dispensaries = Dispensary.signed_up.includes(:dispensary_patients)
  end

  def send_onboarding_invitation
    # send onboarding invitation to given dispensary
    if @dispensary = Dispensary.find_by(id: params[:id])
      @dispensary.onboarding_state = Dispensary.onboarding_states['onboarding_request_approved']

      if @dispensary.save(validate: false)
        DispensaryMailer.invite_dispensary(@dispensary).deliver_now
        redirect_to admins_dispensaries_path, notice: "Onboarding link was sent to dispensary"
      else
        flash[:alert] = "Cannot send invitation due to: #{@dispensary.errors.full_messages}"
        redirect_to admins_dispensaries_path
      end
    else
      flash[:alert] = "Dispensary couldn't be found"
      redirect_to admins_dispensaries_path
    end
  end

  def mass_text
    # send mass text to patients from the given dispensary
    mass_service = MassTextService.new(mass_text_params)
    unless @status = mass_service.send_messages
      @errors = mass_service.errors
    end
  end

  def new
    @dispensary = Dispensary.new
  end

  def create
    @dispensary = Dispensary.new(dispensary_params)
    # update some specific info
    @dispensary.phone_number = @dispensary.phone_number.gsub("(","").gsub(")", "").gsub("-", "")
    @dispensary.onboarding_state = 'signed_up'
    @dispensary.name = @dispensary.name.to_s.strip
    @dispensary.geocode

    if @dispensary.save
      redirect_to admins_dispensaries_path, notice: "Saved successfully"
    else
      render :new
    end
  end

  def edit
    @dispensary = Dispensary.find_by(id: params[:id])
  end

  def update
    @dispensary = Dispensary.find_by(id: params[:id])
    if @dispensary.update(dispensary_params)
      redirect_to admins_dispensaries_path, notice: "Updated successfully"
    else
      render :edit
    end
  end

  protected
  def mass_text_params
    params.require(:mass_text).permit(:dispensary_id, :message)
  end
  
  def dispensary_params
    params.require(:dispensary).permit(:name, :email, :phone_number, :address, :zip_code, :city, :state, :country, :password)
  end

end
