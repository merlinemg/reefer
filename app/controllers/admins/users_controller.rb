class Admins::UsersController < Admins::BaseController

  def index
    @users = User.all.order(name: :asc).page(params[:page])
  end

  def signin_as
    @user = User.find(params[:id])
    sign_in(:user, @user)
    redirect_to my_clubs_users_home_index_path
  end

end