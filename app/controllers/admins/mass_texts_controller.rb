module Admins
  class MassTextsController < Admins::BaseController
    layout "admin"

    def index
      # load only id & name of the signed up dispensaries
      @dispensaries = Dispensary.signed_up_state.select([:id, :name])
    end

    def send_text
      # if dispensary params is blank, meaning on select the blank option
      # was selected, use all signed up dispensaries
      if params[:dispensary].blank?
        @dispensary_ids = Dispensary.signed_up_state.pluck(:id)
      else
        # if not blank, then an id was given
        @dispensary_ids = [ params[:dispensary] ]
      end
      @message = params[:message]

      # check presence for message
      if @message.blank?
        flash[:notice] = "Message blank"
        return redirect_to admins_mass_texts_path
      # check if message is over the 140 char limit
      elsif @message.length > 140
        flash[:notice] = "Message must be maximum 140 characters long"
        return redirect_to admins_mass_texts_path
      # check if we have dispensaries
      elsif @dispensary_ids.empty?
        flash[:notice] = "No dispensary selected"
        return redirect_to admins_mass_texts_path
      end

      # run the worker that assigns an sms worker per dispensary
      AssignSmsMultipleDispensariesWorker.perform_at(Time.now + 1.second, @dispensary_ids, @message)

      # notify the user
      flash[:notice] = "Starting sending multiple sms texts!"
      redirect_to admins_mass_texts_path
    end

  end
end