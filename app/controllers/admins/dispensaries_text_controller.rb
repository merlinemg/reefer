class Admins::DispensariesTextController < Admins::BaseController
  layout "admin"

  def index
   @dispensary = Dispensary.all
  end

  def update
    @dispensary = Dispensary.find_by(id: params[:dispensary_id])
    dt = DispensaryText.find_or_initialize_by(dispensary_id: params[:dispensary_id])
    remaining_text = 0
    if dt.send_sms_count.nil?
        dt.send_sms_count = 0
    end
    if dt.remaining_text.nil?
        remaining_text = 0
        dt.remaining_text = 0
        dt.send_sms_count = 0
    else
        remaining_text = dt.remaining_text
    end
    if params[:type] == 'update_text'
        remaining_text = 0
        value = params[:value]
        if dt.remaining_text.nil?
            dt.remaining_text = value.to_i
            remaining_text = value.to_i
        else
            dt.remaining_text += value.to_i
            remaining_text = dt.remaining_text
        end
        if dt.remaining_text < 0
            dt.remaining_text = 0
            remaining_text = 0
        end
        dt.access = params[:access]
        dt.twilio_phone_number = params[:twilio_phone_number]
        dt.save
    elsif params[:type] == 'update_access'
        dt.access = params[:access]
        dt.save
    end
    render json:{status:"success",remaining_text:remaining_text}
  end
end