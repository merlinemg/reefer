class Admins::FaqsController < Admins::BaseController
  layout "admin"
  before_action :set_faq, only: [:edit, :update, :destroy]

  def index
    @faqs = Faq.all.page(params[:page])
  end

  def new
    @faq = Faq.new
  end

  def create
    @faq = Faq.new(faq_params)
    if @faq.save
      redirect_to admins_faqs_path, notice: "Saved successfully"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @faq.update(faq_params)
      redirect_to admins_faqs_path, notice: "Saved successfully"
    else
      render :edit
    end
  end

  def destroy
    @faq.destroy
    redirect_to admins_faqs_path
  end

  private
    def faq_params
      params.require(:faq).permit(:title, :content)
    end

    def set_faq
      @faq = Faq.find_by(id: params[:id])
    end
end
