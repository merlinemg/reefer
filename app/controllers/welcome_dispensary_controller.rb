class WelcomeDispensaryController < ApplicationController
  layout "welcome_white"
  before_action :set_navbar_for

  def index
  end

  def waiting_list
  end

  def join_waiting_list
    @new_dispensary = Dispensary.new(dispensary_params)
    @new_dispensary.onboarding_state = 'onboarding_request'

    if @new_dispensary.save
      DispensaryMailer.new_join_waiting_list_request(@new_dispensary.email, @new_dispensary.name, @new_dispensary.phone_number).deliver_now
      render :thank_you
    else
      render :index, alert: @new_dispensary.errors.full_messages.join(". ")
    end
  end

  def thank_you
    if params[:email].present? && params[:message].present?
      ContactMailer.send_question(params).deliver_now
    end
  end

  protected
  def set_navbar_for
    @navbar_for = "dispensary"
  end

  def dispensary_params
    params.require(:dispensary).permit(:id, :email, :name, :phone_number)
  end

end
