class NotificationSerializer < ActiveModel::Serializer
  attributes :id, :kind, :message, :upper_message, :points, :seen_at, :dispensary_id, :lower_message

  def message
    object.message.to_s
  end

   def upper_message
    object.upper_message.to_s
  end

  def lower_message
    object.lower_message.to_s
  end
end
