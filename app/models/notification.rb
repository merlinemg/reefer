class Notification < ApplicationRecord
  belongs_to :user
  belongs_to :dispensary
  enum kind: [
    'free_joint', 'small_reward', 'medium_reward', 'large_reward',
    'invitation', 'visit', 'visit_for_friend', 'sign_up', 'dispensary_a', 'dispensary_b',
    'claim_reward', 'fb_reward', 'restore_invitation'
  ]

  default_scope { order(created_at: :desc) }
  scope :unseen, -> { where(seen_at: nil) }

  FB_CONNECT_LOCALE= 'activerecord.models.notifications.fb_connect'
  YOU_VE_EARNED_LOCALE='activerecord.models.notifications.you_ve_earned'
  FREE_JOINT_LOCALE='activerecord.models.notifications.free_joint'
  YOU_VISITED_LOCALE='activerecord.models.notifications.you_visited'
  FRIEND_VISITED_LOCALE='activerecord.models.notifications.friend_visited'
  YOU_HAVE_N_POINTS_LOCALE='activerecord.models.notifications.you_have_n_points'
  YOU_ARE_NOW_FRIENDS_LOCALE= 'activerecord.models.notifications.you_are_now_friends'
  YOU_SIGNED_UP_LOCALE = 'activerecord.models.notifications.signed_up'
  CONGRATS_LOCALE= 'activerecord.models.notifications.congrats'
  CLAIM_REWARD_LOCALE='activerecord.models.notifications.claim_reward'

  def points
    DispensaryPatient::POINTS.fetch(kind.to_sym, 0)
  end

  class << self
    def create_invitee_signed_up(user, dispensary, invitee)
      create user: user, dispensary_id: dispensary.id, kind: 'small_reward',
        upper_message: "#{invitee.name} signed up with your link!",
        message: "#{dispensary.name}",
        lower_message: "Get Reeferal reward when he visits!"
    end

    def create_reeferal_free_join(user, dispensary, inviter)
      create user: user, dispensary_id: dispensary.id, kind: 'free_joint',
        upper_message: "You accepted #{inviter.name}'s reeferal!",
        message: "You've got a free gram at #{dispensary.name}"
    end

    def create_both_friendships(user, dispensary, friend)
      Notification.create_invitation(user, dispensary, friend)
      Notification.create_invitation(friend, dispensary, user)
    end

    def restore_friendships(user, friend)
      Notification.restore_friendship(user, friend)
      Notification.restore_friendship(friend, user)
    end

    def restore_friendship(user, friend)
      create user: user, kind: 'restore_invitation',
        upper_message: I18n.t(YOU_ARE_NOW_FRIENDS_LOCALE, friend: friend.name),
        message: "You already received friend bonus from #{friend.name}"
    end

    def create_can_collect_free_join(user, friend, dispensary)
      create user: user, dispensary_id: dispensary.id, kind: 'free_joint',
        upper_message: "#{friend.name}  accepted your Reeferal!",
        message: "You've got a free gram at #{dispensary.name}"
    end

    def create_fb_connect(user, dispensary)
      create user: user, dispensary_id: dispensary.id, kind: 'fb_reward',
        upper_message: "Facebook connect",
        message: I18n.t(FB_CONNECT_LOCALE, n: DispensaryPatient::POINTS[:fb_reward], where: dispensary.name)
    end

    def create_small_reward(user, dispensary)
      create user: user, dispensary: dispensary, kind: 'small_reward',
        upper_message: I18n.t(YOU_HAVE_N_POINTS_LOCALE, n: dispensary.small_reward.points),
        message: I18n.t(YOU_VE_EARNED_LOCALE, what: dispensary.small_reward.name, where: dispensary.name)
    end

    def create_medium_reward(user, dispensary)
      create user: user, dispensary: dispensary, kind: 'medium_reward',
        upper_message: I18n.t(YOU_HAVE_N_POINTS_LOCALE, n: dispensary.medium_reward.points),
        message: I18n.t(YOU_VE_EARNED_LOCALE, what: dispensary.medium_reward.name, where: dispensary.name)
    end

    def create_large_reward(user, dispensary)
      create user: user, dispensary: dispensary, kind: 'large_reward',
        upper_message: I18n.t(YOU_HAVE_N_POINTS_LOCALE, n: dispensary.large_reward.points),
        message: I18n.t(YOU_VE_EARNED_LOCALE, what: dispensary.large_reward.name, where: dispensary.name)
    end

    def create_invitation(user, dispensary, friend)
      create user: user, dispensary: dispensary, kind: 'invitation',
        upper_message: I18n.t(YOU_ARE_NOW_FRIENDS_LOCALE, friend: friend.name),
        message: dispensary.name
    end

    def create_onboarding(user, dispensary, kind)
      create user: user, dispensary: dispensary, kind: kind,
        upper_message: I18n.t(YOU_SIGNED_UP_LOCALE),
        message: dispensary.name
    end

    def create_you_visit(user, dispensary)
      create user: user, dispensary: dispensary, kind: 'visit',
        upper_message: I18n.t(YOU_VISITED_LOCALE),
        message: dispensary.name
    end

    def claim_free_joint(user, dispensary)
      create user: user, dispensary_id: dispensary.id, kind: 'claim_reward',
        upper_message: I18n.t(CONGRATS_LOCALE),
        message: I18n.t(CLAIM_REWARD_LOCALE, reward: 'free joint', where: dispensary.name)
    end

    def claim_small_reward(user, dispensary)
      create user: user, dispensary_id: dispensary.id, kind: 'claim_reward',
        upper_message: I18n.t(CONGRATS_LOCALE),
        message: I18n.t(CLAIM_REWARD_LOCALE, reward: dispensary.small_reward.name, where: dispensary.name)
    end

    def claim_medium_reward(user, dispensary)
      create user: user, dispensary_id: dispensary.id, kind: 'claim_reward',
        upper_message: I18n.t(CONGRATS_LOCALE),
        message: I18n.t(CLAIM_REWARD_LOCALE, reward: dispensary.medium_reward.name, where: dispensary.name)
    end
    
    def claim_large_reward(user, dispensary)
      create user: user, dispensary_id: dispensary.id, kind: 'claim_reward',
        upper_message: I18n.t(CONGRATS_LOCALE),
        message: I18n.t(CLAIM_REWARD_LOCALE, reward: dispensary.large_reward.name, where: dispensary.name)
    end
  end # self
end
