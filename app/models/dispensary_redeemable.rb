class DispensaryRedeemable < ApplicationRecord
  belongs_to :dispensary_deal
  belongs_to :dispensary_patient

  alias_method :deal, :dispensary_deal
  alias_method :patient, :dispensary_patient

  before_create :set_redeemed_at

  def set_redeemed_at
    self.redeemed_at = DateTime.now
  end
end
