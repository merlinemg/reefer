class Dispensary < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :omniauthable, :validatable, :recoverable,
  devise :database_authenticatable, :registerable, :rememberable, :trackable, :recoverable, :validatable
  acts_as_token_authenticatable

  enum onboarding_state: ['onboarding_request', 'onboarding_request_approved', 'signed_up']

  # scopes
  scope :signed_up_state, ->() { where(onboarding_state: Dispensary.onboarding_states['signed_up']) }
  # validations
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }, if: [:onboarding_request?, :onboarding_request_approved?]
  validates :name, :email, :phone_number, presence: true, if: [:onboarding_request?, :onboarding_request_approved?]
  validates :password, :password_confirmation, :city, :state, :zip_code, presence: true, if: :onboarding_request_approved?
  validate  :password_matches?, if: [:onboarding_request_approved?]
  validates :password, length: { in: 6..128 }, if: [:onboarding_request_approved?]
  validates_format_of :name, with: /\A([a-zA-Z]|[-]|\s)+\z/, if: [:onboarding_request?, :onboarding_request_approved?]
  validates_length_of :name, :maximum => 50, if: [:onboarding_request?, :onboarding_request_approved?]
  validates_uniqueness_of :phone_number, case_sensitive: false
  validates_uniqueness_of :email
  # hooks
  before_validation :strip_name, if: [:onboarding_request?, :onboarding_request_approved?]
  before_validation :remove_phone_number_formatting
  after_validation :reset_authentication_token, if: :password_reset?
  after_validation :sign_up_dispensary , if: :onboarding_request_approved?
  after_save :create_default_deals, if: :signed_up?
  before_create :generate_magic_link
  # geocoder
  geocoded_by :full_street_address
  after_validation :geocode, if: :full_address_changed?
  # relations
  has_many :notifications
  has_many :user_visitation, dependent: :destroy
  has_many :store_hours
  accepts_nested_attributes_for :store_hours
  has_many :dispensary_patients
  has_many :patients,
    through: :dispensary_patients,
    source: :user,
    class_name: "User"
  has_many :dispensary_deals, ->(){ order(points: :asc)}
  has_many :dispensary_redeemables, through: :dispensary_deals

  # alias methods
  alias_method :deals, :dispensary_deals
  alias_method :deals2, :dispensary_deals

  class << self
    def sample(nr = 1)
      if nr.eql?(1)
        where(onboarding_state: Dispensary.onboarding_states['signed_up']).reload.sample
      else
        where(onboarding_state: Dispensary.onboarding_states['signed_up']).reload.sample(nr)
      end
    end

    def find_by_magic_link(_magic_link)
      dispensary = self.onboarding_request_approved.find_by(magic_link: _magic_link)
      raise CanCan::AccessDenied unless dispensary.present?
      dispensary
    end
  end

  def small_reward
    self.deals.first
  end

  def is_message_left
    status = ''
    print self.get_patients_count,":::::::::::"
    if self.dispensary_text.remaining_text == 0  || self.dispensary_text.remaining_text < self.get_patients_count
        status =  "disabled='disabled'"
    end
    return status
  end

  def get_patients
      if self.patients.any?
        # get only patients who have visited the dispensary
        patient_ids = DispensaryPatient.all.where(dispensary_id: self.id).pluck(:user_id).uniq
        return patient_ids
      end
  end
  def get_patients_count
    total = self.get_patients
    if !total.nil?
        return total.length
    else
        total = 0
    end
  end
  def medium_reward
    self.deals[1]
  end
  def dispensary_text
    begin
        dt = DispensaryText.find_by(dispensary_id: self.id)
    rescue
        dt = nil
    end
    return dt
  end

  def dispensary_text_access_status
    dt = nil
    begin
        if DispensaryText.find_by(dispensary_id: self.id).access == true
            dt = true
        end
    rescue
        dt = nil
    end
    return dt
  end
  def large_reward
    self.deals.last
  end
  def referred_users
    counter = 0
    self.dispensary_patients.where(:dispensary_id=>self.id).each do |patient|
        if patient.reeferal_user_id.present?
            counter += 1
        end
    end
    return counter
  end

  def get_user_point(user_id)
    self.dispensary_patients.where(:dispensary_id=>self.id).each do |patient|
        if patient.user_id == user_id
            return patient
        end
    end
  end
  def redeemed_rewards
    self.dispensary_redeemables.group_by(&:dispensary_deal_id).map do |id, content|
      {id: id, deal: content.first.dispensary_deal.name, redeemed: content.count}
    end
  end

  def password_required?
    new_record? ? false : super
  end

  def full_street_address
    [address, city, state, zip_code].join(', ')
  end

  def create_new_referral_deal
        dr = DispensaryReferralDeal.new
        dr.dispensaries_id = self.id
        dr.points = "N/A"
        dr.name = "Referral Reward"
        dr.save
  end

  def basic_details(with_id: false)
    x = {
      name: name,
      address: address,
      zip_code: zip_code,
      city: city,
      state: state,
      country: country,
      longitude: longitude,
      latitude: latitude
    }
    x[:id] = id if with_id
    x
  end

  def shipping_address
    state_address = [self.state, self.zip_code].compact.join(" ")
    [self.city, state_address].compact.join(", ")
  end

  def create_default_deals
    flag = 0
    begin
        dr = DispensaryReferralDeal.find_by(dispensaries_id: self.id)
        if dr.nil?
            self.create_new_referral_deal
            flag = 1
        end
    rescue
        if flag ==0
            self.create_new_referral_deal
        end
    end
    return if self.deals.any?
    DispensaryDeal.create([
      {
        dispensary_id: id,
        name: "Free Joint",
        points: DispensaryPatient::POINTS[:small_reward]
      }, {
        dispensary_id: id,
        name: "Free Gram",
        points: DispensaryPatient::POINTS[:medium_reward]
      }, {
        dispensary_id: id,
        name: "Free Eighth",
        points: DispensaryPatient::POINTS[:large_reward]
      }
    ])
  end

  def password_matches?
    unless self.password.eql?(password_confirmation)
      errors.add(:password, "did not match")
      errors.add(:password_confirmation, "must match the password")
    end
  end

private
  def strip_name
    self.name = name.to_s.strip
  end

  def sign_up_dispensary
    self.onboarding_state = 'signed_up'
  end

  def remove_phone_number_formatting
    self.phone_number = self.phone_number.to_s.gsub('(', '').gsub(')','').gsub('-','').strip
  end

  def generate_magic_link
    self.magic_link = (Time.now.to_i + (1..99).to_a.sample).to_s(25)
  end

  def full_address_changed?
    address_changed? or city_changed? or state_changed? or zip_code_changed?
  end

  def password_reset?
    self.authentication_token = nil if password.present? and password_confirmation.present? and password.eql?(password_confirmation)
  end
end
