class DispensaryText < ApplicationRecord
  belongs_to :dispensary

  def sms_average_count
    difference = 0
    if self.remaining_text
        total_count = self.remaining_text+self.send_sms_count
        if self.send_sms_count
            difference = self.send_sms_count.to_f / total_count.to_f
            difference *= 100
        else
            difference = 100
        end
    end
    print difference.nan?,"<><<>>>>"
    if difference.nan?
        difference = 0
    else
        difference=difference.round(2)
    end

    return difference
  end
end
