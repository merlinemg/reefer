class User < ApplicationRecord
  FRIENDLY_STEP_NAMES = {
    'enter_phone_nr' => "Enter phone number",
    'enter_confirmation_code'=> 'Enter confirmation code',
    'enter_password'=> 'Enter password',
    'enter_name'=> 'Enter name',
    'signed_up' => 'Signed up'
  }
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :omniauthable, :validatable,
  devise :database_authenticatable, :registerable, :rememberable, :trackable, :recoverable

  # friendly id - helps use of slugs
  extend FriendlyId
  friendly_id :hex_code, use: :slugged

  enum onboarding_step: ['enter_phone_nr', 'enter_confirmation_code', 'enter_password', 'enter_name', 'signed_up']
  enum sign_up_kind: ['online', 'in_store', 'invitation']

  attr_accessor :provided_confirmation_code, :ref

  validates_presence_of :onboarding_step

  # constant with regular expresion that validates format of phone number
  FORMAT_PHONE_NUMBER_USA = /\A\d{3}\ \d{7}\z/

  has_many :user_visitation, dependent: :destroy

  validates_uniqueness_of :phone_number, case_sensitive: false
  validates_presence_of :phone_number, on: :create, if: :enter_phone_nr?
  validates_length_of :phone_number, minimum: 10, maximum: 15, if: :enter_phone_nr?
  validates_format_of :phone_number, with: FORMAT_PHONE_NUMBER_USA

  before_validation :remove_phone_number_formatting, if: :enter_phone_nr?

  validates_presence_of :provided_confirmation_code, on: :update, if: :enter_confirmation_code?
  validate :confirmation_code_matches?, if: :enter_confirmation_code?

  validates_presence_of :password, :password_confirmation, on: :update, if: :enter_password?
  validates :password, length: { in: 6..128 }, if: :enter_password?
  validate  :password_matches?, if: :enter_password?

  validates_presence_of :name, on: :update, if: :enter_name?
  validates_format_of :name, with: /\A([a-zA-Z]|[-]|\s)+\z/, if: :enter_name?
  validates_length_of :name, :maximum => 30, if: :enter_name?
  before_validation :strip_name, if: :enter_name?

  before_create :set_confirmation_code, :generate_magic_link, :set_hex_code, :set_autoaccept_fb

  has_attached_file :avatar,
    styles: { medium: "500x500>", thumb: "80x80>", profile: "240x240>", small: "40x40>" },
    default_url: ":style/missing.png", processors: [:cropper]

  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  has_many :notifications

  has_many :dispensary_patients, dependent: :destroy

  has_many :dispensaries, through: :dispensary_patients

  has_many :inviter_invitations,
    class_name: "Invitation",
    foreign_key: 'invitee_id'

  has_many :invitee_invitations,
    class_name: "Invitation",
    foreign_key: 'inviter_id'

  has_many :inviters,
    through: :inviter_invitations,
    source: :inviter,
    class_name: "User",
    foreign_key: "inviter_id"

  has_many :invitees,
    through: :invitee_invitations,
    source: :invitee,
    class_name: "User",
    foreign_key: "invitee_id"

  has_many :all_inviter_invitations,
    -> { where(invitation_state: [0,1,2]) },
    class_name: "Invitation",
    foreign_key: 'invitee_id'

  has_many :all_invitee_invitations,
    -> { where(invitation_state: [0,1,2]) },
    class_name: "Invitation",
    foreign_key: 'inviter_id'

  has_many :all_inviters,
    through: :all_inviter_invitations,
    source: :inviter,
    class_name: "User",
    foreign_key: "inviter_id"

  has_many :all_invitees,
    through: :all_invitee_invitations,
    source: :invitee,
    class_name: "User",
    foreign_key: "invitee_id"

  Invitation.invitation_states.each do |invitation_state, number|
    has_many "#{invitation_state}_inviter_invitations".to_sym,
      -> { where(invitation_state: number) },
      class_name: "Invitation",
      foreign_key: 'invitee_id'
    has_many "#{invitation_state}_invitee_invitations".to_sym,
      -> { where(invitation_state: number) },
      class_name: "Invitation",
      foreign_key: 'inviter_id'

    has_many "#{invitation_state}_inviters".to_sym,
      through: "#{invitation_state}_inviter_invitations".to_sym,
      source: :inviter,
      class_name: "User",
      foreign_key: "inviter_id"

    has_many "#{invitation_state}_invitees".to_sym,
      through: "#{invitation_state}_invitee_invitations".to_sym,
      source: :invitee,
      class_name: "User",
      foreign_key: "invitee_id"
  end

  Notification.kinds.each do |notification_kind, number|
    has_many "#{notification_kind}_notifications".to_sym,
      -> { where(kind: number) },
      class_name: "Notification",
      foreign_key: 'user_id'
  end

  # logic for cropping image
  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h

  def cropping?
    !crop_x.blank? && !crop_y.blank? && !crop_w.blank? && !crop_h.blank?
  end

  def reprocess_avatar
    avatar.reprocess!
  end

   def avatar_geometry(style = :original)
    @geometry ||= {}
    @geometry[style] ||= Paperclip::Geometry.from_file("http:"+avatar.url(style))
  end

  alias_method :friendship_invitations, :pending_inviter_invitations

  def set_autoaccept_fb
    self.auto_accept_fb_friends = true
  end

  def pending_friends_ids
    pending_invitees.pluck(:facebook_id)
  end

  def phone_number_formated
    if self.phone_number.length == 11
      "(#{self.phone_number[0..2]}) #{self.phone_number[4..6]}-#{self.phone_number[7..10]}"
    else
      self.phone_number
    end
  end

  class << self
    def reset_token_generator(user)
      # taken from devise #set_reset_password_token
      raw, enc = Devise.token_generator.generate(user.class, :reset_password_token)
      user.reset_password_token   = enc
      user.reset_password_sent_at = Time.now.utc
      user.save(validate: false)
      raw
    end

    def find_signed_up_or_create_new(phone_number)
      _phone_number = phone_number.gsub("(","").gsub(")", "").gsub("-","")
      user = find_by(phone_number: _phone_number)
      if user.nil?
        user = self.online_sign_up(phone_number, 'invitation')
      end
      user
    end

    def in_store_sign_up(dispensary_id, phone_number)
      user = new(sign_up_kind: 'in_store', phone_number: phone_number, onboarding_step: 'enter_phone_nr')
      if user.save
        DispensaryPatient.create_for_in_store_signup(user, dispensary_id)
      end
      user
    end

    def online_sign_up(phone_number, sign_up_kind='online')
      user = new(sign_up_kind: sign_up_kind, phone_number: phone_number, onboarding_step: 'enter_phone_nr')
      user.save
      user
    end

    def sign_up_enter_confirmation_code(user, provided_confirmation_code)
      user.onboarding_step = 'enter_confirmation_code'
      user.provided_confirmation_code = provided_confirmation_code.to_s.strip.to_i
      unless user.save
        user.onboarding_step = user.onboarding_step_was
      end
      user
    end

    def invitation_sign_up(inviter_id, phone_number)
      if invitee = User.find_by(phone_number: phone_number)
        Invitation.create(inviter_id: inviter_id, invitee_id: invitee.id)
      else
        invitee = new(sign_up_kind: 'invitation', phone_number: phone_number, onboarding_step: 'enter_phone_nr')
        if invitee.save
          Invitation.create(inviter_id: inviter_id, invitee_id: invitee.id)
        end
      end
      invitee
    end
  end # class << self

  def mark_notifications_seen
    self.notifications.unseen.update_all(seen_at: DateTime.now)
    self.friendship_invitations.unseen.update_all(seen_at: DateTime.now)
  end

  def confirm_enter_password_step!
    self.onboarding_step = 'enter_password'
    if self.save
      self.signed_up!
    else
      self.onboarding_step = self.onboarding_step_was
    end
    self
  end

  def confirm_enter_name_step!
    self.onboarding_step = 'enter_name'
    unless self.save
      self.onboarding_step = self.onboarding_step_was
    end
    self
  end

  def all_invitations
    Invitation.where(
      id: all_invitee_invitation_ids + all_inviter_invitation_ids
    ).order(id: :asc)
  end

  def friend_ids
    accepted_inviter_ids + accepted_invitee_ids
  end

  def friends
    User.where(id: friend_ids ).distinct
  end

  def facebook_connected?
    self.facebook_id.present?
  end

  def facebook_attributes(graph)
    {
      "id": self.id,
      "name": self.name,
      "photo": self.avatar? ? self.avatar.url(:thumb) : graph.get_picture(facebook_id),
      "dispensaries": self.dispensaries_friendly
    }
  end

  def main_picture
    if self.avatar? || !self.fb_picture.present?
      self.avatar.url(:thumb)
    else
      self.fb_picture
    end
  end

  def dispensaries_friendly
    self.dispensaries.last(3).map{ |x| x.name }.join(', ')
  end

  def all_friendship_relations_user_ids
    all_inviter_ids + all_invitee_ids
  end

  def all_friendship_relations_users
    User.where(id: all_friendship_relations_user_ids ).distinct
  end

  def all_friendship_relations_facebook_ids
    all_friendship_relations_users.pluck(:facebook_id)
  end

  def all_dispensaries
    tmp_friend_ids = friend_ids
    Dispensary.
      where(onboarding_state: Dispensary.onboarding_states['signed_up']).
      includes(:dispensary_deals, dispensary_patients: [:user]).
      map do |d|
        dispensary_hash(d, tmp_friend_ids)
      end
  end

  def all_dispensaries_sorted
    # all dispensary patiens records where current user has more then 0 points
    visited_dispensary_patients = dispensary_patients.where("dispensary_patients.points > 0").includes(dispensary: (:dispensary_deals)).
      where(dispensaries: {onboarding_state: Dispensary.onboarding_states['signed_up']} ).
      order("dispensary_patients.points desc")

    # create a hash object for each record
    visited_dispensaries = visited_dispensary_patients.map do |dp|
      dispensary_hash_from_dp(dp, friend_ids)
    end.compact

    # return all not visited dispensaries and create a hash object for each
    # not_visited_dispensaries = Dispensary.includes(:dispensary_deals, :dispensary_patients).where("id not in (?)", visited_dispensary_patients.pluck(:dispensary_id)).signed_up_state.shuffle.map do |d|
    #   dispensary_hash(d, friend_ids)
    # end
    not_visited_dispensaries = []
    # combine visited and non visited, resulting in all dispensaries
    visited_dispensaries + not_visited_dispensaries
  end

  def users_dispensaries_sorted
    dispensaries.
      includes(:dispensary_deals, dispensary_patients: [:user]).
      signed_up_state.
      order("dispensary_patients.points DESC")
  end

  def my_dispensaries
    _dispensary_patients = dispensary_patients.includes(dispensary: (:dispensary_deals)).
      where(dispensaries: {onboarding_state: Dispensary.onboarding_states['signed_up']} ).
      order("dispensary_patients.points DESC")

    dispensaries_hashed = _dispensary_patients.map do |dp|
      dispensary_hash_from_dp(dp, friend_ids)
    end
    dispensaries_hashed
  end

  def dispensary_hash(_dispensary, _friend_ids)
    if  dp = _dispensary.dispensary_patients.where(user_id: self.id).first
      dispensary_points = dp.points
    else
      dispensary_points = 0
    end

    {
      dispensary: _dispensary.basic_details(with_id: true),
      friends:  _friend_ids.any? ? _dispensary.patients.where(id: _friend_ids).pluck(:name) : [],
      points: dispensary_points,
      deals: _dispensary.deals.map {|x| { name: x.name, points: x.points } }
    }
  end

  def dispensary_hash_from_dp(dp, friend_ids)
    # find friends
    friends_names = friend_ids.any? ? dp.dispensary.patients.where(id: friend_ids).pluck(:name) : []
    # make hash
    {
      dispensary: dp.dispensary.basic_details(with_id: true),
      friends:  friends_names,
      points: dp.points,
      deals: dp.dispensary.deals.order(:points).map {|x| { name: x.name, points: x.points } }
    }
  end

  def my_dispensaries_sorted
    _dispensary_patients = dispensary_patients.includes(dispensary: (:dispensary_deals)).
      where(dispensaries: {onboarding_state: Dispensary.onboarding_states['signed_up']} ).
      order("dispensary_patients.points DESC").where("dispensary_patients.points >= 30")

    dispensaries_hashed = _dispensary_patients.map do |dp|
      dispensary_hash_from_dp(dp, friend_ids)
    end
    dispensaries_hashed
  end

  def dispensary(dispensary_id)
    d = Dispensary.where(onboarding_state: Dispensary.onboarding_states['signed_up']).
      includes(:store_hours, :dispensary_deals).
      where(id: dispensary_id).first

    if d
      _points, _points_cap = 0, 0
      if dp = d.dispensary_patients.find_by(user_id: self.id)
        _points = dp.points
        _points_cap = dp.points_cap
      end
      {
        dispensary: d.basic_details(with_id: true),
        store_hours: d.store_hours.map{|sh| sh.basic_details}.flatten,
        friends: d.patients.where(id: friend_ids).pluck(:name),
        points: _points,
        points_cap: _points_cap,
        deals: d.deals.map {|x| { name: x.name, points: x.points } }
      }
    end
  end

  def visited_a_dispensary?
    dispensary_patients.where.not(last_visit: nil).any?
  end

  def has_relations_to_any_dispensaries?
    dispensary_patients.any?
  end

  def last_visited_dispensary_relation
    dispensary_patients.where.not(last_visit: nil).order(last_visit: :desc).first
  end

  def last_visited_dispensary_or_random
    dp = dispensary_patients.where.not(last_visit:nil).order(last_visit: :desc).first
    if dp.nil?
      Dispensary.signed_up_state.sample
    else
      dp.dispensary.present? ? dp.dispensary : Dispensary.signed_up_state.sample
    end
  end

  def last_visited_dispensary_or_random_point
    dp = DispensaryPatient.where(user_id: self.id).order(last_visit: :desc).first
    if dp.present?
        begin
            return DispensaryReferralDeal.find_by(dispensaries_id: dp.dispensary.id).name
        rescue
            'Free Reward'
        end
    else
        'Free Reward'
    end
  end

  def set_confirmation_code
    self.confirmation_code = 5.times.map{rand(1..9)}.join.to_i
  end

  def set_hex_code
    self.hex_code = SecureRandom.hex
  end

  def confirmation_code_matches?
    unless self.confirmation_code.eql?(provided_confirmation_code)
      errors.add(:provided_confirmation_code, "must match the sent confirmation code")
    end
  end

  def password_matches?
    unless self.password.eql?(password_confirmation)
      errors.add(:password_confirmation, "must match the password")
    end
  end

  def koala
    Koala::Facebook::API.new(self.fb_access_token, ENV['FACEBOOK_APP_SECRET'])
  end

  def fb_valid?
    self.fb_access_token.present? && self.facebook_id.present? && !self.fb_expired?
  end

  def fb_expired?
    self.fb_expires_at.nil? || self.fb_expires_at < Time.now
  end

  def fb_disconnect
    self.facebook_id = nil
    self.fb_access_token = nil
    self.fb_expires_at = nil
    self.fb_picture = nil
    self.save
  end

  # steps checks
  def is_step_enter_phone_nr?
    self.onboarding_step == "enter_phone_nr"
  end

  def is_step_enter_confirmation_code?
    self.onboarding_step == "enter_confirmation_code"
  end

  def is_step_enter_password?
    self.onboarding_step == "enter_password"
  end

   def is_step_enter_name?
    self.onboarding_step == "enter_name"
  end

  def is_step_signed_up?
    self.onboarding_step == "signed_up"
  end

  def resend_confirmation_code
    set_confirmation_code
    if self.save
      send_confirmation_code
    end
  end

  def send_confirmation_code
    return true if Rails.env.test?
    # reset the user to enter phone step
    self.onboarding_step == "enter_phone_nr"
    self.save
    # send to the phone number
    status = SMS.send_to(
      self.phone_number,
      I18n.t(
        "sms.confirmation_code",
        code: self.confirmation_code
      )
    )
    # return based on message response
    return status[:message] == "OK" ? true : false
  end

  def any_notifications_unseen?
    self.notifications.unseen.any? || self.friendship_invitations.unseen.any?
  end

  private
  def strip_name
    self.name = name.to_s.strip
  end

  def generate_magic_link
    self.magic_link = (Time.now.to_i + (1..99).to_a.sample).to_s(25)
  end

  def set_next_step
    if onboarding_step.present?
      unless signed_up?
        self.onboarding_step = self.onboarding_steps[onboarding_step] + 1
      end
    else
      self.onboarding_step = 'enter_phone_nr'
    end
  end

  def remove_phone_number_formatting
    self.phone_number = self.phone_number.to_s.gsub('(', '').gsub(')','').gsub('-','').strip
  end
end
