require 'twilio-ruby'
class SMS

  class << self
    def send_to(recipient_phone_nr, message)
      # sends an sms with given message to the recepient phone number
      # if debug sms is turned on, it will send the message to
      # the debug sms phone number
      if ENV['DEBUG_SMS'].eql?("1")
        recipient_phone_nr = ENV['DEBUG_SMS_PHONE_NUMBER']
      end

      client = Twilio::REST::Client.new
      unless client
        puts "----------"
        puts "Error: Cannot create twillio rest client"
        puts "----------"
        return {
          message: 'FAILED',
          error: {
            message: "Cannot create twillio rest client",
            code: 500
          }
        }
      end

      begin
        response = client.messages.create({
          from: ENV['TWILIO_OUTGOING_PHONE_NUMBER'],
          to: recipient_phone_nr,
          body: message
        })

        return {
          message: 'OK',
          status: response.status,
          response: response
        }
      rescue Twilio::REST::RequestError => error
        puts "----------"
        puts "Error: #{error.code}"
        puts "Message: #{error.message}"
        puts "----------"

        return {
          message: 'FAILED',
          error: {
            message: error.message,
            code: error.code
          }
        }
      rescue StandardError => error
        puts "----------"
        puts "Message: #{error.message}"
        puts "----------"

        return {
          message: 'FAILED',
          error: {
            message: error.message
          }
        }
      end
    end
  end
end