class Invitation < ApplicationRecord
  # enables soft deletion
  acts_as_paranoid

  PENDING_STATUS = 'pending'
  ACCEPTED_STATUS = 'accepted'
  REJECTED_STATUS = 'rejected'

  belongs_to :inviter, class_name: "User", foreign_key: "inviter_id"
  belongs_to :invitee, class_name: "User", foreign_key: "invitee_id"

  validates_uniqueness_of :inviter, scope: [:invitee]
  validates_uniqueness_of :invitee, scope: [:inviter]

  enum invitation_state: [PENDING_STATUS, ACCEPTED_STATUS, REJECTED_STATUS]
  scope :by_user, -> (user) { self.where("invitee_id = :user_id OR inviter_id = :user_id", user_id: user.id) }
  scope :unseen, -> { where(seen_at: nil) }

  def inviters_last_visited_dispensary_or_random_dispensary
    (inviter.last_visited_dispensary_relation and inviter.last_visited_dispensary_relation.dispensary) || Dispensary.signed_up_state.sample
  end

end
