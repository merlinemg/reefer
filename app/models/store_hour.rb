class StoreHour < ApplicationRecord
  belongs_to :dispensary
  validates_presence_of :from_time, :to_time

  OPEN_HOURS = {
    "01:00 AM"=>0,"01:30 AM"=>1,"02:00 AM"=>2,"02:30 AM"=>3,"03:00 AM"=>4,"03:30 AM"=>5,
    "04:00 AM"=>6,"04:30 AM"=>7,"05:00 AM"=>8,"05:30 AM"=>9,"06:00 AM"=>10,"06:30 AM"=>11,
    "07:00 AM"=>12,"07:30 AM"=>13,"08:00 AM"=>14,"08:30 AM"=>15,"09:00 AM"=>16,"09:30 AM"=>17,
    "10:00 AM"=>18,"10:30 AM"=>19,"11:00 AM"=>20,"11:30 AM"=>21,"12:00 AM"=>22,"12:30 AM"=>23,
    "01:00 PM"=>24,"01:30 PM"=>25,"02:00 PM"=>26,"02:30 PM"=>27,"03:00 PM"=>28,"03:30 PM"=>29,
    "04:00 PM"=>30,"04:30 PM"=>31,"05:00 PM"=>32,"05:30 PM"=>33,"06:00 PM"=>34,"06:30 PM"=>35,
    "07:00 PM"=>36,"07:30 PM"=>37,"08:00 PM"=>38,"08:30 PM"=>39,"09:00 PM"=>40,"09:30 PM"=>41,
    "10:00 PM"=>42,"10:30 PM"=>43,"11:00 PM"=>44,"11:30 PM"=>45,"12:00 PM"=>46,"12:30 PM"=>47
  }

  DAYS = {
    "sunday" => "Sunday",
    "monday" => "Monday",
    "tuesday" => "Tuesday",
    "wednesday" => "Wednesday",
    "thursday" => "Thursday",
    "friday" => "Friday",
    "saturday" => "Saturday"
  }

  def set_default_from_to_times
    self.from_time = 18
    self.to_time = 22
  end

  def set_from_time=(time_string)
    self.from_time = number_returner(time_string)
  end
  def set_to_time=(time_string)
    self.to_time = number_returner(time_string)
  end
  def get_from_time
    time_returner(self.from_time)
  end
  def get_to_time
    time_returner(self.to_time)
  end

  def basic_details
    details = []
    DAYS.each do |k,v|
      if self.send(k)
        details << {
          day: DAYS[k],
          from_time: self.get_from_time,
          to_time: self.get_to_time
        }
      end
    end
    details
  end

private
  def number_returner(time_string)
    OPEN_HOURS[time_string] if OPEN_HOURS.keys.include?(time_string)
  end

  def time_returner(number)
    OPEN_HOURS.invert[number] if OPEN_HOURS.invert.keys.include?(number)
  end
end
