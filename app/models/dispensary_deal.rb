class DispensaryDeal < ApplicationRecord
  belongs_to :dispensary

  has_many :dispensary_redeemables

  validates :name, presence: true, length: {minimum: 3}
  validates :points, presence: true, length: {maximum: 5}, :numericality => true

  def to_s
    name
  end

  def redeemed_rewards
    total_count = 0
    self.dispensary.dispensary_redeemables.group_by(&:dispensary_deal_id).map do |id, content|
      total_count += content.count
    end
    self.dispensary_redeemables.group_by(&:dispensary_deal_id).map do |id, content|
      avg_count = (Float(content.count)/Float(total_count))*100
      {id: id, deal: content.first.dispensary_deal.name, redeemed: content.count,total_count:avg_count.round(2)}
    end
  end
end
