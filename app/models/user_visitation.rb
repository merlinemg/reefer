class UserVisitation < ApplicationRecord
  default_scope {order('visited_at ASC')}
  belongs_to :user
  belongs_to :dispensary
end
