class GenerateAllDispensariesStatisticsWorker
  include Sidekiq::Worker

  def perform(email, attachement_name)
    data = StatisticService.all_dispensaries_statistics
    csv = StatisticService.export_to_csv(data)
    StatisticsMailer.send_statistics(email, csv, attachement_name).deliver
  end

end
