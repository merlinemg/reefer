class SendSmsWorker
  include Sidekiq::Worker
  sidekiq_options queue: :sms, retry: 3, backtrace: true

  def perform(user_id, message)
    user = User.find_by(id: user_id)
    # if user is found send sms message
    if user && user.signed_up?
      begin
        response = SMS.send_to(user.phone_number, message)
        # if response from twillio is not OK, report to Rollbar
        if !response[:message] == "OK"
         Rollbar.log('error', "Failed to send SMS to user_id: #{user_id} with message: #{message}")
        end
      rescue => e
        # rescue any error from Twillio and report to Rollbar
        Rollbar.error(e)
      end
    end
  end
end
