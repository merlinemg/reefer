class AssignSmsMultipleDispensariesWorker
  include Sidekiq::Worker

  def perform(dispensary_ids, message)
    if dispensary_ids.count == 1
      # just one dispensary, use service the normal way
      MassTextService.new({dispensary_id: dispensary_ids.first, message: message}).send_messages
    else
      # multiple dispensaries, use the send message to all
      MassTextService.send_message_to_all(dispensary_ids, message)
    end
  end

end