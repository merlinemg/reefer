class GenerateReeferStatisticsWorker
  include Sidekiq::Worker

  def perform(email, month, year, attachement_name)
    data = StatisticService.reefer_networks_statistics(month, year)
    csv = StatisticService.export_to_csv(data)
    StatisticsMailer.send_statistics(email, csv, attachement_name).deliver
  end

end
