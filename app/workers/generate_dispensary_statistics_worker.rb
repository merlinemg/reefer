class GenerateDispensaryStatisticsWorker
  include Sidekiq::Worker

  def perform(email, dispensary_id, month, attachement_name)
    data = StatisticService.dispensary_statistics(dispensary_id, month)
    csv = StatisticService.export_to_csv(data)
    StatisticsMailer.send_statistics(email, csv, attachement_name).deliver
  end

end
