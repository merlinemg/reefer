class MassTextService

  def initialize(params)
    @id = params[:dispensary_id]
    @message = params[:message]
    @errors = []
    validate
  end

  def errors
    @errors
  end

  def send_messages
    # return false if there was some errors with validations
    if @errors.any?
      false
    else
      # else set Dispensary and start proccessing if no errors
      find_dispensary_and_send
    end
  end

  def send_message_to_all(patient_ids)
    proccess_patients(patient_ids)
  end

  def self.send_message_to_all(dispensary_ids, message)
    # get uniq patients from those dispensaries
    patient_ids = Notification.all.where(dispensary_id: dispensary_ids).where(kind: "visit").pluck(:user_id).uniq
    service = self.new(message: message)
    service.send_message_to_all(patient_ids) 
  end

  private
  def find_dispensary_and_send
    # find Dispensary
    dispensary = Dispensary.includes(:patients).find_by(id: @id)
    if dispensary
      if dispensary.patients.any?
        # get only patients who have visited the dispensary
        patient_ids = Notification.all.where(dispensary_id: dispensary.id).where(kind: "visit").pluck(:user_id).uniq
        proccess_patients(patient_ids)
      else
        # return error if there is no patients for this Dispensary
        @errors << {key: :dispensary_id, full_message: "Dispensary with id: #{@id} don't have any patients!"}
        false
      end
    else
      # return error if we can't find Dispensary with provided ID
      @errors << {key: :dispensary_id, full_message: "Can't find dispensary with id: #{@id}!"}
      false
    end
  end

  def proccess_patients(patient_ids)
    # get all sidekiq scheduled jobs
    ss = Sidekiq::ScheduledSet.new
    # get all scheduled jobs from sms worker
    jobs = ss.select {|retri| retri.klass == 'SendSmsWorker' }
    # if there is any scheduled job, set new job to be performed at last job time + 1 second
    if jobs.any?
      last = jobs.last.at + 1.second
    else
      # if there is no scheduled job, set new job to be performed at now
      last = Time.now.utc
    end
    # loop through patients and set each job to be performed one second latter
    patient_ids.each_with_index do |id, index|
      SendSmsWorker.perform_at((last + (index).seconds), id, @message)
    end
  end

  # validate required fields
  def validate
    if @id.blank?
      @errors << {key: :dispensary_id, full_message: "Dispensary id can't be blank!"}
    end

    # validate message is not empty, so we not send empty sms
    if @message.blank?
      @errors << {key: :message, full_message: "Message can't be blank!"}

    # Max number of characters in one sms
    elsif @message.length > 140
      @errors << {key: :message, full_message: "Message maxlength is 140 characters and you have #{@message.length} characters!"}
    end
  end
end
