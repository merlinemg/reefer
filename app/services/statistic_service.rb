class StatisticService

  def self.reefer_networks_statistics(month, year)
    # define rows
    rows = []
    rows << ["Reefer network statistics for", year, month]

    # start month
    start_of_month = DateTime.new(year.to_i, month.to_i, 1)
    # end month
    end_of_month = start_of_month.end_of_month
    # moth range
    month_range = start_of_month..end_of_month

    # add row for month selection
    rows << ["Date range for selection", start_of_month, end_of_month]

    # set headers for next line
    rows << ["Reefer", "", "Definition", "Number calculations"]

    # Load dispensaries and users & include other tables to avoid n+1 queries
    dispensaries = Dispensary.all
    users = User.all

    # Total number of users
    total_number_of_users = users.count
    rows << ["Total number of users", total_number_of_users, "Total number of users on server" ,"User.all.count, no conditions, pure users count"]

    # Monthly active users
    monthly_active_users = Notification.all.where(kind: 'visit').where(created_at: month_range).pluck(:user_id).uniq
    total_number_or_users_checked_in_this_month = monthly_active_users.count
    rows << ["Total number of users checked in this month", total_number_or_users_checked_in_this_month, "Number of users that checked in this month", "Notifications (type: visit) created at month range, get uniq user id"]

    #Total active users
    # total_active_users = users.joins(:all_invitees).where(invitations: { created_at: month_range }).count
    total_active_users = Invitation.where(created_at: month_range).where(invitation_state: Invitation::ACCEPTED_STATUS).pluck(:inviter_id).uniq.count
    rows << ["Total active users", total_active_users, "Total number of unique users that have referred 1 person or more", "Accepted invitations this month, get uniq inviters count"]

    #Total number of referrals sign ups
    #total_number_of_referrals_sign_ups = users.joins(:all_inviters).where(invitations: { created_at: month_range }).count
    total_number_of_referrals_sign_ups = Notification.where(created_at: month_range).where(kind: "free_joint").where("upper_message ilike '%You accepted%'").pluck(:user_id).uniq.count
    rows << ["Total number of referrals sign ups", total_number_of_referrals_sign_ups, "Total number of users referred", "Notifications (type: free joint) as when the user sign ups via reefer this notification gets created"]

    #Get all users, which are converted referrals and visited a dispensary
    # converted_referrals = users.joins(:all_inviters, :notifications).where(invitations: { created_at: month_range }).where(notifications: { kind: "visit" }).distinct
    #Total number of converted referrals
    invitees = Invitation.where(created_at: month_range).where(invitation_state: Invitation::ACCEPTED_STATUS).pluck(:invitee_id).uniq
    invitees_visits = Notification.where(created_at: month_range).where(kind: "visit").where(user_id: invitees).pluck(:user_id).uniq
    total_number_of_converted_referrals = invitees_visits.count
    rows << ["Total number of converted referrals", total_number_of_converted_referrals, "Total number of referrals that checked in", "Get all invitees for this month #{invitees.count}, then get all notifications type visit for these invitees which is the result"]

    # Average friends per active user
    sum_of_friends_for_monthly_active_users = User.where(id: monthly_active_users).sum do |u|
      u.friend_ids.count
    end
    average_friends_per_active_user = sum_of_friends_for_monthly_active_users.to_f / monthly_active_users.count.to_f
    rows << ["Avg. number of friends per active user", average_friends_per_active_user.round(2), "Total number of friends per active user divided by total number of active users", "Friends sum for all monthly active users #{sum_of_friends_for_monthly_active_users} / monthly active users #{monthly_active_users.count}"]

    # Avg. checkins per converted referrals
    sum_of_checkins_per_converted_referrals = Notification.where(created_at: month_range).where(kind: "visit").where(user_id: invitees).count
    avg_checkings_per_converted_referrals = sum_of_checkins_per_converted_referrals.to_f / total_number_of_converted_referrals.to_f
    rows << ["Avg. checkins per converted referrals", avg_checkings_per_converted_referrals.round(2), "converted referral is someone who was added and then went into store", "#{sum_of_checkins_per_converted_referrals} / #{total_number_of_converted_referrals}"]

    # Total number of dispensaries
    total_number_of_dispensaries = dispensaries.count
    rows << ["Total number of dispensaries", total_number_of_dispensaries]

    # Active dispensaries per month
    active_dispensaries = Notification.all.where(kind: 'visit').where(created_at: month_range).pluck(:dispensary_id).uniq.count
    rows << ["Active dispensaries per month", active_dispensaries, "A dispensary that has at least 1 checkin within the month"]

    #Return merged data
    rows
  end

  def self.dispensary_statistics(dispensary_id, month)
    # start month
    start_of_month = DateTime.new(Date.today.year, month.to_i, 1)
    # end month
    end_of_month = start_of_month.end_of_month
    # moth range
    month_range = start_of_month..end_of_month

    # Find given dispensary
    dispensary = Dispensary.find dispensary_id

    #Get locale for claim reward and get
    #Dispensary Referral rewards
    claim_reward_locale = 'activerecord.models.notifications.claim_reward'
    referral_rewards = Notification.all.where(created_at: month_range).where(dispensary_id: dispensary.id).where(kind: "claim_reward").where(message: I18n.t(claim_reward_locale, reward: 'free joint', where: dispensary.name)).count

    #Total number of users checked in this month
    users_checked_in_this_month_ids = Notification.all.where(dispensary_id: dispensary.id).where(created_at: month_range).where(kind: "visit").pluck(:user_id).uniq
    users_checked_in_this_month = User.all.where(id: users_checked_in_this_month_ids)
    total_users_checked_in_this_month = users_checked_in_this_month.count.to_f
    #Total users signed up at this dispensary
    signed_up_users = dispensary.dispensary_patients.where("last_visit is not null").count

    #Total number of active users for this dispensary
    active_users = User.all.joins(:all_invitees, :dispensary_patients).where(dispensary_patients: { dispensary_id: dispensary.id }).where("dispensary_patients.last_visit is not null").where(invitations: { created_at: month_range })
    total_active_users = active_users.count.to_f

    #Get sum of all visits
    sum = 0
    users_checked_in_this_month.each { |f| sum += f.notifications.where(created_at: month_range).where(kind: 'visit').count }

    #Average checkins per users this month
    avg_checkins_per_users_this_month = sum.to_f / (total_users_checked_in_this_month == 0 ? 1 : total_users_checked_in_this_month)

    #Get sum of all visits
    sum = 0
    active_users.includes(:notifications).each { |f| sum += f.notifications.where(created_at: month_range).where(kind:'visit').count }

    #Average checkins per active users this month
    avg_checkins_per_active_users_this_month = sum.to_f / (total_active_users == 0 ? 1 : total_active_users)

    #Total number of checkins this month
    total_number_of_checkins_this_month = Notification.all.where(dispensary_id: dispensary.id).where(created_at: month_range).where(kind: "visit").count.to_f

    #Get all pending referrals
    total_pending_referrals = []
    #Iterate over all users
    User.all.includes(:dispensary_patients).each do |s|
      #Check if there are any users on the pending waiting list that are invited to given dispensary
      unless s.pending_invitees.blank? || s.dispensary_patients.order(last_visit: :desc).first.dispensary_id != dispensary.id
        total_pending_referrals << s.pending_invitees
      end
    end
    total_pending_referrals = total_pending_referrals.count.to_f

    #Get all ids of reffered users
    referred_and_signed_up_users_ids = Notification.where(kind: 'free_joint').where(message: "You've got a free gram at #{dispensary.name}").where("upper_message LIKE ?", "You accepted%").where(created_at: month_range).pluck(:user_id).uniq
    #count all users who visited a dispensary to get number of converted referrals
    converted_referral_visits = Notification.where(kind: "visit").where(user_id: referred_and_signed_up_users_ids).where(dispensary_id: dispensary.id).pluck(:user_id)
    total_converted_referrals = converted_referral_visits.uniq.count.to_f

    #Total referred people to this dispensary
    total_referrals_to_dispensary = total_pending_referrals + referred_and_signed_up_users_ids.count.to_f

    #Mean number of referrals per Active User
    total_referrals = total_pending_referrals + referred_and_signed_up_users_ids.count.to_f
    mean_referrals = total_referrals / (total_active_users == 0 ? 1 : total_active_users)

    #Average checkins per converted referral
    total_number_of_visits = converted_referral_visits.count.to_f
    average_checkins_per_converted_referral = total_number_of_visits / (total_converted_referrals == 0 ? 1 : total_converted_referrals)

    #Create a new array with dispensary statistics names
    rows = [["Referral Reward"], ["Total # of users checked in this month"],
      ["Total Users at this dispensary"], ["Total Active Users at this dispensary"],
      ["Average checkins per user this month"], ["Average checkins per active user this month"],
      ["Total number of checkins this month"], ["Total Referred people to this dispensary"],
      ["Total converted referrals to this dispensary"], ["Mean # of referrals per Active User"],
      ["Average checkins per converted referral"]]

    #Put all the data into an array
    data = [
      referral_rewards.round(2),
      total_users_checked_in_this_month.round(2),
      signed_up_users.round(2),
      total_active_users.round(2),
      avg_checkins_per_users_this_month.round(2),
      avg_checkins_per_active_users_this_month.round(2),
      total_number_of_checkins_this_month.round(2),
      total_referrals_to_dispensary.round(2),
      total_converted_referrals.round(2),
      mean_referrals.round(2),
      average_checkins_per_converted_referral.round(2)
    ]

    #Create new array with dispensary statistics descriptions
    descriptions = ["(ex. joint, gram, eighth), Reward given for referral",
      "number of users that have checked in to this dispensary","Anyone that has points at this dispensary",
      "Anyone who has referred a friend to this dispensary",
      "Average number of visits this month of everyone that has points at this dispensary",
      "Average number of visits this month of user who referred to dispensary",
      "All checkins at this dispensary this month",
      "All users who have been referred to this dispensary (Pending and checked in)",
      "All referred users that checked in at least once at this dispensary",
      "the average (mean) number of users referred per active user",
      "Average number of visits per referred user that checked in at least once"]

    #Merge header, data and description
    rows.length.times do |c|
      rows[c] << data[c] << descriptions[c]
    end

    #Add loyalty and referral rows
    rows.insert(2, ["Loyalty"])
    rows.insert(8, ["Referral"])

    #Define headers
    headers = [["Data Starting: (#{start_of_month.strftime("%m/%d/%y")})"],
      ["Current Date: (#{DateTime.now.strftime("%m/%d/%y")})"],
      ["Name:", dispensary.name, "Definition"], ["Dispensary ID #:", dispensary.id],
      [""]]

    #Reverse headers and iterate over them so they will be first
    headers.reverse.each do |h|
      rows.unshift(h)
    end

    #Return merged data
    rows
  end

  def self.all_dispensaries_statistics
    rows = Dispensary.all.map { |d| [d.id, d.name, d.created_at.strftime("%d %b %H:%M")] }
    rows.unshift(["Id", "Name", "Created at"])
  end

  def self.export_to_csv(data)
    require 'csv'
    CSV.generate(headers: false, col_sep: ";") do |csv|
      data.each do |row|
        csv << row
      end
      #Last row, to fix excel import
      csv << [""]
    end
  end

end
