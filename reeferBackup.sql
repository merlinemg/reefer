--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admins; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE admins (
    id integer NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.admins OWNER TO postgres;

--
-- Name: admins_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE admins_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admins_id_seq OWNER TO postgres;

--
-- Name: admins_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE admins_id_seq OWNED BY admins.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.ar_internal_metadata OWNER TO postgres;

--
-- Name: dispensaries; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dispensaries (
    id integer NOT NULL,
    name character varying,
    email character varying,
    phone_number character varying,
    address character varying,
    zip_code character varying,
    city character varying,
    state character varying,
    country character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    longitude numeric,
    latitude numeric,
    onboarding_step integer,
    onboarding_state integer DEFAULT 0,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    authentication_token character varying(30),
    magic_link character varying
);


ALTER TABLE public.dispensaries OWNER TO postgres;

--
-- Name: dispensaries_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dispensaries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dispensaries_id_seq OWNER TO postgres;

--
-- Name: dispensaries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dispensaries_id_seq OWNED BY dispensaries.id;


--
-- Name: dispensary_deals; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dispensary_deals (
    id integer NOT NULL,
    dispensary_id integer,
    points integer,
    name character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.dispensary_deals OWNER TO postgres;

--
-- Name: dispensary_deals_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dispensary_deals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dispensary_deals_id_seq OWNER TO postgres;

--
-- Name: dispensary_deals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dispensary_deals_id_seq OWNED BY dispensary_deals.id;


--
-- Name: dispensary_patients; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dispensary_patients (
    id integer NOT NULL,
    user_id integer,
    dispensary_id integer,
    points integer DEFAULT 0,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    gets_free_joint boolean DEFAULT false,
    points_cap integer DEFAULT 0,
    last_visit timestamp without time zone,
    free_joint_message character varying,
    reeferal_user_id integer
);


ALTER TABLE public.dispensary_patients OWNER TO postgres;

--
-- Name: dispensary_patients_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dispensary_patients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dispensary_patients_id_seq OWNER TO postgres;

--
-- Name: dispensary_patients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dispensary_patients_id_seq OWNED BY dispensary_patients.id;


--
-- Name: dispensary_redeemables; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dispensary_redeemables (
    id integer NOT NULL,
    dispensary_deal_id integer,
    dispensary_patient_id integer,
    redeemed_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.dispensary_redeemables OWNER TO postgres;

--
-- Name: dispensary_redeemables_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dispensary_redeemables_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dispensary_redeemables_id_seq OWNER TO postgres;

--
-- Name: dispensary_redeemables_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dispensary_redeemables_id_seq OWNED BY dispensary_redeemables.id;


--
-- Name: dispensary_referral_deals; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dispensary_referral_deals (
    id integer NOT NULL,
    points character varying,
    name character varying,
    dispensaries_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.dispensary_referral_deals OWNER TO postgres;

--
-- Name: dispensary_referral_deals_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dispensary_referral_deals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dispensary_referral_deals_id_seq OWNER TO postgres;

--
-- Name: dispensary_referral_deals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dispensary_referral_deals_id_seq OWNED BY dispensary_referral_deals.id;


--
-- Name: dispensary_text_logs; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dispensary_text_logs (
    id integer NOT NULL,
    dispensary_id integer,
    capaign_size integer,
    remaining_sms integer,
    successfull_send integer,
    from_phone_number character varying,
    send_date timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.dispensary_text_logs OWNER TO postgres;

--
-- Name: dispensary_text_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dispensary_text_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dispensary_text_logs_id_seq OWNER TO postgres;

--
-- Name: dispensary_text_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dispensary_text_logs_id_seq OWNED BY dispensary_text_logs.id;


--
-- Name: dispensary_texts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dispensary_texts (
    id integer NOT NULL,
    access boolean,
    dispensary_id integer,
    remaining_text integer,
    send_sms_count integer,
    twilio_phone_number character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.dispensary_texts OWNER TO postgres;

--
-- Name: dispensary_texts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dispensary_texts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dispensary_texts_id_seq OWNER TO postgres;

--
-- Name: dispensary_texts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dispensary_texts_id_seq OWNED BY dispensary_texts.id;


--
-- Name: faqs; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE faqs (
    id integer NOT NULL,
    title character varying,
    content text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.faqs OWNER TO postgres;

--
-- Name: faqs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE faqs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.faqs_id_seq OWNER TO postgres;

--
-- Name: faqs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE faqs_id_seq OWNED BY faqs.id;


--
-- Name: friendly_id_slugs; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE friendly_id_slugs (
    id integer NOT NULL,
    slug character varying NOT NULL,
    sluggable_id integer NOT NULL,
    sluggable_type character varying(50),
    scope character varying,
    created_at timestamp without time zone
);


ALTER TABLE public.friendly_id_slugs OWNER TO postgres;

--
-- Name: friendly_id_slugs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE friendly_id_slugs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.friendly_id_slugs_id_seq OWNER TO postgres;

--
-- Name: friendly_id_slugs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE friendly_id_slugs_id_seq OWNED BY friendly_id_slugs.id;


--
-- Name: invitations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE invitations (
    id integer NOT NULL,
    inviter_id integer,
    invitee_id integer,
    invitation_state integer DEFAULT 0,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    seen_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE public.invitations OWNER TO postgres;

--
-- Name: invitations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE invitations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.invitations_id_seq OWNER TO postgres;

--
-- Name: invitations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE invitations_id_seq OWNED BY invitations.id;


--
-- Name: notifications; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE notifications (
    id integer NOT NULL,
    user_id integer,
    dispensary_id integer,
    kind integer,
    message character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    upper_message character varying,
    seen_at timestamp without time zone,
    lower_message character varying
);


ALTER TABLE public.notifications OWNER TO postgres;

--
-- Name: notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notifications_id_seq OWNER TO postgres;

--
-- Name: notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE notifications_id_seq OWNED BY notifications.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO postgres;

--
-- Name: store_hours; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE store_hours (
    id integer NOT NULL,
    dispensary_id integer,
    from_time integer,
    to_time integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    sunday boolean,
    monday boolean,
    tuesday boolean,
    wednesday boolean,
    thursday boolean,
    friday boolean,
    saturday boolean
);


ALTER TABLE public.store_hours OWNER TO postgres;

--
-- Name: store_hours_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE store_hours_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.store_hours_id_seq OWNER TO postgres;

--
-- Name: store_hours_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE store_hours_id_seq OWNED BY store_hours.id;


--
-- Name: user_visitations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_visitations (
    id integer NOT NULL,
    user_id integer,
    dispensary_id integer,
    visited_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.user_visitations OWNER TO postgres;

--
-- Name: user_visitations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_visitations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_visitations_id_seq OWNER TO postgres;

--
-- Name: user_visitations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_visitations_id_seq OWNED BY user_visitations.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying,
    phone_number character varying,
    confirmation_code integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    sign_up_kind integer DEFAULT 0,
    magic_link character varying,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    onboarding_step integer,
    hex_code character varying,
    slug character varying,
    facebook_id character varying,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    auto_accept_fb_friends boolean DEFAULT true,
    avatar_file_name character varying,
    avatar_content_type character varying,
    avatar_file_size integer,
    avatar_updated_at timestamp without time zone,
    fb_access_token character varying,
    fb_expires_at timestamp without time zone,
    fb_picture character varying
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: visitor_logs; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE visitor_logs (
    id integer NOT NULL,
    ip character varying,
    visited_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.visitor_logs OWNER TO postgres;

--
-- Name: visitor_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE visitor_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.visitor_logs_id_seq OWNER TO postgres;

--
-- Name: visitor_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE visitor_logs_id_seq OWNED BY visitor_logs.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admins ALTER COLUMN id SET DEFAULT nextval('admins_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dispensaries ALTER COLUMN id SET DEFAULT nextval('dispensaries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dispensary_deals ALTER COLUMN id SET DEFAULT nextval('dispensary_deals_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dispensary_patients ALTER COLUMN id SET DEFAULT nextval('dispensary_patients_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dispensary_redeemables ALTER COLUMN id SET DEFAULT nextval('dispensary_redeemables_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dispensary_referral_deals ALTER COLUMN id SET DEFAULT nextval('dispensary_referral_deals_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dispensary_text_logs ALTER COLUMN id SET DEFAULT nextval('dispensary_text_logs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dispensary_texts ALTER COLUMN id SET DEFAULT nextval('dispensary_texts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faqs ALTER COLUMN id SET DEFAULT nextval('faqs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY friendly_id_slugs ALTER COLUMN id SET DEFAULT nextval('friendly_id_slugs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY invitations ALTER COLUMN id SET DEFAULT nextval('invitations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY notifications ALTER COLUMN id SET DEFAULT nextval('notifications_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY store_hours ALTER COLUMN id SET DEFAULT nextval('store_hours_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_visitations ALTER COLUMN id SET DEFAULT nextval('user_visitations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visitor_logs ALTER COLUMN id SET DEFAULT nextval('visitor_logs_id_seq'::regclass);


--
-- Data for Name: admins; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY admins (id, email, encrypted_password, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, created_at, updated_at) FROM stdin;
1	admin@reeferloyalty.com	$2a$11$QIOA84VZQ5yryvui7Fuprew6rfYIzh6fS1LbAs6sZsoNny40LNufW	\N	34	2017-06-16 08:42:56.024981	2017-06-16 06:57:17.231934	202.88.237.237	202.88.237.237	2017-05-14 08:01:06.672522	2017-06-16 08:42:56.026408
\.


--
-- Name: admins_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('admins_id_seq', 1, true);


--
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
environment	development	2017-05-14 08:00:52.598488	2017-05-14 08:00:52.598488
\.


--
-- Data for Name: dispensaries; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY dispensaries (id, name, email, phone_number, address, zip_code, city, state, country, created_at, updated_at, encrypted_password, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, longitude, latitude, onboarding_step, onboarding_state, reset_password_token, reset_password_sent_at, authentication_token, magic_link) FROM stdin;
10	Dispensary A	test_A@gmail.com	987 6543210	\N	\N	\N	\N	\N	2017-06-13 05:01:33.206637	2017-06-13 05:03:49.601946		\N	0	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	9PoxcU5K5Kz8m_3kERzc	6384343
2	dispensary1	test4@gmail.com	702 5957773	\N	\N	\N	\N	\N	2017-05-14 08:06:00.867211	2017-05-28 05:27:02.510547	$2a$11$YVDCblOrEsLSryNJSBsLvep.LbPsQgp61Y3lFIlMoyKUG.Vi1mnYa	\N	4	2017-05-17 15:44:50.853482	2017-05-17 15:41:04.873757	127.0.0.1	127.0.0.1	\N	\N	\N	1	\N	\N	xnror_G1izL2xVKE7FGlU	631dngd
11	NEW dispensary	test6@gmail.com	702 5957750	\N	\N	\N	\N	\N	2017-05-14 08:06:00.867211	2017-06-13 07:53:11.724392	$2a$11$YVDCblOrEsLSryNJSBsLvep.LbPsQgp61Y3lFIlMoyKUG.Vi1mnYa	\N	7	2017-06-13 07:53:11.722762	2017-06-13 07:45:52.463891	202.88.237.237	202.191.64.218	\N	\N	\N	1	\N	\N	xnror_G1izL2xVKE7FkkkGU	631dngd
13	Abhilash	abhilash@spericorn.com	702 5458577	\N	\N	\N	\N	\N	2017-06-13 08:06:25.643113	2017-06-13 08:34:47.8665	$2a$11$YVDCblOrEsLSryNJSBsLvep.LbPsQgp61Y3lFIlMoyKUG.Vi1mnYa	\N	6	2017-06-13 08:34:47.864749	2017-06-13 08:33:05.890937	202.191.64.218	202.191.64.218	\N	\N	\N	1	e641166df615b1fe573c2f53ae1d2e9038ddea0acbb1bcaa806a073f505c611c	2017-06-13 08:07:30.45234	HvG-zdRzo2WNC6WJybzy	6384l11
19	REFERRAL_Test	referral1@gmail.com	709 0569866	qqq	1111	qq	qqq	qq	2017-06-16 05:55:13.546112	2017-06-16 06:25:17.651903	$2a$11$8qIkr51WK.mAyLh149UblelLJy8Qwc.sm75PIkBbZpO5XHH3tWWEm	\N	2	2017-06-16 06:25:17.650532	2017-06-16 06:04:09.511129	202.88.237.237	202.88.237.237	-100.3891665	25.7578786	\N	2	\N	\N	abAxsd2WjYWVCVBPuQAp	638kn24
14	Dispensary_12	qwerty@123.com	569 8569855	test	1111	test	test	test	2017-06-13 08:59:30.245644	2017-06-13 09:25:35.071767	$2a$11$TBI7EAgu5Wf73MpVjsika.A02PDtThQfeDxarVKZpti4k1Vh6HcPm	\N	1	2017-06-13 09:00:14.618354	2017-06-13 09:00:14.618354	202.88.237.237	202.88.237.237	-122.296438	37.883542	\N	2	\N	\N	HAREuEpH1QeCTQ_XsqJY	638510b
1	Tester	test3@gmail.com	702 5957770	\N	\N	\N	\N	\N	2017-05-14 08:06:00.867211	2017-06-15 03:45:53.372577	$2a$11$YVDCblOrEsLSryNJSBsLvep.LbPsQgp61Y3lFIlMoyKUG.Vi1mnYa	\N	20	2017-06-15 03:45:53.3702	2017-06-13 08:54:20.746222	202.191.64.218	202.88.237.237	\N	\N	\N	1	\N	\N	xnror_G1izL2xVKE7FGU	631dngd
16	Test_Dispensary	new@gmail.com	707 0707070	Test	12345	Test	Test	Test	2017-06-13 10:41:24.132202	2017-06-16 08:45:53.812969	$2a$11$bz9q8Cnr/lPQ05BJRj0U6OnsnV88ZQnWwjuxurLLGMsZjEhzas7gG	\N	19	2017-06-16 08:45:53.811535	2017-06-16 06:15:00.900685	202.88.237.237	202.88.237.237	-122.5349461	45.3096498	\N	2	\N	\N	HYjqRz-bu63yMZBoxbMN	6385amj
20	R_Test	r@gmail.com	984 6125698	test	1020	test	test	test	2017-06-16 06:43:39.426769	2017-06-16 09:06:57.925752	$2a$11$0teK/Y7zx8h8xr8Y2jJmuO6qMnnPlEfxwDei.CnUHw1dKZn1AX/aO	\N	2	2017-06-16 09:06:57.924315	2017-06-16 06:47:56.107748	202.88.237.237	202.88.237.237	-102.371806	31.854275	\N	2	\N	\N	ia-EskppFfddqDLoHX_L	638l2kd
18	TEST	test5@gmail.com	702 6789556	\N	\N	\N	\N	\N	2017-06-13 07:57:37.22851	2017-06-16 03:29:38.072328	$2a$11$YVDCblOrEsLSryNJSBsLvep.LbPsQgp61Y3lFIlMoyKUG.Vi1mnYa	\N	18	2017-06-16 03:29:38.070723	2017-06-14 07:15:41.945026	202.191.64.218	202.191.64.218	\N	\N	\N	1	\N	\N	z1iTidyAohdxjaEF8hq-	6384k2d
15	Reefer_Test	Reefer@gmail.com	666 6666666	test	12345	test	test	test	2017-06-13 09:26:25.014599	2017-06-16 04:11:02.901555	$2a$11$wjrPXkAs85gTOjNiLVa26.FgIgUI8.mcmueubnMjCCzTOX1IWtYVa	\N	8	2017-06-16 04:11:02.900022	2017-06-15 03:46:18.89527	202.88.237.237	202.191.64.218	-122.5349461	45.3096498	\N	2	\N	\N	zhMBGaEnDx9uFNNs38CC	63853em
17	Test	qqq@rrr.com	222 2222222	test	1020	test	test	test	2017-06-16 05:25:40.90626	2017-06-16 05:52:55.811528	$2a$11$HBCY55ngk7hvZrsMCHeEGOohdjvSdB96JQQtYerHfFMDAy5is2wyS	\N	3	2017-06-16 05:45:27.943149	2017-06-16 05:35:47.564096	202.88.237.237	202.88.237.237	-102.371806	31.854275	\N	2	\N	\N	sqcJ2aedZvkdaydhpc1Z	638kk54
\.


--
-- Name: dispensaries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('dispensaries_id_seq', 20, true);


--
-- Data for Name: dispensary_deals; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY dispensary_deals (id, dispensary_id, points, name, created_at, updated_at) FROM stdin;
1	1	30	Free Joint	2017-01-12 14:23:09.386131	2017-01-12 14:23:09.386131
2	1	90	Free Gram	2017-01-12 14:23:09.390017	2017-01-12 14:23:09.390017
3	1	150	Free Eighth	2017-01-12 14:23:09.393178	2017-01-12 14:23:09.393178
4	2	30	Free Joint	2017-01-13 10:32:02.442783	2017-01-13 10:32:02.442783
5	2	90	Free Gram	2017-01-13 10:32:02.446199	2017-01-13 10:32:02.446199
6	2	150	Free Eighth	2017-01-13 10:32:02.449525	2017-01-13 10:32:02.449525
7	3	30	Free Joint	2017-01-17 19:34:26.895071	2017-01-17 19:34:26.895071
8	3	90	Free Gram	2017-01-17 19:34:26.897228	2017-01-17 19:34:26.897228
9	3	150	Free Eighth	2017-01-17 19:34:26.899231	2017-01-17 19:34:26.899231
10	4	30	Free Joint	2017-01-17 20:08:32.574928	2017-01-17 20:08:32.574928
11	4	90	Free Gram	2017-01-17 20:08:32.577324	2017-01-17 20:08:32.577324
12	4	150	Free Eighth	2017-01-17 20:08:32.579422	2017-01-17 20:08:32.579422
16	6	30	Free Joint	2017-01-18 00:57:26.1567	2017-01-18 00:57:26.1567
17	6	90	Free Gram	2017-01-18 00:57:26.16644	2017-01-18 00:57:26.16644
18	6	150	Free Eighth	2017-01-18 00:57:26.170005	2017-01-18 00:57:26.170005
19	7	30	Free Joint	2017-01-18 10:05:49.54552	2017-01-18 10:05:49.54552
20	7	90	Free Gram	2017-01-18 10:05:49.549309	2017-01-18 10:05:49.549309
21	7	150	Free Eighth	2017-01-18 10:05:49.552628	2017-01-18 10:05:49.552628
22	8	30	Free Joint	2017-01-24 10:27:33.724308	2017-01-24 10:27:33.724308
23	8	90	Free Gram	2017-01-24 10:27:33.727556	2017-01-24 10:27:33.727556
24	8	150	Free Eighth	2017-01-24 10:27:33.729976	2017-01-24 10:27:33.729976
26	9	90	Free Gram	2017-03-23 00:20:50.13701	2017-03-23 00:20:50.13701
27	9	150	Free Eighth	2017-03-23 00:20:50.146744	2017-03-23 00:20:50.146744
25	9	50	Free Joint	2017-03-23 00:20:50.125219	2017-03-23 19:58:19.795425
15	5	1	Free Eighth	2017-01-18 00:41:28.386231	2017-03-23 21:16:44.930202
13	5	31	Free edible	2017-01-18 00:41:28.382056	2017-03-23 21:17:26.271711
14	5	26	Free Joint	2017-01-18 00:41:28.384191	2017-03-23 21:17:33.123795
28	1	250	Free Fixed	2017-01-12 14:23:09.393178	2017-04-13 09:52:09.933433
35	10	30	Free Joint	2017-04-19 05:24:22.856421	2017-04-19 05:24:22.856421
36	10	90	Free Gram	2017-04-19 05:24:22.906492	2017-04-19 05:24:22.906492
37	10	150	Free Eighth	2017-04-19 05:24:22.908166	2017-04-19 05:24:22.908166
34	15	150	Free Eighth	2017-04-15 04:36:26.791435	2017-04-22 04:59:46.892627
38	18	30	Free Joint	2017-04-24 06:35:46.210604	2017-04-24 06:35:46.210604
39	18	90	Free Gram	2017-04-24 06:35:46.242632	2017-04-24 06:35:46.242632
40	18	150	Free Eighth	2017-04-24 06:35:46.244135	2017-04-24 06:35:46.244135
32	15	89	Free edible	2017-04-15 04:36:26.787729	2017-04-27 04:55:21.608026
33	15	15	Free Gram	2017-04-15 04:36:26.78986	2017-05-01 20:35:52.362217
41	12	30	Free Joint	2017-06-13 08:17:27.727299	2017-06-13 08:17:27.727299
42	12	90	Free Gram	2017-06-13 08:17:27.757164	2017-06-13 08:17:27.757164
43	12	150	Free Eighth	2017-06-13 08:17:27.768707	2017-06-13 08:17:27.768707
44	13	30	Free Joint	2017-06-13 08:21:00.552645	2017-06-13 08:21:00.552645
45	13	90	Free Gram	2017-06-13 08:21:00.559109	2017-06-13 08:21:00.559109
46	13	150	Free Eighth	2017-06-13 08:21:00.57014	2017-06-13 08:21:00.57014
47	14	30	Free Joint	2017-06-13 08:59:30.253393	2017-06-13 08:59:30.253393
48	14	90	Free Gram	2017-06-13 08:59:30.255144	2017-06-13 08:59:30.255144
49	14	150	Free Eighth	2017-06-13 08:59:30.256724	2017-06-13 08:59:30.256724
50	16	30	Free Joint	2017-06-13 10:41:24.157928	2017-06-13 10:41:24.157928
51	16	90	Free Gram	2017-06-13 10:41:24.160211	2017-06-13 10:41:24.160211
52	16	150	Free Eighth	2017-06-13 10:41:24.162543	2017-06-13 10:41:24.162543
53	17	30	Free Joint	2017-06-16 05:25:40.928765	2017-06-16 05:25:40.928765
54	17	90	Free Gram	2017-06-16 05:25:40.931373	2017-06-16 05:25:40.931373
55	17	150	Free Eighth	2017-06-16 05:25:40.93356	2017-06-16 05:25:40.93356
56	19	30	Free Joint	2017-06-16 05:55:13.55444	2017-06-16 05:55:13.55444
57	19	90	Free Gram	2017-06-16 05:55:13.556303	2017-06-16 05:55:13.556303
58	19	150	Free Eighth	2017-06-16 05:55:13.558327	2017-06-16 05:55:13.558327
59	20	30	Free Joint	2017-06-16 06:43:39.437372	2017-06-16 06:43:39.437372
60	20	90	Free Gram	2017-06-16 06:43:39.439772	2017-06-16 06:43:39.439772
61	20	150	Free Eighth	2017-06-16 06:43:39.442182	2017-06-16 06:43:39.442182
\.


--
-- Name: dispensary_deals_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('dispensary_deals_id_seq', 61, true);


--
-- Data for Name: dispensary_patients; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY dispensary_patients (id, user_id, dispensary_id, points, created_at, updated_at, gets_free_joint, points_cap, last_visit, free_joint_message, reeferal_user_id) FROM stdin;
185	158	1	0	2017-05-17 15:51:46.186954	2017-05-17 15:51:46.186954	t	0	\N	You accepted rest reeferal!	2
1	2	1	10	2017-05-14 08:52:53.723012	2017-05-27 15:26:52.705445	t	0	2017-05-14 08:52:53.723012	You successfully reefered zxczxc to Tester. Nice work	\N
186	172	1	0	2017-05-27 15:26:52.826997	2017-05-27 15:26:52.826997	t	0	\N	You accepted rest reeferal!	2
187	2	13	10	2017-06-13 08:46:03.772832	2017-06-13 08:46:03.83198	f	0	2017-06-13 08:46:03.825704	\N	\N
188	181	13	10	2017-06-13 09:01:45.168086	2017-06-13 09:01:45.168086	f	0	2017-06-13 09:01:45.13705	\N	\N
224	217	20	10	2017-06-16 07:02:18.818933	2017-06-16 08:16:00.578515	f	0	2017-06-16 07:02:18.799604	You successfully reefered RefB to R_Test. Nice work	\N
190	182	14	10	2017-06-13 09:06:14.499303	2017-06-13 09:06:14.499303	f	0	2017-06-13 09:06:14.486985	\N	\N
191	183	14	10	2017-06-13 09:09:26.829389	2017-06-13 09:09:26.829389	f	0	2017-06-13 09:09:26.811054	\N	\N
189	181	14	30	2017-06-13 09:03:21.49676	2017-06-13 09:10:08.944522	f	0	2017-06-13 09:03:21.510955	\N	\N
192	184	14	10	2017-06-13 09:13:51.669204	2017-06-13 09:13:51.669204	f	0	2017-06-13 09:13:51.648436	\N	\N
193	185	14	10	2017-06-13 09:22:03.314219	2017-06-13 09:22:03.314219	f	0	2017-06-13 09:22:03.289665	\N	\N
194	2	15	10	2017-06-13 09:33:23.117101	2017-06-13 09:33:23.139529	f	0	2017-06-13 09:33:23.130532	\N	\N
195	186	15	10	2017-06-13 09:34:54.44596	2017-06-13 09:34:54.44596	f	0	2017-06-13 09:34:54.424873	\N	\N
196	187	15	10	2017-06-13 09:37:25.743676	2017-06-13 09:37:25.743676	f	0	2017-06-13 09:37:25.723031	\N	\N
197	188	15	10	2017-06-13 09:43:21.176252	2017-06-13 09:43:21.176252	f	0	2017-06-13 09:43:21.085295	\N	\N
198	189	15	10	2017-06-13 10:13:50.142578	2017-06-13 10:13:50.142578	f	0	2017-06-13 10:13:50.092753	\N	\N
199	176	15	10	2017-06-13 10:28:10.889037	2017-06-13 10:28:10.911522	f	0	2017-06-13 10:28:10.905892	\N	\N
200	190	15	10	2017-06-13 10:29:39.538164	2017-06-13 10:29:39.538164	f	0	2017-06-13 10:29:39.522239	\N	\N
201	191	15	10	2017-06-13 10:35:28.521561	2017-06-13 10:35:28.521561	f	0	2017-06-13 10:35:28.498729	\N	\N
202	192	15	10	2017-06-13 10:37:55.357143	2017-06-13 10:37:55.357143	f	0	2017-06-13 10:37:55.342693	\N	\N
204	186	16	10	2017-06-13 12:10:14.473244	2017-06-13 12:10:14.496766	f	0	2017-06-13 12:10:14.490454	\N	\N
205	193	16	10	2017-06-13 12:12:09.865525	2017-06-13 12:12:09.865525	f	0	2017-06-13 12:12:09.796707	\N	\N
206	194	1	10	2017-06-15 17:37:19.491399	2017-06-15 17:37:19.491399	f	0	2017-06-15 17:37:19.463219	\N	\N
207	195	1	10	2017-06-15 17:39:56.781983	2017-06-15 17:39:56.781983	f	0	2017-06-15 17:39:56.72349	\N	\N
208	196	1	10	2017-06-15 17:44:54.897483	2017-06-15 17:44:54.897483	f	0	2017-06-15 17:44:54.879833	\N	\N
209	197	16	10	2017-06-16 04:13:15.491413	2017-06-16 04:13:15.491413	f	0	2017-06-16 04:13:15.471039	\N	\N
210	198	16	10	2017-06-16 04:14:04.612502	2017-06-16 04:14:04.612502	f	0	2017-06-16 04:14:04.59565	\N	\N
211	201	16	10	2017-06-16 04:41:57.571125	2017-06-16 04:41:57.571125	f	0	2017-06-16 04:41:57.552302	\N	\N
212	202	16	10	2017-06-16 04:42:55.081636	2017-06-16 04:45:36.623078	t	0	2017-06-16 04:42:55.067877	You successfully reefered zxczczxc to Test_Dispensary. Nice work	\N
213	203	16	10	2017-06-16 04:44:51.845874	2017-06-16 04:45:38.752114	f	0	2017-06-16 04:45:36.503328	You accepted sadfsad reeferal!	\N
214	206	16	10	2017-06-16 04:55:24.805722	2017-06-16 04:55:24.805722	f	0	2017-06-16 04:55:24.792295	\N	\N
203	2	16	20	2017-06-13 11:58:21.773142	2017-06-16 05:00:16.116158	f	0	2017-06-16 05:00:16.101903	\N	\N
215	207	16	10	2017-06-16 05:13:40.215554	2017-06-16 05:13:40.215554	f	0	2017-06-16 05:13:40.192423	\N	\N
216	2	17	10	2017-06-16 05:46:09.413316	2017-06-16 05:46:09.438263	f	0	2017-06-16 05:46:09.433709	\N	\N
227	220	20	10	2017-06-16 08:29:48.188183	2017-06-16 08:31:38.629137	f	0	2017-06-16 08:31:36.453552	You accepted RefB reeferal!	\N
218	209	19	10	2017-06-16 05:57:28.935391	2017-06-16 05:57:28.935391	f	0	2017-06-16 05:57:28.920059	\N	\N
217	194	16	20	2017-06-16 05:51:18.183966	2017-06-16 06:05:03.692991	f	0	2017-06-16 05:51:18.197154	\N	\N
219	186	19	10	2017-06-16 06:14:36.311704	2017-06-16 06:14:36.335964	f	0	2017-06-16 06:14:36.326557	\N	\N
220	210	19	10	2017-06-16 06:22:16.585338	2017-06-16 06:22:16.605048	f	0	2017-06-16 06:22:16.60045	\N	\N
221	212	19	10	2017-06-16 06:25:45.616984	2017-06-16 06:29:38.26084	f	0	2017-06-16 06:25:45.601128	You successfully reefered sweet to REFERRAL_Test. Nice work	\N
222	213	19	0	2017-06-16 06:29:38.299935	2017-06-16 06:29:38.299935	t	0	\N	You accepted tester reeferal!	212
223	215	20	10	2017-06-16 06:48:45.956026	2017-06-16 06:48:45.956026	f	0	2017-06-16 06:48:45.939902	\N	\N
226	219	20	10	2017-06-16 08:06:49.257284	2017-06-16 08:33:09.8937	f	0	2017-06-16 08:08:31.568734	You successfully reefered RefC to R_Test. Nice work	\N
225	218	20	10	2017-06-16 07:07:58.882252	2017-06-16 07:12:43.150238	f	0	2017-06-16 07:12:40.886682	You accepted PatB reeferal!	\N
\.


--
-- Name: dispensary_patients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('dispensary_patients_id_seq', 227, true);


--
-- Data for Name: dispensary_redeemables; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY dispensary_redeemables (id, dispensary_deal_id, dispensary_patient_id, redeemed_at, created_at, updated_at) FROM stdin;
1	32	2	2017-01-17 10:36:05.988626	2017-01-17 10:36:05.988536	2017-01-17 10:36:05.988536
2	33	23	2017-02-06 18:42:08.426429	2017-02-06 18:42:08.426205	2017-02-06 18:42:08.426205
3	34	25	2017-03-23 21:18:47.820599	2017-03-23 21:18:47.820475	2017-03-23 21:18:47.820475
4	32	88	2017-04-21 07:06:11.774488	2017-04-21 07:06:11.774334	2017-04-21 07:06:11.774334
5	32	92	2017-04-21 07:07:43.465781	2017-04-21 07:07:43.465607	2017-04-21 07:07:43.465607
6	32	94	2017-04-24 04:28:32.064985	2017-04-24 04:28:32.06454	2017-04-24 04:28:32.06454
7	32	95	2017-04-24 04:30:15.263051	2017-04-24 04:30:15.262936	2017-04-24 04:30:15.262936
8	32	88	2017-04-24 04:30:16.177246	2017-04-24 04:30:16.177166	2017-04-24 04:30:16.177166
9	32	95	2017-04-24 05:25:44.748411	2017-04-24 05:25:44.748281	2017-04-24 05:25:44.748281
10	32	96	2017-04-24 05:28:56.526432	2017-04-24 05:28:56.526243	2017-04-24 05:28:56.526243
11	32	96	2017-04-24 05:30:39.826169	2017-04-24 05:30:39.826058	2017-04-24 05:30:39.826058
12	33	96	2017-04-24 05:31:52.036223	2017-04-24 05:31:52.03615	2017-04-24 05:31:52.03615
13	34	96	2017-04-24 05:37:27.540894	2017-04-24 05:37:27.540794	2017-04-24 05:37:27.540794
14	33	96	2017-04-24 05:38:21.851737	2017-04-24 05:38:21.851636	2017-04-24 05:38:21.851636
15	32	96	2017-04-24 05:39:01.238281	2017-04-24 05:39:01.238171	2017-04-24 05:39:01.238171
16	32	95	2017-04-24 05:44:59.98336	2017-04-24 05:44:59.983234	2017-04-24 05:44:59.983234
17	34	96	2017-04-24 05:48:08.743152	2017-04-24 05:48:08.743023	2017-04-24 05:48:08.743023
18	34	96	2017-04-24 05:49:01.755137	2017-04-24 05:49:01.755058	2017-04-24 05:49:01.755058
19	32	96	2017-04-24 05:49:36.406911	2017-04-24 05:49:36.406738	2017-04-24 05:49:36.406738
20	32	100	2017-04-24 06:09:51.699803	2017-04-24 06:09:51.699725	2017-04-24 06:09:51.699725
21	33	96	2017-04-24 06:51:38.904994	2017-04-24 06:51:38.904895	2017-04-24 06:51:38.904895
22	32	96	2017-04-24 06:52:12.207095	2017-04-24 06:52:12.206933	2017-04-24 06:52:12.206933
23	34	95	2017-04-24 06:52:45.422564	2017-04-24 06:52:45.422312	2017-04-24 06:52:45.422312
24	32	95	2017-04-24 06:53:11.758087	2017-04-24 06:53:11.757901	2017-04-24 06:53:11.757901
25	34	95	2017-04-24 06:53:39.174061	2017-04-24 06:53:39.173949	2017-04-24 06:53:39.173949
26	33	95	2017-04-24 06:54:14.169123	2017-04-24 06:54:14.169043	2017-04-24 06:54:14.169043
27	33	95	2017-04-24 06:54:33.273359	2017-04-24 06:54:33.27324	2017-04-24 06:54:33.27324
28	33	95	2017-04-24 06:55:12.049794	2017-04-24 06:55:12.049618	2017-04-24 06:55:12.049618
29	33	95	2017-04-24 06:55:36.215118	2017-04-24 06:55:36.214944	2017-04-24 06:55:36.214944
30	34	98	2017-04-24 06:57:10.171891	2017-04-24 06:57:10.171785	2017-04-24 06:57:10.171785
31	32	101	2017-04-24 06:57:35.632467	2017-04-24 06:57:35.632355	2017-04-24 06:57:35.632355
32	33	101	2017-04-24 06:58:08.586447	2017-04-24 06:58:08.586306	2017-04-24 06:58:08.586306
33	32	101	2017-04-24 06:58:27.984244	2017-04-24 06:58:27.984148	2017-04-24 06:58:27.984148
34	33	88	2017-04-24 07:00:37.727851	2017-04-24 07:00:37.727727	2017-04-24 07:00:37.727727
35	32	108	2017-04-24 07:24:06.487194	2017-04-24 07:24:06.487059	2017-04-24 07:24:06.487059
36	32	109	2017-04-24 07:29:26.788748	2017-04-24 07:29:26.788651	2017-04-24 07:29:26.788651
37	32	109	2017-04-24 07:31:29.33244	2017-04-24 07:31:29.332364	2017-04-24 07:31:29.332364
38	32	108	2017-04-24 07:32:14.990692	2017-04-24 07:32:14.990584	2017-04-24 07:32:14.990584
39	32	110	2017-04-24 07:32:49.428942	2017-04-24 07:32:49.428844	2017-04-24 07:32:49.428844
40	34	106	2017-04-24 07:33:59.170316	2017-04-24 07:33:59.17019	2017-04-24 07:33:59.17019
41	32	111	2017-04-24 07:38:01.205325	2017-04-24 07:38:01.205225	2017-04-24 07:38:01.205225
42	32	112	2017-04-24 07:38:27.938968	2017-04-24 07:38:27.938875	2017-04-24 07:38:27.938875
43	32	114	2017-04-24 07:40:24.08501	2017-04-24 07:40:24.084894	2017-04-24 07:40:24.084894
44	32	111	2017-04-24 07:42:17.376985	2017-04-24 07:42:17.376886	2017-04-24 07:42:17.376886
45	32	115	2017-04-24 07:42:51.014751	2017-04-24 07:42:51.014674	2017-04-24 07:42:51.014674
46	32	110	2017-04-24 08:24:46.634723	2017-04-24 08:24:46.634619	2017-04-24 08:24:46.634619
47	32	118	2017-04-24 08:26:00.369399	2017-04-24 08:26:00.369295	2017-04-24 08:26:00.369295
48	32	132	2017-04-24 08:26:55.5798	2017-04-24 08:26:55.579586	2017-04-24 08:26:55.579586
49	32	132	2017-04-24 08:27:08.600384	2017-04-24 08:27:08.600297	2017-04-24 08:27:08.600297
50	33	88	2017-04-24 08:36:30.513123	2017-04-24 08:36:30.513024	2017-04-24 08:36:30.513024
51	32	95	2017-04-24 10:02:56.543283	2017-04-24 10:02:56.543172	2017-04-24 10:02:56.543172
52	32	95	2017-04-24 10:19:52.460519	2017-04-24 10:19:52.460432	2017-04-24 10:19:52.460432
53	32	133	2017-04-24 10:43:23.312308	2017-04-24 10:43:23.312193	2017-04-24 10:43:23.312193
54	32	134	2017-04-24 10:45:15.718934	2017-04-24 10:45:15.718798	2017-04-24 10:45:15.718798
55	32	135	2017-04-24 10:46:20.219131	2017-04-24 10:46:20.218999	2017-04-24 10:46:20.218999
56	32	136	2017-04-24 11:04:01.594192	2017-04-24 11:04:01.594063	2017-04-24 11:04:01.594063
57	32	137	2017-04-24 11:08:17.994091	2017-04-24 11:08:17.993983	2017-04-24 11:08:17.993983
58	32	116	2017-04-24 11:08:36.576994	2017-04-24 11:08:36.576829	2017-04-24 11:08:36.576829
59	32	137	2017-04-24 11:10:18.036272	2017-04-24 11:10:18.036115	2017-04-24 11:10:18.036115
60	32	137	2017-04-24 11:11:05.803836	2017-04-24 11:11:05.803692	2017-04-24 11:11:05.803692
61	32	137	2017-04-24 11:11:43.587657	2017-04-24 11:11:43.587567	2017-04-24 11:11:43.587567
62	32	138	2017-04-24 11:13:53.844681	2017-04-24 11:13:53.844588	2017-04-24 11:13:53.844588
63	32	138	2017-04-24 11:14:12.621673	2017-04-24 11:14:12.621594	2017-04-24 11:14:12.621594
64	32	139	2017-04-24 11:14:38.21884	2017-04-24 11:14:38.218766	2017-04-24 11:14:38.218766
65	32	139	2017-04-24 11:15:09.815836	2017-04-24 11:15:09.815764	2017-04-24 11:15:09.815764
66	32	140	2017-04-24 11:15:44.864228	2017-04-24 11:15:44.864156	2017-04-24 11:15:44.864156
67	32	140	2017-04-24 11:16:20.693222	2017-04-24 11:16:20.69311	2017-04-24 11:16:20.69311
68	32	141	2017-04-24 11:17:03.150174	2017-04-24 11:17:03.150097	2017-04-24 11:17:03.150097
69	32	141	2017-04-24 11:17:38.609507	2017-04-24 11:17:38.609435	2017-04-24 11:17:38.609435
70	32	142	2017-04-24 11:18:32.542235	2017-04-24 11:18:32.542117	2017-04-24 11:18:32.542117
71	32	142	2017-04-24 11:19:12.084532	2017-04-24 11:19:12.084432	2017-04-24 11:19:12.084432
72	32	143	2017-04-24 11:19:54.416716	2017-04-24 11:19:54.416625	2017-04-24 11:19:54.416625
73	32	143	2017-04-24 11:20:08.36335	2017-04-24 11:20:08.363207	2017-04-24 11:20:08.363207
74	32	144	2017-04-24 11:20:43.97966	2017-04-24 11:20:43.979587	2017-04-24 11:20:43.979587
75	32	144	2017-04-24 11:21:12.725483	2017-04-24 11:21:12.725308	2017-04-24 11:21:12.725308
76	32	145	2017-04-24 11:21:32.784639	2017-04-24 11:21:32.78456	2017-04-24 11:21:32.78456
77	32	145	2017-04-24 11:22:00.897209	2017-04-24 11:22:00.897113	2017-04-24 11:22:00.897113
78	32	146	2017-04-24 11:22:17.864559	2017-04-24 11:22:17.864464	2017-04-24 11:22:17.864464
79	32	146	2017-04-24 11:22:34.210763	2017-04-24 11:22:34.210638	2017-04-24 11:22:34.210638
80	32	147	2017-04-24 11:23:12.647278	2017-04-24 11:23:12.647133	2017-04-24 11:23:12.647133
81	32	147	2017-04-24 11:23:40.951484	2017-04-24 11:23:40.951387	2017-04-24 11:23:40.951387
82	32	148	2017-04-24 11:24:16.074678	2017-04-24 11:24:16.074574	2017-04-24 11:24:16.074574
83	32	148	2017-04-24 11:24:43.113023	2017-04-24 11:24:43.112929	2017-04-24 11:24:43.112929
84	32	149	2017-04-24 11:25:17.088583	2017-04-24 11:25:17.088488	2017-04-24 11:25:17.088488
85	32	149	2017-04-24 11:25:47.137282	2017-04-24 11:25:47.137202	2017-04-24 11:25:47.137202
86	32	150	2017-04-24 11:26:16.61866	2017-04-24 11:26:16.618561	2017-04-24 11:26:16.618561
87	32	150	2017-04-24 11:26:46.426458	2017-04-24 11:26:46.426293	2017-04-24 11:26:46.426293
88	32	151	2017-04-24 11:27:19.643825	2017-04-24 11:27:19.643701	2017-04-24 11:27:19.643701
89	32	151	2017-04-24 11:27:48.42033	2017-04-24 11:27:48.420211	2017-04-24 11:27:48.420211
90	32	152	2017-04-24 11:28:24.447089	2017-04-24 11:28:24.446906	2017-04-24 11:28:24.446906
91	32	152	2017-04-24 11:28:59.042228	2017-04-24 11:28:59.042155	2017-04-24 11:28:59.042155
92	32	153	2017-04-24 11:29:56.816507	2017-04-24 11:29:56.816378	2017-04-24 11:29:56.816378
93	32	153	2017-04-24 11:30:27.969415	2017-04-24 11:30:27.969343	2017-04-24 11:30:27.969343
94	32	154	2017-04-24 11:30:59.493232	2017-04-24 11:30:59.493127	2017-04-24 11:30:59.493127
95	32	155	2017-04-24 11:31:22.616904	2017-04-24 11:31:22.616811	2017-04-24 11:31:22.616811
96	32	156	2017-04-24 12:06:23.53166	2017-04-24 12:06:23.531541	2017-04-24 12:06:23.531541
97	33	95	2017-04-24 12:13:40.589027	2017-04-24 12:13:40.588831	2017-04-24 12:13:40.588831
98	32	95	2017-04-24 12:14:21.278762	2017-04-24 12:14:21.278606	2017-04-24 12:14:21.278606
99	32	88	2017-04-24 18:29:37.365753	2017-04-24 18:29:37.36562	2017-04-24 18:29:37.36562
100	33	95	2017-04-25 04:03:09.474678	2017-04-25 04:03:09.474536	2017-04-25 04:03:09.474536
101	33	95	2017-04-25 04:25:45.112688	2017-04-25 04:25:45.112545	2017-04-25 04:25:45.112545
102	32	141	2017-04-25 04:31:37.574892	2017-04-25 04:31:37.574769	2017-04-25 04:31:37.574769
103	32	162	2017-04-25 04:35:10.687649	2017-04-25 04:35:10.687573	2017-04-25 04:35:10.687573
104	32	96	2017-04-25 04:39:00.062686	2017-04-25 04:39:00.062595	2017-04-25 04:39:00.062595
105	32	96	2017-04-25 04:39:24.712955	2017-04-25 04:39:24.712878	2017-04-25 04:39:24.712878
106	32	96	2017-04-25 04:39:47.889303	2017-04-25 04:39:47.889198	2017-04-25 04:39:47.889198
107	32	96	2017-04-25 04:40:14.057424	2017-04-25 04:40:14.057321	2017-04-25 04:40:14.057321
108	32	162	2017-04-25 04:57:51.745341	2017-04-25 04:57:51.745226	2017-04-25 04:57:51.745226
109	32	95	2017-04-25 05:01:44.376934	2017-04-25 05:01:44.37683	2017-04-25 05:01:44.37683
110	32	95	2017-04-25 05:02:31.26625	2017-04-25 05:02:31.266124	2017-04-25 05:02:31.266124
111	32	95	2017-04-25 05:03:29.129844	2017-04-25 05:03:29.129695	2017-04-25 05:03:29.129695
112	32	164	2017-04-25 05:16:51.357473	2017-04-25 05:16:51.357357	2017-04-25 05:16:51.357357
113	32	163	2017-04-25 05:18:01.301699	2017-04-25 05:18:01.301543	2017-04-25 05:18:01.301543
114	32	165	2017-04-25 05:18:12.391447	2017-04-25 05:18:12.391365	2017-04-25 05:18:12.391365
115	33	162	2017-04-25 05:27:52.167954	2017-04-25 05:27:52.167827	2017-04-25 05:27:52.167827
116	33	162	2017-04-25 06:26:54.904857	2017-04-25 06:26:54.904662	2017-04-25 06:26:54.904662
117	32	167	2017-04-25 06:29:39.336004	2017-04-25 06:29:39.335905	2017-04-25 06:29:39.335905
118	32	148	2017-04-25 07:39:14.74029	2017-04-25 07:39:14.740211	2017-04-25 07:39:14.740211
119	33	162	2017-04-25 07:40:57.509413	2017-04-25 07:40:57.509302	2017-04-25 07:40:57.509302
120	32	142	2017-04-25 07:42:04.605529	2017-04-25 07:42:04.60536	2017-04-25 07:42:04.60536
121	33	141	2017-04-25 08:08:09.023624	2017-04-25 08:08:09.023487	2017-04-25 08:08:09.023487
122	32	109	2017-04-26 02:34:34.243843	2017-04-26 02:34:34.243714	2017-04-26 02:34:34.243714
123	32	168	2017-04-26 02:37:38.68616	2017-04-26 02:37:38.686031	2017-04-26 02:37:38.686031
124	32	95	2017-04-26 04:37:21.82451	2017-04-26 04:37:21.82439	2017-04-26 04:37:21.82439
125	33	170	2017-04-26 04:37:37.18814	2017-04-26 04:37:37.188003	2017-04-26 04:37:37.188003
126	33	98	2017-04-26 04:37:52.454084	2017-04-26 04:37:52.4539	2017-04-26 04:37:52.4539
127	33	95	2017-04-26 13:05:46.046194	2017-04-26 13:05:46.046068	2017-04-26 13:05:46.046068
128	33	96	2017-04-27 04:55:20.013325	2017-04-27 04:55:20.013199	2017-04-27 04:55:20.013199
129	33	88	2017-04-27 05:05:29.004638	2017-04-27 05:05:29.004548	2017-04-27 05:05:29.004548
130	33	88	2017-04-27 05:05:39.291397	2017-04-27 05:05:39.291282	2017-04-27 05:05:39.291282
131	33	105	2017-05-02 03:56:24.218229	2017-05-02 03:56:24.218101	2017-05-02 03:56:24.218101
132	32	98	2017-05-05 04:39:31.561781	2017-05-05 04:39:31.561607	2017-05-05 04:39:31.561607
133	33	95	2017-05-05 04:43:04.32181	2017-05-05 04:43:04.321735	2017-05-05 04:43:04.321735
134	33	95	2017-05-05 04:43:23.848918	2017-05-05 04:43:23.848827	2017-05-05 04:43:23.848827
135	33	109	2017-05-05 04:45:26.549049	2017-05-05 04:45:26.548951	2017-05-05 04:45:26.548951
136	33	95	2017-05-05 04:46:08.872603	2017-05-05 04:46:08.872448	2017-05-05 04:46:08.872448
137	32	96	2017-05-05 04:47:15.936208	2017-05-05 04:47:15.936126	2017-05-05 04:47:15.936126
138	33	162	2017-05-05 06:14:11.780202	2017-05-05 06:14:11.780083	2017-05-05 06:14:11.780083
139	15	25	2017-05-08 20:02:59.443194	2017-05-08 20:02:59.44304	2017-05-08 20:02:59.44304
140	15	180	2017-05-09 00:30:52.026566	2017-05-09 00:30:52.026477	2017-05-09 00:30:52.026477
141	15	69	2017-05-09 00:50:56.822027	2017-05-09 00:50:56.821869	2017-05-09 00:50:56.821869
142	15	69	2017-05-09 00:51:18.755758	2017-05-09 00:51:18.755663	2017-05-09 00:51:18.755663
143	33	98	2017-05-10 07:10:12.683135	2017-05-10 07:10:12.682937	2017-05-10 07:10:12.682937
\.


--
-- Name: dispensary_redeemables_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('dispensary_redeemables_id_seq', 143, true);


--
-- Data for Name: dispensary_referral_deals; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY dispensary_referral_deals (id, points, name, dispensaries_id, created_at, updated_at) FROM stdin;
13	N/A	Referral Reward	13	2017-06-13 08:34:47.919268	2017-06-13 08:34:47.919268
14	N/A	Referral Reward	1	2017-06-13 08:54:20.844751	2017-06-13 08:54:20.844751
15	N/A	Referral Reward	14	2017-06-13 08:59:30.248802	2017-06-13 08:59:30.248802
16	N/A	Referral Reward	15	2017-06-13 09:26:25.017748	2017-06-13 09:26:25.017748
17	N/A	Referral Reward	16	2017-06-13 10:41:24.147806	2017-06-13 10:41:24.147806
12	N/A	Referral Reward	18	2017-06-13 08:29:28.738047	2017-06-14 07:15:58.5095
18	N/A	Referral Reward	17	2017-06-16 05:25:40.920973	2017-06-16 05:25:40.920973
19	N/A	Referral Reward	19	2017-06-16 05:55:13.549328	2017-06-16 05:55:13.549328
20	N/A	Referral Reward	20	2017-06-16 06:43:39.430146	2017-06-16 06:43:39.430146
\.


--
-- Name: dispensary_referral_deals_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('dispensary_referral_deals_id_seq', 20, true);


--
-- Data for Name: dispensary_text_logs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY dispensary_text_logs (id, dispensary_id, capaign_size, remaining_sms, successfull_send, from_phone_number, send_date, created_at, updated_at) FROM stdin;
2	1	3	1488	3	[]	2017-06-06 18:34:48.011339	2017-06-06 18:34:48.020936	2017-06-06 18:34:48.020936
3	1	3	1485	3	+13233077418	2017-06-06 18:37:37.586582	2017-06-06 18:37:37.59773	2017-06-06 18:37:37.59773
4	1	3	1482	3	+13233077418	2017-06-06 18:49:20.984308	2017-06-06 18:49:20.996513	2017-06-06 18:49:20.996513
5	1	3	1476	3	+13233077418	2017-06-06 18:57:30.646015	2017-06-06 18:57:30.656047	2017-06-06 18:57:30.656047
6	1	3	1467	3	+13233077418	2017-06-06 18:58:23.549489	2017-06-06 18:58:23.567223	2017-06-06 18:58:23.567223
7	1	3	1464	3	+13233077418	2017-06-06 18:58:33.326196	2017-06-06 18:58:33.328246	2017-06-06 18:58:33.328246
8	1	3	1455	3	+13233077418	2017-06-06 18:59:23.219477	2017-06-06 18:59:23.23317	2017-06-06 18:59:23.23317
9	1	3	1452	3	+13233077418	2017-06-06 18:59:31.296465	2017-06-06 18:59:31.297414	2017-06-06 18:59:31.297414
10	1	3	1249	3	+13233077418	2017-06-06 19:00:09.05596	2017-06-06 19:00:09.073342	2017-06-06 19:00:09.073342
11	1	3	1243	3	+13233077418	2017-06-10 07:08:31.907966	2017-06-10 07:08:31.920038	2017-06-10 07:08:31.920038
12	1	3	1237	3	+13233077418	2017-06-10 07:10:28.095086	2017-06-10 07:10:28.106774	2017-06-10 07:10:28.106774
13	1	3	1232	3	+13233077418	2017-06-10 17:13:22.114233	2017-06-10 17:13:22.130763	2017-06-10 17:13:22.130763
14	1	3	-3	3	+13233077418	2017-06-11 09:50:03.726707	2017-06-11 09:50:03.738325	2017-06-11 09:50:03.738325
15	1	3	-3	3	+13233077418	2017-06-11 10:03:59.715111	2017-06-11 10:03:59.73714	2017-06-11 10:03:59.73714
16	1	3	-1	3	+13233077418	2017-06-11 10:05:50.96523	2017-06-11 10:05:50.977804	2017-06-11 10:05:50.977804
17	1	3	494	3	+13233077418	2017-06-11 10:08:30.407869	2017-06-11 10:08:30.419695	2017-06-11 10:08:30.419695
18	1	3	488	3	+13233077418	2017-06-11 10:09:45.21208	2017-06-11 10:09:45.221733	2017-06-11 10:09:45.221733
19	1	3	482	3	+13233077418	2017-06-11 10:10:06.967515	2017-06-11 10:10:06.968723	2017-06-11 10:10:06.968723
20	1	3	479	3	+13233077418	2017-06-11 10:11:01.025104	2017-06-11 10:11:01.04048	2017-06-11 10:11:01.04048
\.


--
-- Name: dispensary_text_logs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('dispensary_text_logs_id_seq', 20, true);


--
-- Data for Name: dispensary_texts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY dispensary_texts (id, access, dispensary_id, remaining_text, send_sms_count, twilio_phone_number, created_at, updated_at) FROM stdin;
20	f	14	13	0	+13233077418	2017-06-13 09:01:22.963372	2017-06-13 09:32:24.3319
22	t	16	55	64	+13233077418	2017-06-13 11:00:26.000116	2017-06-16 06:16:12.919708
26	t	20	0	0	\N	2017-06-16 06:48:13.784155	2017-06-16 06:48:13.784155
17	f	10	0	0		2017-06-13 05:03:32.988251	2017-06-13 05:03:32.988251
21	f	15	17	14	+13233077	2017-06-13 09:32:22.399157	2017-06-13 12:52:28.398677
19	f	11	0	0	\N	2017-06-13 07:48:02.353442	2017-06-13 07:48:03.963269
18	f	1	9	36	+13233077418	2017-06-13 05:06:01.28486	2017-06-13 09:08:02.801568
23	f	17	10	0		2017-06-16 05:27:36.079483	2017-06-16 06:00:06.183247
25	f	13	60	0		2017-06-16 06:02:31.76658	2017-06-16 06:02:41.654138
24	t	19	10	0	+13233077418	2017-06-16 06:00:08.479044	2017-06-16 06:11:46.723411
\.


--
-- Name: dispensary_texts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('dispensary_texts_id_seq', 26, true);


--
-- Data for Name: faqs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY faqs (id, title, content, created_at, updated_at) FROM stdin;
1	How does the point breakdown work?	Every time you check in at a dispensary, you receive 10 points and each of your friends receives 2 points (at that dispensary). Accordingly, if one of your friends checks in at a dispensary, you will receive 2 points (at that dispensary).	2017-05-14 08:01:06.700244	2017-05-14 08:01:06.700244
2	How does the cap work?	The cap is a limit on the number of points you can earn from friends at each dispensary. The cap resets every time you check in at a particular dispensary. For example, if you have earned 28 points (the cap) from friends at dispensary A, you cannot earn any more points from friends (at dispensary A) until your next check in at dispensary A.	2017-05-14 08:01:06.714402	2017-05-14 08:01:06.714402
3	How do I redeem a reward?	When you check in at a dispensary and have enough points to redeem a reward, your eligible rewards will appear on the tablet. Once you pick a reward, you will be asked to confirm your choice. The tablet will prompt you to show the confirmation page to your budtender, who will then provide your reward. The point value of the redeemed reward is deducted from your total balance at that dispensary upon your confirmation.	2017-05-14 08:01:06.725481	2017-05-14 08:01:06.725481
4	Is there a mobile application?	-No – unfortunately, due to iOS regulations, we will not be able to provide a mobile application at this time. However, we have mobile optimized our website so you can access the website on your phone easily.	2017-05-14 08:01:06.736511	2017-05-14 08:01:06.736511
\.


--
-- Name: faqs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('faqs_id_seq', 4, true);


--
-- Data for Name: friendly_id_slugs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY friendly_id_slugs (id, slug, sluggable_id, sluggable_type, scope, created_at) FROM stdin;
\.


--
-- Name: friendly_id_slugs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('friendly_id_slugs_id_seq', 1, false);


--
-- Data for Name: invitations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY invitations (id, inviter_id, invitee_id, invitation_state, created_at, updated_at, seen_at, deleted_at) FROM stdin;
1	2	3	1	2017-05-14 08:54:24.237784	2017-05-14 08:54:24.237784	\N	\N
25	2	158	1	2017-05-17 15:51:46.007069	2017-05-17 15:51:46.007069	\N	\N
26	158	164	1	2017-05-17 16:30:55.409563	2017-05-17 16:30:55.409563	\N	\N
27	168	169	1	2017-05-27 08:38:27.839633	2017-05-27 08:38:27.839633	\N	\N
28	170	171	1	2017-05-27 13:59:33.600949	2017-05-27 13:59:33.600949	\N	\N
29	2	172	1	2017-05-27 15:26:52.555553	2017-05-27 15:26:52.555553	\N	\N
30	173	174	1	2017-05-30 17:31:47.65781	2017-05-30 17:31:47.65781	\N	\N
31	202	203	1	2017-06-16 04:44:51.743546	2017-06-16 04:44:51.743546	\N	\N
32	202	2	1	2017-06-16 05:05:12.590607	2017-06-16 05:05:12.590607	\N	\N
33	212	213	1	2017-06-16 06:29:38.227546	2017-06-16 06:29:38.227546	\N	\N
34	217	218	1	2017-06-16 07:07:58.815436	2017-06-16 07:07:58.815436	\N	\N
35	217	219	1	2017-06-16 08:06:49.190173	2017-06-16 08:06:49.190173	\N	2017-06-16 08:20:28.125047
36	217	219	1	2017-06-16 08:25:39.845538	2017-06-16 08:25:39.845538	\N	2017-06-16 08:34:10.933427
37	219	220	1	2017-06-16 08:29:48.119678	2017-06-16 08:29:48.119678	\N	2017-06-16 08:35:18.777259
38	217	219	1	2017-06-16 08:36:30.405921	2017-06-16 08:36:30.405921	\N	2017-06-16 08:37:28.793256
\.


--
-- Name: invitations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('invitations_id_seq', 38, true);


--
-- Data for Name: notifications; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY notifications (id, user_id, dispensary_id, kind, message, created_at, updated_at, upper_message, seen_at, lower_message) FROM stdin;
2	2	1	7	Test1	2017-01-13 10:07:54.032769	2017-01-13 10:07:54.032769	You signed up for Reefer!	2017-01-13 10:10:33.539907	\N
3	3	1	7	Test1	2017-01-13 15:23:29.299275	2017-01-13 15:23:29.299275	You signed up for Reefer!	\N	\N
4	3	2	8	Test 2	2017-01-13 15:23:29.309128	2017-01-13 15:23:29.309128	You signed up for Reefer!	\N	\N
6	3	1	4	Test1	2017-01-13 15:23:29.47567	2017-01-13 15:23:29.47567	You and Iztok are now friends!	\N	\N
7	3	1	0	You've got a free joint at Test1	2017-01-13 15:23:29.483386	2017-01-13 15:23:29.483386	You accepted Iztok's reeferal!	\N	\N
8	3	1	5	Test1	2017-01-13 15:26:19.724393	2017-01-13 15:26:19.724393	You visited	\N	\N
10	4	1	7	Test1	2017-01-13 15:36:34.868191	2017-01-13 15:36:34.868191	You signed up for Reefer!	\N	\N
11	4	2	8	Test 2	2017-01-13 15:36:34.89189	2017-01-13 15:36:34.89189	You signed up for Reefer!	\N	\N
5	2	1	4	Test1	2017-01-13 15:23:29.449781	2017-01-13 15:23:29.449781	IztokM signed up with your link!	2017-01-17 11:01:09.012617	Get Reeferal reward when he visits!
9	2	1	6	Test1	2017-01-13 15:26:19.757853	2017-01-13 15:26:19.757853	IztokM visited	2017-01-17 11:01:09.012617	\N
12	2	1	1	You've earned a Free Joint at Test1	2017-01-17 10:35:48.027301	2017-01-17 10:35:48.027301	You have 30 points!	2017-01-17 11:01:09.012617	\N
13	2	1	5	Test1	2017-01-17 10:35:48.046272	2017-01-17 10:35:48.046272	You visited	2017-01-17 11:01:09.012617	\N
14	2	1	10	You claimed your Free Joint at Test1	2017-01-17 10:36:05.995247	2017-01-17 10:36:05.995247	Congrats!	2017-01-17 11:01:09.012617	\N
15	3	1	1	You've earned a Free Joint at Test1	2017-01-17 12:58:32.824133	2017-01-17 12:58:32.824133	You have 30 points!	\N	\N
16	3	1	5	Test1	2017-01-17 12:58:32.836656	2017-01-17 12:58:32.836656	You visited	\N	\N
17	5	1	7	Test1	2017-01-17 12:58:33.36037	2017-01-17 12:58:33.36037	You signed up for Reefer!	\N	\N
18	5	2	9	Test 2	2017-01-17 12:58:33.388262	2017-01-17 12:58:33.388262	You signed up for Reefer!	\N	\N
21	2	1	1	Test1	2017-01-17 13:04:29.858382	2017-01-17 13:04:29.858382	TestUserA signed up with your link!	2017-01-17 13:04:46.599223	Get Reeferal reward when he visits!
19	6	1	7	Test1	2017-01-17 13:04:29.790736	2017-01-17 13:04:29.790736	You signed up for Reefer!	2017-01-17 13:04:55.781283	\N
20	6	2	8	Test 2	2017-01-17 13:04:29.797283	2017-01-17 13:04:29.797283	You signed up for Reefer!	2017-01-17 13:04:55.781283	\N
22	6	1	0	You've got a free joint at Test1	2017-01-17 13:04:29.902031	2017-01-17 13:04:29.902031	You accepted Iztok's reeferal!	2017-01-17 13:04:55.781283	\N
27	8	3	7	Test Dispensary	2017-01-17 19:41:49.387197	2017-01-17 19:41:49.387197	You signed up for Reefer!	2017-01-17 19:46:13.88948	\N
28	8	2	9	Test 2	2017-01-17 19:41:49.415721	2017-01-17 19:41:49.415721	You signed up for Reefer!	2017-01-17 19:46:13.88948	\N
29	9	3	7	Test Dispensary	2017-01-17 19:55:07.266501	2017-01-17 19:55:07.266501	You signed up for Reefer!	\N	\N
30	9	2	9	Test 2	2017-01-17 19:55:07.331192	2017-01-17 19:55:07.331192	You signed up for Reefer!	\N	\N
31	10	4	7	Test Dispensary 2 Blue	2017-01-17 20:12:58.272967	2017-01-17 20:12:58.272967	You signed up for Reefer!	\N	\N
32	10	2	9	Test 2	2017-01-17 20:12:58.295806	2017-01-17 20:12:58.295806	You signed up for Reefer!	\N	\N
33	11	4	7	Test Dispensary 2 Blue	2017-01-17 20:23:54.459041	2017-01-17 20:23:54.459041	You signed up for Reefer!	\N	\N
34	11	2	9	Test 2	2017-01-17 20:23:54.530979	2017-01-17 20:23:54.530979	You signed up for Reefer!	\N	\N
1	1	1	7	Test1	2017-01-13 01:55:49.305068	2017-01-13 01:55:49.305068	You signed up for Reefer!	2017-01-17 20:44:00.749591	\N
26	1	3	5	Test Dispensary	2017-01-17 19:39:19.263068	2017-01-17 19:39:19.263068	You visited	2017-01-17 20:44:00.749591	\N
35	12	5	7	Zachs Dispensary	2017-01-18 00:44:27.73021	2017-01-18 00:44:27.73021	You signed up for Reefer!	2017-01-18 00:45:44.440017	\N
36	12	1	9	Test1	2017-01-18 00:44:27.761591	2017-01-18 00:44:27.761591	You signed up for Reefer!	2017-01-18 00:45:44.440017	\N
40	12	6	5	Brian's Dispensary	2017-01-18 01:00:05.183359	2017-01-18 01:00:05.183359	You visited	2017-01-18 01:01:00.573463	\N
37	13	5	7	Zachs Dispensary	2017-01-18 00:57:36.21593	2017-01-18 00:57:36.21593	You signed up for Reefer!	2017-01-18 01:02:26.655834	\N
38	13	3	9	Test Dispensary	2017-01-18 00:57:36.260616	2017-01-18 00:57:36.260616	You signed up for Reefer!	2017-01-18 01:02:26.655834	\N
39	13	6	5	Brian's Dispensary	2017-01-18 00:58:58.677792	2017-01-18 00:58:58.677792	You visited	2017-01-18 01:02:26.655834	\N
41	13	1	7	Test1	2017-01-18 01:01:33.386451	2017-01-18 01:01:33.386451	You signed up for Reefer!	2017-01-18 01:02:26.655834	\N
42	13	5	8	Zachs Dispensary	2017-01-18 01:01:33.400253	2017-01-18 01:01:33.400253	You signed up for Reefer!	2017-01-18 01:02:26.655834	\N
43	13	6	9	Brian's Dispensary	2017-01-18 01:01:33.41646	2017-01-18 01:01:33.41646	You signed up for Reefer!	2017-01-18 01:02:26.655834	\N
44	13	6	1	Brian's Dispensary	2017-01-18 01:01:33.959902	2017-01-18 01:01:33.959902	Brian signed up with your link!	2017-01-18 01:02:26.655834	Get Reeferal reward when he visits!
48	14	1	7	Test1	2017-01-18 01:17:33.672958	2017-01-18 01:17:33.672958	You signed up for Reefer!	2017-01-18 01:17:49.654174	\N
49	14	3	8	Test Dispensary	2017-01-18 01:17:33.679984	2017-01-18 01:17:33.679984	You signed up for Reefer!	2017-01-18 01:17:49.654174	\N
50	14	5	9	Zachs Dispensary	2017-01-18 01:17:33.687042	2017-01-18 01:17:33.687042	You signed up for Reefer!	2017-01-18 01:17:49.654174	\N
52	14	6	0	You've got a free joint at Brian's Dispensary	2017-01-18 01:17:33.89154	2017-01-18 01:17:33.89154	You accepted Zach wise's reeferal!	2017-01-18 01:17:49.654174	\N
23	7	3	7	Test Dispensary	2017-01-17 19:35:47.947545	2017-01-17 19:35:47.947545	You signed up for Reefer!	2017-01-19 20:09:50.389649	\N
24	7	2	8	Test 2	2017-01-17 19:35:47.966116	2017-01-17 19:35:47.966116	You signed up for Reefer!	2017-01-19 20:09:50.389649	\N
25	7	1	9	Test1	2017-01-17 19:35:47.976638	2017-01-17 19:35:47.976638	You signed up for Reefer!	2017-01-19 20:09:50.389649	\N
51	12	6	1	Brian's Dispensary	2017-01-18 01:17:33.850089	2017-01-18 01:17:33.850089	Jordan signed up with your link!	2017-01-25 08:26:48.981473	Get Reeferal reward when he visits!
45	13	6	0	You've got a free joint at Brian's Dispensary	2017-01-18 01:01:34.135963	2017-01-18 01:01:34.135963	You accepted Brian's reeferal!	2017-01-18 01:02:26.655834	\N
53	15	5	7	Zachs Dispensary	2017-01-18 04:54:47.256311	2017-01-18 04:54:47.256311	You signed up for Reefer!	\N	\N
54	15	1	9	Test1	2017-01-18 04:54:47.276755	2017-01-18 04:54:47.276755	You signed up for Reefer!	\N	\N
55	16	2	7	Test 2	2017-01-18 10:12:39.924519	2017-01-18 10:12:39.924519	You signed up for Reefer!	2017-01-18 12:52:16.90457	\N
56	16	3	8	Test Dispensary	2017-01-18 10:12:39.93984	2017-01-18 10:12:39.93984	You signed up for Reefer!	2017-01-18 12:52:16.90457	\N
57	16	7	9	Iztok's dispensary	2017-01-18 10:12:39.946864	2017-01-18 10:12:39.946864	You signed up for Reefer!	2017-01-18 12:52:16.90457	\N
58	16	7	5	Iztok's dispensary	2017-01-18 12:39:35.538109	2017-01-18 12:39:35.538109	You visited	2017-01-18 12:52:16.90457	\N
59	16	7	1	Iztok's dispensary	2017-01-18 12:53:41.110024	2017-01-18 12:53:41.110024	IztokS signed up with your link!	2017-01-18 12:54:07.170597	Get Reeferal reward when he visits!
60	18	7	0	You've got a free joint at Iztok's dispensary	2017-01-18 12:53:41.131615	2017-01-18 12:53:41.131615	You accepted IztokD's reeferal!	2017-01-18 12:55:18.778747	\N
61	18	7	5	Iztok's dispensary	2017-01-18 12:54:45.206097	2017-01-18 12:54:45.206097	You visited	2017-01-18 12:55:18.778747	\N
63	18	7	10	You claimed your free joint at Iztok's dispensary	2017-01-18 12:54:46.351496	2017-01-18 12:54:46.351496	Congrats!	2017-01-18 12:55:18.778747	\N
64	18	7	1	Iztok's dispensary	2017-01-18 13:01:17.318258	2017-01-18 13:01:17.318258	iztokP signed up with your link!	\N	Get Reeferal reward when he visits!
65	19	7	0	You've got a free joint at Iztok's dispensary	2017-01-18 13:01:17.34413	2017-01-18 13:01:17.34413	You accepted IztokS's reeferal!	\N	\N
46	13	6	0	You've got a free joint at Brian's Dispensary	2017-01-18 01:16:21.133629	2017-01-18 01:16:21.133629	Brian  accepted your Reeferal!	2017-01-18 20:19:35.375331	\N
47	13	6	10	You claimed your free joint at Brian's Dispensary	2017-01-18 01:16:21.829233	2017-01-18 01:16:21.829233	Congrats!	2017-01-18 20:19:35.375331	\N
66	13	5	5	Zachs Dispensary	2017-01-18 20:16:00.856044	2017-01-18 20:16:00.856044	You visited	2017-01-18 20:19:35.375331	\N
67	20	5	7	Zachs Dispensary	2017-01-18 20:42:57.81849	2017-01-18 20:42:57.81849	You signed up for Reefer!	\N	\N
68	21	5	7	Zachs Dispensary	2017-01-18 20:43:46.115484	2017-01-18 20:43:46.115484	You signed up for Reefer!	\N	\N
69	22	5	7	Zachs Dispensary	2017-01-18 20:45:14.012831	2017-01-18 20:45:14.012831	You signed up for Reefer!	\N	\N
71	23	6	7	Brian's Dispensary	2017-01-18 22:51:05.892001	2017-01-18 22:51:05.892001	You signed up for Reefer!	\N	\N
72	14	6	5	Brian's Dispensary	2017-01-18 22:53:00.174024	2017-01-18 22:53:00.174024	You visited	\N	\N
74	14	6	10	You claimed your free joint at Brian's Dispensary	2017-01-18 22:53:01.015518	2017-01-18 22:53:01.015518	Congrats!	\N	\N
75	27	1	7	Test1	2017-01-19 10:35:38.673464	2017-01-19 10:35:38.673464	You signed up for Reefer!	2017-01-19 10:37:31.726809	\N
76	24	1	5	Test1	2017-01-19 10:46:12.763936	2017-01-19 10:46:12.763936	You visited	\N	\N
77	24	1	5	Test1	2017-01-19 11:16:08.419624	2017-01-19 11:16:08.419624	You visited	\N	\N
78	24	1	1	You've earned a Free Joint at Test1	2017-01-19 11:18:36.467073	2017-01-19 11:18:36.467073	You have 30 points!	\N	\N
79	24	1	5	Test1	2017-01-19 11:18:36.478262	2017-01-19 11:18:36.478262	You visited	\N	\N
80	24	1	5	Test1	2017-01-19 11:21:19.864023	2017-01-19 11:21:19.864023	You visited	\N	\N
82	10	3	5	Test Dispensary	2017-01-19 19:42:40.193129	2017-01-19 19:42:40.193129	You visited	\N	\N
83	28	3	7	Test Dispensary	2017-01-19 19:44:45.305257	2017-01-19 19:44:45.305257	You signed up for Reefer!	\N	\N
84	29	3	7	Test Dispensary	2017-01-19 20:04:05.243944	2017-01-19 20:04:05.243944	You signed up for Reefer!	\N	\N
81	7	3	5	Test Dispensary	2017-01-19 19:41:44.446049	2017-01-19 19:41:44.446049	You visited	2017-01-19 20:09:50.389649	\N
85	1	3	5	Test Dispensary	2017-01-19 20:18:06.56883	2017-01-19 20:18:06.56883	You visited	\N	\N
86	24	1	1	Test1	2017-01-20 11:01:14.624833	2017-01-20 11:01:14.624833	IztokMO signed up with your link!	\N	Get Reeferal reward when he visits!
87	31	1	0	You've got a free joint at Test1	2017-01-20 11:01:14.676742	2017-01-20 11:01:14.676742	You accepted IztokME's reeferal!	\N	\N
62	16	7	0	You've got a free joint at Iztok's dispensary	2017-01-18 12:54:45.446865	2017-01-18 12:54:45.446865	IztokS  accepted your Reeferal!	2017-01-20 11:02:41.490798	\N
88	32	7	7	Iztok's dispensary	2017-01-20 11:42:05.725054	2017-01-20 11:42:05.725054	You signed up for Reefer!	\N	\N
89	14	6	5	Brian's Dispensary	2017-01-20 21:16:17.538812	2017-01-20 21:16:17.538812	You visited	\N	\N
90	15	6	5	Brian's Dispensary	2017-01-23 01:41:17.199309	2017-01-23 01:41:17.199309	You visited	\N	\N
91	33	6	7	Brian's Dispensary	2017-01-23 01:50:46.679963	2017-01-23 01:50:46.679963	You signed up for Reefer!	\N	\N
92	34	6	7	Brian's Dispensary	2017-01-23 01:50:58.107275	2017-01-23 01:50:58.107275	You signed up for Reefer!	\N	\N
93	35	6	7	Brian's Dispensary	2017-01-23 02:22:52.380498	2017-01-23 02:22:52.380498	You signed up for Reefer!	2017-01-23 02:25:15.755921	\N
95	37	6	7	Brian's Dispensary	2017-01-23 02:26:27.165811	2017-01-23 02:26:27.165811	You signed up for Reefer!	\N	\N
94	36	6	7	Brian's Dispensary	2017-01-23 02:23:28.208901	2017-01-23 02:23:28.208901	You signed up for Reefer!	2017-01-23 02:26:59.120183	\N
98	39	7	0	You've got a free gram at Iztok's dispensary	2017-01-24 10:10:17.611911	2017-01-24 10:10:17.611911	You accepted TwoIztokT's reeferal!	\N	\N
99	39	7	5	Iztok's dispensary	2017-01-24 10:11:36.809017	2017-01-24 10:11:36.809017	You visited	\N	\N
101	39	7	10	You claimed your free joint at Iztok's dispensary	2017-01-24 10:11:37.297644	2017-01-24 10:11:37.297644	Congrats!	\N	\N
102	40	8	7	PovioLabs Dispensary	2017-01-24 10:33:22.445624	2017-01-24 10:33:22.445624	You signed up for Reefer!	2017-01-24 10:48:47.941518	\N
103	40	8	1	PovioLabs Dispensary	2017-01-24 10:49:52.713192	2017-01-24 10:49:52.713192	IztokTS signed up with your link!	\N	Get Reeferal reward when he visits!
104	41	8	0	You've got a free gram at PovioLabs Dispensary	2017-01-24 10:49:52.763247	2017-01-24 10:49:52.763247	You accepted IztokTF's reeferal!	2017-01-24 10:50:00.785235	\N
106	40	8	0	You've got a free gram at PovioLabs Dispensary	2017-01-24 10:51:22.411744	2017-01-24 10:51:22.411744	IztokTS  accepted your Reeferal!	\N	\N
96	38	7	5	Iztok's dispensary	2017-01-24 10:08:01.287929	2017-01-24 10:08:01.287929	You visited	2017-01-24 11:02:51.084985	\N
97	38	7	1	Iztok's dispensary	2017-01-24 10:10:17.59493	2017-01-24 10:10:17.59493	TwoIztokF signed up with your link!	2017-01-24 11:02:51.084985	Get Reeferal reward when he visits!
100	38	7	0	You've got a free gram at Iztok's dispensary	2017-01-24 10:11:36.989017	2017-01-24 10:11:36.989017	TwoIztokF  accepted your Reeferal!	2017-01-24 11:02:51.084985	\N
108	42	8	7	PovioLabs Dispensary	2017-01-24 14:06:27.352931	2017-01-24 14:06:27.352931	You signed up for Reefer!	\N	\N
109	42	8	5	PovioLabs Dispensary	2017-01-24 14:06:27.359499	2017-01-24 14:06:27.359499	You visited	\N	\N
111	43	8	5	PovioLabs Dispensary	2017-01-24 15:16:17.245227	2017-01-24 15:16:17.245227	You visited	2017-01-24 15:17:28.570625	\N
70	12	5	5	Zachs Dispensary	2017-01-18 20:49:27.425078	2017-01-18 20:49:27.425078	You visited	2017-01-25 08:26:48.981473	\N
105	41	8	5	PovioLabs Dispensary	2017-01-24 10:51:21.954976	2017-01-24 10:51:21.954976	You visited	2017-02-01 22:23:07.848555	\N
107	41	8	10	You claimed your free joint at PovioLabs Dispensary	2017-01-24 10:51:22.861635	2017-01-24 10:51:22.861635	Congrats!	2017-02-01 22:23:07.848555	\N
110	43	8	7	PovioLabs Dispensary	2017-01-24 15:16:17.236306	2017-01-24 15:16:17.236306	You signed up for Reefer!	2017-01-24 15:17:28.570625	\N
112	43	8	5	PovioLabs Dispensary	2017-01-24 15:22:32.354143	2017-01-24 15:22:32.354143	You visited	\N	\N
113	44	8	7	PovioLabs Dispensary	2017-01-24 15:24:19.31331	2017-01-24 15:24:19.31331	You signed up for Reefer!	\N	\N
114	44	8	5	PovioLabs Dispensary	2017-01-24 15:24:19.330708	2017-01-24 15:24:19.330708	You visited	\N	\N
115	43	8	1	You've earned a Free Joint at PovioLabs Dispensary	2017-01-24 15:52:40.672051	2017-01-24 15:52:40.672051	You have 30 points!	\N	\N
116	43	8	5	PovioLabs Dispensary	2017-01-24 15:52:40.684272	2017-01-24 15:52:40.684272	You visited	\N	\N
117	43	8	5	PovioLabs Dispensary	2017-01-24 15:53:45.736475	2017-01-24 15:53:45.736475	You visited	\N	\N
118	43	8	5	PovioLabs Dispensary	2017-01-24 15:54:20.191438	2017-01-24 15:54:20.191438	You visited	\N	\N
119	45	8	7	PovioLabs Dispensary	2017-01-24 16:13:20.316309	2017-01-24 16:13:20.316309	You signed up for Reefer!	\N	\N
120	45	8	5	PovioLabs Dispensary	2017-01-24 16:13:20.323183	2017-01-24 16:13:20.323183	You visited	\N	\N
121	46	8	7	PovioLabs Dispensary	2017-01-24 16:14:21.330167	2017-01-24 16:14:21.330167	You signed up for Reefer!	\N	\N
122	46	8	5	PovioLabs Dispensary	2017-01-24 16:14:21.336044	2017-01-24 16:14:21.336044	You visited	\N	\N
73	12	6	0	You've got a free joint at Brian's Dispensary	2017-01-18 22:53:00.634559	2017-01-18 22:53:00.634559	Jordan  accepted your Reeferal!	2017-01-25 08:26:48.981473	\N
123	32	8	5	PovioLabs Dispensary	2017-01-27 13:39:02.910516	2017-01-27 13:39:02.910516	You visited	\N	\N
124	32	1	5	Test1	2017-01-27 13:49:01.192249	2017-01-27 13:49:01.192249	You visited	\N	\N
126	47	5	0	You've got a free gram at Zachs Dispensary	2017-01-28 05:22:18.870485	2017-01-28 05:22:18.870485	You accepted Zach wise's reeferal!	\N	\N
127	47	5	5	Zachs Dispensary	2017-01-28 05:22:51.238672	2017-01-28 05:22:51.238672	You visited	\N	\N
129	47	5	10	You claimed your free joint at Zachs Dispensary	2017-01-28 05:22:51.783326	2017-01-28 05:22:51.783326	Congrats!	\N	\N
132	2	8	5	PovioLabs Dispensary	2017-01-30 08:57:22.762124	2017-01-30 08:57:22.762124	You visited	\N	\N
133	24	8	5	PovioLabs Dispensary	2017-01-30 09:48:06.131164	2017-01-30 09:48:06.131164	You visited	\N	\N
134	40	8	5	PovioLabs Dispensary	2017-01-30 10:03:19.163898	2017-01-30 10:03:19.163898	You visited	\N	\N
135	40	8	10	You claimed your free joint at PovioLabs Dispensary	2017-01-30 10:03:19.565088	2017-01-30 10:03:19.565088	Congrats!	\N	\N
125	12	5	1	Zachs Dispensary	2017-01-28 05:22:18.836888	2017-01-28 05:22:18.836888	Sean signed up with your link!	2017-01-30 19:48:57.576752	Get Reeferal reward when he visits!
128	12	5	0	You've got a free gram at Zachs Dispensary	2017-01-28 05:22:51.493325	2017-01-28 05:22:51.493325	Sean  accepted your Reeferal!	2017-01-30 19:48:57.576752	\N
130	12	5	5	Zachs Dispensary	2017-01-28 05:30:35.015669	2017-01-28 05:30:35.015669	You visited	2017-01-30 19:48:57.576752	\N
131	12	5	10	You claimed your free joint at Zachs Dispensary	2017-01-28 05:30:35.407049	2017-01-28 05:30:35.407049	Congrats!	2017-01-30 19:48:57.576752	\N
136	12	5	5	Zachs Dispensary	2017-02-06 18:41:59.300117	2017-02-06 18:41:59.300117	You visited	\N	\N
137	12	5	10	You claimed your Free Joint at Zachs Dispensary	2017-02-06 18:42:08.468245	2017-02-06 18:42:08.468245	Congrats!	\N	\N
138	48	5	7	Zachs Dispensary	2017-02-06 18:43:03.989096	2017-02-06 18:43:03.989096	You signed up for Reefer!	2017-02-06 18:44:55.113579	\N
139	48	5	5	Zachs Dispensary	2017-02-06 18:43:03.995519	2017-02-06 18:43:03.995519	You visited	2017-02-06 18:44:55.113579	\N
140	15	5	5	Zachs Dispensary	2017-02-09 00:25:07.096068	2017-02-09 00:25:07.096068	You visited	\N	\N
141	49	5	7	Zachs Dispensary	2017-02-12 21:17:41.367911	2017-02-12 21:17:41.367911	You signed up for Reefer!	2017-02-12 21:19:14.898809	\N
142	49	5	5	Zachs Dispensary	2017-02-12 21:17:41.376347	2017-02-12 21:17:41.376347	You visited	2017-02-12 21:19:14.898809	\N
143	52	5	1	You've earned a Free Eighth at Zachs Dispensary	2017-03-23 21:17:56.191623	2017-03-23 21:17:56.191623	You have 1 points!	\N	\N
144	52	5	7	Zachs Dispensary	2017-03-23 21:17:56.205734	2017-03-23 21:17:56.205734	You signed up for Reefer!	\N	\N
145	52	5	5	Zachs Dispensary	2017-03-23 21:17:56.212755	2017-03-23 21:17:56.212755	You visited	\N	\N
146	13	5	2	You've earned a Free Joint at Zachs Dispensary	2017-03-23 21:18:31.954213	2017-03-23 21:18:31.954213	You have 26 points!	\N	\N
147	13	5	5	Zachs Dispensary	2017-03-23 21:18:31.969104	2017-03-23 21:18:31.969104	You visited	\N	\N
148	13	5	10	You claimed your Free Eighth at Zachs Dispensary	2017-03-23 21:18:47.826934	2017-03-23 21:18:47.826934	Congrats!	\N	\N
149	59	15	1	tttwww	2017-04-15 05:47:39.475292	2017-04-15 05:47:39.475292	Hshshs signed up with your link!	\N	Get Reeferal reward when he visits!
150	59	15	1	tttwww	2017-04-15 05:48:07.560056	2017-04-15 05:48:07.560056	Hshshs signed up with your link!	\N	Get Reeferal reward when he visits!
151	60	15	0	You've got a free gram at tttwww	2017-04-15 05:48:07.595367	2017-04-15 05:48:07.595367	You accepted ffyvgjkvb's reeferal!	\N	\N
152	59	15	1	tttwww	2017-04-15 05:49:01.666529	2017-04-15 05:49:01.666529	Hshshs signed up with your link!	\N	Get Reeferal reward when he visits!
153	60	15	0	You've got a free gram at tttwww	2017-04-15 05:49:01.689894	2017-04-15 05:49:01.689894	You accepted ffyvgjkvb's reeferal!	\N	\N
154	59	15	1	tttwww	2017-04-15 06:08:33.852447	2017-04-15 06:08:33.852447	Gdbdbhehrhr signed up with your link!	\N	Get Reeferal reward when he visits!
155	61	15	0	You've got a free gram at tttwww	2017-04-15 06:08:33.881541	2017-04-15 06:08:33.881541	You accepted ffyvgjkvb's reeferal!	\N	\N
156	61	7	1	Iztok's dispensary	2017-04-15 06:20:16.510395	2017-04-15 06:20:16.510395	Vzvsvsgsh signed up with your link!	\N	Get Reeferal reward when he visits!
157	62	7	0	You've got a free gram at Iztok's dispensary	2017-04-15 06:20:16.550992	2017-04-15 06:20:16.550992	You accepted Gdbdbhehrhr's reeferal!	\N	\N
158	62	3	1	Test Dispensary	2017-04-15 06:32:59.670372	2017-04-15 06:32:59.670372	Hzhxhdhs signed up with your link!	\N	Get Reeferal reward when he visits!
159	63	3	0	You've got a free gram at Test Dispensary	2017-04-15 06:32:59.697698	2017-04-15 06:32:59.697698	You accepted Vzvsvsgsh's reeferal!	\N	\N
160	62	2	1	Test 2	2017-04-15 07:08:53.5076	2017-04-15 07:08:53.5076	Vsbdbdbs signed up with your link!	\N	Get Reeferal reward when he visits!
161	69	2	0	You've got a free gram at Test 2	2017-04-15 07:08:53.605405	2017-04-15 07:08:53.605405	You accepted Vzvsvsgsh's reeferal!	\N	\N
162	54	1	1	Test1	2017-04-19 09:43:16.610227	2017-04-19 09:43:16.610227	vvv signed up with your link!	\N	Get Reeferal reward when he visits!
163	54	1	1	Test1	2017-04-19 09:44:19.693144	2017-04-19 09:44:19.693144	vvv signed up with your link!	\N	Get Reeferal reward when he visits!
164	73	1	0	You've got a free gram at Test1	2017-04-19 09:44:19.734186	2017-04-19 09:44:19.734186	You accepted dsfdsfsdf's reeferal!	\N	\N
165	74	15	7	tttwww	2017-04-21 06:40:23.098517	2017-04-21 06:40:23.098517	You signed up for Reefer!	\N	\N
166	74	15	5	tttwww	2017-04-21 06:40:23.20482	2017-04-21 06:40:23.20482	You visited	\N	\N
167	75	15	7	tttwww	2017-04-21 06:42:07.498791	2017-04-21 06:42:07.498791	You signed up for Reefer!	\N	\N
168	75	15	5	tttwww	2017-04-21 06:42:07.509985	2017-04-21 06:42:07.509985	You visited	\N	\N
169	13	15	1	You've earned a Free Coffee at tttwww	2017-04-21 06:48:21.943671	2017-04-21 06:48:21.943671	You have 5 points!	\N	\N
170	13	15	5	tttwww	2017-04-21 06:48:21.98334	2017-04-21 06:48:21.98334	You visited	\N	\N
171	76	15	1	You've earned a Free edible at tttwww	2017-04-21 06:52:11.994243	2017-04-21 06:52:11.994243	You have 5 points!	\N	\N
172	76	15	7	tttwww	2017-04-21 06:52:12.009791	2017-04-21 06:52:12.009791	You signed up for Reefer!	\N	\N
173	76	15	5	tttwww	2017-04-21 06:52:12.019797	2017-04-21 06:52:12.019797	You visited	\N	\N
174	77	15	1	You've earned a Free edible at tttwww	2017-04-21 06:52:13.025628	2017-04-21 06:52:13.025628	You have 5 points!	\N	\N
175	77	15	7	tttwww	2017-04-21 06:52:13.101242	2017-04-21 06:52:13.101242	You signed up for Reefer!	\N	\N
176	77	15	5	tttwww	2017-04-21 06:52:13.125732	2017-04-21 06:52:13.125732	You visited	\N	\N
177	78	15	1	You've earned a Free edible at tttwww	2017-04-21 06:57:13.331142	2017-04-21 06:57:13.331142	You have 5 points!	\N	\N
178	78	15	7	tttwww	2017-04-21 06:57:13.345985	2017-04-21 06:57:13.345985	You signed up for Reefer!	\N	\N
179	78	15	5	tttwww	2017-04-21 06:57:13.355593	2017-04-21 06:57:13.355593	You visited	\N	\N
180	13	15	5	tttwww	2017-04-21 07:06:00.960239	2017-04-21 07:06:00.960239	You visited	\N	\N
181	13	15	10	You claimed your Free edible at tttwww	2017-04-21 07:06:11.855012	2017-04-21 07:06:11.855012	Congrats!	\N	\N
182	12	15	1	You've earned a Free edible at tttwww	2017-04-21 07:07:32.334959	2017-04-21 07:07:32.334959	You have 5 points!	\N	\N
183	12	15	5	tttwww	2017-04-21 07:07:32.370887	2017-04-21 07:07:32.370887	You visited	\N	\N
184	12	15	10	You claimed your Free edible at tttwww	2017-04-21 07:07:43.474847	2017-04-21 07:07:43.474847	Congrats!	\N	\N
185	12	5	1	You've earned a Free Eighth at Zachs Dispensary	2017-04-22 06:42:57.111085	2017-04-22 06:42:57.111085	You have 1 points!	\N	\N
186	54	15	1	You've earned a Free edible at tttwww	2017-04-22 07:31:04.516493	2017-04-22 07:31:04.516493	You have 5 points!	\N	\N
187	54	15	5	tttwww	2017-04-22 07:31:04.541193	2017-04-22 07:31:04.541193	You visited	\N	\N
188	24	8	1	You've earned a Free Joint at PovioLabs Dispensary	2017-04-22 07:35:15.834187	2017-04-22 07:35:15.834187	You have 30 points!	\N	\N
189	12	15	5	tttwww	2017-04-22 17:38:43.64342	2017-04-22 17:38:43.64342	You visited	\N	\N
190	12	15	5	tttwww	2017-04-22 17:44:35.206947	2017-04-22 17:44:35.206947	You visited	\N	\N
191	13	15	1	You've earned a Free edible at tttwww	2017-04-22 17:44:47.479731	2017-04-22 17:44:47.479731	You have 5 points!	\N	\N
192	13	15	5	tttwww	2017-04-22 17:44:47.501917	2017-04-22 17:44:47.501917	You visited	\N	\N
193	12	15	5	tttwww	2017-04-22 17:45:26.757428	2017-04-22 17:45:26.757428	You visited	\N	\N
194	12	15	5	tttwww	2017-04-22 17:57:22.424034	2017-04-22 17:57:22.424034	You visited	\N	\N
195	79	15	1	You've earned a Free edible at tttwww	2017-04-24 04:25:51.00076	2017-04-24 04:25:51.00076	You have 5 points!	\N	\N
196	79	15	7	tttwww	2017-04-24 04:25:51.048924	2017-04-24 04:25:51.048924	You signed up for Reefer!	\N	\N
197	79	15	5	tttwww	2017-04-24 04:25:51.058547	2017-04-24 04:25:51.058547	You visited	\N	\N
198	80	15	1	You've earned a Free edible at tttwww	2017-04-24 04:28:19.60275	2017-04-24 04:28:19.60275	You have 5 points!	\N	\N
199	80	15	7	tttwww	2017-04-24 04:28:19.62161	2017-04-24 04:28:19.62161	You signed up for Reefer!	\N	\N
200	80	15	5	tttwww	2017-04-24 04:28:19.632305	2017-04-24 04:28:19.632305	You visited	\N	\N
201	80	15	10	You claimed your Free edible at tttwww	2017-04-24 04:28:32.129227	2017-04-24 04:28:32.129227	Congrats!	\N	\N
202	13	15	5	tttwww	2017-04-24 04:29:30.034245	2017-04-24 04:29:30.034245	You visited	\N	\N
203	24	15	1	You've earned a Free edible at tttwww	2017-04-24 04:30:05.285768	2017-04-24 04:30:05.285768	You have 5 points!	\N	\N
204	24	15	5	tttwww	2017-04-24 04:30:05.305111	2017-04-24 04:30:05.305111	You visited	\N	\N
205	24	15	10	You claimed your Free edible at tttwww	2017-04-24 04:30:15.273007	2017-04-24 04:30:15.273007	Congrats!	\N	\N
206	13	15	10	You claimed your Free edible at tttwww	2017-04-24 04:30:16.188039	2017-04-24 04:30:16.188039	Congrats!	\N	\N
207	41	8	1	You've earned a Free Joint at PovioLabs Dispensary	2017-04-24 04:32:22.361265	2017-04-24 04:32:22.361265	You have 30 points!	\N	\N
208	41	8	1	You've earned a Free Joint at PovioLabs Dispensary	2017-04-24 04:32:22.375167	2017-04-24 04:32:22.375167	You have 30 points!	\N	\N
209	41	8	1	You've earned a Free Joint at PovioLabs Dispensary	2017-04-24 04:32:22.385477	2017-04-24 04:32:22.385477	You have 30 points!	\N	\N
210	41	8	2	You've earned a Free Gram at PovioLabs Dispensary	2017-04-24 04:32:26.271811	2017-04-24 04:32:26.271811	You have 90 points!	\N	\N
211	41	15	1	You've earned a Free edible at tttwww	2017-04-24 04:33:26.744289	2017-04-24 04:33:26.744289	You have 5 points!	\N	\N
212	41	15	5	tttwww	2017-04-24 04:33:26.75514	2017-04-24 04:33:26.75514	You visited	\N	\N
213	13	3	1	You've earned a Free Joint at Test Dispensary	2017-04-24 04:38:27.261622	2017-04-24 04:38:27.261622	You have 30 points!	\N	\N
214	13	3	2	You've earned a Free Gram at Test Dispensary	2017-04-24 04:38:45.343731	2017-04-24 04:38:45.343731	You have 90 points!	\N	\N
215	47	5	1	You've earned a Free Eighth at Zachs Dispensary	2017-04-24 04:42:45.481116	2017-04-24 04:42:45.481116	You have 1 points!	\N	\N
216	7	1	1	You've earned a Free Joint at Test1	2017-04-24 04:48:42.521792	2017-04-24 04:48:42.521792	You have 30 points!	\N	\N
217	13	15	1	You've earned a Free edible at tttwww	2017-04-24 04:51:15.337399	2017-04-24 04:51:15.337399	You have 5 points!	\N	\N
218	24	15	1	You've earned a Free edible at tttwww	2017-04-24 05:17:25.257363	2017-04-24 05:17:25.257363	You have 5 points!	\N	\N
219	24	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 05:25:44.808207	2017-04-24 05:25:44.808207	Congrats!	\N	\N
220	41	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 05:28:56.548351	2017-04-24 05:28:56.548351	Congrats!	\N	\N
221	41	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 05:30:19.006564	2017-04-24 05:30:19.006564	You have 5 points!	\N	\N
222	41	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 05:30:39.846584	2017-04-24 05:30:39.846584	Congrats!	\N	\N
223	41	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:31:42.731092	2017-04-24 05:31:42.731092	You have 91 points!	\N	\N
224	41	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 05:31:52.01283	2017-04-24 05:31:52.01283	You have 5 points!	\N	\N
225	41	15	10	You claimed your Free Gram at Test Vishnu	2017-04-24 05:31:52.046182	2017-04-24 05:31:52.046182	Congrats!	\N	\N
226	81	15	7	Test Vishnu	2017-04-24 05:32:39.977824	2017-04-24 05:32:39.977824	You signed up for Reefer!	\N	\N
227	81	15	5	Test Vishnu	2017-04-24 05:32:39.987797	2017-04-24 05:32:39.987797	You visited	\N	\N
228	32	15	5	Test Vishnu	2017-04-24 05:33:32.414708	2017-04-24 05:33:32.414708	You visited	\N	\N
229	32	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 05:33:36.885789	2017-04-24 05:33:36.885789	Congrats!	\N	\N
230	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:14.353108	2017-04-24 05:34:14.353108	You have 91 points!	\N	\N
231	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:14.436635	2017-04-24 05:34:14.436635	You have 91 points!	\N	\N
232	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:14.524016	2017-04-24 05:34:14.524016	You have 91 points!	\N	\N
233	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:17.102092	2017-04-24 05:34:17.102092	You have 91 points!	\N	\N
234	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:17.165492	2017-04-24 05:34:17.165492	You have 91 points!	\N	\N
235	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:17.259457	2017-04-24 05:34:17.259457	You have 91 points!	\N	\N
236	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:17.370917	2017-04-24 05:34:17.370917	You have 91 points!	\N	\N
237	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:17.476927	2017-04-24 05:34:17.476927	You have 91 points!	\N	\N
238	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:17.515585	2017-04-24 05:34:17.515585	You have 91 points!	\N	\N
239	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:20.722028	2017-04-24 05:34:20.722028	You have 91 points!	\N	\N
240	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:20.806339	2017-04-24 05:34:20.806339	You have 91 points!	\N	\N
241	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:20.82064	2017-04-24 05:34:20.82064	You have 91 points!	\N	\N
242	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:20.836255	2017-04-24 05:34:20.836255	You have 91 points!	\N	\N
243	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:20.839472	2017-04-24 05:34:20.839472	You have 91 points!	\N	\N
244	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:20.854013	2017-04-24 05:34:20.854013	You have 91 points!	\N	\N
245	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:25.434619	2017-04-24 05:34:25.434619	You have 91 points!	\N	\N
246	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:25.5182	2017-04-24 05:34:25.5182	You have 91 points!	\N	\N
247	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:25.521755	2017-04-24 05:34:25.521755	You have 91 points!	\N	\N
248	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:25.537821	2017-04-24 05:34:25.537821	You have 91 points!	\N	\N
249	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:25.555052	2017-04-24 05:34:25.555052	You have 91 points!	\N	\N
250	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:25.559194	2017-04-24 05:34:25.559194	You have 91 points!	\N	\N
251	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:28.492601	2017-04-24 05:34:28.492601	You have 91 points!	\N	\N
252	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:28.519524	2017-04-24 05:34:28.519524	You have 91 points!	\N	\N
253	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:28.572469	2017-04-24 05:34:28.572469	You have 91 points!	\N	\N
254	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:28.584515	2017-04-24 05:34:28.584515	You have 91 points!	\N	\N
255	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:28.593265	2017-04-24 05:34:28.593265	You have 91 points!	\N	\N
256	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:28.606077	2017-04-24 05:34:28.606077	You have 91 points!	\N	\N
257	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:31.879296	2017-04-24 05:34:31.879296	You have 91 points!	\N	\N
258	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:31.91063	2017-04-24 05:34:31.91063	You have 91 points!	\N	\N
264	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:34.229467	2017-04-24 05:34:34.229467	You have 91 points!	\N	\N
269	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:40.746215	2017-04-24 05:34:40.746215	You have 91 points!	\N	\N
278	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:43.601651	2017-04-24 05:34:43.601651	You have 91 points!	\N	\N
284	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:46.120174	2017-04-24 05:34:46.120174	You have 91 points!	\N	\N
289	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:48.232517	2017-04-24 05:34:48.232517	You have 91 points!	\N	\N
296	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:51.7061	2017-04-24 05:34:51.7061	You have 91 points!	\N	\N
302	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:55.730604	2017-04-24 05:34:55.730604	You have 91 points!	\N	\N
308	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:56.316564	2017-04-24 05:34:56.316564	You have 91 points!	\N	\N
316	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:59.581062	2017-04-24 05:34:59.581062	You have 91 points!	\N	\N
318	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:02.900629	2017-04-24 05:35:02.900629	You have 91 points!	\N	\N
324	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:06.018811	2017-04-24 05:35:06.018811	You have 91 points!	\N	\N
331	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:08.025351	2017-04-24 05:35:08.025351	You have 91 points!	\N	\N
259	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:31.941663	2017-04-24 05:34:31.941663	You have 91 points!	\N	\N
265	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:34.236291	2017-04-24 05:34:34.236291	You have 91 points!	\N	\N
270	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:40.833721	2017-04-24 05:34:40.833721	You have 91 points!	\N	\N
279	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:43.616889	2017-04-24 05:34:43.616889	You have 91 points!	\N	\N
285	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:46.147124	2017-04-24 05:34:46.147124	You have 91 points!	\N	\N
290	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:48.246594	2017-04-24 05:34:48.246594	You have 91 points!	\N	\N
297	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:51.71007	2017-04-24 05:34:51.71007	You have 91 points!	\N	\N
305	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:56.279849	2017-04-24 05:34:56.279849	You have 91 points!	\N	\N
311	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:59.500845	2017-04-24 05:34:59.500845	You have 91 points!	\N	\N
320	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:02.95599	2017-04-24 05:35:02.95599	You have 91 points!	\N	\N
325	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:06.067177	2017-04-24 05:35:06.067177	You have 91 points!	\N	\N
332	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:08.065768	2017-04-24 05:35:08.065768	You have 91 points!	\N	\N
260	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:31.970961	2017-04-24 05:34:31.970961	You have 91 points!	\N	\N
266	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:34.25283	2017-04-24 05:34:34.25283	You have 91 points!	\N	\N
272	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:40.874412	2017-04-24 05:34:40.874412	You have 91 points!	\N	\N
280	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:43.626798	2017-04-24 05:34:43.626798	You have 91 points!	\N	\N
286	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:46.157447	2017-04-24 05:34:46.157447	You have 91 points!	\N	\N
292	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:48.391163	2017-04-24 05:34:48.391163	You have 91 points!	\N	\N
298	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:51.732739	2017-04-24 05:34:51.732739	You have 91 points!	\N	\N
299	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:54.469335	2017-04-24 05:34:54.469335	You have 91 points!	\N	\N
303	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:55.727596	2017-04-24 05:34:55.727596	You have 91 points!	\N	\N
309	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:56.344046	2017-04-24 05:34:56.344046	You have 91 points!	\N	\N
314	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:59.550862	2017-04-24 05:34:59.550862	You have 91 points!	\N	\N
321	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:02.978626	2017-04-24 05:35:02.978626	You have 91 points!	\N	\N
326	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:06.097155	2017-04-24 05:35:06.097155	You have 91 points!	\N	\N
333	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:08.131834	2017-04-24 05:35:08.131834	You have 91 points!	\N	\N
261	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:31.995321	2017-04-24 05:34:31.995321	You have 91 points!	\N	\N
267	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:34.271623	2017-04-24 05:34:34.271623	You have 91 points!	\N	\N
273	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:40.891265	2017-04-24 05:34:40.891265	You have 91 points!	\N	\N
276	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:43.573203	2017-04-24 05:34:43.573203	You have 91 points!	\N	\N
281	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:46.037924	2017-04-24 05:34:46.037924	You have 91 points!	\N	\N
287	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:48.13311	2017-04-24 05:34:48.13311	You have 91 points!	\N	\N
293	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:51.559723	2017-04-24 05:34:51.559723	You have 91 points!	\N	\N
301	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:55.725654	2017-04-24 05:34:55.725654	You have 91 points!	\N	\N
306	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:56.291473	2017-04-24 05:34:56.291473	You have 91 points!	\N	\N
315	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:59.564367	2017-04-24 05:34:59.564367	You have 91 points!	\N	\N
317	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:02.865116	2017-04-24 05:35:02.865116	You have 91 points!	\N	\N
323	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:05.979005	2017-04-24 05:35:05.979005	You have 91 points!	\N	\N
329	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:07.981321	2017-04-24 05:35:07.981321	You have 91 points!	\N	\N
262	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:32.000499	2017-04-24 05:34:32.000499	You have 91 points!	\N	\N
268	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:34.281577	2017-04-24 05:34:34.281577	You have 91 points!	\N	\N
274	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:40.901155	2017-04-24 05:34:40.901155	You have 91 points!	\N	\N
277	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:43.586396	2017-04-24 05:34:43.586396	You have 91 points!	\N	\N
282	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:46.08502	2017-04-24 05:34:46.08502	You have 91 points!	\N	\N
288	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:48.144751	2017-04-24 05:34:48.144751	You have 91 points!	\N	\N
295	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:51.689002	2017-04-24 05:34:51.689002	You have 91 points!	\N	\N
304	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:55.732793	2017-04-24 05:34:55.732793	You have 91 points!	\N	\N
310	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:56.380644	2017-04-24 05:34:56.380644	You have 91 points!	\N	\N
313	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:59.527854	2017-04-24 05:34:59.527854	You have 91 points!	\N	\N
322	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:02.987783	2017-04-24 05:35:02.987783	You have 91 points!	\N	\N
328	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:06.178212	2017-04-24 05:35:06.178212	You have 91 points!	\N	\N
334	13	15	3	You've earned a Free Eighth at Test Vishnu	2017-04-24 05:35:08.189503	2017-04-24 05:35:08.189503	You have 150 points!	\N	\N
263	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:34.188633	2017-04-24 05:34:34.188633	You have 91 points!	\N	\N
294	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:51.594913	2017-04-24 05:34:51.594913	You have 91 points!	\N	\N
307	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:56.293707	2017-04-24 05:34:56.293707	You have 91 points!	\N	\N
271	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:40.848287	2017-04-24 05:34:40.848287	You have 91 points!	\N	\N
300	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:54.481003	2017-04-24 05:34:54.481003	You have 91 points!	\N	\N
275	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:43.517839	2017-04-24 05:34:43.517839	You have 91 points!	\N	\N
327	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:06.133525	2017-04-24 05:35:06.133525	You have 91 points!	\N	\N
283	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:46.110203	2017-04-24 05:34:46.110203	You have 91 points!	\N	\N
312	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:59.509655	2017-04-24 05:34:59.509655	You have 91 points!	\N	\N
330	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:07.995759	2017-04-24 05:35:07.995759	You have 91 points!	\N	\N
291	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:34:48.347507	2017-04-24 05:34:48.347507	You have 91 points!	\N	\N
319	13	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:35:02.917691	2017-04-24 05:35:02.917691	You have 91 points!	\N	\N
335	41	15	10	You claimed your Free Eighth at Test Vishnu	2017-04-24 05:37:27.561332	2017-04-24 05:37:27.561332	Congrats!	\N	\N
336	41	15	10	You claimed your Free Gram at Test Vishnu	2017-04-24 05:38:21.870169	2017-04-24 05:38:21.870169	Congrats!	\N	\N
337	41	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 05:39:01.267262	2017-04-24 05:39:01.267262	Congrats!	\N	\N
338	32	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 05:39:58.626146	2017-04-24 05:39:58.626146	You have 20 points!	\N	\N
339	32	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 05:39:58.630925	2017-04-24 05:39:58.630925	You have 20 points!	\N	\N
340	24	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 05:44:30.481852	2017-04-24 05:44:30.481852	You have 91 points!	\N	\N
341	24	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 05:44:59.948907	2017-04-24 05:44:59.948907	You have 20 points!	\N	\N
342	24	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 05:45:00.046234	2017-04-24 05:45:00.046234	Congrats!	\N	\N
343	41	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 05:48:08.711862	2017-04-24 05:48:08.711862	You have 20 points!	\N	\N
344	41	15	10	You claimed your Free Eighth at Test Vishnu	2017-04-24 05:48:08.825279	2017-04-24 05:48:08.825279	Congrats!	\N	\N
345	24	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 05:48:59.390276	2017-04-24 05:48:59.390276	You have 20 points!	\N	\N
346	41	15	10	You claimed your Free Eighth at Test Vishnu	2017-04-24 05:49:01.766622	2017-04-24 05:49:01.766622	Congrats!	\N	\N
347	41	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 05:49:36.415296	2017-04-24 05:49:36.415296	Congrats!	\N	\N
348	82	15	7	Test Vishnu	2017-04-24 05:56:11.36044	2017-04-24 05:56:11.36044	You signed up for Reefer!	\N	\N
349	82	15	5	Test Vishnu	2017-04-24 05:56:11.371401	2017-04-24 05:56:11.371401	You visited	\N	\N
350	82	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 05:56:16.254947	2017-04-24 05:56:16.254947	Congrats!	\N	\N
351	82	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 05:57:02.51306	2017-04-24 05:57:02.51306	Congrats!	\N	\N
352	24	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 06:07:53.125518	2017-04-24 06:07:53.125518	You have 92 points!	\N	\N
353	24	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 06:07:56.041481	2017-04-24 06:07:56.041481	You have 92 points!	\N	\N
354	83	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 06:09:45.032061	2017-04-24 06:09:45.032061	You have 5 points!	\N	\N
355	83	15	7	Test Vishnu	2017-04-24 06:09:45.047942	2017-04-24 06:09:45.047942	You signed up for Reefer!	\N	\N
356	83	15	5	Test Vishnu	2017-04-24 06:09:45.058342	2017-04-24 06:09:45.058342	You visited	\N	\N
357	83	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 06:09:51.710737	2017-04-24 06:09:51.710737	Congrats!	\N	\N
358	41	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 06:51:38.882253	2017-04-24 06:51:38.882253	You have 5 points!	\N	\N
359	41	15	10	You claimed your Free Gram at Test Vishnu	2017-04-24 06:51:38.913149	2017-04-24 06:51:38.913149	Congrats!	\N	\N
360	41	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 06:52:12.241146	2017-04-24 06:52:12.241146	Congrats!	\N	\N
361	24	15	10	You claimed your Free Eighth at Test Vishnu	2017-04-24 06:52:45.433314	2017-04-24 06:52:45.433314	Congrats!	\N	\N
362	24	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 06:53:11.767173	2017-04-24 06:53:11.767173	Congrats!	\N	\N
363	24	15	10	You claimed your Free Eighth at Test Vishnu	2017-04-24 06:53:39.184474	2017-04-24 06:53:39.184474	Congrats!	\N	\N
364	24	15	10	You claimed your Free Gram at Test Vishnu	2017-04-24 06:54:14.182313	2017-04-24 06:54:14.182313	Congrats!	\N	\N
365	24	15	10	You claimed your Free Gram at Test Vishnu	2017-04-24 06:54:33.283683	2017-04-24 06:54:33.283683	Congrats!	\N	\N
366	24	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 06:55:12.016264	2017-04-24 06:55:12.016264	You have 92 points!	\N	\N
367	24	15	10	You claimed your Free Gram at Test Vishnu	2017-04-24 06:55:12.058095	2017-04-24 06:55:12.058095	Congrats!	\N	\N
368	24	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 06:55:36.196234	2017-04-24 06:55:36.196234	You have 5 points!	\N	\N
369	24	15	10	You claimed your Free Gram at Test Vishnu	2017-04-24 06:55:36.224928	2017-04-24 06:55:36.224928	Congrats!	\N	\N
370	12	15	5	Test Vishnu	2017-04-24 06:56:14.080943	2017-04-24 06:56:14.080943	You visited	\N	\N
371	12	15	5	Test Vishnu	2017-04-24 06:56:35.067639	2017-04-24 06:56:35.067639	You visited	\N	\N
372	32	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 06:57:10.130998	2017-04-24 06:57:10.130998	You have 92 points!	\N	\N
373	32	15	10	You claimed your Free Eighth at Test Vishnu	2017-04-24 06:57:10.183128	2017-04-24 06:57:10.183128	Congrats!	\N	\N
374	46	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 06:57:32.640168	2017-04-24 06:57:32.640168	You have 5 points!	\N	\N
375	46	15	5	Test Vishnu	2017-04-24 06:57:32.652288	2017-04-24 06:57:32.652288	You visited	\N	\N
376	46	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 06:57:35.642849	2017-04-24 06:57:35.642849	Congrats!	\N	\N
377	46	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 06:58:08.558108	2017-04-24 06:58:08.558108	You have 92 points!	\N	\N
378	46	15	10	You claimed your Free Gram at Test Vishnu	2017-04-24 06:58:08.596656	2017-04-24 06:58:08.596656	Congrats!	\N	\N
379	46	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 06:58:27.947779	2017-04-24 06:58:27.947779	You have 5 points!	\N	\N
380	46	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 06:58:27.995163	2017-04-24 06:58:27.995163	Congrats!	\N	\N
381	13	15	3	You've earned a Free Eighth at Test Vishnu	2017-04-24 07:00:34.069729	2017-04-24 07:00:34.069729	You have 150 points!	\N	\N
382	13	15	5	Test Vishnu	2017-04-24 07:00:34.095969	2017-04-24 07:00:34.095969	You visited	\N	\N
383	13	15	10	You claimed your Free Gram at Test Vishnu	2017-04-24 07:00:37.73862	2017-04-24 07:00:37.73862	Congrats!	\N	\N
384	84	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 07:15:14.21315	2017-04-24 07:15:14.21315	You have 5 points!	\N	\N
385	84	15	7	Test Vishnu	2017-04-24 07:15:14.228756	2017-04-24 07:15:14.228756	You signed up for Reefer!	\N	\N
386	84	15	5	Test Vishnu	2017-04-24 07:15:14.239181	2017-04-24 07:15:14.239181	You visited	\N	\N
387	85	18	7	Test Vishnu	2017-04-24 07:17:04.079933	2017-04-24 07:17:04.079933	You signed up for Reefer!	\N	\N
388	85	18	5	Test Vishnu	2017-04-24 07:17:04.090063	2017-04-24 07:17:04.090063	You visited	\N	\N
389	12	15	5	Test Vishnu	2017-04-24 07:17:14.163403	2017-04-24 07:17:14.163403	You visited	\N	\N
390	86	18	7	Test Vishnu	2017-04-24 07:17:49.938559	2017-04-24 07:17:49.938559	You signed up for Reefer!	\N	\N
391	86	18	5	Test Vishnu	2017-04-24 07:17:49.948243	2017-04-24 07:17:49.948243	You visited	\N	\N
392	12	15	5	Test Vishnu	2017-04-24 07:21:28.148585	2017-04-24 07:21:28.148585	You visited	\N	\N
393	87	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 07:22:45.572181	2017-04-24 07:22:45.572181	You have 5 points!	\N	\N
394	87	15	7	Test Vishnu	2017-04-24 07:22:45.589949	2017-04-24 07:22:45.589949	You signed up for Reefer!	\N	\N
395	87	15	5	Test Vishnu	2017-04-24 07:22:45.600047	2017-04-24 07:22:45.600047	You visited	\N	\N
396	33	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 07:23:00.328273	2017-04-24 07:23:00.328273	You have 5 points!	\N	\N
397	33	15	5	Test Vishnu	2017-04-24 07:23:00.345949	2017-04-24 07:23:00.345949	You visited	\N	\N
402	89	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 07:23:56.583641	2017-04-24 07:23:56.583641	You have 5 points!	\N	\N
403	89	15	7	Test Vishnu	2017-04-24 07:23:56.592483	2017-04-24 07:23:56.592483	You signed up for Reefer!	\N	\N
404	89	15	5	Test Vishnu	2017-04-24 07:23:56.603734	2017-04-24 07:23:56.603734	You visited	\N	\N
425	93	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 07:38:27.950709	2017-04-24 07:38:27.950709	Congrats!	\N	\N
398	24	15	5	Test Vishnu	2017-04-24 07:23:09.808335	2017-04-24 07:23:09.808335	You visited	\N	\N
417	33	15	10	You claimed your Free Eighth at Test Vishnu	2017-04-24 07:33:59.17942	2017-04-24 07:33:59.17942	Congrats!	\N	\N
421	92	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 07:38:01.215679	2017-04-24 07:38:01.215679	Congrats!	\N	\N
399	88	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 07:23:19.433987	2017-04-24 07:23:19.433987	You have 5 points!	\N	\N
400	88	15	7	Test Vishnu	2017-04-24 07:23:19.449874	2017-04-24 07:23:19.449874	You signed up for Reefer!	\N	\N
401	88	15	5	Test Vishnu	2017-04-24 07:23:19.459755	2017-04-24 07:23:19.459755	You visited	\N	\N
411	90	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 07:31:29.343167	2017-04-24 07:31:29.343167	Congrats!	\N	\N
418	92	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 07:37:56.888166	2017-04-24 07:37:56.888166	You have 5 points!	\N	\N
419	92	15	7	Test Vishnu	2017-04-24 07:37:56.899414	2017-04-24 07:37:56.899414	You signed up for Reefer!	\N	\N
420	92	15	5	Test Vishnu	2017-04-24 07:37:56.910012	2017-04-24 07:37:56.910012	You visited	\N	\N
405	89	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 07:24:06.497804	2017-04-24 07:24:06.497804	Congrats!	\N	\N
406	32	15	2	You've earned a Free Gram at Test Vishnu	2017-04-24 07:28:48.42111	2017-04-24 07:28:48.42111	You have 92 points!	\N	\N
416	91	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 07:32:49.440308	2017-04-24 07:32:49.440308	Congrats!	\N	\N
407	90	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 07:29:20.593282	2017-04-24 07:29:20.593282	You have 5 points!	\N	\N
408	90	15	7	Test Vishnu	2017-04-24 07:29:20.609012	2017-04-24 07:29:20.609012	You signed up for Reefer!	\N	\N
409	90	15	5	Test Vishnu	2017-04-24 07:29:20.619213	2017-04-24 07:29:20.619213	You visited	\N	\N
410	90	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 07:29:26.799538	2017-04-24 07:29:26.799538	Congrats!	\N	\N
422	93	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 07:38:23.524347	2017-04-24 07:38:23.524347	You have 5 points!	\N	\N
423	93	15	7	Test Vishnu	2017-04-24 07:38:23.539252	2017-04-24 07:38:23.539252	You signed up for Reefer!	\N	\N
424	93	15	5	Test Vishnu	2017-04-24 07:38:23.549246	2017-04-24 07:38:23.549246	You visited	\N	\N
412	89	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 07:32:15.002601	2017-04-24 07:32:15.002601	Congrats!	\N	\N
413	91	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 07:32:44.44052	2017-04-24 07:32:44.44052	You have 5 points!	\N	\N
414	91	15	7	Test Vishnu	2017-04-24 07:32:44.450045	2017-04-24 07:32:44.450045	You signed up for Reefer!	\N	\N
415	91	15	5	Test Vishnu	2017-04-24 07:32:44.461273	2017-04-24 07:32:44.461273	You visited	\N	\N
426	94	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 07:39:48.281693	2017-04-24 07:39:48.281693	You have 5 points!	\N	\N
427	94	15	7	Test Vishnu	2017-04-24 07:39:48.29767	2017-04-24 07:39:48.29767	You signed up for Reefer!	\N	\N
428	94	15	5	Test Vishnu	2017-04-24 07:39:48.308524	2017-04-24 07:39:48.308524	You visited	\N	\N
429	95	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 07:40:19.582688	2017-04-24 07:40:19.582688	You have 5 points!	\N	\N
430	95	15	7	Test Vishnu	2017-04-24 07:40:19.641692	2017-04-24 07:40:19.641692	You signed up for Reefer!	\N	\N
431	95	15	5	Test Vishnu	2017-04-24 07:40:19.652972	2017-04-24 07:40:19.652972	You visited	\N	\N
432	95	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 07:40:24.095989	2017-04-24 07:40:24.095989	Congrats!	\N	\N
433	92	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 07:42:17.387442	2017-04-24 07:42:17.387442	Congrats!	\N	\N
434	96	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 07:42:46.36418	2017-04-24 07:42:46.36418	You have 5 points!	\N	\N
435	96	15	7	Test Vishnu	2017-04-24 07:42:46.373269	2017-04-24 07:42:46.373269	You signed up for Reefer!	\N	\N
436	96	15	5	Test Vishnu	2017-04-24 07:42:46.382953	2017-04-24 07:42:46.382953	You visited	\N	\N
437	96	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 07:42:51.025009	2017-04-24 07:42:51.025009	Congrats!	\N	\N
438	56	15	5	Test Vishnu	2017-04-24 07:45:33.30191	2017-04-24 07:45:33.30191	You visited	\N	\N
439	97	15	7	Test Vishnu	2017-04-24 07:46:34.339112	2017-04-24 07:46:34.339112	You signed up for Reefer!	\N	\N
440	97	15	5	Test Vishnu	2017-04-24 07:46:34.347956	2017-04-24 07:46:34.347956	You visited	\N	\N
441	98	15	7	Test Vishnu	2017-04-24 07:49:24.646592	2017-04-24 07:49:24.646592	You signed up for Reefer!	\N	\N
442	98	15	5	Test Vishnu	2017-04-24 07:49:24.6572	2017-04-24 07:49:24.6572	You visited	\N	\N
443	99	15	7	Test Vishnu	2017-04-24 07:50:43.081721	2017-04-24 07:50:43.081721	You signed up for Reefer!	\N	\N
444	99	15	5	Test Vishnu	2017-04-24 07:50:43.090471	2017-04-24 07:50:43.090471	You visited	\N	\N
445	100	15	7	Test Vishnu	2017-04-24 07:52:01.249983	2017-04-24 07:52:01.249983	You signed up for Reefer!	\N	\N
446	100	15	5	Test Vishnu	2017-04-24 07:52:01.26071	2017-04-24 07:52:01.26071	You visited	\N	\N
447	101	18	7	Test Vishnu	2017-04-24 07:55:01.072482	2017-04-24 07:55:01.072482	You signed up for Reefer!	\N	\N
448	101	18	5	Test Vishnu	2017-04-24 07:55:01.082246	2017-04-24 07:55:01.082246	You visited	\N	\N
449	102	18	7	Test Vishnu	2017-04-24 07:55:46.636654	2017-04-24 07:55:46.636654	You signed up for Reefer!	\N	\N
450	102	18	5	Test Vishnu	2017-04-24 07:55:46.646252	2017-04-24 07:55:46.646252	You visited	\N	\N
451	103	18	7	Test Vishnu	2017-04-24 07:58:16.253851	2017-04-24 07:58:16.253851	You signed up for Reefer!	\N	\N
452	103	18	5	Test Vishnu	2017-04-24 07:58:16.260758	2017-04-24 07:58:16.260758	You visited	\N	\N
453	104	18	7	Test Vishnu	2017-04-24 07:59:58.658625	2017-04-24 07:59:58.658625	You signed up for Reefer!	\N	\N
454	104	18	5	Test Vishnu	2017-04-24 07:59:58.67152	2017-04-24 07:59:58.67152	You visited	\N	\N
455	105	18	7	Test Vishnu	2017-04-24 08:01:08.949838	2017-04-24 08:01:08.949838	You signed up for Reefer!	\N	\N
456	105	18	5	Test Vishnu	2017-04-24 08:01:08.95705	2017-04-24 08:01:08.95705	You visited	\N	\N
457	106	18	7	Test Vishnu	2017-04-24 08:01:44.666962	2017-04-24 08:01:44.666962	You signed up for Reefer!	\N	\N
458	106	18	5	Test Vishnu	2017-04-24 08:01:44.679823	2017-04-24 08:01:44.679823	You visited	\N	\N
459	107	18	7	Test Vishnu	2017-04-24 08:03:36.782376	2017-04-24 08:03:36.782376	You signed up for Reefer!	\N	\N
460	107	18	5	Test Vishnu	2017-04-24 08:03:36.793841	2017-04-24 08:03:36.793841	You visited	\N	\N
461	108	15	7	Test Vishnu	2017-04-24 08:06:58.828867	2017-04-24 08:06:58.828867	You signed up for Reefer!	\N	\N
462	108	15	5	Test Vishnu	2017-04-24 08:06:58.846003	2017-04-24 08:06:58.846003	You visited	\N	\N
463	109	15	7	Test Vishnu	2017-04-24 08:08:55.476056	2017-04-24 08:08:55.476056	You signed up for Reefer!	\N	\N
464	109	15	5	Test Vishnu	2017-04-24 08:08:55.485168	2017-04-24 08:08:55.485168	You visited	\N	\N
465	110	15	7	Test Vishnu	2017-04-24 08:15:46.907107	2017-04-24 08:15:46.907107	You signed up for Reefer!	\N	\N
466	110	15	5	Test Vishnu	2017-04-24 08:15:46.9123	2017-04-24 08:15:46.9123	You visited	\N	\N
467	111	15	7	Test Vishnu	2017-04-24 08:19:29.562148	2017-04-24 08:19:29.562148	You signed up for Reefer!	\N	\N
468	111	15	5	Test Vishnu	2017-04-24 08:19:29.573149	2017-04-24 08:19:29.573149	You visited	\N	\N
469	91	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 08:24:46.654309	2017-04-24 08:24:46.654309	Congrats!	\N	\N
470	98	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 08:26:00.330475	2017-04-24 08:26:00.330475	You have 5 points!	\N	\N
471	98	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 08:26:00.381404	2017-04-24 08:26:00.381404	Congrats!	\N	\N
472	112	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 08:26:50.998908	2017-04-24 08:26:50.998908	You have 5 points!	\N	\N
473	112	15	7	Test Vishnu	2017-04-24 08:26:51.017714	2017-04-24 08:26:51.017714	You signed up for Reefer!	\N	\N
474	112	15	5	Test Vishnu	2017-04-24 08:26:51.028771	2017-04-24 08:26:51.028771	You visited	\N	\N
475	112	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 08:26:55.597761	2017-04-24 08:26:55.597761	Congrats!	\N	\N
476	112	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 08:27:08.625458	2017-04-24 08:27:08.625458	Congrats!	\N	\N
477	111	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 08:31:14.808366	2017-04-24 08:31:14.808366	You have 5 points!	\N	\N
478	111	15	5	Test Vishnu	2017-04-24 08:31:14.840692	2017-04-24 08:31:14.840692	You visited	\N	\N
479	13	15	10	You claimed your Free Gram at Test Vishnu	2017-04-24 08:36:30.534738	2017-04-24 08:36:30.534738	Congrats!	\N	\N
480	24	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 10:02:56.563061	2017-04-24 10:02:56.563061	Congrats!	\N	\N
481	24	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 10:19:52.426073	2017-04-24 10:19:52.426073	You have 5 points!	\N	\N
482	24	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 10:19:52.468755	2017-04-24 10:19:52.468755	Congrats!	\N	\N
483	113	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 10:43:10.795038	2017-04-24 10:43:10.795038	You have 5 points!	\N	\N
484	113	15	7	Test Vishnu	2017-04-24 10:43:10.806479	2017-04-24 10:43:10.806479	You signed up for Reefer!	\N	\N
485	113	15	5	Test Vishnu	2017-04-24 10:43:10.817568	2017-04-24 10:43:10.817568	You visited	\N	\N
486	113	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 10:43:23.321876	2017-04-24 10:43:23.321876	Congrats!	\N	\N
487	114	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 10:45:10.156371	2017-04-24 10:45:10.156371	You have 5 points!	\N	\N
488	114	15	7	Test Vishnu	2017-04-24 10:45:10.173474	2017-04-24 10:45:10.173474	You signed up for Reefer!	\N	\N
489	114	15	5	Test Vishnu	2017-04-24 10:45:10.183195	2017-04-24 10:45:10.183195	You visited	\N	\N
490	114	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 10:45:15.731028	2017-04-24 10:45:15.731028	Congrats!	\N	\N
491	115	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 10:46:07.511078	2017-04-24 10:46:07.511078	You have 5 points!	\N	\N
492	115	15	7	Test Vishnu	2017-04-24 10:46:07.523952	2017-04-24 10:46:07.523952	You signed up for Reefer!	\N	\N
493	115	15	5	Test Vishnu	2017-04-24 10:46:07.535228	2017-04-24 10:46:07.535228	You visited	\N	\N
499	117	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:07:44.832108	2017-04-24 11:07:44.832108	You have 5 points!	\N	\N
500	117	15	7	Test Vishnu	2017-04-24 11:07:44.843206	2017-04-24 11:07:44.843206	You signed up for Reefer!	\N	\N
501	117	15	5	Test Vishnu	2017-04-24 11:07:44.853985	2017-04-24 11:07:44.853985	You visited	\N	\N
505	117	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:10:18.058426	2017-04-24 11:10:18.058426	Congrats!	\N	\N
506	117	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:11:03.225699	2017-04-24 11:11:03.225699	You have 5 points!	\N	\N
507	117	15	5	Test Vishnu	2017-04-24 11:11:03.243125	2017-04-24 11:11:03.243125	You visited	\N	\N
510	117	15	5	Test Vishnu	2017-04-24 11:11:40.68062	2017-04-24 11:11:40.68062	You visited	\N	\N
512	118	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:13:46.946784	2017-04-24 11:13:46.946784	You have 5 points!	\N	\N
513	118	15	7	Test Vishnu	2017-04-24 11:13:46.961511	2017-04-24 11:13:46.961511	You signed up for Reefer!	\N	\N
514	118	15	5	Test Vishnu	2017-04-24 11:13:46.972478	2017-04-24 11:13:46.972478	You visited	\N	\N
494	115	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 10:46:20.229351	2017-04-24 10:46:20.229351	Congrats!	\N	\N
495	116	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:03:53.863584	2017-04-24 11:03:53.863584	You have 5 points!	\N	\N
496	116	15	7	Test Vishnu	2017-04-24 11:03:53.87941	2017-04-24 11:03:53.87941	You signed up for Reefer!	\N	\N
497	116	15	5	Test Vishnu	2017-04-24 11:03:53.889144	2017-04-24 11:03:53.889144	You visited	\N	\N
508	117	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:11:05.811679	2017-04-24 11:11:05.811679	Congrats!	\N	\N
516	118	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:14:09.869721	2017-04-24 11:14:09.869721	You have 5 points!	\N	\N
517	118	15	5	Test Vishnu	2017-04-24 11:14:09.895345	2017-04-24 11:14:09.895345	You visited	\N	\N
498	116	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:04:01.615	2017-04-24 11:04:01.615	Congrats!	\N	\N
502	117	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:08:18.016892	2017-04-24 11:08:18.016892	Congrats!	\N	\N
578	127	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:23:12.658782	2017-04-24 11:23:12.658782	Congrats!	\N	\N
503	56	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:08:36.547291	2017-04-24 11:08:36.547291	You have 5 points!	\N	\N
504	56	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:08:36.58564	2017-04-24 11:08:36.58564	Congrats!	\N	\N
522	119	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:14:38.229786	2017-04-24 11:14:38.229786	Congrats!	\N	\N
509	117	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:11:23.366789	2017-04-24 11:11:23.366789	You have 5 points!	\N	\N
511	117	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:11:43.596862	2017-04-24 11:11:43.596862	Congrats!	\N	\N
515	118	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:13:53.8554	2017-04-24 11:13:53.8554	Congrats!	\N	\N
518	118	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:14:12.632688	2017-04-24 11:14:12.632688	Congrats!	\N	\N
519	119	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:14:33.419826	2017-04-24 11:14:33.419826	You have 5 points!	\N	\N
520	119	15	7	Test Vishnu	2017-04-24 11:14:33.429476	2017-04-24 11:14:33.429476	You signed up for Reefer!	\N	\N
521	119	15	5	Test Vishnu	2017-04-24 11:14:33.441145	2017-04-24 11:14:33.441145	You visited	\N	\N
523	119	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:15:06.880157	2017-04-24 11:15:06.880157	You have 5 points!	\N	\N
524	119	15	5	Test Vishnu	2017-04-24 11:15:06.900132	2017-04-24 11:15:06.900132	You visited	\N	\N
525	119	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:15:09.825881	2017-04-24 11:15:09.825881	Congrats!	\N	\N
526	120	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:15:40.423651	2017-04-24 11:15:40.423651	You have 5 points!	\N	\N
527	120	15	7	Test Vishnu	2017-04-24 11:15:40.433625	2017-04-24 11:15:40.433625	You signed up for Reefer!	\N	\N
528	120	15	5	Test Vishnu	2017-04-24 11:15:40.443306	2017-04-24 11:15:40.443306	You visited	\N	\N
529	120	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:15:44.916909	2017-04-24 11:15:44.916909	Congrats!	\N	\N
530	120	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:16:18.224799	2017-04-24 11:16:18.224799	You have 5 points!	\N	\N
531	120	15	5	Test Vishnu	2017-04-24 11:16:18.249891	2017-04-24 11:16:18.249891	You visited	\N	\N
532	120	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:16:20.702709	2017-04-24 11:16:20.702709	Congrats!	\N	\N
533	121	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:16:54.924472	2017-04-24 11:16:54.924472	You have 5 points!	\N	\N
534	121	15	7	Test Vishnu	2017-04-24 11:16:54.941249	2017-04-24 11:16:54.941249	You signed up for Reefer!	\N	\N
535	121	15	5	Test Vishnu	2017-04-24 11:16:54.950612	2017-04-24 11:16:54.950612	You visited	\N	\N
536	121	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:17:03.160985	2017-04-24 11:17:03.160985	Congrats!	\N	\N
537	121	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:17:35.790428	2017-04-24 11:17:35.790428	You have 5 points!	\N	\N
538	121	15	5	Test Vishnu	2017-04-24 11:17:35.809369	2017-04-24 11:17:35.809369	You visited	\N	\N
539	121	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:17:38.620132	2017-04-24 11:17:38.620132	Congrats!	\N	\N
540	122	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:18:27.948058	2017-04-24 11:18:27.948058	You have 5 points!	\N	\N
541	122	15	7	Test Vishnu	2017-04-24 11:18:27.96212	2017-04-24 11:18:27.96212	You signed up for Reefer!	\N	\N
542	122	15	5	Test Vishnu	2017-04-24 11:18:27.973655	2017-04-24 11:18:27.973655	You visited	\N	\N
543	122	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:18:32.551713	2017-04-24 11:18:32.551713	Congrats!	\N	\N
544	122	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:19:08.439997	2017-04-24 11:19:08.439997	You have 5 points!	\N	\N
545	122	15	5	Test Vishnu	2017-04-24 11:19:08.463953	2017-04-24 11:19:08.463953	You visited	\N	\N
546	122	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:19:12.095245	2017-04-24 11:19:12.095245	Congrats!	\N	\N
547	123	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:19:49.837908	2017-04-24 11:19:49.837908	You have 5 points!	\N	\N
548	123	15	7	Test Vishnu	2017-04-24 11:19:49.848337	2017-04-24 11:19:49.848337	You signed up for Reefer!	\N	\N
549	123	15	5	Test Vishnu	2017-04-24 11:19:49.858873	2017-04-24 11:19:49.858873	You visited	\N	\N
550	123	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:19:54.427785	2017-04-24 11:19:54.427785	Congrats!	\N	\N
551	123	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:20:05.732816	2017-04-24 11:20:05.732816	You have 5 points!	\N	\N
552	123	15	5	Test Vishnu	2017-04-24 11:20:05.752211	2017-04-24 11:20:05.752211	You visited	\N	\N
553	123	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:20:08.373913	2017-04-24 11:20:08.373913	Congrats!	\N	\N
554	124	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:20:39.584458	2017-04-24 11:20:39.584458	You have 5 points!	\N	\N
555	124	15	7	Test Vishnu	2017-04-24 11:20:39.600877	2017-04-24 11:20:39.600877	You signed up for Reefer!	\N	\N
556	124	15	5	Test Vishnu	2017-04-24 11:20:39.611545	2017-04-24 11:20:39.611545	You visited	\N	\N
557	124	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:20:43.990018	2017-04-24 11:20:43.990018	Congrats!	\N	\N
558	124	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:21:10.170629	2017-04-24 11:21:10.170629	You have 5 points!	\N	\N
559	124	15	5	Test Vishnu	2017-04-24 11:21:10.197054	2017-04-24 11:21:10.197054	You visited	\N	\N
560	124	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:21:12.736196	2017-04-24 11:21:12.736196	Congrats!	\N	\N
561	125	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:21:28.287654	2017-04-24 11:21:28.287654	You have 5 points!	\N	\N
562	125	15	7	Test Vishnu	2017-04-24 11:21:28.300809	2017-04-24 11:21:28.300809	You signed up for Reefer!	\N	\N
563	125	15	5	Test Vishnu	2017-04-24 11:21:28.311671	2017-04-24 11:21:28.311671	You visited	\N	\N
564	125	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:21:32.795485	2017-04-24 11:21:32.795485	Congrats!	\N	\N
565	125	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:21:58.246556	2017-04-24 11:21:58.246556	You have 5 points!	\N	\N
566	125	15	5	Test Vishnu	2017-04-24 11:21:58.266589	2017-04-24 11:21:58.266589	You visited	\N	\N
567	125	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:22:00.908387	2017-04-24 11:22:00.908387	Congrats!	\N	\N
568	126	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:22:13.711126	2017-04-24 11:22:13.711126	You have 5 points!	\N	\N
569	126	15	7	Test Vishnu	2017-04-24 11:22:13.728382	2017-04-24 11:22:13.728382	You signed up for Reefer!	\N	\N
570	126	15	5	Test Vishnu	2017-04-24 11:22:13.769188	2017-04-24 11:22:13.769188	You visited	\N	\N
571	126	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:22:17.917929	2017-04-24 11:22:17.917929	Congrats!	\N	\N
572	126	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:22:31.329093	2017-04-24 11:22:31.329093	You have 5 points!	\N	\N
573	126	15	5	Test Vishnu	2017-04-24 11:22:31.346608	2017-04-24 11:22:31.346608	You visited	\N	\N
574	126	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:22:34.257948	2017-04-24 11:22:34.257948	Congrats!	\N	\N
575	127	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:23:08.471321	2017-04-24 11:23:08.471321	You have 5 points!	\N	\N
576	127	15	7	Test Vishnu	2017-04-24 11:23:08.490106	2017-04-24 11:23:08.490106	You signed up for Reefer!	\N	\N
577	127	15	5	Test Vishnu	2017-04-24 11:23:08.500139	2017-04-24 11:23:08.500139	You visited	\N	\N
775	52	15	5	Test Vishnu	2017-05-01 05:14:39.570763	2017-05-01 05:14:39.570763	You visited	\N	\N
579	127	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:23:38.203817	2017-04-24 11:23:38.203817	You have 5 points!	\N	\N
580	127	15	5	Test Vishnu	2017-04-24 11:23:38.223192	2017-04-24 11:23:38.223192	You visited	\N	\N
581	127	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:23:40.961999	2017-04-24 11:23:40.961999	Congrats!	\N	\N
582	128	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:24:11.854521	2017-04-24 11:24:11.854521	You have 5 points!	\N	\N
583	128	15	7	Test Vishnu	2017-04-24 11:24:11.863569	2017-04-24 11:24:11.863569	You signed up for Reefer!	\N	\N
584	128	15	5	Test Vishnu	2017-04-24 11:24:11.87365	2017-04-24 11:24:11.87365	You visited	\N	\N
585	128	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:24:16.085582	2017-04-24 11:24:16.085582	Congrats!	\N	\N
586	128	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:24:40.469134	2017-04-24 11:24:40.469134	You have 5 points!	\N	\N
587	128	15	5	Test Vishnu	2017-04-24 11:24:40.520941	2017-04-24 11:24:40.520941	You visited	\N	\N
593	129	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:25:44.123697	2017-04-24 11:25:44.123697	You have 5 points!	\N	\N
594	129	15	5	Test Vishnu	2017-04-24 11:25:44.148305	2017-04-24 11:25:44.148305	You visited	\N	\N
600	130	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:26:43.384113	2017-04-24 11:26:43.384113	You have 5 points!	\N	\N
601	130	15	5	Test Vishnu	2017-04-24 11:26:43.415827	2017-04-24 11:26:43.415827	You visited	\N	\N
613	132	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:28:24.456851	2017-04-24 11:28:24.456851	Congrats!	\N	\N
620	133	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:29:56.827023	2017-04-24 11:29:56.827023	Congrats!	\N	\N
588	128	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:24:43.123943	2017-04-24 11:24:43.123943	Congrats!	\N	\N
589	129	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:25:12.685693	2017-04-24 11:25:12.685693	You have 5 points!	\N	\N
590	129	15	7	Test Vishnu	2017-04-24 11:25:12.700265	2017-04-24 11:25:12.700265	You signed up for Reefer!	\N	\N
591	129	15	5	Test Vishnu	2017-04-24 11:25:12.710435	2017-04-24 11:25:12.710435	You visited	\N	\N
592	129	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:25:17.099389	2017-04-24 11:25:17.099389	Congrats!	\N	\N
595	129	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:25:47.147655	2017-04-24 11:25:47.147655	Congrats!	\N	\N
599	130	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:26:16.629075	2017-04-24 11:26:16.629075	Congrats!	\N	\N
609	131	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:27:48.430593	2017-04-24 11:27:48.430593	Congrats!	\N	\N
596	130	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:26:11.830602	2017-04-24 11:26:11.830602	You have 5 points!	\N	\N
597	130	15	7	Test Vishnu	2017-04-24 11:26:11.861563	2017-04-24 11:26:11.861563	You signed up for Reefer!	\N	\N
598	130	15	5	Test Vishnu	2017-04-24 11:26:11.919652	2017-04-24 11:26:11.919652	You visited	\N	\N
610	132	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:28:20.343424	2017-04-24 11:28:20.343424	You have 5 points!	\N	\N
611	132	15	7	Test Vishnu	2017-04-24 11:28:20.362264	2017-04-24 11:28:20.362264	You signed up for Reefer!	\N	\N
612	132	15	5	Test Vishnu	2017-04-24 11:28:20.373123	2017-04-24 11:28:20.373123	You visited	\N	\N
617	133	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:29:52.55358	2017-04-24 11:29:52.55358	You have 5 points!	\N	\N
618	133	15	7	Test Vishnu	2017-04-24 11:29:52.56255	2017-04-24 11:29:52.56255	You signed up for Reefer!	\N	\N
619	133	15	5	Test Vishnu	2017-04-24 11:29:52.573773	2017-04-24 11:29:52.573773	You visited	\N	\N
602	130	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:26:46.437085	2017-04-24 11:26:46.437085	Congrats!	\N	\N
603	131	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:27:14.626318	2017-04-24 11:27:14.626318	You have 5 points!	\N	\N
604	131	15	7	Test Vishnu	2017-04-24 11:27:14.64345	2017-04-24 11:27:14.64345	You signed up for Reefer!	\N	\N
605	131	15	5	Test Vishnu	2017-04-24 11:27:14.653733	2017-04-24 11:27:14.653733	You visited	\N	\N
616	132	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:28:59.055144	2017-04-24 11:28:59.055144	Congrats!	\N	\N
623	133	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:30:27.980109	2017-04-24 11:30:27.980109	Congrats!	\N	\N
606	131	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:27:19.65457	2017-04-24 11:27:19.65457	Congrats!	\N	\N
607	131	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:27:45.423828	2017-04-24 11:27:45.423828	You have 5 points!	\N	\N
608	131	15	5	Test Vishnu	2017-04-24 11:27:45.450227	2017-04-24 11:27:45.450227	You visited	\N	\N
614	132	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:28:56.56488	2017-04-24 11:28:56.56488	You have 5 points!	\N	\N
615	132	15	5	Test Vishnu	2017-04-24 11:28:56.579947	2017-04-24 11:28:56.579947	You visited	\N	\N
621	133	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:30:23.079427	2017-04-24 11:30:23.079427	You have 5 points!	\N	\N
622	133	15	5	Test Vishnu	2017-04-24 11:30:23.106743	2017-04-24 11:30:23.106743	You visited	\N	\N
624	134	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:30:52.917021	2017-04-24 11:30:52.917021	You have 5 points!	\N	\N
625	134	15	7	Test Vishnu	2017-04-24 11:30:52.935892	2017-04-24 11:30:52.935892	You signed up for Reefer!	\N	\N
626	134	15	5	Test Vishnu	2017-04-24 11:30:52.9458	2017-04-24 11:30:52.9458	You visited	\N	\N
627	134	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:30:59.504345	2017-04-24 11:30:59.504345	Congrats!	\N	\N
628	135	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:31:17.153858	2017-04-24 11:31:17.153858	You have 5 points!	\N	\N
629	135	15	7	Test Vishnu	2017-04-24 11:31:17.163896	2017-04-24 11:31:17.163896	You signed up for Reefer!	\N	\N
630	135	15	5	Test Vishnu	2017-04-24 11:31:17.174293	2017-04-24 11:31:17.174293	You visited	\N	\N
631	135	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 11:31:22.627899	2017-04-24 11:31:22.627899	Congrats!	\N	\N
632	124	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:34:32.009612	2017-04-24 11:34:32.009612	You have 5 points!	\N	\N
633	129	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 11:42:04.631964	2017-04-24 11:42:04.631964	You have 5 points!	\N	\N
634	136	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 12:06:12.382594	2017-04-24 12:06:12.382594	You have 5 points!	\N	\N
635	136	15	7	Test Vishnu	2017-04-24 12:06:12.392596	2017-04-24 12:06:12.392596	You signed up for Reefer!	\N	\N
636	136	15	5	Test Vishnu	2017-04-24 12:06:12.40363	2017-04-24 12:06:12.40363	You visited	\N	\N
637	136	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 12:06:23.547253	2017-04-24 12:06:23.547253	Congrats!	\N	\N
638	24	15	3	You've earned a Free Eighth at Test Vishnu	2017-04-24 12:13:34.028444	2017-04-24 12:13:34.028444	You have 150 points!	\N	\N
639	24	15	5	Test Vishnu	2017-04-24 12:13:34.049089	2017-04-24 12:13:34.049089	You visited	\N	\N
640	24	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 12:13:40.5537	2017-04-24 12:13:40.5537	You have 5 points!	\N	\N
641	24	15	10	You claimed your Free Gram at Test Vishnu	2017-04-24 12:13:40.596698	2017-04-24 12:13:40.596698	Congrats!	\N	\N
642	24	15	5	Test Vishnu	2017-04-24 12:14:17.549969	2017-04-24 12:14:17.549969	You visited	\N	\N
643	24	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 12:14:21.298099	2017-04-24 12:14:21.298099	Congrats!	\N	\N
644	120	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 12:46:33.371349	2017-04-24 12:46:33.371349	You have 5 points!	\N	\N
645	137	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 18:28:37.809293	2017-04-24 18:28:37.809293	You have 5 points!	\N	\N
646	137	15	7	Test Vishnu	2017-04-24 18:28:37.843556	2017-04-24 18:28:37.843556	You signed up for Reefer!	\N	\N
647	137	15	5	Test Vishnu	2017-04-24 18:28:37.85313	2017-04-24 18:28:37.85313	You visited	\N	\N
648	13	15	10	You claimed your Free edible at Test Vishnu	2017-04-24 18:29:37.373068	2017-04-24 18:29:37.373068	Congrats!	\N	\N
649	138	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 18:31:48.420821	2017-04-24 18:31:48.420821	You have 5 points!	\N	\N
650	138	15	7	Test Vishnu	2017-04-24 18:31:48.432568	2017-04-24 18:31:48.432568	You signed up for Reefer!	\N	\N
651	138	15	5	Test Vishnu	2017-04-24 18:31:48.443398	2017-04-24 18:31:48.443398	You visited	\N	\N
652	139	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 18:37:52.290997	2017-04-24 18:37:52.290997	You have 5 points!	\N	\N
653	139	15	7	Test Vishnu	2017-04-24 18:37:52.308722	2017-04-24 18:37:52.308722	You signed up for Reefer!	\N	\N
654	139	15	5	Test Vishnu	2017-04-24 18:37:52.318948	2017-04-24 18:37:52.318948	You visited	\N	\N
655	140	15	1	You've earned a Free edible at Test Vishnu	2017-04-24 18:45:14.219311	2017-04-24 18:45:14.219311	You have 5 points!	\N	\N
656	140	15	7	Test Vishnu	2017-04-24 18:45:14.238582	2017-04-24 18:45:14.238582	You signed up for Reefer!	\N	\N
657	140	15	5	Test Vishnu	2017-04-24 18:45:14.249068	2017-04-24 18:45:14.249068	You visited	\N	\N
658	24	15	10	You claimed your Free Gram at Test Vishnu	2017-04-25 04:03:09.542573	2017-04-25 04:03:09.542573	Congrats!	\N	\N
659	24	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 04:25:45.086474	2017-04-25 04:25:45.086474	You have 5 points!	\N	\N
660	24	15	10	You claimed your Free Gram at Test Vishnu	2017-04-25 04:25:45.130472	2017-04-25 04:25:45.130472	Congrats!	\N	\N
661	121	15	10	You claimed your Free edible at Test Vishnu	2017-04-25 04:31:37.58471	2017-04-25 04:31:37.58471	Congrats!	\N	\N
662	141	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 04:32:47.435959	2017-04-25 04:32:47.435959	You have 5 points!	\N	\N
663	141	15	7	Test Vishnu	2017-04-25 04:32:47.494115	2017-04-25 04:32:47.494115	You signed up for Reefer!	\N	\N
664	141	15	5	Test Vishnu	2017-04-25 04:32:47.504393	2017-04-25 04:32:47.504393	You visited	\N	\N
665	43	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 04:35:07.635376	2017-04-25 04:35:07.635376	You have 5 points!	\N	\N
666	43	15	5	Test Vishnu	2017-04-25 04:35:07.646119	2017-04-25 04:35:07.646119	You visited	\N	\N
667	43	15	10	You claimed your Free edible at Test Vishnu	2017-04-25 04:35:10.698364	2017-04-25 04:35:10.698364	Congrats!	\N	\N
668	41	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 04:38:53.195001	2017-04-25 04:38:53.195001	You have 5 points!	\N	\N
669	41	15	5	Test Vishnu	2017-04-25 04:38:53.221941	2017-04-25 04:38:53.221941	You visited	\N	\N
670	41	15	10	You claimed your Free edible at Test Vishnu	2017-04-25 04:39:00.073479	2017-04-25 04:39:00.073479	Congrats!	\N	\N
671	41	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 04:39:24.679882	2017-04-25 04:39:24.679882	You have 5 points!	\N	\N
672	41	15	10	You claimed your Free edible at Test Vishnu	2017-04-25 04:39:24.725367	2017-04-25 04:39:24.725367	Congrats!	\N	\N
673	41	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 04:39:47.869447	2017-04-25 04:39:47.869447	You have 5 points!	\N	\N
674	41	15	10	You claimed your Free edible at Test Vishnu	2017-04-25 04:39:47.899784	2017-04-25 04:39:47.899784	Congrats!	\N	\N
675	41	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 04:40:14.034368	2017-04-25 04:40:14.034368	You have 5 points!	\N	\N
676	41	15	10	You claimed your Free edible at Test Vishnu	2017-04-25 04:40:14.06586	2017-04-25 04:40:14.06586	Congrats!	\N	\N
677	43	15	10	You claimed your Free edible at Test Vishnu	2017-04-25 04:57:51.769076	2017-04-25 04:57:51.769076	Congrats!	\N	\N
678	24	15	10	You claimed your Free edible at Test Vishnu	2017-04-25 05:01:44.401438	2017-04-25 05:01:44.401438	Congrats!	\N	\N
679	24	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 05:02:31.23996	2017-04-25 05:02:31.23996	You have 5 points!	\N	\N
680	24	15	10	You claimed your Free edible at Test Vishnu	2017-04-25 05:02:31.27417	2017-04-25 05:02:31.27417	Congrats!	\N	\N
681	24	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 05:03:26.067776	2017-04-25 05:03:26.067776	You have 5 points!	\N	\N
682	24	15	5	Test Vishnu	2017-04-25 05:03:26.078095	2017-04-25 05:03:26.078095	You visited	\N	\N
683	24	15	10	You claimed your Free edible at Test Vishnu	2017-04-25 05:03:29.177031	2017-04-25 05:03:29.177031	Congrats!	\N	\N
684	142	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 05:14:27.987518	2017-04-25 05:14:27.987518	You have 5 points!	\N	\N
685	142	15	7	Test Vishnu	2017-04-25 05:14:28.005055	2017-04-25 05:14:28.005055	You signed up for Reefer!	\N	\N
686	142	15	5	Test Vishnu	2017-04-25 05:14:28.015337	2017-04-25 05:14:28.015337	You visited	\N	\N
687	143	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 05:16:45.262471	2017-04-25 05:16:45.262471	You have 5 points!	\N	\N
688	143	15	7	Test Vishnu	2017-04-25 05:16:45.326727	2017-04-25 05:16:45.326727	You signed up for Reefer!	\N	\N
689	143	15	5	Test Vishnu	2017-04-25 05:16:45.337083	2017-04-25 05:16:45.337083	You visited	\N	\N
696	145	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 05:21:43.854839	2017-04-25 05:21:43.854839	You have 5 points!	\N	\N
697	145	15	7	Test Vishnu	2017-04-25 05:21:43.874345	2017-04-25 05:21:43.874345	You signed up for Reefer!	\N	\N
698	145	15	5	Test Vishnu	2017-04-25 05:21:43.884848	2017-04-25 05:21:43.884848	You visited	\N	\N
699	43	15	10	You claimed your Free Gram at Test Vishnu	2017-04-25 05:27:52.193845	2017-04-25 05:27:52.193845	Congrats!	\N	\N
729	90	15	10	You claimed your Free edible at Test Vishnu	2017-04-26 02:34:34.46754	2017-04-26 02:34:34.46754	Congrats!	\N	\N
690	143	15	10	You claimed your Free edible at Test Vishnu	2017-04-25 05:16:51.381479	2017-04-25 05:16:51.381479	Congrats!	\N	\N
712	146	15	10	You claimed your Free edible at Test Vishnu	2017-04-25 06:29:39.346731	2017-04-25 06:29:39.346731	Congrats!	\N	\N
716	43	15	2	You've earned a Free Gram at Test Vishnu	2017-04-25 07:40:50.311585	2017-04-25 07:40:50.311585	You have 92 points!	\N	\N
717	43	15	5	Test Vishnu	2017-04-25 07:40:50.339543	2017-04-25 07:40:50.339543	You visited	\N	\N
691	144	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 05:18:00.58022	2017-04-25 05:18:00.58022	You have 5 points!	\N	\N
692	144	15	7	Test Vishnu	2017-04-25 05:18:00.594507	2017-04-25 05:18:00.594507	You signed up for Reefer!	\N	\N
693	144	15	5	Test Vishnu	2017-04-25 05:18:00.602649	2017-04-25 05:18:00.602649	You visited	\N	\N
703	118	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 05:35:15.192198	2017-04-25 05:35:15.192198	You have 5 points!	\N	\N
705	122	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 05:42:33.590624	2017-04-25 05:42:33.590624	You have 5 points!	\N	\N
706	133	15	2	You've earned a Free Gram at Test Vishnu	2017-04-25 06:16:22.229086	2017-04-25 06:16:22.229086	You have 92 points!	\N	\N
715	128	15	10	You claimed your Free edible at Test Vishnu	2017-04-25 07:39:14.803371	2017-04-25 07:39:14.803371	Congrats!	\N	\N
694	142	15	10	You claimed your Free edible at Test Vishnu	2017-04-25 05:18:01.31997	2017-04-25 05:18:01.31997	Congrats!	\N	\N
727	90	15	1	You've earned a Free edible at Test Vishnu	2017-04-26 02:34:22.36288	2017-04-26 02:34:22.36288	You have 5 points!	\N	\N
728	90	15	5	Test Vishnu	2017-04-26 02:34:22.870243	2017-04-26 02:34:22.870243	You visited	\N	\N
695	144	15	10	You claimed your Free edible at Test Vishnu	2017-04-25 05:18:12.40428	2017-04-25 05:18:12.40428	Congrats!	\N	\N
722	122	15	10	You claimed your Free edible at Test Vishnu	2017-04-25 07:42:04.668422	2017-04-25 07:42:04.668422	Congrats!	\N	\N
700	132	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 05:32:28.468504	2017-04-25 05:32:28.468504	You have 5 points!	\N	\N
701	133	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 05:32:37.547782	2017-04-25 05:32:37.547782	You have 5 points!	\N	\N
702	24	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 05:33:05.686592	2017-04-25 05:33:05.686592	You have 5 points!	\N	\N
704	123	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 05:35:20.509189	2017-04-25 05:35:20.509189	You have 5 points!	\N	\N
707	43	15	2	You've earned a Free Gram at Test Vishnu	2017-04-25 06:26:54.879831	2017-04-25 06:26:54.879831	You have 92 points!	\N	\N
708	43	15	10	You claimed your Free Gram at Test Vishnu	2017-04-25 06:26:54.923327	2017-04-25 06:26:54.923327	Congrats!	\N	\N
713	128	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 07:39:09.324599	2017-04-25 07:39:09.324599	You have 5 points!	\N	\N
714	128	15	5	Test Vishnu	2017-04-25 07:39:09.414996	2017-04-25 07:39:09.414996	You visited	\N	\N
718	43	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 07:40:57.488028	2017-04-25 07:40:57.488028	You have 5 points!	\N	\N
719	43	15	10	You claimed your Free Gram at Test Vishnu	2017-04-25 07:40:57.51804	2017-04-25 07:40:57.51804	Congrats!	\N	\N
720	128	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 07:41:49.366856	2017-04-25 07:41:49.366856	You have 5 points!	\N	\N
721	122	15	5	Test Vishnu	2017-04-25 07:42:00.447991	2017-04-25 07:42:00.447991	You visited	\N	\N
709	146	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 06:29:31.92248	2017-04-25 06:29:31.92248	You have 5 points!	\N	\N
710	146	15	7	Test Vishnu	2017-04-25 06:29:31.936644	2017-04-25 06:29:31.936644	You signed up for Reefer!	\N	\N
711	146	15	5	Test Vishnu	2017-04-25 06:29:31.94743	2017-04-25 06:29:31.94743	You visited	\N	\N
723	121	15	3	You've earned a Free Eighth at Test Vishnu	2017-04-25 08:08:00.497642	2017-04-25 08:08:00.497642	You have 150 points!	\N	\N
724	121	15	5	Test Vishnu	2017-04-25 08:08:00.525807	2017-04-25 08:08:00.525807	You visited	\N	\N
725	121	15	1	You've earned a Free edible at Test Vishnu	2017-04-25 08:08:08.986466	2017-04-25 08:08:08.986466	You have 5 points!	\N	\N
726	121	15	10	You claimed your Free Gram at Test Vishnu	2017-04-25 08:08:09.030184	2017-04-25 08:08:09.030184	Congrats!	\N	\N
730	14	15	1	You've earned a Free edible at Test Vishnu	2017-04-26 02:37:32.00597	2017-04-26 02:37:32.00597	You have 1 points!	\N	\N
731	14	15	5	Test Vishnu	2017-04-26 02:37:32.04016	2017-04-26 02:37:32.04016	You visited	\N	\N
732	14	15	10	You claimed your Free edible at Test Vishnu	2017-04-26 02:37:38.703635	2017-04-26 02:37:38.703635	Congrats!	\N	\N
733	14	15	1	You've earned a Free edible at Test Vishnu	2017-04-26 02:39:49.538289	2017-04-26 02:39:49.538289	You have 1 points!	\N	\N
734	147	15	1	You've earned a Free edible at Test Vishnu	2017-04-26 02:49:07.630076	2017-04-26 02:49:07.630076	You have 1 points!	\N	\N
735	147	15	7	Test Vishnu	2017-04-26 02:49:07.74699	2017-04-26 02:49:07.74699	You signed up for Reefer!	\N	\N
736	147	15	5	Test Vishnu	2017-04-26 02:49:07.774276	2017-04-26 02:49:07.774276	You visited	\N	\N
737	33	15	3	You've earned a Free Eighth at Test Vishnu	2017-04-26 02:52:46.403753	2017-04-26 02:52:46.403753	You have 150 points!	\N	\N
738	33	15	5	Test Vishnu	2017-04-26 02:52:46.434074	2017-04-26 02:52:46.434074	You visited	\N	\N
739	117	15	1	You've earned a Free edible at Test Vishnu	2017-04-26 02:56:54.947654	2017-04-26 02:56:54.947654	You have 1 points!	\N	\N
740	117	15	5	Test Vishnu	2017-04-26 02:56:54.995867	2017-04-26 02:56:54.995867	You visited	\N	\N
741	133	15	2	You've earned a Free Gram at Test Vishnu	2017-04-26 03:04:57.617169	2017-04-26 03:04:57.617169	You have 92 points!	\N	\N
742	133	15	5	Test Vishnu	2017-04-26 03:04:57.668747	2017-04-26 03:04:57.668747	You visited	\N	\N
743	24	15	5	Test Vishnu	2017-04-26 04:37:16.808528	2017-04-26 04:37:16.808528	You visited	\N	\N
744	24	15	10	You claimed your Free edible at Test Vishnu	2017-04-26 04:37:21.937493	2017-04-26 04:37:21.937493	Congrats!	\N	\N
745	148	15	1	You've earned a Free Gram at Test Vishnu	2017-04-26 04:37:32.157795	2017-04-26 04:37:32.157795	You have 1 points!	\N	\N
746	148	15	7	Test Vishnu	2017-04-26 04:37:32.220049	2017-04-26 04:37:32.220049	You signed up for Reefer!	\N	\N
747	148	15	5	Test Vishnu	2017-04-26 04:37:32.230498	2017-04-26 04:37:32.230498	You visited	\N	\N
748	148	15	10	You claimed your Free Gram at Test Vishnu	2017-04-26 04:37:37.199182	2017-04-26 04:37:37.199182	Congrats!	\N	\N
749	32	15	2	You've earned a Free edible at Test Vishnu	2017-04-26 04:37:48.430377	2017-04-26 04:37:48.430377	You have 92 points!	\N	\N
750	32	15	5	Test Vishnu	2017-04-26 04:37:48.460544	2017-04-26 04:37:48.460544	You visited	\N	\N
751	32	15	2	You've earned a Free edible at Test Vishnu	2017-04-26 04:37:52.416136	2017-04-26 04:37:52.416136	You have 92 points!	\N	\N
752	32	15	10	You claimed your Free Gram at Test Vishnu	2017-04-26 04:37:52.471735	2017-04-26 04:37:52.471735	Congrats!	\N	\N
753	24	15	5	Test Vishnu	2017-04-26 13:05:40.6124	2017-04-26 13:05:40.6124	You visited	\N	\N
754	24	15	10	You claimed your Free Gram at Test Vishnu	2017-04-26 13:05:46.278828	2017-04-26 13:05:46.278828	Congrats!	\N	\N
755	128	15	1	You've earned a Free Gram at Test Vishnu	2017-04-26 13:08:54.732451	2017-04-26 13:08:54.732451	You have 1 points!	\N	\N
756	128	15	5	Test Vishnu	2017-04-26 13:08:54.79615	2017-04-26 13:08:54.79615	You visited	\N	\N
757	128	15	1	You've earned a Free Gram at Test Vishnu	2017-04-26 13:10:00.854429	2017-04-26 13:10:00.854429	You have 1 points!	\N	\N
758	128	15	1	You've earned a Free Gram at Test Vishnu	2017-04-26 13:10:00.913192	2017-04-26 13:10:00.913192	You have 1 points!	\N	\N
759	41	15	5	Test Vishnu	2017-04-27 04:55:12.964266	2017-04-27 04:55:12.964266	You visited	\N	\N
760	41	15	10	You claimed your Free Gram at Test Vishnu	2017-04-27 04:55:20.127442	2017-04-27 04:55:20.127442	Congrats!	\N	\N
761	13	15	1	You've earned a Free Gram at Test Vishnu	2017-04-27 04:55:32.066238	2017-04-27 04:55:32.066238	You have 1 points!	\N	\N
762	13	15	5	Test Vishnu	2017-04-27 04:55:32.103365	2017-04-27 04:55:32.103365	You visited	\N	\N
763	13	15	10	You claimed your Free Gram at Test Vishnu	2017-04-27 05:05:29.016685	2017-04-27 05:05:29.016685	Congrats!	\N	\N
764	13	15	1	You've earned a Free Gram at Test Vishnu	2017-04-27 05:05:39.264563	2017-04-27 05:05:39.264563	You have 1 points!	\N	\N
765	13	15	10	You claimed your Free Gram at Test Vishnu	2017-04-27 05:05:39.298355	2017-04-27 05:05:39.298355	Congrats!	\N	\N
766	122	15	1	You've earned a Free Gram at Test Vishnu	2017-04-27 05:13:47.587631	2017-04-27 05:13:47.587631	You have 1 points!	\N	\N
767	122	15	5	Test Vishnu	2017-04-27 05:13:47.622613	2017-04-27 05:13:47.622613	You visited	\N	\N
768	24	15	1	You've earned a Free Gram at Test Vishnu	2017-04-27 09:19:15.632844	2017-04-27 09:19:15.632844	You have 1 points!	\N	\N
769	24	15	5	Test Vishnu	2017-04-27 09:19:15.703046	2017-04-27 09:19:15.703046	You visited	\N	\N
770	41	15	1	You've earned a Free Gram at Test Vishnu	2017-04-27 09:19:43.953797	2017-04-27 09:19:43.953797	You have 1 points!	\N	\N
771	41	15	5	Test Vishnu	2017-04-27 09:19:44.082762	2017-04-27 09:19:44.082762	You visited	\N	\N
772	90	15	1	You've earned a Free Gram at Test Vishnu	2017-05-01 04:55:16.114541	2017-05-01 04:55:16.114541	You have 1 points!	\N	\N
773	90	15	5	Test Vishnu	2017-05-01 04:55:16.348596	2017-05-01 04:55:16.348596	You visited	\N	\N
774	52	15	1	You've earned a Free Gram at Test Vishnu	2017-05-01 05:14:39.499236	2017-05-01 05:14:39.499236	You have 1 points!	\N	\N
776	90	15	1	You've earned a Free Gram at Test Vishnu	2017-05-01 17:36:13.826731	2017-05-01 17:36:13.826731	You have 1 points!	\N	\N
777	90	15	5	Test Vishnu	2017-05-01 17:36:14.591828	2017-05-01 17:36:14.591828	You visited	\N	\N
778	13	15	1	You've earned a Free Gram at Test Vishnu	2017-05-01 17:44:27.449215	2017-05-01 17:44:27.449215	You have 1 points!	\N	\N
779	13	15	5	Test Vishnu	2017-05-01 17:44:27.490311	2017-05-01 17:44:27.490311	You visited	\N	\N
780	149	15	7	Ard a pussay	2017-05-01 20:37:53.825405	2017-05-01 20:37:53.825405	You signed up for Reefer!	\N	\N
781	149	15	5	Ard a pussay	2017-05-01 20:37:53.835492	2017-05-01 20:37:53.835492	You visited	\N	\N
782	52	15	5	Ard a pussay	2017-05-01 21:25:12.133542	2017-05-01 21:25:12.133542	You visited	\N	\N
783	87	15	5	Ard a pussay	2017-05-02 03:55:23.124956	2017-05-02 03:55:23.124956	You visited	\N	\N
784	44	15	5	Ard a pussay	2017-05-02 03:55:59.984097	2017-05-02 03:55:59.984097	You visited	\N	\N
785	87	15	10	You claimed your Free Gram at Ard a pussay	2017-05-02 03:56:24.352472	2017-05-02 03:56:24.352472	Congrats!	\N	\N
786	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-02 07:00:37.607767	2017-05-02 07:00:37.607767	You have 15 points!	\N	\N
787	90	15	5	Ard a pussay	2017-05-02 07:00:37.625914	2017-05-02 07:00:37.625914	You visited	\N	\N
788	150	15	7	Ard a pussay	2017-05-02 07:03:29.155183	2017-05-02 07:03:29.155183	You signed up for Reefer!	\N	\N
789	150	15	5	Ard a pussay	2017-05-02 07:03:29.165346	2017-05-02 07:03:29.165346	You visited	\N	\N
790	141	15	5	Ard a pussay	2017-05-02 08:41:38.478597	2017-05-02 08:41:38.478597	You visited	\N	\N
791	151	15	7	Ard a pussay	2017-05-02 18:51:12.020839	2017-05-02 18:51:12.020839	You signed up for Reefer!	\N	\N
792	151	15	5	Ard a pussay	2017-05-02 18:51:12.112572	2017-05-02 18:51:12.112572	You visited	\N	\N
793	152	15	7	Ard a pussay	2017-05-03 07:55:57.389179	2017-05-03 07:55:57.389179	You signed up for Reefer!	\N	\N
794	152	15	5	Ard a pussay	2017-05-03 07:55:57.501764	2017-05-03 07:55:57.501764	You visited	\N	\N
795	153	15	7	Ard a pussay	2017-05-03 18:20:08.425109	2017-05-03 18:20:08.425109	You signed up for Reefer!	\N	\N
796	153	15	5	Ard a pussay	2017-05-03 18:20:08.583667	2017-05-03 18:20:08.583667	You visited	\N	\N
797	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-03 18:21:13.113632	2017-05-03 18:21:13.113632	You have 15 points!	\N	\N
798	90	15	5	Ard a pussay	2017-05-03 18:21:13.13462	2017-05-03 18:21:13.13462	You visited	\N	\N
799	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-04 06:57:18.895637	2017-05-04 06:57:18.895637	You have 15 points!	\N	\N
800	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-04 18:24:11.114036	2017-05-04 18:24:11.114036	You have 15 points!	\N	\N
801	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-04 18:24:11.937087	2017-05-04 18:24:11.937087	You have 15 points!	\N	\N
802	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-04 18:24:12.346428	2017-05-04 18:24:12.346428	You have 15 points!	\N	\N
803	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-04 18:24:12.749652	2017-05-04 18:24:12.749652	You have 15 points!	\N	\N
804	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-04 18:24:13.169128	2017-05-04 18:24:13.169128	You have 15 points!	\N	\N
805	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-04 18:24:13.572246	2017-05-04 18:24:13.572246	You have 15 points!	\N	\N
806	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-04 18:24:48.402754	2017-05-04 18:24:48.402754	You have 15 points!	\N	\N
807	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-04 18:24:48.830894	2017-05-04 18:24:48.830894	You have 15 points!	\N	\N
808	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-04 18:24:49.293023	2017-05-04 18:24:49.293023	You have 15 points!	\N	\N
809	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-04 18:24:49.723752	2017-05-04 18:24:49.723752	You have 15 points!	\N	\N
810	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-04 18:24:50.145922	2017-05-04 18:24:50.145922	You have 15 points!	\N	\N
811	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-04 18:24:50.57866	2017-05-04 18:24:50.57866	You have 15 points!	\N	\N
812	32	15	2	You've earned a Free edible at Ard a pussay	2017-05-05 04:39:25.09944	2017-05-05 04:39:25.09944	You have 89 points!	\N	\N
813	32	15	5	Ard a pussay	2017-05-05 04:39:25.210008	2017-05-05 04:39:25.210008	You visited	\N	\N
814	32	15	1	You've earned a Free Gram at Ard a pussay	2017-05-05 04:39:31.513808	2017-05-05 04:39:31.513808	You have 15 points!	\N	\N
815	32	15	10	You claimed your Free edible at Ard a pussay	2017-05-05 04:39:31.674016	2017-05-05 04:39:31.674016	Congrats!	\N	\N
816	24	15	5	Ard a pussay	2017-05-05 04:42:37.317109	2017-05-05 04:42:37.317109	You visited	\N	\N
817	24	15	10	You claimed your Free Gram at Ard a pussay	2017-05-05 04:43:04.332142	2017-05-05 04:43:04.332142	Congrats!	\N	\N
818	24	15	1	You've earned a Free Gram at Ard a pussay	2017-05-05 04:43:23.816254	2017-05-05 04:43:23.816254	You have 15 points!	\N	\N
819	24	15	10	You claimed your Free Gram at Ard a pussay	2017-05-05 04:43:23.858736	2017-05-05 04:43:23.858736	Congrats!	\N	\N
820	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-05 04:45:21.160899	2017-05-05 04:45:21.160899	You have 15 points!	\N	\N
821	90	15	5	Ard a pussay	2017-05-05 04:45:21.181284	2017-05-05 04:45:21.181284	You visited	\N	\N
822	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-05 04:45:26.533501	2017-05-05 04:45:26.533501	You have 15 points!	\N	\N
823	90	15	10	You claimed your Free Gram at Ard a pussay	2017-05-05 04:45:26.56041	2017-05-05 04:45:26.56041	Congrats!	\N	\N
824	24	15	1	You've earned a Free Gram at Ard a pussay	2017-05-05 04:46:08.830546	2017-05-05 04:46:08.830546	You have 15 points!	\N	\N
825	24	15	10	You claimed your Free Gram at Ard a pussay	2017-05-05 04:46:08.880411	2017-05-05 04:46:08.880411	Congrats!	\N	\N
826	41	15	2	You've earned a Free edible at Ard a pussay	2017-05-05 04:47:11.972214	2017-05-05 04:47:11.972214	You have 89 points!	\N	\N
827	41	15	5	Ard a pussay	2017-05-05 04:47:11.991823	2017-05-05 04:47:11.991823	You visited	\N	\N
828	41	15	10	You claimed your Free edible at Ard a pussay	2017-05-05 04:47:15.946585	2017-05-05 04:47:15.946585	Congrats!	\N	\N
829	43	15	1	You've earned a Free Gram at Ard a pussay	2017-05-05 06:14:04.825255	2017-05-05 06:14:04.825255	You have 15 points!	\N	\N
830	43	15	5	Ard a pussay	2017-05-05 06:14:04.860826	2017-05-05 06:14:04.860826	You visited	\N	\N
831	43	15	10	You claimed your Free Gram at Ard a pussay	2017-05-05 06:14:11.797893	2017-05-05 06:14:11.797893	Congrats!	\N	\N
832	154	15	7	Ard a pussay	2017-05-05 07:01:35.933072	2017-05-05 07:01:35.933072	You signed up for Reefer!	\N	\N
833	154	15	5	Ard a pussay	2017-05-05 07:01:35.942444	2017-05-05 07:01:35.942444	You visited	\N	\N
834	24	15	1	You've earned a Free Gram at Ard a pussay	2017-05-05 07:06:40.077977	2017-05-05 07:06:40.077977	You have 15 points!	\N	\N
835	24	15	5	Ard a pussay	2017-05-05 07:06:40.097826	2017-05-05 07:06:40.097826	You visited	\N	\N
836	43	15	1	You've earned a Free Gram at Ard a pussay	2017-05-05 07:07:05.25993	2017-05-05 07:07:05.25993	You have 15 points!	\N	\N
837	43	15	5	Ard a pussay	2017-05-05 07:07:05.285259	2017-05-05 07:07:05.285259	You visited	\N	\N
838	90	15	1	You've earned a Free Gram at Ard a pussay	2017-05-06 01:09:21.666621	2017-05-06 01:09:21.666621	You have 15 points!	\N	\N
839	90	15	5	Ard a pussay	2017-05-06 01:09:21.752204	2017-05-06 01:09:21.752204	You visited	\N	\N
840	13	15	5	Ard a pussay	2017-05-06 01:13:17.090321	2017-05-06 01:13:17.090321	You visited	\N	\N
841	13	5	3	You've earned a Free edible at Zachs Dispensary	2017-05-08 20:02:35.108898	2017-05-08 20:02:35.108898	You have 31 points!	\N	\N
842	13	5	5	Zachs Dispensary	2017-05-08 20:02:35.677299	2017-05-08 20:02:35.677299	You visited	\N	\N
843	13	5	2	You've earned a Free Joint at Zachs Dispensary	2017-05-08 20:02:59.361908	2017-05-08 20:02:59.361908	You have 26 points!	\N	\N
844	13	5	10	You claimed your Free Eighth at Zachs Dispensary	2017-05-08 20:02:59.580725	2017-05-08 20:02:59.580725	Congrats!	\N	\N
845	155	15	7	Ard a pussay	2017-05-08 22:36:46.601507	2017-05-08 22:36:46.601507	You signed up for Reefer!	\N	\N
846	155	15	5	Ard a pussay	2017-05-08 22:36:46.685878	2017-05-08 22:36:46.685878	You visited	\N	\N
847	13	15	2	You've earned a Free edible at Ard a pussay	2017-05-08 22:43:23.865843	2017-05-08 22:43:23.865843	You have 89 points!	\N	\N
848	13	15	5	Ard a pussay	2017-05-08 22:43:23.979508	2017-05-08 22:43:23.979508	You visited	\N	\N
849	90	15	5	Ard a pussay	2017-05-08 22:55:24.26632	2017-05-08 22:55:24.26632	You visited	\N	\N
850	52	15	5	Ard a pussay	2017-05-09 00:22:02.253672	2017-05-09 00:22:02.253672	You visited	\N	\N
851	90	5	1	You've earned a Free Eighth at Zachs Dispensary	2017-05-09 00:28:41.349912	2017-05-09 00:28:41.349912	You have 1 points!	\N	\N
852	90	5	5	Zachs Dispensary	2017-05-09 00:28:41.366565	2017-05-09 00:28:41.366565	You visited	\N	\N
853	52	5	5	Zachs Dispensary	2017-05-09 00:29:55.327864	2017-05-09 00:29:55.327864	You visited	\N	\N
854	90	5	10	You claimed your Free Eighth at Zachs Dispensary	2017-05-09 00:30:52.090017	2017-05-09 00:30:52.090017	Congrats!	\N	\N
855	147	5	1	You've earned a Free Eighth at Zachs Dispensary	2017-05-09 00:39:02.870925	2017-05-09 00:39:02.870925	You have 1 points!	\N	\N
856	147	5	5	Zachs Dispensary	2017-05-09 00:39:02.887882	2017-05-09 00:39:02.887882	You visited	\N	\N
857	15	5	2	You've earned a Free Joint at Zachs Dispensary	2017-05-09 00:46:52.995014	2017-05-09 00:46:52.995014	You have 26 points!	\N	\N
858	15	5	5	Zachs Dispensary	2017-05-09 00:46:53.067367	2017-05-09 00:46:53.067367	You visited	\N	\N
861	47	5	10	You claimed your Free Eighth at Zachs Dispensary	2017-05-09 00:50:56.83201	2017-05-09 00:50:56.83201	Congrats!	\N	\N
862	47	5	2	You've earned a Free Joint at Zachs Dispensary	2017-05-09 00:51:18.720015	2017-05-09 00:51:18.720015	You have 26 points!	\N	\N
863	47	5	10	You claimed your Free Eighth at Zachs Dispensary	2017-05-09 00:51:18.765895	2017-05-09 00:51:18.765895	Congrats!	\N	\N
859	47	5	2	You've earned a Free Joint at Zachs Dispensary	2017-05-09 00:50:40.471057	2017-05-09 00:50:40.471057	You have 26 points!	\N	\N
860	47	5	5	Zachs Dispensary	2017-05-09 00:50:40.538414	2017-05-09 00:50:40.538414	You visited	\N	\N
864	52	15	5	Ard a pussay	2017-05-09 07:02:52.758225	2017-05-09 07:02:52.758225	You visited	\N	\N
865	24	15	5	Ard a pussay	2017-05-09 09:29:04.181626	2017-05-09 09:29:04.181626	You visited	\N	\N
866	90	5	1	You've earned a Free Eighth at Zachs Dispensary	2017-05-09 20:55:20.368358	2017-05-09 20:55:20.368358	You have 1 points!	\N	\N
867	90	5	5	Zachs Dispensary	2017-05-09 20:55:20.411894	2017-05-09 20:55:20.411894	You visited	\N	\N
868	33	15	5	Ard a pussay	2017-05-09 21:06:00.267015	2017-05-09 21:06:00.267015	You visited	\N	\N
869	33	5	1	You've earned a Free Eighth at Zachs Dispensary	2017-05-09 21:06:39.635027	2017-05-09 21:06:39.635027	You have 1 points!	\N	\N
870	33	5	5	Zachs Dispensary	2017-05-09 21:06:39.652533	2017-05-09 21:06:39.652533	You visited	\N	\N
871	156	15	7	Ard a pussay	2017-05-10 07:09:00.405819	2017-05-10 07:09:00.405819	You signed up for Reefer!	\N	\N
872	156	15	5	Ard a pussay	2017-05-10 07:09:00.485998	2017-05-10 07:09:00.485998	You visited	\N	\N
873	157	15	7	Ard a pussay	2017-05-10 07:09:38.429503	2017-05-10 07:09:38.429503	You signed up for Reefer!	\N	\N
874	157	15	5	Ard a pussay	2017-05-10 07:09:38.502646	2017-05-10 07:09:38.502646	You visited	\N	\N
875	32	15	5	Ard a pussay	2017-05-10 07:10:05.646409	2017-05-10 07:10:05.646409	You visited	\N	\N
876	32	15	10	You claimed your Free Gram at Ard a pussay	2017-05-10 07:10:12.965715	2017-05-10 07:10:12.965715	Congrats!	\N	\N
877	90	15	5	Ard a pussay	2017-05-10 17:57:56.210349	2017-05-10 17:57:56.210349	You visited	\N	\N
878	13	15	5	Ard a pussay	2017-05-10 18:01:11.773505	2017-05-10 18:01:11.773505	You visited	\N	\N
879	2	1	1	Tester	2017-05-17 15:51:46.164986	2017-05-17 15:51:46.164986	sdffsdf signed up with your link!	\N	Get Reeferal reward when he visits!
880	158	1	0	You've got a free gram at Tester	2017-05-17 15:51:46.195492	2017-05-17 15:51:46.195492	You accepted rest's reeferal!	\N	\N
881	2	1	1	Tester	2017-05-27 15:26:52.747777	2017-05-27 15:26:52.747777	zxczxc signed up with your link!	\N	Get Reeferal reward when he visits!
882	172	1	0	You've got a free gram at Tester	2017-05-27 15:26:52.835483	2017-05-27 15:26:52.835483	You accepted rest's reeferal!	\N	\N
883	2	13	5	Abhilash	2017-06-13 08:46:03.863021	2017-06-13 08:46:03.863021	You visited	\N	\N
884	181	13	7	Abhilash	2017-06-13 09:01:45.202827	2017-06-13 09:01:45.202827	You signed up for Reefer!	\N	\N
885	181	13	5	Abhilash	2017-06-13 09:01:45.220289	2017-06-13 09:01:45.220289	You visited	\N	\N
886	181	14	5	Dispensary_12	2017-06-13 09:03:21.533747	2017-06-13 09:03:21.533747	You visited	\N	\N
887	181	14	1	You've earned a Free Joint at Dispensary_12	2017-06-13 09:04:04.979165	2017-06-13 09:04:04.979165	You have 30 points!	\N	\N
888	182	14	7	Dispensary_12	2017-06-13 09:06:14.510683	2017-06-13 09:06:14.510683	You signed up for Reefer!	\N	\N
889	182	14	5	Dispensary_12	2017-06-13 09:06:14.521997	2017-06-13 09:06:14.521997	You visited	\N	\N
890	183	14	7	Dispensary_12	2017-06-13 09:09:26.847026	2017-06-13 09:09:26.847026	You signed up for Reefer!	\N	\N
891	183	14	5	Dispensary_12	2017-06-13 09:09:26.855874	2017-06-13 09:09:26.855874	You visited	\N	\N
892	184	14	7	Dispensary_12	2017-06-13 09:13:51.683234	2017-06-13 09:13:51.683234	You signed up for Reefer!	\N	\N
893	184	14	5	Dispensary_12	2017-06-13 09:13:51.695624	2017-06-13 09:13:51.695624	You visited	\N	\N
894	185	14	7	Dispensary_12	2017-06-13 09:22:03.324158	2017-06-13 09:22:03.324158	You signed up for Reefer!	\N	\N
895	185	14	5	Dispensary_12	2017-06-13 09:22:03.335386	2017-06-13 09:22:03.335386	You visited	\N	\N
896	2	15	5	Reefer_Test	2017-06-13 09:33:23.153988	2017-06-13 09:33:23.153988	You visited	\N	\N
897	186	15	7	Reefer_Test	2017-06-13 09:34:54.460279	2017-06-13 09:34:54.460279	You signed up for Reefer!	\N	\N
898	186	15	5	Reefer_Test	2017-06-13 09:34:54.471713	2017-06-13 09:34:54.471713	You visited	\N	\N
899	187	15	7	Reefer_Test	2017-06-13 09:37:25.760263	2017-06-13 09:37:25.760263	You signed up for Reefer!	\N	\N
900	187	15	5	Reefer_Test	2017-06-13 09:37:25.769481	2017-06-13 09:37:25.769481	You visited	\N	\N
901	188	15	7	Reefer_Test	2017-06-13 09:43:21.2287	2017-06-13 09:43:21.2287	You signed up for Reefer!	\N	\N
902	188	15	5	Reefer_Test	2017-06-13 09:43:21.235493	2017-06-13 09:43:21.235493	You visited	\N	\N
903	189	15	7	Reefer_Test	2017-06-13 10:13:50.177911	2017-06-13 10:13:50.177911	You signed up for Reefer!	\N	\N
904	189	15	5	Reefer_Test	2017-06-13 10:13:50.189123	2017-06-13 10:13:50.189123	You visited	\N	\N
905	176	15	5	Reefer_Test	2017-06-13 10:28:10.932415	2017-06-13 10:28:10.932415	You visited	\N	\N
906	190	15	7	Reefer_Test	2017-06-13 10:29:39.546401	2017-06-13 10:29:39.546401	You signed up for Reefer!	\N	\N
907	190	15	5	Reefer_Test	2017-06-13 10:29:39.557101	2017-06-13 10:29:39.557101	You visited	\N	\N
908	191	15	7	Reefer_Test	2017-06-13 10:35:28.548843	2017-06-13 10:35:28.548843	You signed up for Reefer!	\N	\N
909	191	15	5	Reefer_Test	2017-06-13 10:35:28.556365	2017-06-13 10:35:28.556365	You visited	\N	\N
910	192	15	7	Reefer_Test	2017-06-13 10:37:55.366414	2017-06-13 10:37:55.366414	You signed up for Reefer!	\N	\N
911	192	15	5	Reefer_Test	2017-06-13 10:37:55.377693	2017-06-13 10:37:55.377693	You visited	\N	\N
912	2	16	5	Test_Dispensary	2017-06-13 11:58:21.833946	2017-06-13 11:58:21.833946	You visited	\N	\N
913	186	16	5	Test_Dispensary	2017-06-13 12:10:14.545313	2017-06-13 12:10:14.545313	You visited	\N	\N
914	193	16	7	Test_Dispensary	2017-06-13 12:12:09.912443	2017-06-13 12:12:09.912443	You signed up for Reefer!	\N	\N
915	193	16	5	Test_Dispensary	2017-06-13 12:12:09.933034	2017-06-13 12:12:09.933034	You visited	\N	\N
916	194	1	7	Tester	2017-06-15 17:37:19.556901	2017-06-15 17:37:19.556901	You signed up for Reefer!	\N	\N
917	194	1	5	Tester	2017-06-15 17:37:19.643082	2017-06-15 17:37:19.643082	You visited	\N	\N
918	195	1	7	Tester	2017-06-15 17:39:56.835987	2017-06-15 17:39:56.835987	You signed up for Reefer!	\N	\N
919	195	1	5	Tester	2017-06-15 17:39:56.853036	2017-06-15 17:39:56.853036	You visited	\N	\N
920	196	1	7	Tester	2017-06-15 17:44:54.903779	2017-06-15 17:44:54.903779	You signed up for Reefer!	\N	\N
921	196	1	5	Tester	2017-06-15 17:44:54.915777	2017-06-15 17:44:54.915777	You visited	\N	\N
922	197	16	7	Test_Dispensary	2017-06-16 04:13:15.519664	2017-06-16 04:13:15.519664	You signed up for Reefer!	\N	\N
923	197	16	5	Test_Dispensary	2017-06-16 04:13:15.527578	2017-06-16 04:13:15.527578	You visited	\N	\N
924	198	16	7	Test_Dispensary	2017-06-16 04:14:04.619303	2017-06-16 04:14:04.619303	You signed up for Reefer!	\N	\N
925	198	16	5	Test_Dispensary	2017-06-16 04:14:04.630362	2017-06-16 04:14:04.630362	You visited	\N	\N
926	201	16	7	Test_Dispensary	2017-06-16 04:41:57.587547	2017-06-16 04:41:57.587547	You signed up for Reefer!	\N	\N
927	201	16	5	Test_Dispensary	2017-06-16 04:41:57.598787	2017-06-16 04:41:57.598787	You visited	\N	\N
928	202	16	7	Test_Dispensary	2017-06-16 04:42:55.091242	2017-06-16 04:42:55.091242	You signed up for Reefer!	\N	\N
929	202	16	5	Test_Dispensary	2017-06-16 04:42:55.102183	2017-06-16 04:42:55.102183	You visited	\N	\N
930	202	16	1	Test_Dispensary	2017-06-16 04:44:51.8205	2017-06-16 04:44:51.8205	zxczczxc signed up with your link!	\N	Get Reeferal reward when he visits!
931	203	16	0	You've got a free gram at Test_Dispensary	2017-06-16 04:44:51.85432	2017-06-16 04:44:51.85432	You accepted sadfsad's reeferal!	\N	\N
932	203	16	5	Test_Dispensary	2017-06-16 04:45:36.558131	2017-06-16 04:45:36.558131	You visited	\N	\N
933	202	16	0	You've got a free gram at Test_Dispensary	2017-06-16 04:45:37.937334	2017-06-16 04:45:37.937334	zxczczxc  accepted your Reeferal!	\N	\N
934	203	16	10	You claimed your free joint at Test_Dispensary	2017-06-16 04:45:38.768948	2017-06-16 04:45:38.768948	Congrats!	\N	\N
935	206	16	7	Test_Dispensary	2017-06-16 04:55:24.816135	2017-06-16 04:55:24.816135	You signed up for Reefer!	\N	\N
936	206	16	5	Test_Dispensary	2017-06-16 04:55:24.827049	2017-06-16 04:55:24.827049	You visited	\N	\N
937	2	16	5	Test_Dispensary	2017-06-16 05:00:16.13354	2017-06-16 05:00:16.13354	You visited	\N	\N
938	207	16	7	Test_Dispensary	2017-06-16 05:13:40.229773	2017-06-16 05:13:40.229773	You signed up for Reefer!	\N	\N
939	207	16	5	Test_Dispensary	2017-06-16 05:13:40.238575	2017-06-16 05:13:40.238575	You visited	\N	\N
940	2	17	5	REFERRAL_Test	2017-06-16 05:46:09.450437	2017-06-16 05:46:09.450437	You visited	\N	\N
941	194	16	5	Test_Dispensary	2017-06-16 05:51:18.211939	2017-06-16 05:51:18.211939	You visited	\N	\N
942	209	19	7	REFERRAL_Test	2017-06-16 05:57:28.965664	2017-06-16 05:57:28.965664	You signed up for Reefer!	\N	\N
943	209	19	5	REFERRAL_Test	2017-06-16 05:57:28.976554	2017-06-16 05:57:28.976554	You visited	\N	\N
944	186	19	5	REFERRAL_Test	2017-06-16 06:14:36.376056	2017-06-16 06:14:36.376056	You visited	\N	\N
945	210	19	5	REFERRAL_Test	2017-06-16 06:22:16.62552	2017-06-16 06:22:16.62552	You visited	\N	\N
946	212	19	7	REFERRAL_Test	2017-06-16 06:25:45.625205	2017-06-16 06:25:45.625205	You signed up for Reefer!	\N	\N
947	212	19	5	REFERRAL_Test	2017-06-16 06:25:45.636455	2017-06-16 06:25:45.636455	You visited	\N	\N
948	212	19	1	REFERRAL_Test	2017-06-16 06:29:38.272756	2017-06-16 06:29:38.272756	sweet signed up with your link!	\N	Get Reeferal reward when he visits!
949	213	19	0	You've got a free gram at REFERRAL_Test	2017-06-16 06:29:38.305975	2017-06-16 06:29:38.305975	You accepted tester's reeferal!	\N	\N
950	215	20	7	R_Test	2017-06-16 06:48:45.963383	2017-06-16 06:48:45.963383	You signed up for Reefer!	\N	\N
951	215	20	5	R_Test	2017-06-16 06:48:45.975495	2017-06-16 06:48:45.975495	You visited	\N	\N
952	217	20	7	R_Test	2017-06-16 07:02:18.834996	2017-06-16 07:02:18.834996	You signed up for Reefer!	\N	\N
953	217	20	5	R_Test	2017-06-16 07:02:18.845965	2017-06-16 07:02:18.845965	You visited	\N	\N
954	217	20	1	R_Test	2017-06-16 07:07:58.85605	2017-06-16 07:07:58.85605	RefA signed up with your link!	\N	Get Reeferal reward when he visits!
955	218	20	0	You've got a free gram at R_Test	2017-06-16 07:07:58.888792	2017-06-16 07:07:58.888792	You accepted PatB's reeferal!	\N	\N
956	218	20	5	R_Test	2017-06-16 07:12:40.917108	2017-06-16 07:12:40.917108	You visited	\N	\N
957	217	20	0	You've got a free gram at R_Test	2017-06-16 07:12:42.361796	2017-06-16 07:12:42.361796	RefA  accepted your Reeferal!	\N	\N
958	218	20	10	You claimed your free joint at R_Test	2017-06-16 07:12:43.163683	2017-06-16 07:12:43.163683	Congrats!	\N	\N
959	217	20	10	You claimed your free joint at R_Test	2017-06-16 07:16:17.631327	2017-06-16 07:16:17.631327	Congrats!	\N	\N
960	217	20	1	R_Test	2017-06-16 08:06:49.23289	2017-06-16 08:06:49.23289	RefB signed up with your link!	\N	Get Reeferal reward when he visits!
961	219	20	0	You've got a free gram at R_Test	2017-06-16 08:06:49.267458	2017-06-16 08:06:49.267458	You accepted PatB's reeferal!	\N	\N
962	219	20	5	R_Test	2017-06-16 08:08:31.595114	2017-06-16 08:08:31.595114	You visited	\N	\N
963	217	20	0	You've got a free gram at R_Test	2017-06-16 08:08:33.049781	2017-06-16 08:08:33.049781	RefB  accepted your Reeferal!	\N	\N
964	219	20	10	You claimed your free joint at R_Test	2017-06-16 08:08:33.853075	2017-06-16 08:08:33.853075	Congrats!	\N	\N
965	217	20	10	You claimed your free joint at R_Test	2017-06-16 08:16:00.600658	2017-06-16 08:16:00.600658	Congrats!	\N	\N
966	219	20	1	R_Test	2017-06-16 08:29:48.162648	2017-06-16 08:29:48.162648	RefC signed up with your link!	\N	Get Reeferal reward when he visits!
967	220	20	0	You've got a free gram at R_Test	2017-06-16 08:29:48.194999	2017-06-16 08:29:48.194999	You accepted RefB's reeferal!	\N	\N
968	220	20	5	R_Test	2017-06-16 08:31:36.47979	2017-06-16 08:31:36.47979	You visited	\N	\N
969	219	20	0	You've got a free gram at R_Test	2017-06-16 08:31:37.822061	2017-06-16 08:31:37.822061	RefC  accepted your Reeferal!	\N	\N
970	220	20	10	You claimed your free joint at R_Test	2017-06-16 08:31:38.646226	2017-06-16 08:31:38.646226	Congrats!	\N	\N
971	219	20	10	You claimed your free joint at R_Test	2017-06-16 08:33:09.907607	2017-06-16 08:33:09.907607	Congrats!	\N	\N
\.


--
-- Name: notifications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('notifications_id_seq', 971, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY schema_migrations (version) FROM stdin;
20160905120843
20160905134155
20160905153205
20160905155145
20160905155803
20160905204457
20160906094251
20160906095102
20160907151347
20160907152754
20160908154518
20160909092128
20160909141022
20160909145220
20160912115851
20160912140320
20160912161334
20160915095728
20160915105422
20160917101651
20160917101855
20160917155254
20160917175403
20160917185936
20160918112339
20160919092132
20160919123514
20160919145310
20160919204044
20160920102104
20160920125212
20160922115341
20160926202818
20160927103328
20161005185530
20161008192726
20161010102503
20161024145016
20161025161748
20161029224728
20161030101607
20161031212605
20161031225818
20161103230618
20170116135052
20170415042627
20170528054242
20170603092930
20170603094616
20170604152102
20170606163957
\.


--
-- Data for Name: store_hours; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY store_hours (id, dispensary_id, from_time, to_time, created_at, updated_at, sunday, monday, tuesday, wednesday, thursday, friday, saturday) FROM stdin;
1	15	18	22	2017-04-19 13:14:17.406398	2017-04-24 05:22:43.030441	t	f	t	f	f	f	f
2	15	18	22	2017-04-25 05:00:41.844612	2017-04-25 05:37:31.591306	t	f	f	f	f	f	f
\.


--
-- Name: store_hours_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('store_hours_id_seq', 2, true);


--
-- Data for Name: user_visitations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY user_visitations (id, user_id, dispensary_id, visited_at, created_at, updated_at) FROM stdin;
47	118	15	2017-04-24 11:14:09.90463	2017-04-24 11:14:09.905462	2017-04-24 11:14:09.905462
49	120	15	2017-04-24 11:16:18.259272	2017-04-24 11:16:18.260107	2017-04-24 11:16:18.260107
52	123	15	2017-04-24 11:20:05.76116	2017-04-24 11:20:05.76208	2017-04-24 11:20:05.76208
45	117	15	2017-03-24 11:11:03.253792	2017-04-24 11:11:03.26337	2017-04-24 11:11:03.26337
46	117	15	2017-03-24 11:11:40.689885	2017-04-24 11:11:40.690886	2017-04-24 11:11:40.690886
48	119	15	2017-02-24 11:15:06.909538	2017-04-24 11:15:06.91098	2017-04-24 11:15:06.91098
50	121	15	2017-04-21 11:17:35.819269	2017-04-24 11:17:35.820566	2017-04-24 11:17:35.820566
53	124	15	2017-04-24 11:21:10.206539	2017-04-24 11:21:10.207527	2017-04-24 11:21:10.207527
51	122	15	2017-04-21 11:19:08.472566	2017-04-24 11:19:08.473529	2017-04-24 11:19:08.473529
54	125	15	2017-04-24 11:21:58.275218	2017-04-24 11:21:58.276191	2017-04-24 11:21:58.276191
55	126	15	2017-04-24 11:22:31.355737	2017-04-24 11:22:31.356576	2017-04-24 11:22:31.356576
56	127	15	2017-04-24 11:23:38.232548	2017-04-24 11:23:38.233409	2017-04-24 11:23:38.233409
57	128	15	2017-04-24 11:24:40.560869	2017-04-24 11:24:40.561931	2017-04-24 11:24:40.561931
58	129	15	2017-04-24 11:25:44.157018	2017-04-24 11:25:44.157871	2017-04-24 11:25:44.157871
59	130	15	2017-04-24 11:26:43.424467	2017-04-24 11:26:43.425697	2017-04-24 11:26:43.425697
61	132	15	2017-04-24 11:28:56.588759	2017-04-24 11:28:56.589969	2017-04-24 11:28:56.589969
62	133	15	2017-04-24 11:30:23.116175	2017-04-24 11:30:23.117279	2017-04-24 11:30:23.117279
60	132	15	2017-04-24 11:27:45.459776	2017-04-24 11:27:45.460845	2017-04-24 11:27:45.460845
63	24	15	2017-04-24 12:13:34.121576	2017-04-24 12:13:34.122465	2017-04-24 12:13:34.122465
64	24	15	2017-04-24 12:14:17.559196	2017-04-24 12:14:17.560236	2017-04-24 12:14:17.560236
65	43	15	2017-04-25 04:35:07.654987	2017-04-25 04:35:07.655986	2017-04-25 04:35:07.655986
66	41	15	2017-04-25 04:38:53.230608	2017-04-25 04:38:53.231589	2017-04-25 04:38:53.231589
67	24	15	2017-04-25 05:03:26.089325	2017-04-25 05:03:26.094439	2017-04-25 05:03:26.094439
68	128	15	2017-04-25 07:39:09.423872	2017-04-25 07:39:09.425877	2017-04-25 07:39:09.425877
69	43	15	2017-04-25 07:40:50.348864	2017-04-25 07:40:50.349818	2017-04-25 07:40:50.349818
70	122	15	2017-04-25 07:42:00.456929	2017-04-25 07:42:00.458151	2017-04-25 07:42:00.458151
71	121	15	2017-04-25 08:08:00.53479	2017-04-25 08:08:00.535766	2017-04-25 08:08:00.535766
72	90	15	2017-04-26 02:34:22.923287	2017-04-26 02:34:22.946414	2017-04-26 02:34:22.946414
73	14	15	2017-04-26 02:37:32.070775	2017-04-26 02:37:32.082487	2017-04-26 02:37:32.082487
74	33	15	2017-04-26 02:52:46.443126	2017-04-26 02:52:46.444447	2017-04-26 02:52:46.444447
75	117	15	2017-04-26 02:56:55.005145	2017-04-26 02:56:55.006545	2017-04-26 02:56:55.006545
76	133	15	2017-04-26 03:04:57.67756	2017-04-26 03:04:57.67881	2017-04-26 03:04:57.67881
77	24	15	2017-04-26 04:37:16.882759	2017-04-26 04:37:16.883968	2017-04-26 04:37:16.883968
78	32	15	2017-04-26 04:37:48.469494	2017-04-26 04:37:48.471059	2017-04-26 04:37:48.471059
79	24	15	2017-04-26 13:05:40.711071	2017-04-26 13:05:40.712652	2017-04-26 13:05:40.712652
80	128	15	2017-04-26 13:08:54.805214	2017-04-26 13:08:54.806437	2017-04-26 13:08:54.806437
81	41	15	2017-04-27 04:55:13.040896	2017-04-27 04:55:13.043073	2017-04-27 04:55:13.043073
82	13	15	2017-04-27 04:55:32.112713	2017-04-27 04:55:32.114627	2017-04-27 04:55:32.114627
83	122	15	2017-04-27 05:13:47.631325	2017-04-27 05:13:47.633201	2017-04-27 05:13:47.633201
84	24	15	2017-04-27 09:19:15.711768	2017-04-27 09:19:15.713506	2017-04-27 09:19:15.713506
85	41	15	2017-04-27 09:19:44.108803	2017-04-27 09:19:44.135659	2017-04-27 09:19:44.135659
86	90	15	2017-05-01 04:55:16.379708	2017-05-01 04:55:16.380703	2017-05-01 04:55:16.380703
87	52	15	2017-05-01 05:14:39.580802	2017-05-01 05:14:39.582321	2017-05-01 05:14:39.582321
88	90	15	2017-05-01 17:36:14.617369	2017-05-01 17:36:14.619168	2017-05-01 17:36:14.619168
89	13	15	2017-05-01 17:44:27.499087	2017-05-01 17:44:27.500602	2017-05-01 17:44:27.500602
90	52	15	2017-05-01 21:25:12.158868	2017-05-01 21:25:12.160923	2017-05-01 21:25:12.160923
91	87	15	2017-05-02 03:55:23.197563	2017-05-02 03:55:23.19858	2017-05-02 03:55:23.19858
95	90	15	2017-05-04 18:21:13.143431	2017-05-03 18:21:13.144702	2017-05-03 18:21:13.144702
94	141	15	2017-05-04 08:41:38.762255	2017-05-02 08:41:38.837739	2017-05-02 08:41:38.837739
92	44	15	2017-05-04 03:55:59.993634	2017-05-02 03:55:59.99453	2017-05-02 03:55:59.99453
93	90	15	2017-05-04 07:00:37.635048	2017-05-02 07:00:37.636431	2017-05-02 07:00:37.636431
96	32	15	2017-05-05 04:39:25.218985	2017-05-05 04:39:25.221207	2017-05-05 04:39:25.221207
97	24	15	2017-05-05 04:42:37.323314	2017-05-05 04:42:37.324618	2017-05-05 04:42:37.324618
98	90	15	2017-05-05 04:45:21.190656	2017-05-05 04:45:21.193222	2017-05-05 04:45:21.193222
99	41	15	2017-05-05 04:47:11.998238	2017-05-05 04:47:11.999214	2017-05-05 04:47:11.999214
100	43	15	2017-05-05 06:14:04.870122	2017-05-05 06:14:04.872059	2017-05-05 06:14:04.872059
101	24	15	2017-05-05 07:06:40.107241	2017-05-05 07:06:40.108224	2017-05-05 07:06:40.108224
102	43	15	2017-05-05 07:07:05.294201	2017-05-05 07:07:05.295424	2017-05-05 07:07:05.295424
103	90	15	2017-05-06 01:09:21.761494	2017-05-06 01:09:21.763471	2017-05-06 01:09:21.763471
104	13	15	2017-05-06 01:13:17.221422	2017-05-06 01:13:17.223015	2017-05-06 01:13:17.223015
105	13	5	2017-05-08 20:02:35.723008	2017-05-08 20:02:35.889089	2017-05-08 20:02:35.889089
106	13	15	2017-05-08 22:43:23.987806	2017-05-08 22:43:23.989983	2017-05-08 22:43:23.989983
107	90	15	2017-05-08 22:55:24.2758	2017-05-08 22:55:24.27744	2017-05-08 22:55:24.27744
108	52	15	2017-05-09 00:22:02.261512	2017-05-09 00:22:02.262998	2017-05-09 00:22:02.262998
109	90	5	2017-05-09 00:28:41.375538	2017-05-09 00:28:41.376808	2017-05-09 00:28:41.376808
110	52	5	2017-05-09 00:29:55.389124	2017-05-09 00:29:55.390612	2017-05-09 00:29:55.390612
111	147	5	2017-05-09 00:39:02.971054	2017-05-09 00:39:02.97307	2017-05-09 00:39:02.97307
112	15	5	2017-05-09 00:46:53.076241	2017-05-09 00:46:53.077504	2017-05-09 00:46:53.077504
113	47	5	2017-05-09 00:50:40.546119	2017-05-09 00:50:40.547291	2017-05-09 00:50:40.547291
114	52	15	2017-05-09 07:02:52.940824	2017-05-09 07:02:52.942649	2017-05-09 07:02:52.942649
115	24	15	2017-05-09 09:29:04.194632	2017-05-09 09:29:04.196432	2017-05-09 09:29:04.196432
116	90	5	2017-05-09 20:55:20.420926	2017-05-09 20:55:20.422546	2017-05-09 20:55:20.422546
117	33	15	2017-05-09 21:06:00.339606	2017-05-09 21:06:00.355543	2017-05-09 21:06:00.355543
118	33	5	2017-05-09 21:06:39.661157	2017-05-09 21:06:39.662681	2017-05-09 21:06:39.662681
119	32	15	2017-05-10 07:10:05.728993	2017-05-10 07:10:05.730529	2017-05-10 07:10:05.730529
120	90	15	2017-05-10 17:57:56.48891	2017-05-10 17:57:57.011821	2017-05-10 17:57:57.011821
121	13	15	2017-05-10 18:01:11.870215	2017-05-10 18:01:11.880381	2017-05-10 18:01:11.880381
122	2	13	2017-06-13 08:46:03.948957	2017-06-13 08:46:03.957206	2017-06-13 08:46:03.957206
123	181	14	2017-06-13 09:03:21.543988	2017-06-13 09:03:21.545443	2017-06-13 09:03:21.545443
124	2	15	2017-06-13 09:33:23.163828	2017-06-13 09:33:23.1652	2017-06-13 09:33:23.1652
125	176	15	2017-06-13 10:28:10.941536	2017-06-13 10:28:10.949795	2017-06-13 10:28:10.949795
126	2	16	2017-06-13 11:58:21.885117	2017-06-13 11:58:21.897282	2017-06-13 11:58:21.897282
127	186	16	2017-06-13 12:10:14.559657	2017-06-13 12:10:14.56833	2017-06-13 12:10:14.56833
128	203	16	2017-06-16 04:45:36.56943	2017-06-16 04:45:36.578676	2017-06-16 04:45:36.578676
129	2	16	2017-06-16 05:00:16.165569	2017-06-16 05:00:16.166819	2017-06-16 05:00:16.166819
130	2	17	2017-06-16 05:46:09.470706	2017-06-16 05:46:09.478747	2017-06-16 05:46:09.478747
131	194	16	2017-06-16 05:51:18.219913	2017-06-16 05:51:18.222186	2017-06-16 05:51:18.222186
132	186	19	2017-06-16 06:14:36.384108	2017-06-16 06:14:36.394427	2017-06-16 06:14:36.394427
133	210	19	2017-06-16 06:22:16.632902	2017-06-16 06:22:16.634047	2017-06-16 06:22:16.634047
134	218	20	2017-06-16 07:12:40.926949	2017-06-16 07:12:40.92813	2017-06-16 07:12:40.92813
135	219	20	2017-06-16 08:08:31.605122	2017-06-16 08:08:31.606381	2017-06-16 08:08:31.606381
136	220	20	2017-06-16 08:31:36.489069	2017-06-16 08:31:36.490388	2017-06-16 08:31:36.490388
\.


--
-- Name: user_visitations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_visitations_id_seq', 136, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, name, phone_number, confirmation_code, created_at, updated_at, sign_up_kind, magic_link, encrypted_password, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, onboarding_step, hex_code, slug, facebook_id, reset_password_token, reset_password_sent_at, auto_accept_fb_friends, avatar_file_name, avatar_content_type, avatar_file_size, avatar_updated_at, fb_access_token, fb_expires_at, fb_picture) FROM stdin;
1	\N	710 2355588	22946	2017-05-14 08:45:52.762119	2017-05-14 08:45:52.910272	2	631e2ff		\N	0	\N	\N	\N	\N	0	43199afee84b6c8a876b1209a56bc252	43199afee84b6c8a876b1209a56bc252	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
167	\N	702 1568747	98364	2017-05-27 08:34:47.546177	2017-05-27 08:34:47.673858	2	634anh6		\N	0	\N	\N	\N	\N	0	d5e2f663b6e109351f28ed69401254c8	d5e2f663b6e109351f28ed69401254c8	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
158	sdffsdf	702 5958631	95745	2017-05-17 15:51:04.33241	2017-05-17 15:51:48.121889	2	632782f	$2a$11$rftYcGo52eRnQTauzghkm.abx3Jnvb5A6fkUGnf3uEM2Gvz4WAUHq	\N	1	2017-05-17 15:51:48.120847	2017-05-17 15:51:48.120847	127.0.0.1	127.0.0.1	4	da7bfc749c081df173cb20572b89c4c2	da7bfc749c081df173cb20572b89c4c2	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
159	\N	702 6568213	78289	2017-05-17 15:52:30.205738	2017-05-17 15:52:30.245005	2	6327860		\N	0	\N	\N	\N	\N	0	72953ea2794ce175d77ff81d571c3906	72953ea2794ce175d77ff81d571c3906	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
160	\N	702 5957776	87831	2017-05-17 16:09:59.447795	2017-05-17 16:17:38.235488	2	63279n2		\N	0	\N	\N	\N	\N	1	2ce7bcb04d0a7fb7d54b73695859157e	2ce7bcb04d0a7fb7d54b73695859157e	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
161	\N	798 4946161	68128	2017-05-17 16:18:54.635911	2017-05-17 16:18:54.772813	2	6327ahn		\N	0	\N	\N	\N	\N	0	dc1a9470308907bdd50cdef69559f3c8	dc1a9470308907bdd50cdef69559f3c8	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
162	\N	702 6595333	11743	2017-05-17 16:19:21.238545	2017-05-17 16:19:21.273291	2	6327akg		\N	0	\N	\N	\N	\N	0	3970dca73b666fd593cd135783f12bcb	3970dca73b666fd593cd135783f12bcb	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
163	\N	702 5298745	37341	2017-05-17 16:21:13.666154	2017-05-17 16:21:13.700182	2	6327and		\N	0	\N	\N	\N	\N	0	3e10000eec60c8c12f2f97e47f23ad2c	3e10000eec60c8c12f2f97e47f23ad2c	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
168	xzczxc	702 5757785	96321	2017-05-27 08:35:35.380729	2017-05-27 08:36:41.424613	2	634anfo	$2a$11$X9NF/ofArhUPPJvaPKL8W.puREDeVpj8a691fFKsQdRCjYgl7PNze	\N	1	2017-05-27 08:36:41.423268	2017-05-27 08:36:41.423268	127.0.0.1	127.0.0.1	4	5323c80a4f8ef8bfbcc83e9d8aa140ef	5323c80a4f8ef8bfbcc83e9d8aa140ef	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
164	cvxcvxcv	702 5987467	96764	2017-05-17 16:21:46.143699	2017-05-17 16:30:55.373128	2	6327b0n	$2a$11$kzbvp.uGyWOgygLiCJ4m1uJvywiXHgW24b5dwt3vzUcfVTlh0XK9m	\N	0	\N	\N	\N	\N	4	0cf8bd56de400062752469f994209e03	0cf8bd56de400062752469f994209e03	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
3	zczd	702 5957773	39251	2017-05-14 08:52:53.723012	2017-05-14 09:11:08.840013	2	631e372	$2a$11$nwt.LJPmxoJil/eZtz6wJ.nVkfNDr5Z2nlXNztUVsaPcfkurt1Lt2	\N	0	\N	\N	\N	\N	4	65073ee3835669d5e2a6ab2086a149b8	65073ee3835669d5e2a6ab2086a149b8	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
165	\N	702 5298736	75784	2017-05-18 15:48:31.673228	2017-05-18 15:48:31.903275	2	632cl41		\N	0	\N	\N	\N	\N	0	914666d90b46f07a0f326dbe824149b0	914666d90b46f07a0f326dbe824149b0	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
177	\N	984 5896589	44151	2017-06-13 04:56:52.104371	2017-06-13 04:56:52.267011	2	63842ka		\N	0	\N	\N	\N	\N	0	d857c1fe44dbf9a9a6b72b3388596e70	d857c1fe44dbf9a9a6b72b3388596e70	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
169	xcvxcv	702 5957766	92359	2017-05-27 08:37:37.945354	2017-05-27 09:59:36.603294	2	634anmc	$2a$11$rtTmDgPdqQAhTnDAwl70yO7M5ntIYMS1uQb9UKE3OLHRqHkTFocNq	\N	1	2017-05-27 09:59:36.602306	2017-05-27 09:59:36.602306	127.0.0.1	127.0.0.1	4	5c6bd15a5a0d026b7fa1dba9119b48da	5c6bd15a5a0d026b7fa1dba9119b48da	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
166	asdasd	702 3556666	79587	2017-05-27 08:10:32.665064	2017-05-27 08:11:48.085567	2	634al79	$2a$11$owGLVe4TUfTMM7slM9vlsuDk8/RMVnsF0aOuf6HQzUuLH6L34yZ7W	\N	1	2017-05-27 08:11:48.084116	2017-05-27 08:11:48.084116	127.0.0.1	127.0.0.1	4	e136a778c683d163331c8e0b8e177621	e136a778c683d163331c8e0b8e177621	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
178	\N	161 3777039	68282	2017-06-13 04:59:11.604088	2017-06-13 04:59:11.733612	2	638430l		\N	0	\N	\N	\N	\N	0	5531a9af39e2f87bb3b48ef06623c628	5531a9af39e2f87bb3b48ef06623c628	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
174	zxczxc	702 5358877	31689	2017-05-30 17:31:07.897193	2017-05-30 17:31:49.487691	2	6354ekh	$2a$11$BQ9Z.61534TyakP8BtE5auyD8a/fZQIaktCsDUnv3hCaQhodZyUp2	\N	1	2017-05-30 17:31:49.486786	2017-05-30 17:31:49.486786	127.0.0.1	127.0.0.1	4	1df9da61d5310708751f6f65797246be	1df9da61d5310708751f6f65797246be	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
172	zxczxc	702 5558777	75133	2017-05-27 15:21:44.349547	2017-05-27 15:26:54.756794	2	634ccgm	$2a$11$zskEhnvmb8ThaZeYaD6a3uX6J43n5PcuXERUQOEtIqy24xK7yhL6m	\N	1	2017-05-27 15:26:54.755943	2017-05-27 15:26:54.755943	127.0.0.1	127.0.0.1	4	ea08f0a91c38c1a69244031bf7f23a70	ea08f0a91c38c1a69244031bf7f23a70	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
170	first	702 5957722	12188	2017-05-27 13:57:13.122908	2017-05-27 13:58:04.157358	2	634c4dk	$2a$11$eeBWXhEdCMJlPRZLyVsFk.Lyn3kTHTdb/q6v4s069ygCa//6HFQmK	\N	1	2017-05-27 13:58:04.15611	2017-05-27 13:58:04.15611	127.0.0.1	127.0.0.1	4	b433bc631e550560e20bbdd3166eea21	b433bc631e550560e20bbdd3166eea21	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
175	\N	702 5298732	24613	2017-06-06 14:49:40.295454	2017-06-06 14:49:40.46884	2	636hh1l		\N	0	\N	\N	\N	\N	0	40b2019ca8a664c4ea0925784af6f17d	40b2019ca8a664c4ea0925784af6f17d	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
171	second	702 5957733	26698	2017-05-27 13:58:31.271844	2017-05-27 13:59:35.850483	2	634c4hb	$2a$11$DwymQnpAaTcgeTXxzz3jROWlCyoJKKrwwybY8GakKrX/rErjHYXCa	\N	1	2017-05-27 13:59:35.849118	2017-05-27 13:59:35.849118	127.0.0.1	127.0.0.1	4	66e3eff9c4e300ff2240a0683f4c68e4	66e3eff9c4e300ff2240a0683f4c68e4	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
173	asdasd	702 2154896	69166	2017-05-30 16:19:05.994427	2017-05-30 16:51:37.851233	2	63547mf	$2a$11$qQavzm7f8T6wwGi0gX.zYud.C03j/W5/hZRdzHHMERQWoRZIECsHS	\N	1	2017-05-30 16:51:37.85	2017-05-30 16:51:37.85	127.0.0.1	127.0.0.1	4	3d83d0e90a8ec2b82618f04cddb93764	3d83d0e90a8ec2b82618f04cddb93764	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
179	\N	702 5957770	26492	2017-06-13 07:51:26.159887	2017-06-13 07:51:26.159887	1	6384jc7		\N	0	\N	\N	\N	\N	0	9d7f61f29f1146d6b6c3bf2a9548c8e3	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
176	adfsadasd	702 5957771	14742	2017-06-07 03:20:34.285588	2017-06-07 03:22:17.882197	2	636ke1n	$2a$11$QiJJTE9obfCWdwtD1rKorucVL0MEvZVWjzsqhQt7A.jUhcJciRQZu	\N	1	2017-06-07 03:22:17.88081	2017-06-07 03:22:17.88081	127.0.0.1	127.0.0.1	4	e2d3528ee09fea475d9ed3ec89af8e52	e2d3528ee09fea475d9ed3ec89af8e52	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
180	\N	702 5657722	74864	2017-06-13 07:52:33.268907	2017-06-13 07:52:33.40208	2	6384jen		\N	0	\N	\N	\N	\N	0	8a2516e534ebaa0b3294bf821b45653f	8a2516e534ebaa0b3294bf821b45653f	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
181	\N	888 8888888	41355	2017-06-13 09:01:45.125871	2017-06-13 09:01:45.125871	1	638515n		\N	0	\N	\N	\N	\N	0	de2fdf21d1d827c23b2c495474e2b3e6	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
182	\N	890 8555555	35955	2017-06-13 09:06:14.469363	2017-06-13 09:06:14.469363	1	63851hl		\N	0	\N	\N	\N	\N	0	c696915376e34dc82cc5adafb7ce9663	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
184	\N	808 6300635	62616	2017-06-13 09:13:51.632327	2017-06-13 09:13:51.632327	1	63852bl		\N	0	\N	\N	\N	\N	0	7cf765e2acba7ae40566f90133826c54	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
185	\N	984 6126298	86942	2017-06-13 09:22:03.272924	2017-06-13 09:22:03.272924	1	638537c		\N	0	\N	\N	\N	\N	0	7947f64719731a3934e55fd0c58499d3	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
187	\N	755 9848609	43735	2017-06-13 09:37:25.702582	2017-06-13 09:37:25.702582	1	63854gg		\N	0	\N	\N	\N	\N	0	786ff64fef73aac7e1a998b3dbcf38c3	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
188	\N	098 8888888	65298	2017-06-13 09:43:21.059672	2017-06-13 09:43:21.059672	1	6385575		\N	0	\N	\N	\N	\N	0	df99bdfd747fea31d296595b4efa3574	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
189	\N	965 8212365	22214	2017-06-13 10:13:50.070469	2017-06-13 10:13:50.070469	1	6385839		\N	0	\N	\N	\N	\N	0	afbd52430fa86e31f0325340650db58b	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
190	\N	808 6311639	53364	2017-06-13 10:29:39.5092	2017-06-13 10:29:39.5092	1	63859i1		\N	0	\N	\N	\N	\N	0	f3557e9086e7f7ca48a2aed2833c3c9c	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
191	\N	989 8989898	24321	2017-06-13 10:35:28.480907	2017-06-13 10:35:28.480907	1	6385a5l		\N	0	\N	\N	\N	\N	0	a8c3d5e0219bd1fefd6540677fe40b75	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
192	\N	987 6543210	17773	2017-06-13 10:37:55.32704	2017-06-13 10:37:55.32704	1	6385aee		\N	0	\N	\N	\N	\N	0	9017844e7a64f34060d9d29cdec95dd2	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
186	\N	755 8049449	69616	2017-06-13 09:34:54.409804	2017-06-16 06:16:18.003296	1	63854b6		\N	0	\N	\N	\N	\N	0	46d912647c1654aa56282be5d0e60ba4	46d912647c1654aa56282be5d0e60ba4	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
193	\N	898 0000000	33911	2017-06-13 12:12:09.779924	2017-06-13 12:12:09.779924	1	6385je3		\N	0	\N	\N	\N	\N	0	1322fba65c2958debe16fba8fd9ebef2	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
183	\N	808 6523569	47478	2017-06-13 09:09:26.786089	2017-06-13 18:58:14.018426	1	638521k		\N	0	\N	\N	\N	\N	0	f9bbd5034eb64d7c2701c111cb53b25f	f9bbd5034eb64d7c2701c111cb53b25f	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
194	\N	818 5226841	23659	2017-06-15 17:37:19.41485	2017-06-15 17:37:19.41485	1	638i269		\N	0	\N	\N	\N	\N	0	4e4ddc36ada48d4a2c42807fa6d43684	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
195	\N	513 2088768	98745	2017-06-15 17:39:56.702157	2017-06-15 17:39:56.702157	1	638i2c8		\N	0	\N	\N	\N	\N	0	f873085201a0dfb23190820eef416866	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
196	\N	213 2908745	64949	2017-06-15 17:44:54.861975	2017-06-15 17:44:54.861975	1	638i2n7		\N	0	\N	\N	\N	\N	0	a85331fd776c4bfd14359d9b9a4f039c	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
197	\N	702 5351544	73513	2017-06-16 04:13:15.456086	2017-06-16 04:13:15.456086	1	638kd6g		\N	0	\N	\N	\N	\N	0	001d2a8150e2995f797dea5dc1dc672e	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
198	\N	702 5112352	78994	2017-06-16 04:14:04.582671	2017-06-16 04:14:04.582671	1	638kd89		\N	0	\N	\N	\N	\N	0	bdbbb6caa4ad03d589094694015e3aff	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
199	\N	702 5955770	65433	2017-06-16 04:40:36.483376	2017-06-16 04:40:36.626529	2	638kfli		\N	0	\N	\N	\N	\N	0	264c2573bc43f171c1cb9b4dd4a07eb8	264c2573bc43f171c1cb9b4dd4a07eb8	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
2	rest	808 6311635	79963	2017-05-14 08:46:08.810786	2017-06-16 05:49:01.390407	2	631e2g5	$2a$11$YVDCblOrEsLSryNJSBsLvep.LbPsQgp61Y3lFIlMoyKUG.Vi1mnYa	\N	11	2017-06-16 05:32:37.176655	2017-06-16 05:29:56.151101	202.88.237.237	202.88.237.237	4	b5f875c9f0afa65e701af418e222d303	b5f875c9f0afa65e701af418e222d303	\N	8a944282162835154a272b66b4c52469c790053b7ee7055042659aa5f72ebd84	2017-06-16 05:49:01.389458	t	\N	\N	\N	\N	\N	\N	\N
209	\N	708 0125888	76436	2017-06-16 05:57:28.899185	2017-06-16 05:58:13.191124	1	638kn70		\N	0	\N	\N	\N	\N	0	1a4bbc1f6e02f067715ec971c0e89008	1a4bbc1f6e02f067715ec971c0e89008	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
200	dfgdfgdfgdfg	702 5955777	64973	2017-06-16 04:40:46.115731	2017-06-16 04:41:41.093377	2	638kg0g	$2a$11$SDiwi8klaVFSIYJqDwTAwenXniD.eVE9UTbmSZwbErygewA7KBViW	\N	1	2017-06-16 04:41:41.091956	2017-06-16 04:41:41.091956	202.88.237.237	202.88.237.237	4	742f4deb7e527f4061c895aea3ff6fe5	742f4deb7e527f4061c895aea3ff6fe5	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
201	\N	702 5957777	63564	2017-06-16 04:41:57.541542	2017-06-16 04:41:57.541542	1	638kg2j		\N	0	\N	\N	\N	\N	0	56805494b125cfefcebf73fe5d808d52	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
213	sweet	984 6126299	32586	2017-06-16 06:28:40.168707	2017-06-16 06:29:39.915186	2	638l187	$2a$11$EuyoF1tungT/I2ZhhjrGVuXSFgXBZjnPiNRZMqZvBAtMdUzq.psVq	\N	1	2017-06-16 06:29:39.914256	2017-06-16 06:29:39.914256	202.88.237.237	202.88.237.237	4	ec42680eca73a5f15aa9fad0afc4985b	ec42680eca73a5f15aa9fad0afc4985b	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
202	sadfsad	702 5956666	49333	2017-06-16 04:42:55.044632	2017-06-16 04:43:57.005179	1	638kg3j	$2a$11$QM/e6mxVOX/P.iV1544rqu8qnKqbMnf9M2RaoWFGmbNEJip.mUAUu	\N	1	2017-06-16 04:43:57.003796	2017-06-16 04:43:57.003796	202.88.237.237	202.88.237.237	4	0644fdbf75524c09e2d64c7df6bf9f58	0644fdbf75524c09e2d64c7df6bf9f58	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
214	\N	984 6125698	63533	2017-06-16 06:45:53.070634	2017-06-16 06:46:56.495975	2	638l2o4		\N	0	\N	\N	\N	\N	1	28a504631632dfa18ec5c78fb64a7f42	28a504631632dfa18ec5c78fb64a7f42	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
210	qwerty	985 6985214	93676	2017-06-16 06:16:44.240026	2017-06-16 06:20:04.993313	2	638l04g	$2a$11$y2yW75igHeeJsMQ8ubMbfe4njEEa68c1NQNFJhwibt61Ysx3AVC4O	\N	1	2017-06-16 06:20:04.991856	2017-06-16 06:20:04.991856	202.88.237.237	202.88.237.237	4	74fdef85dd750e0457b459197fd33a81	74fdef85dd750e0457b459197fd33a81	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
211	\N	986 5986598	86666	2017-06-16 06:21:17.761743	2017-06-16 06:21:17.874471	2	638l0fa		\N	0	\N	\N	\N	\N	0	40d34c2806e136e70fb2a9fa8479f2cd	40d34c2806e136e70fb2a9fa8479f2cd	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
203	zxczczxc	702 5951111	44939	2017-06-16 04:44:16.134853	2017-06-16 04:44:54.669289	2	638kg8f	$2a$11$e5zvXBg6.avz2qt4Hw/azOFBSDIzax7RUkTOAsAJpI.YvV6qNAQTK	\N	1	2017-06-16 04:44:54.668235	2017-06-16 04:44:54.668235	202.88.237.237	202.88.237.237	4	aad086cccd8dffc876f9a36b1a75612d	aad086cccd8dffc876f9a36b1a75612d	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
204	\N	707 0707070	62865	2017-06-16 04:50:47.959927	2017-06-16 04:50:47.98046	2	638kgm7		\N	0	\N	\N	\N	\N	0	3a997098a96e8dfdaf95fff6d04faf06	3a997098a96e8dfdaf95fff6d04faf06	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
205	\N	702 7027027	57319	2017-06-16 04:51:19.463813	2017-06-16 04:51:19.481779	2	638kgoo		\N	0	\N	\N	\N	\N	0	4f4ef906d19591717202519e8d0e3c08	4f4ef906d19591717202519e8d0e3c08	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
206	\N	727 4521111	15279	2017-06-16 04:55:24.768578	2017-06-16 04:55:24.768578	1	638kh9m		\N	0	\N	\N	\N	\N	0	0d148ea049d51cc63bdea5e4b0a093df	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
207	\N	708 0000000	26937	2017-06-16 05:13:40.169855	2017-06-16 05:13:40.169855	1	638kj35		\N	0	\N	\N	\N	\N	0	30332c9a956f622346d09761dae9d29c	\N	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
208	\N	702 0304050	44119	2017-06-16 05:31:13.853469	2017-06-16 05:31:13.960213	2	638kki4		\N	0	\N	\N	\N	\N	0	7861eadaaaf2d549def4d29caf749448	7861eadaaaf2d549def4d29caf749448	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
217	PatB	984 6125258	11614	2017-06-16 07:02:18.785975	2017-06-16 07:33:58.303649	1	638l4c8	$2a$11$JNQ6L6Lhn5sgiHKiyiwwMuzkgjZam8pqGZWbjIuSYZWYrO53azEIK	\N	3	2017-06-16 07:33:58.302234	2017-06-16 07:32:21.419107	202.88.237.237	202.88.237.237	4	4a8629d6f7f5a3925aca187994863412	4a8629d6f7f5a3925aca187994863412	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
212	tester	808 6311254	56522	2017-06-16 06:25:45.581482	2017-06-16 06:27:41.252503	1	638l104	$2a$11$Yn5zm8S25Jxb8JG05C1oNOzdkXm0lBpa2keVeYN2i5QauOYqb.bFm	\N	1	2017-06-16 06:27:41.251236	2017-06-16 06:27:41.251236	202.88.237.237	202.88.237.237	4	4992512e9638c4030749a57810f5e905	4992512e9638c4030749a57810f5e905	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
215	NewTester	984 6125808	24392	2017-06-16 06:48:45.921205	2017-06-16 06:50:17.192562	1	638l35g	$2a$11$CoIPzpZL/b2Er/YBPHUg3u66rqAjUUte9x4/3CgptMXqFPv8jGuMK	\N	1	2017-06-16 06:50:17.191207	2017-06-16 06:50:17.191207	202.88.237.237	202.88.237.237	4	02a6c56e3e100f65bf335fcacc1a8813	02a6c56e3e100f65bf335fcacc1a8813	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
216	ReferralA	984 6125855	89253	2017-06-16 06:51:12.325868	2017-06-16 06:54:49.602069	2	638l3a9	$2a$11$VG29qdFqJAIshHXtT9XONuVDXH5YZp0hTp4z2H/pEizat6xNJp.cK	\N	1	2017-06-16 06:54:49.600728	2017-06-16 06:54:49.600728	202.88.237.237	202.88.237.237	4	d315d70b741803949bca161c80a19c93	d315d70b741803949bca161c80a19c93	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
220	RefC	984 6126200	44636	2017-06-16 08:28:43.810235	2017-06-16 08:29:49.473279	2	638lcl0	$2a$11$upnZfJTlhx1HyXgjWRVZEOh1VAp.fDiTG.Im7h/QeBxnK0bTHuWgy	\N	1	2017-06-16 08:29:49.472041	2017-06-16 08:29:49.472041	202.88.237.237	202.88.237.237	4	fb2748d18aa9903a3766d46775bd5d20	fb2748d18aa9903a3766d46775bd5d20	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
218	RefA	984 2658966	18892	2017-06-16 07:06:37.304122	2017-06-16 07:08:00.206071	2	638l4ma	$2a$11$IDVjENln/JW51nZujEqqRekMut1cPH1JjuuH7Cpuy5g6d.ELqN2J2	\N	1	2017-06-16 07:08:00.205008	2017-06-16 07:08:00.205008	202.88.237.237	202.88.237.237	4	b2a2e6250411978bc47327921d837661	b2a2e6250411978bc47327921d837661	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
219	RefB	984 6461625	77588	2017-06-16 08:05:16.863626	2017-06-16 08:36:30.374438	2	638laga	$2a$11$AcpuE3bmTTyy7dMlwTgBWeOt7tl0sQxTRQ8G7WJBxzq8gHktGx2o.	\N	5	2017-06-16 08:36:30.373019	2017-06-16 08:35:09.985577	202.88.237.237	202.88.237.237	4	d6b974a91d6cd18acfb5a8a4fdf50257	d6b974a91d6cd18acfb5a8a4fdf50257	\N	\N	\N	t	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 220, true);


--
-- Data for Name: visitor_logs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY visitor_logs (id, ip, visited_at, created_at, updated_at) FROM stdin;
\.


--
-- Name: visitor_logs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('visitor_logs_id_seq', 1, false);


--
-- Name: admins_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY admins
    ADD CONSTRAINT admins_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: dispensaries_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dispensaries
    ADD CONSTRAINT dispensaries_pkey PRIMARY KEY (id);


--
-- Name: dispensary_deals_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dispensary_deals
    ADD CONSTRAINT dispensary_deals_pkey PRIMARY KEY (id);


--
-- Name: dispensary_patients_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dispensary_patients
    ADD CONSTRAINT dispensary_patients_pkey PRIMARY KEY (id);


--
-- Name: dispensary_redeemables_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dispensary_redeemables
    ADD CONSTRAINT dispensary_redeemables_pkey PRIMARY KEY (id);


--
-- Name: dispensary_referral_deals_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dispensary_referral_deals
    ADD CONSTRAINT dispensary_referral_deals_pkey PRIMARY KEY (id);


--
-- Name: dispensary_text_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dispensary_text_logs
    ADD CONSTRAINT dispensary_text_logs_pkey PRIMARY KEY (id);


--
-- Name: dispensary_texts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dispensary_texts
    ADD CONSTRAINT dispensary_texts_pkey PRIMARY KEY (id);


--
-- Name: faqs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY faqs
    ADD CONSTRAINT faqs_pkey PRIMARY KEY (id);


--
-- Name: friendly_id_slugs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY friendly_id_slugs
    ADD CONSTRAINT friendly_id_slugs_pkey PRIMARY KEY (id);


--
-- Name: invitations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY invitations
    ADD CONSTRAINT invitations_pkey PRIMARY KEY (id);


--
-- Name: notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: store_hours_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY store_hours
    ADD CONSTRAINT store_hours_pkey PRIMARY KEY (id);


--
-- Name: user_visitations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_visitations
    ADD CONSTRAINT user_visitations_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: visitor_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY visitor_logs
    ADD CONSTRAINT visitor_logs_pkey PRIMARY KEY (id);


--
-- Name: index_admins_on_email; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX index_admins_on_email ON admins USING btree (email);


--
-- Name: index_dispensaries_magic_link; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX index_dispensaries_magic_link ON users USING btree (id, magic_link);


--
-- Name: index_dispensaries_on_authentication_token; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX index_dispensaries_on_authentication_token ON dispensaries USING btree (authentication_token);


--
-- Name: index_dispensaries_on_phone_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX index_dispensaries_on_phone_number ON dispensaries USING btree (phone_number);


--
-- Name: index_dispensary_deals_on_dispensary_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_dispensary_deals_on_dispensary_id ON dispensary_deals USING btree (dispensary_id);


--
-- Name: index_dispensary_patients_on_dispensary_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_dispensary_patients_on_dispensary_id ON dispensary_patients USING btree (dispensary_id);


--
-- Name: index_dispensary_patients_on_reeferal_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_dispensary_patients_on_reeferal_user_id ON dispensary_patients USING btree (reeferal_user_id);


--
-- Name: index_dispensary_patients_on_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_dispensary_patients_on_user_id ON dispensary_patients USING btree (user_id);


--
-- Name: index_dispensary_redeemables_on_dispensary_deal_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_dispensary_redeemables_on_dispensary_deal_id ON dispensary_redeemables USING btree (dispensary_deal_id);


--
-- Name: index_dispensary_redeemables_on_dispensary_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_dispensary_redeemables_on_dispensary_patient_id ON dispensary_redeemables USING btree (dispensary_patient_id);


--
-- Name: index_dispensary_referral_deals_on_dispensaries_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_dispensary_referral_deals_on_dispensaries_id ON dispensary_referral_deals USING btree (dispensaries_id);


--
-- Name: index_dispensary_text_logs_on_dispensary_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_dispensary_text_logs_on_dispensary_id ON dispensary_text_logs USING btree (dispensary_id);


--
-- Name: index_dispensary_texts_on_dispensary_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_dispensary_texts_on_dispensary_id ON dispensary_texts USING btree (dispensary_id);


--
-- Name: index_friendly_id_slugs_on_slug_and_sluggable_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_friendly_id_slugs_on_slug_and_sluggable_type ON friendly_id_slugs USING btree (slug, sluggable_type);


--
-- Name: index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope ON friendly_id_slugs USING btree (slug, sluggable_type, scope);


--
-- Name: index_friendly_id_slugs_on_sluggable_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_friendly_id_slugs_on_sluggable_id ON friendly_id_slugs USING btree (sluggable_id);


--
-- Name: index_friendly_id_slugs_on_sluggable_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_friendly_id_slugs_on_sluggable_type ON friendly_id_slugs USING btree (sluggable_type);


--
-- Name: index_invitations_on_deleted_at; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_invitations_on_deleted_at ON invitations USING btree (deleted_at);


--
-- Name: index_invitations_on_invitee_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_invitations_on_invitee_id ON invitations USING btree (invitee_id);


--
-- Name: index_invitations_on_inviter_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_invitations_on_inviter_id ON invitations USING btree (inviter_id);


--
-- Name: index_notifications_on_dispensary_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_notifications_on_dispensary_id ON notifications USING btree (dispensary_id);


--
-- Name: index_notifications_on_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_notifications_on_user_id ON notifications USING btree (user_id);


--
-- Name: index_store_hours_on_dispensary_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_store_hours_on_dispensary_id ON store_hours USING btree (dispensary_id);


--
-- Name: index_user_visitations_on_dispensary_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_user_visitations_on_dispensary_id ON user_visitations USING btree (dispensary_id);


--
-- Name: index_user_visitations_on_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_user_visitations_on_user_id ON user_visitations USING btree (user_id);


--
-- Name: index_users_magic_link; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX index_users_magic_link ON users USING btree (id, magic_link);


--
-- Name: index_users_on_hex_code; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_users_on_hex_code ON users USING btree (hex_code);


--
-- Name: index_users_on_phone_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_phone_number ON users USING btree (phone_number);


--
-- Name: index_users_on_slug; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_users_on_slug ON users USING btree (slug);


--
-- Name: fk_rails_0dba05b71d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dispensary_texts
    ADD CONSTRAINT fk_rails_0dba05b71d FOREIGN KEY (dispensary_id) REFERENCES dispensaries(id);


--
-- Name: fk_rails_178332ce0e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dispensary_text_logs
    ADD CONSTRAINT fk_rails_178332ce0e FOREIGN KEY (dispensary_id) REFERENCES dispensaries(id);


--
-- Name: fk_rails_54754a7c57; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dispensary_patients
    ADD CONSTRAINT fk_rails_54754a7c57 FOREIGN KEY (dispensary_id) REFERENCES dispensaries(id);


--
-- Name: fk_rails_6c9524cb13; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dispensary_patients
    ADD CONSTRAINT fk_rails_6c9524cb13 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_bff317c429; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dispensary_referral_deals
    ADD CONSTRAINT fk_rails_bff317c429 FOREIGN KEY (dispensaries_id) REFERENCES dispensaries(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

