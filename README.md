# Reefer project

## Ruby
Ruby version is 2.3.1
Rails version is 5.0.1

## Instalation
Git clone the project
Cd to the project
Run bundle install so that the correct gems are downloaded

## Background processing
Some actions are run in the background process so that it does not block the main website.
You need to install redis.

## Database
Default database is set for postgresql.
Rename database.sample.yml to database.yml and update it according to match your settings

### Migration
The following commands will create the database and migrate it to reflect the latest state
rake db:create
rake db:migrate

### Seed
There are some seed data avaliable, just run:
rake db:seed

## ENV file
Rename .env.sample to .env and fill in the correct values

## Deployment
add heroku enviroment to your git remotes if not alread
heroku git:create -a reefer-loyalty-staging -r staging

deployment is a simple git push to that remote
git push staging master:master

if there is any database migrations we need to run that command manually
heroku run rake db:migrate -a reefer-loyalty-staging
heroku restart -a reefer-loyalty-staging

Dont forget to restart the app after the migration 

# Procfile
Procfile contains heroku specific code, which tells heroku how to start the server and how to start the background worker.

## Test cases