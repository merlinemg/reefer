FactoryGirl.define do
  factory :user do
    name do
      %w[johan kenny mirko marko darko sisko branko].sample.titleize
    end
    phone_number { Array.new(3){rand(9).to_s}.join + " " + Array.new(7){rand(9).to_s}.join }
    confirmation_code 1111
    sign_up_kind 'online'
    password 'adijostari'
    password_confirmation 'adijostari'
    onboarding_step 'signed_up'
  end
end
