FactoryGirl.define do
  factory :notification do
    association :user
    association :dispensary
    kind Notification.kinds['small_reward']
    message "You earned yourself a fat bud!"
  end
end
