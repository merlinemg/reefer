FactoryGirl.define do
  factory :invitation do
    association :inviter, factory: :user
    association :invitee, factory: :user
    invitation_state 'pending'
  end
end
