FactoryGirl.define do
  factory :dispensary_text do
    access false
    dispensary nil
    remaining_text 1
    send_sms_count 1
    twilio_phone_number "MyString"
  end
end
