FactoryGirl.define do
  factory :dispensary_deal do
    association :dispensary
    points 150
    name "Badokadonk"
  end
end
