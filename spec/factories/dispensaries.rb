FactoryGirl.define do
  factory :dispensary do
    sequence :email do |n|
      %w[dunkin bronees firecrackers cheebas bluntz].sample + "#{n}@reefer.com"
    end
    phone_number { Array.new(3){rand(9).to_s}.join + " " + Array.new(7){rand(9).to_s}.join }
    sequence :name do |n|
      %w[dunkin bronees firecrackers cheebas bluntz].sample.titleize + " #{n}"
    end
    address "Some street 12"
    zip_code '1232'
    city 'San Francisco'
    state 'CA'
    country 'USA'
    longitude { BigDecimal.new("-118.24277%02d" % (1..99).to_a.sample) }
    latitude  { BigDecimal.new("34.05222%02d" % (1..99).to_a.sample) }
    password 'adijostari'
    password_confirmation 'adijostari'
    onboarding_state 'signed_up'

    trait :signed_up do
      onboarding_state 'signed_up'
    end

    after(:create) do |dispensary|
      FactoryGirl.create :dispensary_deal, dispensary: dispensary, points: DispensaryPatient::POINTS[:small_reward] , name: "small joint"
      FactoryGirl.create :dispensary_deal, dispensary: dispensary, points: DispensaryPatient::POINTS[:medium_reward], name: "medium joint"
      FactoryGirl.create :dispensary_deal, dispensary: dispensary, points: DispensaryPatient::POINTS[:large_reward], name: "huge joint"
    end
  end
end
