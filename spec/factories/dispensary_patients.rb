FactoryGirl.define do
  factory :dispensary_patient do
    association :user
    association :dispensary
    points 0
    points_cap 0
  end
end
