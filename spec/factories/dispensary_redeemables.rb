FactoryGirl.define do
  factory :dispensary_redeemable do
    association :dispensary_deal
    association :dispensary_patient
  end
end
