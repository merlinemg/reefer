FactoryGirl.define do
  factory :store_hour do
    association :dispensary
    set_from_time '10:00 AM'
    set_to_time '10:00 PM'
    sunday false
    monday true
    tuesday false
    wednesday false
    thursday false
    friday false
    saturday false

    trait :all_days_closed do
      sunday false
      monday false
      tuesday false
      wednesday false
      thursday false
      friday false
      saturday false
    end

  end
end
