require 'rails_helper'
RSpec.describe SendSmsWorker, type: :worker do

  it { is_expected.to be_processed_in :sms }
  it { is_expected.to be_retryable 3 }
  it { is_expected.to save_backtrace }

end
