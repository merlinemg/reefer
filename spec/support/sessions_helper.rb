def post_create_session(email, password)
  post("/api/v1/sessions.json",
    params: {
      "dispensary": {
        "email": email,
        "password": password
      }
    })
end

def post_forgot_password_session(email)
  post("/api/v1/sessions/forgot_password.json",
    params: {
      "dispensary": {
        "email": email
      }
    })
end