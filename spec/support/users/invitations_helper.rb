def get_users_invitation(magic_link)
  get("/users/invitations/#{magic_link}")
end

def post_enter_phone_number_invitation(phone_number)
  post("/users/invitations/enter_phone_nr",
    params: {
      "user": {
        "phone_number": phone_number
      }
    })
end

def put_enter_confirmation_code_users_invitation(user_id, confirmation_code)
  put("/users/invitations/#{user_id}/enter_confirmation_code",
    params: {
      "user": {
        "provided_confirmation_code": confirmation_code
      }
    })
end

def put_enter_password_users_invitation(user_id, pw, pw_confirmation)
  put("/users/invitations/#{user_id}/enter_password",
    params: {
      "user": {
        "password": pw,
        "password_confirmation": pw_confirmation
      }
    })
end

def put_enter_name_users_invitation(user_id, name)
  put("/users/invitations/#{user_id}/enter_name",
    params: {
      "user": {
        "name": name
      }
    })
end