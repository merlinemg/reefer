def put_enter_confirmation_code_users_in_store_onboarding(user_id, confirmation_code)
  put("/users/in_store_onboardings/#{user_id}/enter_confirmation_code",
    params: {
      "user": {
        "confirmation_code": confirmation_code
      }
    })
end

def put_enter_password_users_in_store_onboarding(user_id, pw, pw_confirmation)
  put("/users/in_store_onboardings/#{user_id}/enter_password",
    params: {
      "user": {
        "password": pw,
        "password_confirmation": pw_confirmation
      }
    })
end

def put_enter_name_users_in_store_onboarding(user_id, name)
  put("/users/in_store_onboardings/#{user_id}/enter_name",
    params: {
      "user": {
        "name": name
      }
    })
end