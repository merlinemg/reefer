def get_dispensaries_index(dispensary_email, authentication_token)
  get("/api/v1/dispensaries.json",
    headers: {
      "X-Dispensary-Email": dispensary_email,
      "X-Dispensary-Token": authentication_token
    })
end

def get_dispensaries_show(dispensary_email, authentication_token, phone_number)
  get("/api/v1/dispensaries/#{phone_number}.json",
    headers: {
      "X-Dispensary-Email": dispensary_email,
      "X-Dispensary-Token": authentication_token
    })
end


def put_dispensary(dispensary_email, authentication_token, phone_number, params)
  put("/api/v1/dispensaries/#{phone_number}.json",
    params: params,
    headers: {
      "X-Dispensary-Email": dispensary_email,
      "X-Dispensary-Token": authentication_token
    })
end
