require 'rails_helper'

RSpec.describe Dispensary, type: :model do
  subject { FactoryGirl.build :dispensary }

  context "relations" do
    it { should have_many :notifications }
    it { should have_many :store_hours }
    it { should have_many :dispensary_patients }
    it { should have_many :patients }
  end

  describe "#redeemed_rewards" do
    let(:dispensary){ FactoryGirl.create(:dispensary, :signed_up)}
    let(:dispensary_patient){ FactoryGirl.create(:dispensary_patient, points: 32, dispensary: dispensary) }
    let(:dispensary_patient_2){ FactoryGirl.create(:dispensary_patient, points: 32, dispensary: dispensary) }
    let(:dispensary_deal_1){ dispensary.deals.first }

    context "has a patient who redeemed the lowest deal" do
      it "should return redeemed rewards grouped by deal with redeemed count equal 1" do
        status = dispensary_patient.redeem_deal!(dispensary_deal_1)
        results = dispensary.redeemed_rewards
        expect(results).not_to be_nil
        expect(results.first[:deal]).to match(dispensary_deal_1.name)
        expect(results.first[:redeemed]).to equal(1)
      end
    end

    context "has two patients who redeemed the lowest deal" do
      it "should return redeemed rewards grouped by deal with redeemed count equal 2" do
        status = dispensary_patient.redeem_deal!(dispensary_deal_1)
        status = dispensary_patient_2.redeem_deal!(dispensary_deal_1)
        results = dispensary.redeemed_rewards
        expect(results).not_to be_nil
        expect(results.first[:deal]).to match(dispensary_deal_1.name)
        expect(results.first[:redeemed]).to equal(2)
      end
    end
  end

  describe "authentication_token reset after password update" do
    subject { FactoryGirl.create :dispensary }
    it "after password reset the authentication token should not be the same" do
      previous_token = subject.authentication_token
      subject.password = 'blublu'
      subject.password_confirmation = 'blublu'
      subject.save
      expect(previous_token).not_to eq(subject.authentication_token)
    end
  end
end
