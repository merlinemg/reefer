require 'rails_helper'

RSpec.describe DispensaryPatient, type: :model do
  subject { FactoryGirl.build :dispensary_patient }

  context "relations" do
    it { should belong_to :user }
    it { should belong_to :dispensary }
  end

  describe "validations" do
    it "user cannot have two relations to one dispensary" do
      subject.save
      begin
        second_invitation = FactoryGirl.create :dispensary_patient, user: subject.user, dispensary: subject.dispensary
      rescue ActiveRecord::RecordInvalid => error
      end
      expect(subject.dispensary.patients.reload.size.eql?(1)).to be true
    end
  end

  describe "#visit!" do
    let!(:limited_dp) do
      FactoryGirl.create :dispensary_patient, dispensary: dispensary_visit.dispensary, points_cap: 30, points: 30, last_visit: (Time.now - 10.days), gets_free_joint: false
    end
    let!(:addable_dp) do
      FactoryGirl.create :dispensary_patient, dispensary: dispensary_visit.dispensary, points_cap: 28, points: 28, last_visit: (Time.now - 10.days), gets_free_joint: false
    end
    let!(:fresh_dp) do
      FactoryGirl.create :dispensary_patient
    end
    let!(:dispensary_visit) do
      FactoryGirl.create :dispensary_patient, points_cap: 30, points: 30, last_visit: (Time.now - 10.days), gets_free_joint: false
    end

    subject do
      FactoryGirl.create :invitation, invitee: dispensary_visit.user, inviter: limited_dp.user, invitation_state: 'accepted'
      FactoryGirl.create :invitation, invitee: dispensary_visit.user, inviter: addable_dp.user, invitation_state: 'accepted'
      FactoryGirl.create :invitation, invitee: dispensary_visit.user, inviter: fresh_dp.user, invitation_state: 'accepted'
      dispensary_visit.visit!
      dispensary_visit
    end
    it "points cap should be set to 0" do
      expect(subject.points_cap).to eql(0)
    end
    it "points should increase by 10" do
      expect(subject.points).to eql(40)
    end
    it "last visit should be set to current time" do
      expect(subject.last_visit.to_date).to eql(Date.today)
    end

    it "friend with reached points cap shouldn't receive your invitation points and friend with 2 addable points should receive 2 more points" do
      expect(subject.dispensary.dispensary_patients.where(points: 30).where(points_cap: 30).size).to eql(2)
    end

    it "friend who isn't related to dispensary should now be related and get two points" do
      expect(subject.dispensary.dispensary_patients.where(points: 2).where(points_cap: 2).size).to eql(1)
    end
  end

  describe "#redeem_deal!" do
    let(:dispensary){ FactoryGirl.create(:dispensary, :signed_up) }
    let(:dispensary_deal_1){ dispensary.deals.first }

    before do
      subject.update_attributes(points: 32)
    end

    context "user redeems a deal" do
      it "should lower points to that dispensary" do
        expect(subject.redeem_deal!(dispensary_deal_1)).to be_truthy
        expect(subject.points).to equal(2)
      end
    end

  end

end
