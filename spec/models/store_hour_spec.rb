require 'rails_helper'

RSpec.describe StoreHour, type: :model do
  subject { FactoryGirl.build :store_hour }

  context "relations" do
    it { should belong_to :dispensary }
  end

  context "validations" do
    it { should validate_presence_of :from_time }
    it { should validate_presence_of :to_time }
  end

  describe "#basic_details" do
    let(:store_hour){ FactoryGirl.create(:store_hour, :all_days_closed)}
    before do
      store_hour.update_attributes(from_time: 2, to_time: 10, monday: true, friday: true)
    end

    context "given store hour has hours on monday and friday" do
      it "should return 2 entries with details" do

        result = store_hour.basic_details

        expect(result.count).to equal(2)
        expect(result.first[:day]).to match("Monday")
        expect(result.second[:day]).to match("Friday")
        expect(result.first[:from_time]).to match(result.second[:from_time])
      end
    end
  end

end
