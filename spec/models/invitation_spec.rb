require 'rails_helper'

RSpec.describe Invitation, type: :model do
  subject { FactoryGirl.build :invitation }

  context "relations" do
    it { should belong_to :invitee }
    it { should belong_to :inviter }
  end

  describe "should validate uniqueness of inviter and invitee" do
    it "user can create one invitation to invitee" do
      subject.save
      expect(subject.inviter.invitees.size.eql?(1)).to be true
      expect(subject.invitee.inviters.size.eql?(1)).to be true
    end
    it "user cannot have two invitations to invitee" do
      subject.save
      begin
        second_invitation = FactoryGirl.create :invitation, invitee: subject.invitee, inviter: subject.inviter
      rescue ActiveRecord::RecordInvalid => error
      end
      expect(subject.inviter.invitees.reload.size.eql?(1)).to be true
    end
    it "invitee cannot create an invitation to inviter" do
      subject.save
      begin
        second_invitation = FactoryGirl.create :invitation, invitee: subject.inviter, inviter: subject.invitee
      rescue ActiveRecord::RecordInvalid => error
      end
      expect(subject.inviter.invitees.reload.size.eql?(1)).to be true
    end
  end

  describe "cap restrictions" do
    describe "when inviter has 30 points cap and invitee is a new user" do
      let!(:dp) do
        FactoryGirl.create :dispensary_patient, points_cap: 30, points: 30, last_visit: Time.now, gets_free_joint: false
      end
      let!(:invitation) do
        invitee = FactoryGirl.create(:user, sign_up_kind: 'invitation')
        FactoryGirl.create(:invitation, inviter: dp.user, invitee: invitee, invitation_state: 'pending')
      end
      context "inviter" do
        subject do
          invitation.invitation_state ='accepted'
          invitation.save
          invitation.inviter
        end
        it "should't get points on dispensary" do
          expect(subject.last_visited_dispensary_relation.points).to eql(30)
        end
        it "shouldn't get points on dispensary cap" do
          expect(subject.last_visited_dispensary_relation.points_cap).to eql(30)
        end
        it "should get a free joint" do
          expect(subject.last_visited_dispensary_relation.gets_free_joint).to eql(true)
        end
      end
      context "invitee" do
        subject do
          invitation.invitation_state ='accepted'
          invitation.save
          invitation.invitee
        end
        it "should get two points on dispensary" do
          expect(subject.dispensary_patients.first.points).to eql(2)
        end
        it "should get two points on dispensary on points cap" do
          expect(subject.dispensary_patients.first.points_cap).to eql(2)
        end
        it "should get a free joint" do
          expect(subject.dispensary_patients.first.gets_free_joint).to eql(true)
        end
      end
    end
    describe "when invited user has reached the points cap on inviters last visited dispensary" do
      let!(:invitees_dp) do
        FactoryGirl.create :dispensary_patient,
          user: FactoryGirl.create(:user, sign_up_kind: 'invitation'),
          points_cap: 30, points: 30, last_visit: Time.now, gets_free_joint: false
      end
      let!(:inviters_dp) do
        FactoryGirl.create :dispensary_patient, last_visit: Time.now, gets_free_joint: false
      end
      context "inviter" do
        subject do
          invitation = FactoryGirl.create(:invitation,
            inviter: inviters_dp.user,
            invitee: invitees_dp.user,
            invitation_state: 'pending'
          )
          invitation.invitation_state ='accepted'
          invitation.save
          invitation.inviter
        end
        it "should get points on dispensary" do
          expect(subject.last_visited_dispensary_relation.points).to eql(2)
        end
        it "should get points on dispensary cap" do
          expect(subject.last_visited_dispensary_relation.points_cap).to eql(2)
        end
      end
      context "invitee" do
        subject do
          invitation = FactoryGirl.create(:invitation, inviter: inviters_dp.user, invitee: invitees_dp.user, invitation_state: 'pending')
          invitation.invitation_state ='accepted'
          invitation.save
          invitation.invitee
        end
        it "shouldn't get two points on dispensary" do
          expect(subject.dispensary_patients.first.points).to eql(30)
        end
        it "shouldn't get two points on dispensary on points cap" do
          expect(subject.dispensary_patients.first.points_cap).to eql(30)
        end
      end
    end
  end
end
