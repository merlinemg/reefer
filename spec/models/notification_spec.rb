require 'rails_helper'

RSpec.describe Notification, type: :model do
  subject { FactoryGirl.build :notification }

  context "relations" do
    it { should belong_to :user }
    it { should belong_to :dispensary }
  end

  # TODO: add tests for next round of reward notifications when sidebench confirms
  #       how these notifications should be handled
  describe "notifications on signup" do
    describe "in store" do
      before(:all) do
        @d = FactoryGirl.create_list(:dispensary, 3).first
        @phone_number = FactoryGirl.build(:user).phone_number
        @u = User.in_store_sign_up @d.id, @phone_number
        @first_dispensary = @u.dispensary_patients.order(id: :asc).reload.first
        @u.notifications.reload
      end
      it "user should have 3 notifications" do
        expect(@u.notifications.count).to eql(3)
      end
      it "should have dispensary_a, dispensary_b and sign_up kind notifications" do
        expect(@u.notifications.map(&:kind).sort).to eql(["dispensary_a", "dispensary_b", "sign_up"])
      end
    end
    describe "online" do
      subject do
        FactoryGirl.create_list :dispensary, 3
        Dispensary.unscoped.reload

        phone_number = FactoryGirl.build(:user).phone_number
        user = User.online_sign_up phone_number
        user = User.sign_up_enter_confirmation_code(user, user.confirmation_code)
        user.password = 'blablabla'
        user.password_confirmation = 'blablabla'
        user = User.sign_up_enter_password(user)
        user.name = 'Brane'
        user = User.sign_up_enter_name(user)
      end
      it "user should have 3 notifications" do
        expect(subject.notifications.count).to eql(3)
      end
      it "should have dispensary_a, dispensary_b and sign_up kind notifications" do
        expect(subject.notifications.map(&:kind).sort).to eql(["dispensary_a", "dispensary_b", "sign_up"])
      end
    end
  end
  describe "notifications on reward entitlement" do
    describe "small reward" do
      subject do
        FactoryGirl.create :dispensary_patient, points: 30
      end
      it "user should have a notification about the small reward" do
        expect(subject.user.notifications.where(kind: Notification.kinds['small_reward']).size).to eql(1)
      end

      it "upper and main message should match the expected messages" do
        n = subject.user.notifications.where(kind: Notification.kinds['small_reward']).first
        expect(n.upper_message).to eql("You have #{DispensaryPatient::POINTS[:small_reward]} points!")
        expect(n.message).to eql("You've earned a #{subject.dispensary.small_reward.name} at #{subject.dispensary.name}")
      end

      it "upon receiving more points then small reward and less then medium, user shouldn't get new notifications" do
        subject.points += 10
        subject.save
        expect(subject.user.notifications.where(kind: Notification.kinds['small_reward']).size).to eql(1)
      end
    end
    describe "medium reward" do
      subject do
        FactoryGirl.create :dispensary_patient, points: 90
      end
      it "user should have a notification about the reward" do
        expect(subject.user.notifications.where(kind: Notification.kinds['medium_reward']).size).to eql(1)
      end

      it "upper and main message should match the expected messages" do
        n = subject.user.notifications.where(kind: Notification.kinds['medium_reward']).first
        expect(n.upper_message).to eql("You have #{DispensaryPatient::POINTS[:medium_reward]} points!")
        expect(n.message).to eql("You've earned a #{subject.dispensary.medium_reward.name} at #{subject.dispensary.name}")
      end

      it "upon receiving more points then medium reward and less then large, user shouldn't get new notifications" do
        subject.points += 10
        subject.save
        expect(subject.user.notifications.where(kind: Notification.kinds['medium_reward']).size).to eql(1)
      end
    end
    describe "large reward" do
      subject do
        FactoryGirl.create :dispensary_patient, points: 150
      end
      it "user should have a notification about the reward" do
        expect(subject.user.notifications.where(kind: Notification.kinds['large_reward']).size).to eql(1)
      end
      it "upper and main message should match the expected messages" do
        n = subject.user.notifications.where(kind: Notification.kinds['large_reward']).first
        expect(n.upper_message).to eql("You have #{DispensaryPatient::POINTS[:large_reward]} points!")
        expect(n.message).to eql("You've earned a #{subject.dispensary.large_reward.name} at #{subject.dispensary.name}")
      end
    end
  end

  describe "notifications on invitation acceptance" do
    let!(:dispensary_patient) { FactoryGirl.create :dispensary_patient, points: 0, last_visit: Time.now}
    subject do
      i = FactoryGirl.create :invitation,
        inviter: dispensary_patient.user,
        invitee: FactoryGirl.create(:user, sign_up_kind: 'invitation'),
        invitation_state: 'pending'

      i.invitation_state = 'accepted'
      i.save
      i
    end
    context "inviter" do
      it "user should have a notification about getting two points" do
        expect(subject.inviter.notifications.where(kind: Notification.kinds['invitation']).size).to eql(1)
      end
      it "user should have a notification about getting a free joint" do
        expect(subject.inviter.notifications.where(kind: Notification.kinds['free_joint']).size).to eql(1)
      end
      it "inviters upper and main message should match the expected messages" do
        n = subject.inviter.notifications.where(kind: Notification.kinds['invitation']).first
        expect(n.upper_message).to eql("You and #{subject.invitee.name} are now friends")
        expect(n.message).to eql(dispensary_patient.dispensary.name)
      end
    end
    context "invitee" do
      it "user should have a notification about getting two points" do
        expect(subject.invitee.notifications.where(kind: Notification.kinds['invitation']).size).to eql(1)
      end
      it "user should have a notification about getting a free joint" do
        expect(subject.invitee.notifications.where(kind: Notification.kinds['free_joint']).size).to eql(1)
      end
      it "invitees upper and main message should match the expected messages" do
        n = subject.invitee.notifications.where(kind: Notification.kinds['invitation']).first
        expect(n.upper_message).to eql("You and #{subject.inviter.name} are now friends")
        expect(n.message).to eql(dispensary_patient.dispensary.name)
      end
    end
  end

  describe "notifications on dispensary visits" do
    describe "users visits" do
      subject do
        @dp = FactoryGirl.create :dispensary_patient, points: 0
        @dp.visit!
        @dp.user
      end
      it "user should have a notification about getting visitation points" do
        expect(subject.notifications.where(kind: Notification.kinds['you_visit']).size).to eql(1)
      end
      it "upper and main message should match the expected messages" do
        n = subject.notifications.where(kind: Notification.kinds['you_visit']).first
        expect(n.upper_message).to eql("You visited")
        expect(n.message).to eql(@dp.dispensary.name)
      end
    end
    describe "users friends visits" do
      subject do
        @dp = FactoryGirl.create :dispensary_patient, points: 0
        @i = FactoryGirl.create :invitation, inviter: @dp.user, invitation_state: 'accepted'
        @dp.visit!
        @i.invitee
      end
      it "user should have a notification about getting friends visitation points" do
        expect(subject.notifications.where(kind: Notification.kinds['friend_visit']).size).to eql(1)
      end
      it "upper and main message should match the expected messages" do
        n = subject.notifications.where(kind: Notification.kinds['friend_visit']).first
        expect(n.upper_message).to eql("#{@i.inviter.name} visited")
        expect(n.message).to eql(@dp.dispensary.name)
      end
    end
  end
end
