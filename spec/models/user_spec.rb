require 'rails_helper'

RSpec.describe User, type: :model do
  subject { FactoryGirl.build :user }

  context "relations" do
    it { should have_many :inviter_invitations }
    it { should have_many :invitee_invitations }
    it { should have_many :inviters }
    it { should have_many :invitees }

    it { should have_many :pending_inviter_invitations }
    it { should have_many :pending_invitee_invitations }
    it { should have_many :pending_inviters }
    it { should have_many :pending_invitees }

    it { should have_many :accepted_inviter_invitations }
    it { should have_many :accepted_invitee_invitations }
    it { should have_many :accepted_inviters }
    it { should have_many :accepted_invitees }

    it { should have_many :dispensary_patients }
    it { should have_many :dispensaries }

    context "friends" do
      before(:all) do
        @i = FactoryGirl.create :invitation, invitation_state: 'accepted'
        @i.inviter.accepted_invitees.reload
        @i.invitee.accepted_inviters.reload
      end
      it "inviter should have invitee among friends" do
        expect(@i.inviter.friends).to include(@i.invitee)
      end
      it "invitee should have inviter among friends" do
        expect(@i.invitee.friends).to include(@i.inviter)
      end
    end
  end

  context "validations" do
    it { should validate_uniqueness_of(:phone_number).case_insensitive }
    context "onboarding" do
      before { allow(subject).to receive(:enter_phone_nr?).and_return(true) }
      it { should validate_presence_of(:phone_number).on(:create) }
    end
    context "if set_name?" do
      before { allow(subject).to receive(:enter_name?).and_return(true) }
      it { should validate_presence_of(:name).on(:update) }
    end
  end

  context "on create" do
    subject { FactoryGirl.create :user }
    it { expect(subject.magic_link).to be_present }
    it { expect(subject.confirmation_code).to be_present }
  end

  describe "#friendship_invitations" do
    subject do
      FactoryGirl.create(:invitation, invitation_state: 'pending')
    end

    it "invitee should have one pending invitatin" do
      expect(subject.invitee.friendship_invitations).to include(subject)
    end
  end

  describe "#visited_a_dispensary?" do
    context "when user had been to a dispensary" do
      subject do
        dp = FactoryGirl.create :dispensary_patient, last_visit: DateTime.now
        dp.user
      end

      it "should be true" do
        expect(subject.visited_a_dispensary?).to be true
      end
    end
    context "when user hadn't been to a dispensary" do
      subject do
        dp = FactoryGirl.create :dispensary_patient
        dp.user
      end

      it "should be false" do
        expect(subject.visited_a_dispensary?).to be false
      end
    end
  end

  describe "#last_visited_dispensary_relation" do
    context "when user had been to a dispensary" do
      before(:all) do
        dp    = FactoryGirl.create :dispensary_patient, last_visit: DateTime.now.yesterday
        @last_visited_dispensary = FactoryGirl.create :dispensary_patient, user: dp.user, last_visit: DateTime.now
      end
      subject do
        @last_visited_dispensary.user
      end

      it "should return last visited dispensary" do
        expect(subject.last_visited_dispensary_relation).to eql(@last_visited_dispensary)
      end
    end
    context "when user hadn't been to a dispensary" do
      subject do
        dp = FactoryGirl.create :dispensary_patient, last_visit: nil
        dp.user
      end

      it "should be nil" do
        expect(subject.last_visited_dispensary_relation).to be nil
      end
    end
  end

  describe "#dispensary" do
    before(:all) do
      @dispensary = FactoryGirl.create :dispensary
      FactoryGirl.create :store_hour, :all_days_closed, dispensary: @dispensary, monday: true
      FactoryGirl.create :store_hour, :all_days_closed, dispensary: @dispensary, tuesday: true
      FactoryGirl.create :store_hour, :all_days_closed, dispensary: @dispensary, wednesday: true
      FactoryGirl.create :store_hour, :all_days_closed, dispensary: @dispensary, thursday: true
      FactoryGirl.create :store_hour, :all_days_closed, dispensary: @dispensary, friday: true
      FactoryGirl.create :store_hour, :all_days_closed, dispensary: @dispensary, sunday: true
      @friends = FactoryGirl.create_list(:user, 2)
      @user = FactoryGirl.create :user
      @user.accepted_inviters << @friends
      FactoryGirl.create :dispensary_patient, user: @user, dispensary: @dispensary, points: 5
      FactoryGirl.create :dispensary_patient, user: @friends.first, dispensary: @dispensary, points: 5
      FactoryGirl.create :dispensary_patient, user: @friends.last, dispensary: @dispensary, points: 5
      @dispensary.store_hours.reload
      @dispensary.dispensary_patients.reload
      @user.friends.reload
    end
    subject {@user.dispensary(@dispensary.id)}
    it "should have dispensary details" do
      expect(subject.has_key?(:dispensary)).to be true
      expect(subject[:dispensary].has_key?(:name)).to be true
      expect(subject[:dispensary].has_key?(:address)).to be true
      expect(subject[:dispensary].has_key?(:city)).to be true
      expect(subject[:dispensary].has_key?(:state)).to be true
      expect(subject[:dispensary].has_key?(:zip_code)).to be true
      expect(subject[:dispensary].has_key?(:country)).to be true
      expect(subject[:dispensary].has_key?(:longitude)).to be true
      expect(subject[:dispensary].has_key?(:latitude)).to be true
    end
    it "should have friends details" do
      expect(subject.has_key?(:friends)).to be true
      expect(subject[:friends].sort).to eql(@friends.map(&:name).sort)
    end
    it "should have points details" do
      expect(subject.has_key?(:points)).to be true
      expect(subject[:points]).to eql(5)
    end
    it "should have cap points details" do
      expect(subject.has_key?(:points_cap)).to be true
      expect(subject[:points_cap]).to eql(0)
    end
  end

  describe "#all_dispensaries" do
    before(:all) do
      DispensaryPatient.destroy_all
      Dispensary.destroy_all
      @extra_dispensary = FactoryGirl.create :dispensary
      @users_dispensaries = FactoryGirl.create_list(:dispensary, 3)
      @friends_dispensaries = FactoryGirl.create_list(:dispensary, 3)
      @friends = FactoryGirl.create_list(:user, 4)
      @user = FactoryGirl.create :user
      @users_dispensaries.each do |d|
        FactoryGirl.create :dispensary_patient, user: @user, dispensary: d, points: 5
      end
      @friends_dispensaries.each do |d|
        @friends.each do |f|
          FactoryGirl.create :dispensary_patient, user: f, dispensary: d, points: 3
        end
      end
      @user.accepted_inviters << @friends
      @user.reload
    end

    subject {@user.all_dispensaries}

    it "#all_dispensaries should return 7 dispensaries" do
      expect(subject.size).to eql(7)
    end
    it "users dispensaries should show 5 points and no friends" do
      users_dispensary_names = @users_dispensaries.map(&:name)
      collection = subject.select{|d| users_dispensary_names.include?(d[:dispensary][:name]) }
      expect(collection.map{|c| c[:points]}).to eql([5,5,5])
      expect(collection.map{|c| c[:friends]}).to eql([[],[],[]])
    end

    it "friend dispensaries should show 0 points and freind names" do
      friends_dispensary_names = @friends_dispensaries.map(&:name)
      collection = subject.select{|d| friends_dispensary_names.include?(d[:dispensary][:name]) }
      expect(collection.map{|c| c[:points]}).to eql([0,0,0])
      expect(collection.map{|c| c[:friends].sort}).to eql( 3.times.collect{ @friends.map{|f| f.name}.sort })
    end
  end

  describe "#my_dispensaries" do
    before(:all) do
      @extra_dispensary = FactoryGirl.create :dispensary
      @users_dispensaries = FactoryGirl.create_list(:dispensary, 3)
      @friends_dispensaries = FactoryGirl.create_list(:dispensary, 3)
      @friends = FactoryGirl.create_list(:user, 4)
      @user = FactoryGirl.create :user
      @users_dispensaries.each do |d|
        FactoryGirl.create :dispensary_patient, user: @user, dispensary: d, points: 5
      end
      @friends_dispensaries.each do |d|
        @friends.each do |f|
          FactoryGirl.create :dispensary_patient, user: f, dispensary: d, points: 3
        end
      end
      @user.accepted_inviters << @friends
      @user.reload
    end

    subject {@user.my_dispensaries}

    it "#my_dispensaries should return 3 dispensaries" do
      expect(subject.size).to eql(3)
    end
    it "users dispensaries should show 5 points and no friends" do
      expect(subject.map{|c| c[:points]}).to eql([5,5,5])
      expect(subject.map{|c| c[:friends]}).to eql([[],[],[]])
    end
  end

  describe "#my_dispensaries_sorted" do
    before(:all) do
      @extra_dispensary = FactoryGirl.create :dispensary
      @dispensaries = FactoryGirl.create_list(:dispensary, 3)
      @user = FactoryGirl.create :user
      FactoryGirl.create :dispensary_patient, user: @user, dispensary: @dispensaries.last, points: 1
      FactoryGirl.create :dispensary_patient, user: @user, dispensary: @dispensaries.second, points: 30
      FactoryGirl.create :dispensary_patient, user: @user, dispensary: @dispensaries.first, points: 35
      @user.reload
      @user.dispensary_patients.reload
    end

    subject {@user.my_dispensaries_sorted}

    it "#my_dispensaries_sorted should return 3 dispensaries" do
      expect(subject.size).to eql(2)
    end
    it "should return dispensaries with with more points first" do
      expect(subject.first[:points]).to eql(35)
      expect(subject.last[:points]).to eql(30)
    end
  end

  describe "describe user creation through invitation" do
    context "successful" do
      before(:all) do
        @u = FactoryGirl.create :user
        @phone_number = FactoryGirl.build(:user).phone_number
        @u2 = User.invitation_sign_up(@u.id, @phone_number)
      end
      it "user should be created" do
        expect(@u2.new_record?).to be false
      end
      it "inviter should have one pending invitation" do
        expect(@u.pending_invitee_invitations.size).to eql(1)
      end
      it "invitee should have one pending invitation" do
        expect(@u2.pending_inviter_invitations.size).to eql(1)
      end
    end
    context "invitee user already exists" do
      before(:all) do
        @u = FactoryGirl.create(:user)
        @u2 = FactoryGirl.create(:user)
        @returned_user = User.invitation_sign_up @u.id, @u2.phone_number
      end
      it "user should not be created" do
        expect(@returned_user).to eql(@u2)
      end
      it "invitee should have one pending invitation" do
        expect(@u2.pending_inviter_invitations.size).to eql(1)
      end
    end
    describe "failed" do
      context "without phone number" do
        before(:all) do
          @u = FactoryGirl.create(:user)
          @u2 = User.invitation_sign_up @u.id, ''
        end
        it "user should not be created" do
          expect(@u2.new_record?).to be true
        end
        it "expect error phone number can't be blank" do
          expect(@u2.errors[:phone_number]).to eql(["can't be blank", "is too short (minimum is 10 characters)"])
        end
      end
      context "users are freinds already" do
        before(:all) do
          @first_invitation = FactoryGirl.create :invitation, invitation_state: 'accepted'
        end

        subject do
          FactoryGirl.build :invitation, inviter: @first_invitation.inviter, invitee: @first_invitation.invitee
        end

        it "adding anothe invitation should fail" do
          expect(subject.save).to be false
        end
        it "expect errors on inviter and invitee 'has already been taken'" do
          subject.save
          expect(subject.errors[:inviter]).to include("has already been taken")
          expect(subject.errors[:invitee]).to include("has already been taken")
        end
      end
    end
  end

  describe "points for signing up" do
    describe "in store signup" do
      context "successful" do
        before(:all) do
          @d = FactoryGirl.create_list(:dispensary, 3).first
          @phone_number = FactoryGirl.build(:user).phone_number
          @u = User.in_store_sign_up @d.id, @phone_number
          @first_dispensary = @u.dispensary_patients.order(id: :asc).reload.first
          @u.dispensary_patients.reload
        end
        it "user should have three dispensary patitent relations" do
          expect(@u.dispensary_patients.size).to eql(3)
        end
        it "user sign_up_kind should be in_store" do
          expect(@u.sign_up_kind).to eql('in_store')
        end
        it "users first dispensary should be the dispensary user signed up to" do
          expect(@first_dispensary.dispensary).to eql(@d)
        end
        it "users first dispensary should have a visiting time" do
          expect(@first_dispensary.last_visit).to be_present
        end
        it "users first dispensary should have sign up points" do
          expect(@first_dispensary.points).to eql(DispensaryPatient::POINTS[:sign_up])
        end
        it "user should not get a free join at dispensary" do
          expect(@first_dispensary.gets_free_joint).to eql(false)
        end
        it "dispensary one patitent relation with dispensary a points" do
          expect(@u.dispensary_patients.where(points: DispensaryPatient::POINTS[:dispensary_a]).size).to eql(1)
        end
        it "dispensary one patitent relation with dispensary b points" do
          expect(@u.dispensary_patients.where(points: DispensaryPatient::POINTS[:dispensary_b]).size).to eql(1)
        end
      end
      describe "failed" do
        context "without phone number" do
          before(:all) do
            @d = FactoryGirl.create :dispensary
            @u = User.in_store_sign_up @d.id, ''
          end
          it "user should not be created" do
            expect(@u.new_record?).to be true
          end
          it "expect error phone number can't be blank" do
            expect(@u.errors[:phone_number]).to eql(["can't be blank", "is too short (minimum is 10 characters)"])
          end
        end
        context "user already exists" do
          before(:all) do
            @d = FactoryGirl.create :dispensary
            @phone_number = FactoryGirl.create(:user).phone_number
            @u = User.in_store_sign_up @d.id, @phone_number
          end
          it "user should not be created" do
            expect(@u.new_record?).to be true
          end
          it "expect error phone number has already been taken" do
            expect(@u.errors[:phone_number]).to eql(["has already been taken"])
          end
        end
      end
    end
    describe "online signup" do
      describe "enter phone number step" do
        context "successful" do
          subject do
            phone_number = FactoryGirl.build(:user).phone_number
            user = User.online_sign_up phone_number
            user.reload
            user.dispensary_patients.reload
            user
          end

          it { should be_valid }
          it "should have enter_phone_number set as onboarding_step" do
            expect(subject.onboarding_step).to eql('enter_phone_nr')
          end
        end
        describe "failed" do
          context "without phone number" do
            before(:all) do
              @u = User.online_sign_up ''
            end
            it "user should not be created" do
              expect(@u.new_record?).to be true
            end
            it "expect error phone number can't be blank" do
              expect(@u.errors[:phone_number]).to eql(["can't be blank", "is too short (minimum is 10 characters)"])
            end
          end
          context "user already exists" do
            before(:all) do
              @phone_number = FactoryGirl.create(:user).phone_number
              @u = User.online_sign_up @phone_number
            end
            it "user should not be created" do
              expect(@u.new_record?).to be true
            end
            it "expect error phone number has already been taken" do
              expect(@u.errors[:phone_number]).to eql(["has already been taken"])
            end
          end
        end
      end
      describe "enter confirmation code step" do
        context "successful" do
          subject do
            phone_number = FactoryGirl.build(:user).phone_number
            user = User.online_sign_up phone_number
            user = User.sign_up_enter_confirmation_code(user, user.confirmation_code)
          end
          it { should be_valid }
          it "user should have set onboarding_step" do
            expect(subject.onboarding_step).to eql('enter_confirmation_code')
          end
        end
        context "failed" do
          subject do
            phone_number = FactoryGirl.build(:user).phone_number
            user = User.online_sign_up phone_number
            user = User.sign_up_enter_confirmation_code(user, '12345blalba1245')
          end
          it "user should have an error on provided_confirmation_code" do
            expect(subject.errors.any?).to eql(true)
            expect(subject.errors[:provided_confirmation_code]).to eql(["must match the sent confirmation code"])
          end
          it "user should have onboarding_step set to enter_phone_nr" do
            expect(subject.onboarding_step).to eql('enter_phone_nr')
          end
        end
      end
      describe "enter password step" do
        context "successful" do
          subject do
            phone_number = FactoryGirl.build(:user).phone_number
            user = User.online_sign_up phone_number
            user = User.sign_up_enter_confirmation_code(user, user.confirmation_code)
            user.password = 'blablabla'
            user.password_confirmation = 'blablabla'
            user = User.sign_up_enter_password(user)
          end
          it { should be_valid }
          it "user should have set onboarding_step" do
            expect(subject.onboarding_step).to eql('enter_password')
          end
        end
        context "failed" do
          describe "no entry" do
            subject do
              phone_number = FactoryGirl.build(:user).phone_number
              user = User.online_sign_up phone_number
              user = User.sign_up_enter_confirmation_code(user, user.confirmation_code)
              user = User.sign_up_enter_password(user)
            end
            it "user should have an error on password and password_confirmation" do
              expect(subject.errors.any?).to eql(true)
              expect(subject.errors[:password_confirmation]).to eql(["can't be blank"])
              expect(subject.errors[:password]).to eql(["can't be blank", "is too short (minimum is 6 characters)"])
            end
            it "user shouldn't have set onboarding_step to 'enter_confirmation_code'" do
              expect(subject.onboarding_step).to eql('enter_confirmation_code')
            end
          end
          describe "passwords don't match" do
            subject do
              phone_number = FactoryGirl.build(:user).phone_number
              user = User.online_sign_up phone_number
              user = User.sign_up_enter_confirmation_code(user, user.confirmation_code)
              user.password = 'blablabla'
              user.password_confirmation = 'blablahaha'
              user = User.sign_up_enter_password(user)
            end
            it "user should have an error on password_confirmation" do
              expect(subject.errors.any?).to eql(true)
              expect(subject.errors[:password_confirmation]).to eql(["must match the password"])
            end
          end
        end
      end
      describe "enter name step" do
        context "successful" do
          subject do
            FactoryGirl.create_list :dispensary, 3
            Dispensary.unscoped.reload

            phone_number = FactoryGirl.build(:user).phone_number
            user = User.online_sign_up phone_number
            user = User.sign_up_enter_confirmation_code(user, user.confirmation_code)
            user.password = 'blablabla'
            user.password_confirmation = 'blablabla'
            user = User.sign_up_enter_password(user)
            user.name = 'Brane'
            user = User.sign_up_enter_name(user)
          end
          it { should be_valid }
          it "user should have set onboarding_step" do
            expect(subject.onboarding_step).to eql('signed_up')
          end
          it "users shouldn't have any last_visits" do
            expect(subject.last_visited_dispensary_relation).to be nil
          end

          it "user should have three dispensary patitent relations" do
           expect(subject.dispensary_patients.size).to eql(3)
          end

          it "dispensary one patitent relation with sign up points" do
            expect(subject.dispensary_patients.where(points: DispensaryPatient::POINTS[:sign_up]).size).to eql(1)
          end

          it "users dispensary patitent relation shouldn't gets free joint" do
            expect(subject.dispensary_patients.where(points: DispensaryPatient::POINTS[:sign_up]).first.gets_free_joint).to eql(false)
          end

          it "dispensary one patitent relation with dispensary a points" do
            expect(subject.dispensary_patients.where(points: DispensaryPatient::POINTS[:dispensary_a]).size).to eql(1)
          end

          it "dispensary one patitent relation with dispensary b points" do
            expect(subject.dispensary_patients.where(points: DispensaryPatient::POINTS[:dispensary_b]).size).to eql(1)
          end
        end
        context "failed" do
          describe "no entry" do
            subject do
              phone_number = FactoryGirl.build(:user).phone_number
              user = User.online_sign_up phone_number
              user = User.sign_up_enter_confirmation_code(user, user.confirmation_code)
              user.password = 'blablabla'
              user.password_confirmation = 'blablabla'
              user = User.sign_up_enter_password(user)
              user = User.sign_up_enter_name(user)
            end
            it "user should have an error on name" do
              expect(subject.errors.any?).to eql(true)
              expect(subject.errors[:name]).to eql(["can't be blank", "is invalid"])
            end
            it "user shouldn't have set onboarding_step to 'enter_confirmation_code'" do
              expect(subject.onboarding_step).to eql('enter_password')
            end
          end
        end
      end
    end
    describe "points for invitation acceptance" do
      describe "inviter has never visited a dispensery" do
        describe "user invites a new user" do
          before(:all) do
            DispensaryPatient.destroy_all
            Dispensary.destroy_all
            FactoryGirl.create :dispensary
            @expected_nr_of_points = (DispensaryPatient::POINTS[:invitation] + DispensaryPatient::POINTS[:sign_up])

            @u = FactoryGirl.create(:user)
            DispensaryPatient.create_for_online_signup(@u)

            @phone_number = FactoryGirl.build(:user).phone_number
            @u2 = User.invitation_sign_up(@u.id, @phone_number)
            @u2.onboarding_step = 'enter_name'
            @u2.name = 'Branko'
            @u2.save

            @i = @u2.pending_inviter_invitations.first
            @i.invitation_state = 'accepted'
            @i.save

            @u.dispensary_patients.reload
            @u2.dispensary_patients.reload
          end
          context "for inviter" do
            it "should have one dispensary with added 'invitation' points" do
              expect(@i.inviter.dispensary_patients.where(points: @expected_nr_of_points).size).to eql(1)
            end
            it "should get a free joint on dispensary" do
              expect(@i.inviter.dispensary_patients.where(points: @expected_nr_of_points).first.gets_free_joint).to eql(true)
            end
          end

          context "for invitee" do
            it "should have one dispensary with 'invitation' points" do
              expect(@u2.dispensary_patients.where(points: DispensaryPatient::POINTS[:invitation]).size).to eql(1)
            end
            it "should get a free joint on dispensary" do
              expect(@u2.dispensary_patients.where(points: DispensaryPatient::POINTS[:invitation]).first.gets_free_joint).to eql(true)
            end
          end
        end
        describe "user invites an existing user" do
          before(:all) do
            DispensaryPatient.destroy_all
            Dispensary.destroy_all
            FactoryGirl.create :dispensary

            @expected_nr_of_points = (DispensaryPatient::POINTS[:invitation] + DispensaryPatient::POINTS[:sign_up])
            @inviter = FactoryGirl.create(:user)
            DispensaryPatient.create_for_online_signup(@inviter)
            @invitee = FactoryGirl.create(:user)
            DispensaryPatient.create_for_online_signup(@invitee)
            @invitation = FactoryGirl.create(:invitation, inviter: @inviter, invitee: @invitee)
            @invitation.update(invitation_state: 'accepted')
            DispensaryPatient.where(nil).reload
            @invitation.reload
            @inviter.dispensary_patients.reload
            @invitee.dispensary_patients.reload
          end
          context "for inviter" do
            it "should have one dispensary with added 'invitation' points" do
              @inviter.dispensary_patients.reload
              dps = @inviter.dispensary_patients.where(points: @expected_nr_of_points).reload
              expect(dps.size).to eql(1)
            end
          end
          context "for invitee" do
            it "should have one dispensary with added 'invitation' points" do
              @invitee.dispensary_patients.reload
              dps = @invitee.dispensary_patients.where(points: @expected_nr_of_points).reload
              expect(dps.size).to eql(1)
            end
          end
        end
      end
      describe "inviter has visited a dispensary" do
        describe "for inviter who has visited a dispensary and a new invitee user" do
          subject do
            dp = FactoryGirl.create :dispensary_patient, last_visit: Time.now, points: 0, points_cap: 0
            invitation = FactoryGirl.create :invitation,
              inviter: dp.user,
              invitee: FactoryGirl.create(:user, sign_up_kind: 'invitation'),
              invitation_state: 'pending'
            invitation.invitation_state = 'accepted'
            invitation.save
            invitation.reload
          end

          context "inviter" do
            it "should recieve a free joint on last visited dispensary" do
              expect(subject.inviter.last_visited_dispensary_relation.gets_free_joint).to be true
            end
            it "should recieve two points on last visited dispensary" do
              expect(subject.inviter.last_visited_dispensary_relation.points).to eql(2)
            end
          end

          context "invitee" do
            it "invitee should have a dispensary patients relation to the same dispensary as the inviters last dispensary" do
              expect(subject.inviter.last_visited_dispensary_relation.dispensary_id).to eql(subject.invitee.dispensary_patients.first.dispensary_id)
            end
            it "should recieve a free joint on inviters last visited dispensary" do
              expect(subject.invitee.dispensary_patients.first.gets_free_joint).to be true
            end
            it "should recieve two points on inviters last visited dispensary" do
              expect(subject.invitee.dispensary_patients.first.points).to eql(2)
            end

          end
        end
        describe "for inviter who has visited a dispensary and an existing user" do
          subject do
            dp = FactoryGirl.create :dispensary_patient, last_visit: Time.now, points: 0, points_cap: 0, gets_free_joint: false
            dp2 = FactoryGirl.create :dispensary_patient, last_visit: Time.now, points: 0, points_cap: 0, gets_free_joint: false
            invitation = FactoryGirl.create :invitation, inviter: dp.user, invitee: dp2.user, invitation_state: 'pending'
            invitation.invitation_state = 'accepted'
            invitation.save
            invitation.reload
          end

          context "inviter" do
            it "should not recieve a free joint on last visited dispensary" do
              expect(subject.inviter.last_visited_dispensary_relation.gets_free_joint).to be false
            end
            it "should recieve two points on last visited dispensary" do
              expect(subject.inviter.last_visited_dispensary_relation.points).to eql(2)
            end
          end

          context "invitee" do
            it "invitee should have a dispensary patients relation to the same dispensary as the inviters last dispensary" do
              expect(subject.inviter.last_visited_dispensary_relation.dispensary_id).to eql(subject.invitee.dispensary_patients.last.dispensary_id)
            end
            it "should not recieve a free joint on inviters last visited dispensary" do
              expect(subject.invitee.dispensary_patients.last.gets_free_joint).to be false
            end
            it "should recieve two points on inviters last visited dispensary" do
              expect(subject.invitee.dispensary_patients.last.points).to eql(2)
            end
          end
        end
      end
    end
  end
end
