require 'rails_helper'

RSpec.describe MassTextService do
  let(:dispensary) { FactoryGirl.create(:dispensary) }
  let(:dispensary_patient) { FactoryGirl.create(:dispensary_patient) }
  let (:big_message){
    <<-STR
Hey yo, this is some big message
with 141 of characters.
asjdsa dnms amnd samn das
nsa mnd samn dmnsa dmn samd as
Jsdd d sc ds c dsc nds cas
    STR
  }
  context "validations" do
    it 'should fail without args' do
      expect { MassTextService.new() }.to raise_error(ArgumentError)
    end
    it "should return error message when message param is blank" do
      mass_service = MassTextService.new({message: "", dispensary_id: "1"})
      expect(mass_service.errors).to include({key: :message, full_message: "Message can't be blank!"})
    end
    it "should return error message when message param have more then 140 chars" do
      mass_service = MassTextService.new({message: big_message, dispensary_id: "1"})
      expect(mass_service.errors).to include({key: :message, full_message: "Message maxlength is 140 characters and you have #{big_message.length} characters!"})
    end
    it "should return error message when dispensary_id param is blank" do
      mass_service = MassTextService.new({message: "Some message", dispensary_id: ""})
      expect(mass_service.errors).to include({key: :dispensary_id, full_message: "Dispensary id can't be blank!"})
    end
    it "should return error message when dispensary with dispensary_id param can't be found" do
      mass_service = MassTextService.new({message: "Some message", dispensary_id: "#{dispensary.id + 1}"})
      resp = mass_service.send_messages
      expect(resp).to be_falsey
      expect(mass_service.errors).to include({key: :dispensary_id, full_message: "Can't find dispensary with id: #{dispensary.id + 1}!"})
    end
    it "should return error message when dispensary don't have any patients" do
      mass_service = MassTextService.new({message: "Some message", dispensary_id: dispensary.id})
      resp = mass_service.send_messages
      expect(resp).to be_falsey
      expect(mass_service.errors).to include({key: :dispensary_id, full_message: "Dispensary with id: #{dispensary.id} don't have any patients!"})
    end
  end
  context 'worker' do
    it 'should quaue jobs in SendSmsWorker' do
      mass_service = MassTextService.new({message: "Some message", dispensary_id: dispensary_patient.dispensary.id})
      expect {
        mass_service.send_messages
      }.to change(SendSmsWorker.jobs, :size).by(dispensary_patient.dispensary.patients.count)      
      expect(mass_service.errors).to be_empty
    end
  end
end

