require "rails_helper"

describe "API login" do
  context "successful" do
    before(:all) do
      dispensary = FactoryGirl.create :dispensary, password: 'lololo', password_confirmation: 'lololo'
      post_create_session(dispensary.email, 'lololo')
      @r = response
      @json = JSON.parse(@r.body)
    end
    it "should return status 200" do
      expect(@r.status).to eql(200)
    end
    it "should return authentication token" do
      expect(@json).to have_key("authentication_token")
    end
  end
  describe "failed" do
    context "wrong password" do
      before(:all) do
        dispensary = FactoryGirl.create :dispensary, password: 'lololo', password_confirmation: 'lololo'
        post_create_session(dispensary.email, 'hihihi')
        @r = response
      end
      it "should return status 401" do
        expect(@r.status).to eql(401)
      end
    end

    context "unexisting dispensary" do
      before(:all) do
        post_create_session('l@l.com', 'hihihi')
        @r = response
      end
      it "should return status 401" do
        expect(@r.status).to eql(401)
      end
    end

  end
end

describe "API forgot_password" do
  context "successful" do
    before(:all) do
      dispensary = FactoryGirl.create :dispensary
      post_forgot_password_session(dispensary.email)
      @r = response
    end
    it "should return status 200" do
      expect(@r.status).to eql(200)
    end
  end
  describe "failed" do
    context "unexisting dispensary" do
      before(:all) do
        post_forgot_password_session('l@l.com')
        @r = response
      end
      it "should return status 401" do
        expect(@r.status).to eql(401)
      end
    end
  end
end