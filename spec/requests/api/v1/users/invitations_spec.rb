require "rails_helper"

describe "Invitation signup" do
  before(:all) do
    FactoryGirl.create_list :dispensary, 2
    @phone_number = FactoryGirl.build(:user).phone_number
    @dispensary_patient = FactoryGirl.create :dispensary_patient, last_visit: Time.now
    @inviter = @dispensary_patient.user
    get_users_invitation(@inviter.magic_link)
    post_enter_phone_number_invitation(@phone_number)
    @user = User.last.reload
    put_enter_confirmation_code_users_invitation(@user.id, @user.confirmation_code)
    put_enter_password_users_invitation(@user.id, 'lololo', 'lololo')
    put_enter_name_users_invitation(@user.id, 'Branko')
  end
  it "invitees onboarding state should be signed up" do
    expect(@user.reload.onboarding_step).to eql('signed_up')
  end

  it "user should have 3 notifications" do
    expect(@user.notifications.reload.count).to eql(3)
  end
  it "should have dispensary_a, dispensary_b and sign_up kind notifications" do
    expect(@user.notifications.reload.map(&:kind).sort).to eql(["dispensary_a", "dispensary_b", "sign_up"])
  end

end