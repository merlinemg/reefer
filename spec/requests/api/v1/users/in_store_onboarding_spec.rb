require "rails_helper"

describe "In store signup" do
  before(:all) do
    @phone_number = FactoryGirl.build(:user).phone_number
    @dispensary = FactoryGirl.create :dispensary
    # Sign up visit
    get_dispensaries_show(@dispensary.email, @dispensary.authentication_token, @phone_number)

    @dispensary_patient = @dispensary.dispensary_patients.last
    # User onboards
    @user = @dispensary_patient.user
    put_enter_confirmation_code_users_in_store_onboarding(@user.id, @user.confirmation_code)
    put_enter_password_users_in_store_onboarding(@user.id, 'lololo', 'lololo')
    put_enter_name_users_in_store_onboarding(@user.id, 'Branko')
  end
  it "users onboarding state should be signed up" do
    expect(@user.reload.onboarding_step).to eql('signed_up')
  end
end