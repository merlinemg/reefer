require "rails_helper"

describe "API dispensaries#index" do
  context "successful" do
    before(:all) do
      dispensary = FactoryGirl.create :dispensary
      get_dispensaries_index(dispensary.email, dispensary.authentication_token)
      @r = response
      @json = JSON.parse(@r.body)
    end
    it "should return status 200" do
      expect(@r.status).to eql(200)
    end
    it "should return name of small reward" do
      expect(@json).to have_key("dispensary")
      expect(@json["dispensary"]).to have_key("small_reward")
    end
    it "should return name of medium reward" do
      expect(@json["dispensary"]).to have_key("medium_reward")
    end
    it "should return name of large reward" do
      expect(@json["dispensary"]).to have_key("large_reward")
    end
    it "should return name of dispensary" do
      expect(@json["dispensary"]).to have_key("name")
    end
  end
  describe "failed" do
    context "wrong password" do
      before(:all) do
        get_dispensaries_index('l@l.com', 'haha')
        @r = response
      end
      it "should return status 401" do
        expect(@r.status).to eql(401)
      end
    end
  end
end

describe "API dispensaries#show" do
  describe "User is related to dispensary" do
    context "but doesn't have any points on dispensary" do
      before(:all) do
        dp = FactoryGirl.create :dispensary_patient
        get_dispensaries_show(dp.dispensary.email, dp.dispensary.authentication_token, dp.user.phone_number)
        @r = response
        @json = JSON.parse(@r.body)
      end
      it "should return status 200" do
        expect(@r.status).to eql(200)
      end

      it "should return users name and phone number" do
        expect(@json).to have_key("user")
        expect(@json["user"]).to have_key("name")
        expect(@json["user"]).to have_key("phone_number")
      end

      it "should return users dispensary stats" do
        expect(@json).to have_key("dispensary")
        expect(@json["dispensary"]).to have_key("previous_points")
        expect(@json["dispensary"]).to have_key("current_points")
        expect(@json["dispensary"]).to have_key("small_reward")
        expect(@json["dispensary"]).to have_key("medium_reward")
        expect(@json["dispensary"]).to have_key("large_reward")
        expect(@json["dispensary"]["previous_points"]).to eql(0)
        expect(@json["dispensary"]["current_points"]).to eql(10)
        expect(@json["dispensary"]["gets_free_joint"]).to eql(false)
        expect(@json["dispensary"]["user_visited_today"]).to eql(false)
      end
    end
    context "but has 150 points already" do
      before(:all) do
        dp = FactoryGirl.create :dispensary_patient, points: 150
        get_dispensaries_show(dp.dispensary.email, dp.dispensary.authentication_token, dp.user.phone_number)
        @r = response
        @json = JSON.parse(@r.body)
      end
      it "should return status 200" do
        expect(@r.status).to eql(200)
      end
      it "user should still have 150 points" do
        expect(@json["dispensary"]["previous_points"]).to eql(150)
        expect(@json["dispensary"]["current_points"]).to eql(150)
      end
    end
    context "but has visited the dispensary in the same day" do
      before(:all) do
        dp = FactoryGirl.create :dispensary_patient, points: 10, last_visit: Time.now
        get_dispensaries_show(dp.dispensary.email, dp.dispensary.authentication_token, dp.user.phone_number)
        @r = response
        @json = JSON.parse(@r.body)
      end
      it "should have user_visited_today set to true" do
        expect(@json["dispensary"]["user_visited_today"]).to eql(true)
      end
      it "shouldn't get visiting points" do
        expect(@json["dispensary"]["previous_points"]).to eql(10)
        expect(@json["dispensary"]["current_points"]).to eql(10)
      end
    end
  end
  context "User is on reefer but not on the visited dispensary" do
    before(:all) do
      user = FactoryGirl.create :user
      dispensary = FactoryGirl.create :dispensary
      get_dispensaries_show(dispensary.email, dispensary.authentication_token, user.phone_number)
      @r = response
      @json = JSON.parse(@r.body)
    end
    it "should return status 200" do
      expect(@r.status).to eql(200)
    end

    it "should return users name and phone number" do
      expect(@json).to have_key("user")
      expect(@json["user"]).to have_key("name")
      expect(@json["user"]).to have_key("phone_number")
    end

    it "should return users dispensary stats" do
      expect(@json).to have_key("dispensary")
      expect(@json["dispensary"]).to have_key("previous_points")
      expect(@json["dispensary"]).to have_key("current_points")
      expect(@json["dispensary"]["previous_points"]).to eql(0)
      expect(@json["dispensary"]["current_points"]).to eql(10)
    end
  end
  context "User signed up in dispensary" do
    before(:all) do
      @phone_number = FactoryGirl.build(:user).phone_number
      @dispensary = FactoryGirl.create :dispensary
      get_dispensaries_show(@dispensary.email, @dispensary.authentication_token, @phone_number)
      @r = response
      @json = JSON.parse(@r.body)
    end
    it "should return status 200" do
      expect(@r.status).to eql(200)
    end
    it "should return users dispensary stats" do
      expect(@json).to have_key("dispensary")
      expect(@json["dispensary"]).to have_key("previous_points")
      expect(@json["dispensary"]).to have_key("current_points")
      expect(@json["dispensary"]["previous_points"]).to eql(0)
      expect(@json["dispensary"]["current_points"]).to eql(16)
    end
    it "should create a new user and dispensary patient relation with 16 points and no free joint" do
      dispensary_patient_relation = @dispensary.dispensary_patients.joins(:user).find_by(users: {phone_number: @phone_number})
      expect(dispensary_patient_relation).to be_present
      expect(dispensary_patient_relation.points).to eql(16)
      expect(dispensary_patient_relation.gets_free_joint).to eql(false)
    end
  end
end

describe "API dispensaries#update" do
  context "claim free joint after in store signup" do
    before(:all) do
      @phone_number = FactoryGirl.build(:user).phone_number
      @dispensary = FactoryGirl.create :dispensary
      # Sign up visit
      get_dispensaries_show(@dispensary.email, @dispensary.authentication_token, @phone_number)
      @dispensary_patient = @dispensary.dispensary_patients.last

      # User onboards
      @user = @dispensary_patient.user
      put_enter_confirmation_code_users_in_store_onboarding(@user.id, @user.confirmation_code)
      put_enter_password_users_in_store_onboarding(@user.id, 'lololo', 'lololo')
      put_enter_name_users_in_store_onboarding(@user.id, 'Branko')

      # Set patients last visit a day back
      @dispensary_patient.last_visit = @dispensary_patient.last_visit - 1.day
      @dispensary_patient.save

      # Joint reedeming visit
      get_dispensaries_show(@dispensary.email, @dispensary.authentication_token, @phone_number)
      @show_resonse = response
      @show_json = JSON.parse(@show_resonse.body)

      # Joint reedeming
      put_dispensary(@dispensary.email, @dispensary.authentication_token, @phone_number, {dispensary: {free_joint: true}})
      @update_response = response
      @update_json = JSON.parse(@update_response.body)
    end
    it "should return status 200 on dispensaries show" do
      expect(@show_resonse.status).to eql(200)
    end
    it "user should have 26 points (16 sign up points + 10 visiting points)" do
      expect(@show_json["dispensary"]["previous_points"]).to eql(16)
      expect(@show_json["dispensary"]["current_points"]).to eql(26)
    end
    it "user should get a free joint" do
      expect(@show_json["dispensary"]["gets_free_joint"]).to eql(true)
    end
    it "should return status on dispensaries update 200" do
      expect(@update_response.status).to eql(200)
    end
    it "user should have 26 points (16 sign up points + 10 visiting points)" do
      expect(@update_json["dispensary"]["previous_points"]).to eql(26)
      expect(@update_json["dispensary"]["current_points"]).to eql(26)
    end
    it "user should't have a free joint anymore" do
      expect(@update_json["dispensary"]["gets_free_joint"]).to eql(false)
    end
  end
  describe "redeem free joint after friend referral" do
    before(:all) do
      # create inviter and 3 dispensaries
      FactoryGirl.create_list :dispensary, 2
      @dispensary_patient = FactoryGirl.create :dispensary_patient, last_visit: Time.now, points: 0, points_cap: 0
      @dispensary = @dispensary_patient.dispensary
      @inviter = @dispensary_patient.user

      # onboarding with users invitation link
      @phone_number = FactoryGirl.build(:user).phone_number
      get_users_invitation(@inviter.magic_link)
      post_enter_phone_number_invitation(@phone_number)
      @invitee = User.last.reload
      put_enter_confirmation_code_users_invitation(@invitee.id, @invitee.confirmation_code)
      put_enter_password_users_invitation(@invitee.id, 'lololo', 'lololo')
      put_enter_name_users_invitation(@invitee.id, 'Branko')

      # Poins check
      @invitee.dispensary_patients.reload
      @inviter.dispensary_patients.reload
      expect(@inviter.dispensary_patients.sum(:points)).to eql(0)
      expect(@invitee.dispensary_patients.sum(:points)).to eql(
        DispensaryPatient::POINTS[:sign_up] +
        DispensaryPatient::POINTS[:dispensary_a] +
        DispensaryPatient::POINTS[:dispensary_b]
      )

      # accept invitation - both users get +2 points
      @inviter.pending_invitee_invitations.reload
      @inviter.pending_invitee_invitations.last.accepted!

      # Poins check
      @invitee.dispensary_patients.reload
      @inviter.dispensary_patients.reload
      expect(@inviter.dispensary_patients.sum(:points)).to eql(DispensaryPatient::POINTS[:invitation])
      expect(@invitee.dispensary_patients.sum(:points)).to eql(
        DispensaryPatient::POINTS[:sign_up] +
        DispensaryPatient::POINTS[:dispensary_a] +
        DispensaryPatient::POINTS[:dispensary_b] +
        DispensaryPatient::POINTS[:invitation]
      )

      # Invitees free joint claiming dispensary visit - invitee gets +10, inviter gets +2
      get_dispensaries_show(@dispensary.email, @dispensary.authentication_token, @phone_number)
      @show_resonse = response
      @show_json = JSON.parse(@show_resonse.body)
      # Poins check
      @invitee.dispensary_patients.reload
      @inviter.dispensary_patients.reload
      # expect(@inviter.dispensary_patients.sum(:points)).to eql(
      #   DispensaryPatient::POINTS[:invitation] +
      #   DispensaryPatient::POINTS[:visit_for_friend]
      # )
      expect(@invitee.dispensary_patients.sum(:points)).to eql(
        DispensaryPatient::POINTS[:sign_up] +
        DispensaryPatient::POINTS[:dispensary_a] +
        DispensaryPatient::POINTS[:dispensary_b] +
        DispensaryPatient::POINTS[:invitation] +
        DispensaryPatient::POINTS[:visit]
      )

      # Invitees free joint claiming
      put_dispensary(@dispensary.email, @dispensary.authentication_token, @phone_number, {dispensary: {free_joint: true}})
      @update_response = response
      @update_json = JSON.parse(@update_response.body)
    end
    context "upon ivitee visiting dispensary" do
      it "should return status 200 on dispensaries show" do
        expect(@show_resonse.status).to eql(200)
      end
      it "invitee should have a free joint" do
        expect(@show_json["dispensary"]["gets_free_joint"]).to eql(true)
      end
      it "should have free joint message" do
        expect(@show_json["dispensary"]["free_joint_message"]).to eql("#{@inviter.name} reefered you to #{@dispensary.name}. Lucky you!")
      end
    end
    context "upon ivitee claiming free joint" do
      it "should return status on dispensaries update 200" do
        expect(@update_response.status).to eql(200)
      end
      it "invitee should't have a free joint anymore" do
        expect(@update_json["dispensary"]["gets_free_joint"]).to eql(false)
      end
    end
  end
  describe "claim reward" do
    describe "successfuly" do
      context "small reward" do
        before(:all) do
          @dispensary_patient = FactoryGirl.create :dispensary_patient, points: 30
          @dispensary = @dispensary_patient.dispensary
          @phone_number = @dispensary_patient.user.phone_number

          # Small reward reedeming visit
          get_dispensaries_show(@dispensary.email, @dispensary.authentication_token, @phone_number)
          @show_resonse = response
          @show_json = JSON.parse(@show_resonse.body)

          # Claiming small reward
          put_dispensary(@dispensary.email, @dispensary.authentication_token, @phone_number, {dispensary: {small_reward: true}})
          @update_response = response
          @update_json = JSON.parse(@update_response.body)
        end
        it "should return status 200 on dispensaries show" do
          expect(@show_resonse.status).to eql(200)
        end
        it "user should have 40 points on show" do
          expect(@show_json["dispensary"]["previous_points"]).to eql(30)
          expect(@show_json["dispensary"]["current_points"]).to eql(40)
        end

        it "should return status on dispensaries update 200" do
          expect(@update_response.status).to eql(200)
        end
        it "user should have 10 points after claiming small reward (after update)" do
          expect(@update_json["dispensary"]["previous_points"]).to eql(40)
          expect(@update_json["dispensary"]["current_points"]).to eql(10)
        end
      end
      context "medium reward" do
        before(:all) do
          @dispensary_patient = FactoryGirl.create :dispensary_patient, points: 90
          @dispensary = @dispensary_patient.dispensary
          @phone_number = @dispensary_patient.user.phone_number

          # Medium reward reedeming visit
          get_dispensaries_show(@dispensary.email, @dispensary.authentication_token, @phone_number)
          @show_resonse = response
          @show_json = JSON.parse(@show_resonse.body)

          # Claiming medium reward
          put_dispensary(@dispensary.email, @dispensary.authentication_token, @phone_number, {dispensary: {medium_reward: true}})
          @update_response = response
          @update_json = JSON.parse(@update_response.body)
        end
        it "should return status 200 on dispensaries show" do
          expect(@show_resonse.status).to eql(200)
        end
        it "user should have 100 points on show" do
          expect(@show_json["dispensary"]["previous_points"]).to eql(90)
          expect(@show_json["dispensary"]["current_points"]).to eql(100)
        end

        it "should return status on dispensaries update 200" do
          expect(@update_response.status).to eql(200)
        end
        it "user should have 10 points after claiming medium reward (after update)" do
          expect(@update_json["dispensary"]["previous_points"]).to eql(100)
          expect(@update_json["dispensary"]["current_points"]).to eql(10)
        end
      end
      context "large reward" do
        before(:all) do
          @dispensary_patient = FactoryGirl.create :dispensary_patient, points: 150
          @dispensary = @dispensary_patient.dispensary
          @phone_number = @dispensary_patient.user.phone_number

          # Large reward reedeming visit
          get_dispensaries_show(@dispensary.email, @dispensary.authentication_token, @phone_number)
          @show_resonse = response
          @show_json = JSON.parse(@show_resonse.body)

          # Claiming large reward
          put_dispensary(@dispensary.email, @dispensary.authentication_token, @phone_number, {dispensary: {large_reward: true}})
          @update_response = response
          @update_json = JSON.parse(@update_response.body)
        end
        it "should return status 200 on dispensaries show" do
          expect(@show_resonse.status).to eql(200)
        end
        it "user should have 150 points on show" do
          expect(@show_json["dispensary"]["previous_points"]).to eql(150)
          expect(@show_json["dispensary"]["current_points"]).to eql(150)
        end

        it "should return status on dispensaries update 200" do
          expect(@update_response.status).to eql(200)
        end
        it "user should have 0 points after claiming small reward (after update)" do
          expect(@update_json["dispensary"]["previous_points"]).to eql(150)
          expect(@update_json["dispensary"]["current_points"]).to eql(0)
        end
      end

      describe "small reward notifications when claiming the reward for the second time" do
        subject do
          dp = FactoryGirl.create :dispensary_patient, points: 30
          put_dispensary(dp.dispensary.email, dp.dispensary.authentication_token, dp.user.phone_number, {dispensary: {small_reward: true}})
          dp.reload.points = 30
          dp.save
          dp
        end
        it "User should have 1 small reward notification, 1 claim reward notification and 1 small reward notification" do
          expect(
            subject.user.notifications.order(id: :desc).pluck(:kind)
          ).to eql(["small_reward", "claim_reward", "small_reward"])
        end
      end
      describe "medium reward notifications when claiming the reward for the second time" do
        subject do
          dp = FactoryGirl.create :dispensary_patient, points: 90
          put_dispensary(dp.dispensary.email, dp.dispensary.authentication_token, dp.user.phone_number, {dispensary: {medium_reward: true}})
          dp.reload.points = 90
          dp.save
          dp
        end
        it "User should have 1 medium reward notification, 1 claim reward notification and 1 medium reward notification" do
          expect(
            subject.user.notifications.order(id: :desc).pluck(:kind)
          ).to eql(["medium_reward", "claim_reward", "medium_reward"])
        end
      end
      describe "large reward notifications when claiming the reward for the second time" do
        subject do
          dp = FactoryGirl.create :dispensary_patient, points: 150
          put_dispensary(dp.dispensary.email, dp.dispensary.authentication_token, dp.user.phone_number, {dispensary: {large_reward: true}})
          dp.reload.points = 150
          dp.save
          dp
        end
        it "User should have 1 large reward notification, 1 claim reward notification and 1 large reward notification" do
          expect(
            subject.user.notifications.order(id: :desc).pluck(:kind)
          ).to eql(["large_reward", "claim_reward", "large_reward"])
        end
      end
      describe "Reward and claim notifications from 0 to 150" do
        subject do
          dp = FactoryGirl.create :dispensary_patient, points: 0
          dp.points = 15; dp.save;
          dp.points = 30; dp.save;
          dp.points = 70; dp.save;
          dp.points = 90; dp.save;
          dp.points = 120; dp.save;
          dp.points = 150; dp.save;
          put_dispensary(dp.dispensary.email, dp.dispensary.authentication_token, dp.user.phone_number, {dispensary: {large_reward: true}})
          dp
        end
        it "User should have 1 large reward notification, 1 claim reward notification and 1 large reward notification" do
          expect(
            subject.user.notifications.order(id: :desc).pluck(:kind)
          ).to eql(["claim_reward", "large_reward", "medium_reward", "small_reward"])
        end
      end
    end
    describe "failed" do
      context "claiming a free joint without earning a free joint" do
        before(:all) do
          @dispensary_patient = FactoryGirl.create :dispensary_patient, gets_free_joint: false
          @dispensary = @dispensary_patient.dispensary
          @phone_number = @dispensary_patient.user.phone_number

          # Small reward reedeming visit
          get_dispensaries_show(@dispensary.email, @dispensary.authentication_token, @phone_number)
          @show_resonse = response
          @show_json = JSON.parse(@show_resonse.body)

          # Claiming small reward
          put_dispensary(@dispensary.email, @dispensary.authentication_token, @phone_number, {dispensary: {free_joint: true}})
          @update_response = response
          @update_json = JSON.parse(@update_response.body)
        end
        it "should return status 200 on dispensaries show" do
          expect(@show_resonse.status).to eql(200)
        end
        it "user should have 10 visiting points on show" do
          expect(@show_json["dispensary"]["previous_points"]).to eql(0)
          expect(@show_json["dispensary"]["current_points"]).to eql(10)
        end
        it "user should shouldn't get a free joint on show" do
          expect(@show_json["dispensary"]["gets_free_joint"]).to eql(false)
        end
        it "should return status on dispensaries update 200" do
          expect(@update_response.status).to eql(200)
        end
        it "user should have 10 points for visiting (after update)" do
          expect(@update_json["dispensary"]["gets_free_joint"]).to eql(false)
        end
      end
      context "claiming a small reward without having enough points" do
        before(:all) do
          @dispensary_patient = FactoryGirl.create :dispensary_patient, points: 0
          @dispensary = @dispensary_patient.dispensary
          @phone_number = @dispensary_patient.user.phone_number

          # Small reward reedeming visit
          get_dispensaries_show(@dispensary.email, @dispensary.authentication_token, @phone_number)
          @show_resonse = response
          @show_json = JSON.parse(@show_resonse.body)

          # Claiming small reward
          put_dispensary(@dispensary.email, @dispensary.authentication_token, @phone_number, {dispensary: {small_reward: true}})
          @update_response = response
          @update_json = JSON.parse(@update_response.body)
        end
        it "should return status 200 on dispensaries show" do
          expect(@show_resonse.status).to eql(200)
        end
        it "user should have 10 points on show" do
          expect(@show_json["dispensary"]["previous_points"]).to eql(0)
          expect(@show_json["dispensary"]["current_points"]).to eql(10)
        end

        it "should return status on dispensaries update 200" do
          expect(@update_response.status).to eql(200)
        end
        it "user should have 10 points after claiming small reward (after update)" do
          expect(@update_json["dispensary"]["previous_points"]).to eql(10)
          expect(@update_json["dispensary"]["current_points"]).to eql(10)
        end
        it "shouldn't create a dispensary redeemable record" do
          expect(@dispensary_patient.dispensary_redeemables.count).to eql(0)
        end
      end
      context "claiming a medium reward without having enough points" do
        before(:all) do
          @dispensary_patient = FactoryGirl.create :dispensary_patient, points: 0
          @dispensary = @dispensary_patient.dispensary
          @phone_number = @dispensary_patient.user.phone_number

          # Small reward reedeming visit
          get_dispensaries_show(@dispensary.email, @dispensary.authentication_token, @phone_number)
          @show_resonse = response
          @show_json = JSON.parse(@show_resonse.body)

          # Claiming medium reward
          put_dispensary(@dispensary.email, @dispensary.authentication_token, @phone_number, {dispensary: {medium_reward: true}})
          @update_response = response
          @update_json = JSON.parse(@update_response.body)
        end
        it "should return status 200 on dispensaries show" do
          expect(@show_resonse.status).to eql(200)
        end
        it "user should have 10 points on show" do
          expect(@show_json["dispensary"]["previous_points"]).to eql(0)
          expect(@show_json["dispensary"]["current_points"]).to eql(10)
        end

        it "should return status on dispensaries update 200" do
          expect(@update_response.status).to eql(200)
        end
        it "user should have 10 points after claiming medium reward (after update)" do
          expect(@update_json["dispensary"]["previous_points"]).to eql(10)
          expect(@update_json["dispensary"]["current_points"]).to eql(10)
        end
        it "shouldn't create a dispensary redeemable record" do
          expect(@dispensary_patient.dispensary_redeemables.count).to eql(0)
        end
      end
      context "claiming a large reward without having enough points" do
        before(:all) do
          @dispensary_patient = FactoryGirl.create :dispensary_patient, points: 0
          @dispensary = @dispensary_patient.dispensary
          @phone_number = @dispensary_patient.user.phone_number

          # Small reward reedeming visit
          get_dispensaries_show(@dispensary.email, @dispensary.authentication_token, @phone_number)
          @show_resonse = response
          @show_json = JSON.parse(@show_resonse.body)

          # Claiming large reward
          put_dispensary(@dispensary.email, @dispensary.authentication_token, @phone_number, {dispensary: {large_reward: true}})
          @update_response = response
          @update_json = JSON.parse(@update_response.body)
        end
        it "should return status 200 on dispensaries show" do
          expect(@show_resonse.status).to eql(200)
        end
        it "user should have 10 points on show" do
          expect(@show_json["dispensary"]["previous_points"]).to eql(0)
          expect(@show_json["dispensary"]["current_points"]).to eql(10)
        end

        it "should return status on dispensaries update 200" do
          expect(@update_response.status).to eql(200)
        end
        it "user should have 10 points after claiming large reward (after update)" do
          expect(@update_json["dispensary"]["previous_points"]).to eql(10)
          expect(@update_json["dispensary"]["current_points"]).to eql(10)
        end
        it "shouldn't create a dispensary redeemable record" do
          expect(@dispensary_patient.dispensary_redeemables.count).to eql(0)
        end
      end
    end
  end
end