# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Default admin
a = Admin.find_by(email: "admin@reeferloyalty.com")
if a.nil?
  a = Admin.new
  a.email = "admin@reeferloyalty.com"
  a.password = "Reeferboi420!"
  a.save
end

faq_1 = Faq.new
faq_1.title= "How does the point breakdown work?"
faq_1.content= "Every time you check in at a dispensary, you receive 10 points and each of your friends receives 2 points (at that dispensary). Accordingly, if one of your friends checks in at a dispensary, you will receive 2 points (at that dispensary)."
faq_1.save

faq_2 = Faq.new
faq_2.title ="How does the cap work?"
faq_2.content = "The cap is a limit on the number of points you can earn from friends at each dispensary. The cap resets every time you check in at a particular dispensary. For example, if you have earned 28 points (the cap) from friends at dispensary A, you cannot earn any more points from friends (at dispensary A) until your next check in at dispensary A."
faq_2.save

faq_3 = Faq.new
faq_3.title = "How do I redeem a reward?"
faq_3.content = "When you check in at a dispensary and have enough points to redeem a reward, your eligible rewards will appear on the tablet. Once you pick a reward, you will be asked to confirm your choice. The tablet will prompt you to show the confirmation page to your budtender, who will then provide your reward. The point value of the redeemed reward is deducted from your total balance at that dispensary upon your confirmation."
faq_3.save

faq_4 = Faq.new
faq_4.title = "Is there a mobile application?"
faq_4.content = "-No – unfortunately, due to iOS regulations, we will not be able to provide a mobile application at this time. However, we have mobile optimized our website so you can access the website on your phone easily."
faq_4.save
