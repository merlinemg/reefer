class AddAutoAcceptFbFriendsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :auto_accept_fb_friends, :boolean, default: false
  end
end
