class AddDeletedAtToInvitation < ActiveRecord::Migration[5.0]
  def change
    add_column :invitations, :deleted_at, :datetime
    add_index :invitations, :deleted_at
  end
end
