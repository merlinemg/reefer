class AddMagicLinkToDispensaries < ActiveRecord::Migration[5.0]
  def change
    add_column :dispensaries, :magic_link, :string
    add_index :users,  [:id, :magic_link], unique: true, name: "index_dispensaries_magic_link"
  end
end
