
class RenameCapPointsToPointsCapOnDispensaryPatients < ActiveRecord::Migration[5.0]
  def change
    rename_column :dispensary_patients, :cap_points, :points_cap
  end
end
