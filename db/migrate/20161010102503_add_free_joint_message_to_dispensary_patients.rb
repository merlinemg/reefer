class AddFreeJointMessageToDispensaryPatients < ActiveRecord::Migration[5.0]
  def change
    add_column :dispensary_patients, :free_joint_message, :string
  end
end
