class AddMagicLinkToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :magic_link, :string
    add_index :users,  [:id, :magic_link], unique: true, name: "index_users_magic_link"
  end
end
