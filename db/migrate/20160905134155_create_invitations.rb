class CreateInvitations < ActiveRecord::Migration[5.0]
  def change
    create_table :invitations do |t|
      t.integer :inviter_id, index: true
      t.integer :invitee_id, index: true
      t.integer :invitation_state, default: Invitation.invitation_states['pending']
      t.timestamps
    end
  end
end
