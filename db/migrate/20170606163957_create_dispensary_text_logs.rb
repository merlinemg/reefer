class CreateDispensaryTextLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :dispensary_text_logs do |t|
      t.references :dispensary, foreign_key: true
      t.integer :capaign_size
      t.integer :remaining_sms
      t.integer :successfull_send
      t.string :from_phone_number
      t.datetime :send_date

      t.timestamps
    end
  end
end
