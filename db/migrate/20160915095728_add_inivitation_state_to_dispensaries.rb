class AddInivitationStateToDispensaries < ActiveRecord::Migration[5.0]
  def change
    add_column :dispensaries, :onboarding_state, :integer, default: Dispensary.onboarding_states['onboarding_request']
  end
end
