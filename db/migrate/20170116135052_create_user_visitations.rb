class CreateUserVisitations < ActiveRecord::Migration[5.0]
  def change
    create_table :user_visitations do |t|
      t.integer :user_id
      t.integer :dispensary_id
      t.datetime :visited_at

      t.timestamps
    end
    add_index :user_visitations, :user_id
    add_index :user_visitations, :dispensary_id
  end
end
