class CreateDispensaryPatients < ActiveRecord::Migration[5.0]
  def change
    create_table :dispensary_patients do |t|
      t.references :user, foreign_key: true
      t.references :dispensary, foreign_key: true
      t.integer :points, default: 0

      t.timestamps
    end
  end
end
