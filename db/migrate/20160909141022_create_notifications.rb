class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.references :user, index: true
      t.references :dispensary, index: true
      t.integer :kind
      t.string :message

      t.timestamps
    end
  end
end
