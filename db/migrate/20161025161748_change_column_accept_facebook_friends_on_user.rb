class ChangeColumnAcceptFacebookFriendsOnUser < ActiveRecord::Migration[5.0]
  def change
    change_column :users, :auto_accept_fb_friends, :boolean, default: true
  end
end
