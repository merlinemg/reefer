class RemoveRewardFieldsFromDispensaries < ActiveRecord::Migration[5.0]
  def change
    remove_column :dispensaries, :small_reward
    remove_column :dispensaries, :medium_reward
    remove_column :dispensaries, :large_reward
  end
end
