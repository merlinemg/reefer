class AddUpperMessageToNotifications < ActiveRecord::Migration[5.0]
  def change
    add_column :notifications, :upper_message, :string
  end
end
