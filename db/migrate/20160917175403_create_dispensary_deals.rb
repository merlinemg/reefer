class CreateDispensaryDeals < ActiveRecord::Migration[5.0]
  def change
    create_table :dispensary_deals do |t|
      t.integer :dispensary_id
      t.integer :points
      t.string :name

      t.timestamps
    end
    add_index :dispensary_deals, :dispensary_id
  end
end
