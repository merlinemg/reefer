class AddConfirmedAtToUsersAndDispensaries < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :onboarding_step, :integer
    add_column :dispensaries, :onboarding_step, :integer
  end
end
