class AddAuthenticationTokenToDispensaries < ActiveRecord::Migration[5.0]
  def change
    add_column :dispensaries, :authentication_token, :string, limit: 30
    add_index :dispensaries, :authentication_token, unique: true
  end
end
