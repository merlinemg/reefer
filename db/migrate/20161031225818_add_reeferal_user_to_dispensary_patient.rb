class AddReeferalUserToDispensaryPatient < ActiveRecord::Migration[5.0]
  def change
    add_column :dispensary_patients, :reeferal_user_id, :integer
    add_index :dispensary_patients, :reeferal_user_id
  end
end
