class AddDaysToStoreHour < ActiveRecord::Migration[5.0]
  def change
    add_column :store_hours, :sunday, :boolean
    add_column :store_hours, :monday, :boolean
    add_column :store_hours, :tuesday, :boolean
    add_column :store_hours, :wednesday, :boolean
    add_column :store_hours, :thursday, :boolean
    add_column :store_hours, :friday, :boolean
    add_column :store_hours, :saturday, :boolean
  end
end
