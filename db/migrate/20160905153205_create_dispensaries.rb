class CreateDispensaries < ActiveRecord::Migration[5.0]
  def change
    create_table :dispensaries do |t|
      t.string :name
      t.string :email
      t.string :phone_number
      t.string :address
      t.string :zip_code
      t.string :city
      t.string :state
      t.string :country

      t.timestamps
    end
  end
end
