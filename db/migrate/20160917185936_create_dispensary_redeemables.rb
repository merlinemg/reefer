class CreateDispensaryRedeemables < ActiveRecord::Migration[5.0]
  def change
    create_table :dispensary_redeemables do |t|
      t.integer :dispensary_deal_id
      t.integer :dispensary_patient_id
      t.datetime :redeemed_at

      t.timestamps
    end
    add_index :dispensary_redeemables, :dispensary_deal_id
    add_index :dispensary_redeemables, :dispensary_patient_id
  end
end
