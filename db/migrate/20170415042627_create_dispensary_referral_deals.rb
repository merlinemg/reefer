class CreateDispensaryReferralDeals < ActiveRecord::Migration[5.0]
  def change
    create_table :dispensary_referral_deals do |t|
      t.string :points
      t.string :name
      t.references :dispensaries, foreign_key: true

      t.timestamps
    end
  end
end
