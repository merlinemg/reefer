class AddResetTokenToDispensary < ActiveRecord::Migration[5.0]
  def change
    add_column :dispensaries, :reset_password_token, :string
    add_column :dispensaries, :reset_password_sent_at, :datetime
  end
end
