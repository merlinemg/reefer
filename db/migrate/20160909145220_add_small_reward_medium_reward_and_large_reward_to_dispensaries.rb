class AddSmallRewardMediumRewardAndLargeRewardToDispensaries < ActiveRecord::Migration[5.0]
  def change
    add_column :dispensaries, :small_reward, :string
    add_column :dispensaries, :medium_reward, :string
    add_column :dispensaries, :large_reward, :string
  end
end
