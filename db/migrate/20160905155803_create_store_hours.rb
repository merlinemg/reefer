class CreateStoreHours < ActiveRecord::Migration[5.0]
  def change
    create_table :store_hours do |t|
      t.references :dispensary
      t.integer :day
      t.integer :from_time
      t.integer :to_time

      t.timestamps
    end
  end
end
