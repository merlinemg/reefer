class AddHexCodeToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :hex_code, :string
    add_column :users, :slug, :string
    add_index :users, :hex_code
    add_index :users, :slug
  end
end
