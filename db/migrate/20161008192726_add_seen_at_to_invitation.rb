class AddSeenAtToInvitation < ActiveRecord::Migration[5.0]
  def change
    add_column :invitations, :seen_at, :datetime
  end
end
