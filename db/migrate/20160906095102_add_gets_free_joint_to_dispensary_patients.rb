class AddGetsFreeJointToDispensaryPatients < ActiveRecord::Migration[5.0]
  def change
    add_column :dispensary_patients, :gets_free_joint, :boolean, default: false
  end
end
