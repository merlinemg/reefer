class AddSignUpKindToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :sign_up_kind, :integer, default: User.sign_up_kinds['online']
  end
end
