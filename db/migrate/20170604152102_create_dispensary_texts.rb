class CreateDispensaryTexts < ActiveRecord::Migration[5.0]
  def change
    create_table :dispensary_texts do |t|
      t.boolean :access
      t.references :dispensary, foreign_key: true
      t.integer :remaining_text
      t.integer :send_sms_count
      t.string :twilio_phone_number

      t.timestamps
    end
  end
end
