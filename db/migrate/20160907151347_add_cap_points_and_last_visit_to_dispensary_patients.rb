class AddCapPointsAndLastVisitToDispensaryPatients < ActiveRecord::Migration[5.0]
  def change
    add_column :dispensary_patients, :cap_points, :integer, default: 0
    add_column :dispensary_patients, :last_visit, :datetime
  end
end
