class AddSeenAtToNotification < ActiveRecord::Migration[5.0]
  def change
    add_column :notifications, :seen_at, :datetime, default: nil
  end
end
