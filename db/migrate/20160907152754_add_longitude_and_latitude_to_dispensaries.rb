class AddLongitudeAndLatitudeToDispensaries < ActiveRecord::Migration[5.0]
  def change
    add_column :dispensaries, :longitude, :decimal
    add_column :dispensaries, :latitude, :decimal
  end
end
