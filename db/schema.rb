# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170606163957) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "email",               default: "", null: false
    t.string   "encrypted_password",  default: "", null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",       default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
  end

  create_table "dispensaries", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone_number"
    t.string   "address"
    t.string   "zip_code"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.string   "encrypted_password",                default: "", null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.decimal  "longitude"
    t.decimal  "latitude"
    t.integer  "onboarding_step"
    t.integer  "onboarding_state",                  default: 0
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.string   "authentication_token",   limit: 30
    t.string   "magic_link"
    t.index ["authentication_token"], name: "index_dispensaries_on_authentication_token", unique: true, using: :btree
    t.index ["phone_number"], name: "index_dispensaries_on_phone_number", unique: true, using: :btree
  end

  create_table "dispensary_deals", force: :cascade do |t|
    t.integer  "dispensary_id"
    t.integer  "points"
    t.string   "name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["dispensary_id"], name: "index_dispensary_deals_on_dispensary_id", using: :btree
  end

  create_table "dispensary_patients", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "dispensary_id"
    t.integer  "points",             default: 0
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.boolean  "gets_free_joint",    default: false
    t.integer  "points_cap",         default: 0
    t.datetime "last_visit"
    t.string   "free_joint_message"
    t.integer  "reeferal_user_id"
    t.index ["dispensary_id"], name: "index_dispensary_patients_on_dispensary_id", using: :btree
    t.index ["reeferal_user_id"], name: "index_dispensary_patients_on_reeferal_user_id", using: :btree
    t.index ["user_id"], name: "index_dispensary_patients_on_user_id", using: :btree
  end

  create_table "dispensary_redeemables", force: :cascade do |t|
    t.integer  "dispensary_deal_id"
    t.integer  "dispensary_patient_id"
    t.datetime "redeemed_at"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["dispensary_deal_id"], name: "index_dispensary_redeemables_on_dispensary_deal_id", using: :btree
    t.index ["dispensary_patient_id"], name: "index_dispensary_redeemables_on_dispensary_patient_id", using: :btree
  end

  create_table "dispensary_referral_deals", force: :cascade do |t|
    t.string   "points"
    t.string   "name"
    t.integer  "dispensaries_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["dispensaries_id"], name: "index_dispensary_referral_deals_on_dispensaries_id", using: :btree
  end

  create_table "dispensary_text_logs", force: :cascade do |t|
    t.integer  "dispensary_id"
    t.integer  "capaign_size"
    t.integer  "remaining_sms"
    t.integer  "successfull_send"
    t.string   "from_phone_number"
    t.datetime "send_date"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["dispensary_id"], name: "index_dispensary_text_logs_on_dispensary_id", using: :btree
  end

  create_table "dispensary_texts", force: :cascade do |t|
    t.boolean  "access"
    t.integer  "dispensary_id"
    t.integer  "remaining_text"
    t.integer  "send_sms_count"
    t.string   "twilio_phone_number"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["dispensary_id"], name: "index_dispensary_texts_on_dispensary_id", using: :btree
  end

  create_table "faqs", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree
  end

  create_table "invitations", force: :cascade do |t|
    t.integer  "inviter_id"
    t.integer  "invitee_id"
    t.integer  "invitation_state", default: 0
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.datetime "seen_at"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_invitations_on_deleted_at", using: :btree
    t.index ["invitee_id"], name: "index_invitations_on_invitee_id", using: :btree
    t.index ["inviter_id"], name: "index_invitations_on_inviter_id", using: :btree
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "dispensary_id"
    t.integer  "kind"
    t.string   "message"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "upper_message"
    t.datetime "seen_at"
    t.string   "lower_message"
    t.index ["dispensary_id"], name: "index_notifications_on_dispensary_id", using: :btree
    t.index ["user_id"], name: "index_notifications_on_user_id", using: :btree
  end

  create_table "store_hours", force: :cascade do |t|
    t.integer  "dispensary_id"
    t.integer  "from_time"
    t.integer  "to_time"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.boolean  "sunday"
    t.boolean  "monday"
    t.boolean  "tuesday"
    t.boolean  "wednesday"
    t.boolean  "thursday"
    t.boolean  "friday"
    t.boolean  "saturday"
    t.index ["dispensary_id"], name: "index_store_hours_on_dispensary_id", using: :btree
  end

  create_table "user_visitations", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "dispensary_id"
    t.datetime "visited_at"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["dispensary_id"], name: "index_user_visitations_on_dispensary_id", using: :btree
    t.index ["user_id"], name: "index_user_visitations_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "phone_number"
    t.integer  "confirmation_code"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "sign_up_kind",           default: 0
    t.string   "magic_link"
    t.string   "encrypted_password",     default: "",   null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.integer  "onboarding_step"
    t.string   "hex_code"
    t.string   "slug"
    t.string   "facebook_id"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean  "auto_accept_fb_friends", default: true
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "fb_access_token"
    t.datetime "fb_expires_at"
    t.string   "fb_picture"
    t.index ["hex_code"], name: "index_users_on_hex_code", using: :btree
    t.index ["id", "magic_link"], name: "index_dispensaries_magic_link", unique: true, using: :btree
    t.index ["id", "magic_link"], name: "index_users_magic_link", unique: true, using: :btree
    t.index ["phone_number"], name: "index_users_on_phone_number", unique: true, using: :btree
    t.index ["slug"], name: "index_users_on_slug", using: :btree
  end

  create_table "visitor_logs", force: :cascade do |t|
    t.string   "ip"
    t.datetime "visited_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "dispensary_patients", "dispensaries"
  add_foreign_key "dispensary_patients", "users"
  add_foreign_key "dispensary_referral_deals", "dispensaries", column: "dispensaries_id"
  add_foreign_key "dispensary_text_logs", "dispensaries"
  add_foreign_key "dispensary_texts", "dispensaries"
end
