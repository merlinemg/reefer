namespace :user do
  desc "Update confirmation codes to users who have not valid 5 numbers"
  task fix_confirmation_codes: :environment do
    User.where("confirmation_code < 10000").where(onboarding_step: [:enter_phone_nr, :enter_confirmation_code]).each_with_index do |u, i|
      puts "Handling #{i} user #{u.phone_number} as he has code: #{u.confirmation_code}"
      u.resend_confirmation_code
    end
  end
end