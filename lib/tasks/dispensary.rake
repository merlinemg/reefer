namespace :dispensary do

  desc "Creates default deals for dispensaries that dont have it yet"
  task deafult_deals: :environment do
    Dispensary.all.each do |d|
      if d.signed_up?
        d.create_default_deals
      end
    end
  end

end
