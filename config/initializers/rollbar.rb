Rollbar.configure do |config|
  config.access_token = ENV['ROLLBAR_ACCESS_TOKEN']
  # other configuration settings
  # ...

  if Rails.env.development?
    config.enabled = false
  end

end