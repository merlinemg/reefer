Sidekiq.configure_server do |config|
  config.redis = { url: "#{ENV['REDIS_URL']}", namespace: "reefer_#{Rails.env}" }
  # set sidekiq to poll every second for new jobs, as we are sending sms every second
  config.average_scheduled_poll_interval = 1
end

Sidekiq.configure_client do |config|
  config.redis = { url: "#{ENV['REDIS_URL']}", namespace: "reefer_#{Rails.env}" }
end
