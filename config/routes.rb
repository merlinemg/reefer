require 'sidekiq/web'
Rails.application.routes.draw do

  apipie

  devise_for :admins, controllers: { sessions: "admins/sessions" }
  get "/admin" => redirect("/admins")

  devise_for :users, skip: [:sessions, :registrations, :passwords]
  as :user do
    get 'users/sign_in' => 'users/sessions#new', :as => :new_user_session
    post 'users/sign_in' => 'users/sessions#create', :as => :user_session
    get 'users/login/:id' => 'users/sessions#edit', :as => :user_edit_session
    put 'users/login/:id' => 'users/sessions#update', :as => :user_update_session
    delete 'users/signout' => 'users/sessions#destroy', :as => :destroy_user_session
    get 'users/reset/:id' => 'users/sessions#reset', :as => :reset_user_session

    post "user/password" => "users/passwords#create", :as => :user_password
    get "user/password/new/:id" => "users/passwords#new", :as => :new_user_password
    patch "user/password" => "users/passwords#update"
    put "user/password" => "users/passwords#update"
    get "users/password/edit" => "users/passwords#edit", :as => :edit_user_password

    get "users/sign_up" => "users/registrations#new", :as => :new_user_registration
    patch "user/registration" => "users/registrations#update"
    put "user/registration" => "users/registrations#update"
    get "users/registration/edit" => "users/registrations#edit", :as => :edit_user_registration
  end

  devise_for :dispensaries, :controllers => {
    registrations: "dispensaries/registrations",
    sessions: "dispensaries/sessions",
    analytics: "dispensaries/analytics",
    passwords: "dispensaries/passwords"} do
      patch register: "dispensaries/registrations/register"
  end
  devise_scope :dispensary do
    get 'dispensaries/passwords/resend_link', :to => "dispensaries/passwords#resend_link"
  end

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :sessions, only: [:create] do
        collection do
          post :forgot_password
        end
      end
      resources :dispensaries, only: [:index, :show, :update]
    end
  end

  namespace :admins do
    resources :dispensaries, only: [:index, :new, :create, :edit, :update] do
      member do
        put :send_onboarding_invitation
      end
      collection do
        post :mass_text
      end
    end

    resources :mass_texts, only: [:index] do
      collection do
        post :send_text
      end
    end
    resources :dispensaries_text, only: [] do
      collection do
        get :index
      end
    end

    resources :statistics, only: [] do
      collection do
        get :index
        get :export_reefer_network
        get :export_per_dispensary
        get :export_all_dispensaries
      end
    end

    resources :users, only: [:index] do
      member do
        get :signin_as
      end
    end
    resources :faqs

    authenticate :admin do
      mount Sidekiq::Web => '/sidekiq'
    end

    root "base#index"
  end

  # dispensaries pages go here
  namespace :dispensaries do
    resources :sign_up, only: [] do
      member do
        patch :register
      end
    end
    resources :home, only: [:index] do
      collection do
        get :edit
        put :update
        patch :update
        get :how_it_works
      end
    end
    resources :deals, only: [:edit, :update]
    resources :accounts, only: [:index, :update]

    root "home#index"
  end

  # users pages go here
  namespace :users do
    resources :settings, only: [:index, :update] do
      collection do
        post 'fb_callback'
        get  'fb_disconnect'
        get  'fb_connected'
      end

      member do
        put 'upload_avatar'
        get 'crop_avatar'
        put 'update_cropped_avatar'
      end
    end

    resources :invitations, only: [:show] do
      collection do
        get 'enter_phone_nr'
        post 'enter_phone_nr_confirm'
        patch 'enter_phone_nr_confirm'
        get 'send_invitation_sms'
      end
      member do
        get 'resend_code'
        get 'enter_confirmation_code'
        put 'enter_confirmation_code'
        get 'enter_password'
        put 'enter_password'
        get 'enter_name'
        put 'enter_name'

        # put 'invitation_actions', to: "invitations#invitation_actions", as: :invitation_actions
        post 'invitation_status', to: "invitations#invitation_status"
      end
    end

    resources :in_store_onboardings, only: [:show] do
      member do
        get 'enter_confirmation_code'
        put 'enter_confirmation_code'
        get 'enter_password'
        put 'enter_password'
        get 'enter_name'
        put 'enter_name'
        get 'resend_code'
      end
    end

    resources :friends, only: [:index] do
      collection do
        get    :find_possible_friends
        post   :list_friends
        post   :invite_friends
        delete :remove_friend
        get    :current_friends
      end
    end

    resources :onboardings, only: [] do
      collection do
        get 'enter_phone_nr'
        post 'enter_phone_nr'
      end
      member do
        get 'resend_code'
        get 'enter_confirmation_code'
        put 'enter_confirmation_code'
        get 'enter_password'
        put 'enter_password'
        get 'enter_name'
        put 'enter_name'
        get 'share_link'
      end
    end

    resources :home, only: [] do
      collection do
        get :my_clubs
        get :rewards
        get :all_clubs
        get "dispensary_details/:dispensary_id", to: "home#dispensary_details", as: :dispensary_details
      end
    end

    resources :notifications, only: [] do
      collection do
        get :index
        post :mark_seen
      end

      member do
        put :update, defaults: { format: 'json' }
        patch :update, defaults: { format: 'json' }
      end
    end

    root "home#index"
  end

  # public page for dispensary
  get "dispensary", to:"new_dispensary#index"
  get "dispensary/waiting_list", to:"welcome_dispensary#waiting_list"
  post "dispensary/join_waiting_list", to:"welcome_dispensary#join_waiting_list"
  post "dispensary/thank_you", to:"welcome_dispensary#thank_you"

  get "welcome/blog"
  get "welcome/faq"
  get "welcome/contact_us", to:"new_contact#index"
  get "welcome/privacy_policy"
  post "welcome/lets_talk"

  get "api/documentation", to:"apipie/apipies#index"
  get 'dispensaries/analytics' => 'dispensaries/analytics#analytics'
  get 'dispensaries/smart-sms' => 'dispensaries/smart_sms#index'
  post 'dispensaries/smart-sms' => 'dispensaries/smart_sms#send_sms'
  get 'dispensaries/activate-sms' => 'dispensaries/smart_sms#activate_sms'
  post 'dispensaries/activate-sms' => 'dispensaries/smart_sms#smart_sms_request'
  post 'dispensaries/analytics' => 'dispensaries/analytics#fetch'
  put 'dispensaries/analytics' => 'dispensaries/analytics#update'
  post 'admins/dispensaries_text' => 'admins/dispensaries_text#update'
  # default root to user public page
  root "new_home#index"
end
