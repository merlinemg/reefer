require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Reefer
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.time_zone = "America/Los_Angeles"

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.default_locale = :en

    config.generators do |g|
      g.test_framework :rspec, :view_specs => false, :routing_specs => false, :controller_specs => false
      g.fixture_replacement :factory_girl, :dir => 'spec/factories'
      g.assets = false
      g.helper = false
    end

    ActionMailer::Base.delivery_method = :smtp

    config.paperclip_defaults = {
      :storage => :s3,
      :s3_host_name => ENV["AWS_HOST_NAME"],
      s3_region: ENV['AWS_REGION'],
      :s3_credentials => {
        bucket: ENV['AWS_BUCKET_NAME'],
        access_key_id: ENV['AWS_KEY'],
        secret_access_key: ENV['AWS_SECRET'],
      },
      :url =>':s3_domain_url',
      :path => "/#{Rails.env}/:class/:attachment/:id_partition/:style/:filename"
    }

  end
end
